//
//  NKFileManager.m
//  FANSZ
//
//  Created by jinzhenghua on 29/12/2015.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKFileManager.h"
#import "YTKNetworkPrivate.h"
#import "NKMUser.h"
#import "SDImageCache.h"
#import <AVFoundation/AVFoundation.h>

@interface NKFileManager ()

@property (nonatomic, strong) NSString *endpoint;
@property (nonatomic, strong) NSString *imageEndpoint;
@property (nonatomic, strong) NSString *bucket;

@end


@implementation NKFileManager

+(NKFileManager *)fileManager{
    
    static NKFileManager *_instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[NKFileManager alloc] init];
    });
    
    return _instance;
}


-(id)init{
    
    self = [super init];
    if (self) {
        
        self.bucket = @"fd-images";
        self.imageEndpoint = @"img-cn-hangzhou.aliyuncs.com";
        
        self.endpoint = @"oss-cn-hangzhou.aliyuncs.com";
        //fd-images.oss-cn-hangzhou.aliyuncs.com
        
        self.downloadingFiles = [NSMutableArray array];
        
        // 明文设置secret的方式建议只在测试时使用，更多鉴权模式请参考后面的`访问控制`章节
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"NKLlDcbheb5XSETx"
                                                                                                                secretKey:@"Bg9u5mbtH0Rl6K1PIEto28OEKqS5C2"];
        
        self.ossClient = [[OSSClient alloc] initWithEndpoint:[NSString stringWithFormat:@"http://%@", self.endpoint] credentialProvider:credential];
        
    }
    
    return self;
}

- (NSString *)imageURLForObjectKey:(NSString *)objectKey{
    
    return [self imageURLForObjectKey:objectKey imageSize:CGSizeMake(0, 0)];
}

- (NSString *)imageURLForObjectKey:(NSString *)objectKey imageSize:(CGSize)size{
    
    return [self imageURLForObjectKey:objectKey imageSize:size cornerRadius:0];

}

-(NSString *)imageURLForObjectKey:(NSString *)objectKey imageSize:(CGSize)size cornerRadius:(CGFloat)cornerRadius{
    
    if (size.height == 0 && size.width == 0) {
        return [NSString stringWithFormat:@"http://%@.%@/%@", self.bucket, self.imageEndpoint, objectKey];
    }
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    
    int width = size.width * scale;
    int height = size.height * scale;
    cornerRadius = cornerRadius * scale;
    
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@.%@/%@", self.bucket, self.imageEndpoint, objectKey]];
    NSString *url = nil;
    
    if (cornerRadius>0) {
        url = [NSString stringWithFormat:@"http://%@.%@/%@@%dw_%dh_1e_1c_%d-1ci.png", self.bucket, self.imageEndpoint, objectKey, width, height, (int)cornerRadius];
    }
    else{
        //url = [NSString stringWithFormat:@"http://%@.%@/%@@%dw_%dh_1e_1c.%@", self.bucket, self.imageEndpoint, objectKey, width, height, imageUrl.pathExtension];
        url = [NSString stringWithFormat:@"http://%@.%@/%@@%dw_%dh_1e.%@", self.bucket, self.imageEndpoint, objectKey, width, height, imageUrl.pathExtension];
    }
    
    //NSLog(@"%@", url);
    
    return url;
}

-(NSString*)ObjectKeyForImage:(UIImage*)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString *contentMd5 = [OSSUtil base64Md5ForData:imageData];
    NSString *key = [[YTKNetworkPrivate md5StringFromString:contentMd5] stringByAppendingString:@".jpeg"];
    [[SDImageCache sharedImageCache] storeData:imageData forKey:[self imageURLForObjectKey:key]];
    return key;
    
}

-(NSString*)videoURLForObjectKey:(NSString *)objectKey{
    
    return [self objectURLForObjectKey:objectKey];
}
-(NSString*)voiceURLForObjectKey:(NSString *)objectKey{
    
    return [self objectURLForObjectKey:objectKey];
}

-(NSString*)objectURLForObjectKey:(NSString *)objectKey{
    
    return [NSString stringWithFormat:@"http://%@.%@/%@", self.bucket, self.endpoint, objectKey];
}

-(NSString*)ObjectKeyForVoiceURL:(NSURL*)voiceURL{
    
    NSString *contentMd5 = [OSSUtil base64Md5ForFileURL:voiceURL];
    NSString *key = [[YTKNetworkPrivate md5StringFromString:contentMd5] stringByAppendingString:@".amr"];
    
    [[SDImageCache sharedImageCache] storeData:[NSData dataWithContentsOfFile:voiceURL.absoluteString] forKey:[self voiceURLForObjectKey:key]];
    
    return key;
    
}

-(NSString*)ObjectKeyForVideoURL:(NSURL*)videoURL handler:(void (^)())handler{
    
    NSString *contentMd5 = [OSSUtil base64Md5ForFileURL:videoURL];
    NSString *key = [[YTKNetworkPrivate md5StringFromString:contentMd5] stringByAppendingString:@".mp4"];
    
    [[SDImageCache sharedImageCache] storeData:[NSData dataWithContentsOfURL:videoURL] forKey:[self videoURLForObjectKey:key]];

    
    return key;
}


-(void)uploadWithObjectKey:(NSString*)objectKey fileURL:(NSURL*)fileURL block:(OSSContinuationBlock)block{
    
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    put.bucketName = self.bucket;
    
    put.objectKey = objectKey;
    put.uploadingFileURL = fileURL;
 
    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        //NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
    };
    
    OSSTask * putTask = [self.ossClient putObject:put];
    
    [putTask continueWithBlock:^id(OSSTask *task) {
        if (block) {
            
            if (!task.error) {
                block([OSSTask taskWithResult:put.objectKey]);
                YTKLog(@"SUCCESS:%@", put.objectKey);
                
            }
            else{
                YTKLog(@"UPLOADFAILED:%@", task.error);
                block(task);
            }
            
        }
        return nil;
    }];

    
}
-(long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize]/(1024.0);
    }
    return 0;
}
-(void)downloadWithObjectKey:(NSString*)objectKey progress:(OSSNetworkingDownloadProgressBlock)progress block:(OSSContinuationBlock)block{
    
    
    if ([self localFileExistsForObjectKey:objectKey]) {
        
        block([OSSTask taskWithResult:[self localFilePathForObjectKey:objectKey]]);
        
        return;
    }
    
    
    OSSGetObjectRequest * request = [OSSGetObjectRequest new];
    
    // 必填字段
    request.bucketName = self.bucket;
    request.objectKey = objectKey;
    
    // 可选字段
    request.downloadProgress = progress;
    
    request.downloadToFileURL = [NSURL fileURLWithPath:[self localFilePathForObjectKey:objectKey]]; // 如果需要直接下载到文件，需要指明目标文件地址
    
    OSSTask * getTask = [self.ossClient getObject:request];
    
    [getTask continueWithBlock:^id(OSSTask *task) {
        if (block) {
            
            if (!task.error) {
                block(nil);
            }
            else{
                [[SDImageCache sharedImageCache] removeImageForKey:[self objectURLForObjectKey:objectKey]];
                block(task);
            }
            
        }
        return nil;
    }];


    
}

-(void)uploadVideoWithObjectKey:(NSString*)objectKey block:(OSSContinuationBlock)block{
    
    [self uploadWithObjectKey:objectKey fileURL:[NSURL fileURLWithPath:[[SDImageCache sharedImageCache] defaultCachePathForKey:[self videoURLForObjectKey:objectKey]]] block:block];
}

-(void)uploadVoiceWithObjectKey:(NSString*)objectKey block:(OSSContinuationBlock)block{
    
     [self uploadWithObjectKey:objectKey fileURL:[NSURL fileURLWithPath:[[SDImageCache sharedImageCache] defaultCachePathForKey:[self voiceURLForObjectKey:objectKey]]] block:block];
    
}

-(void)uploadWithObjectKey:(NSString*)objectKey block:(OSSContinuationBlock)block{
    
    [self uploadWithObjectKey:objectKey fileURL:[NSURL fileURLWithPath:[[SDImageCache sharedImageCache] defaultCachePathForKey:[self imageURLForObjectKey:objectKey]]] block:block];
}

-(NSString*)uploadImage:(UIImage*)image block:(OSSContinuationBlock)block{
    
    NSString *objectKey = [self ObjectKeyForImage:image];
    [self uploadWithObjectKey:objectKey block:block];
    return objectKey;
}

-(BOOL)localFileExistsForObjectKey:(NSString *)objectKey{
    
    return [[SDImageCache sharedImageCache] diskImageExistsWithKey:[self objectURLForObjectKey:objectKey]];
}

- (BOOL)localFileExistsForObjectUrl:(NSURL *)objectUrl
{
   
    return [[SDImageCache sharedImageCache] diskImageExistsWithKey:[self objectURLForObjectKey:[objectUrl.path substringFromIndex:1]]];
}

-(NSString*)localFilePathForObjectKey:(NSString *)objectKey{

    
    return [[SDImageCache sharedImageCache] defaultCachePathForKey:[self objectURLForObjectKey:objectKey]];
    
}

-(UIImage *)imageWithVideoURL:(NSURL *)videoURL{
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    gen.appliesPreferredTrackTransform = YES;
    
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    
    NSError *error = nil;
    
    CMTime actualTime;
    
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    
    CGImageRelease(image);
    
    return thumb;
}

@end

