//
//  NKFileManager.h
//  FANSZ
//
//  Created by jinzhenghua on 29/12/2015.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AliyunOSSiOS/OSSService.h>
#import "SDImageCache.h"

#define FZVideouality UIImagePickerControllerQualityType640x480

@interface NKFileManager : NSObject

@property (nonatomic, strong) OSSClient *ossClient;
@property (nonatomic, strong) NSMutableArray *downloadingFiles;


+(NKFileManager *)fileManager;


-(NSString*)uploadImage:(UIImage*)image block:(OSSContinuationBlock)block;
-(void)uploadVideoWithObjectKey:(NSString*)objectKey block:(OSSContinuationBlock)block;
-(void)uploadVoiceWithObjectKey:(NSString*)objectKey block:(OSSContinuationBlock)block;
-(void)uploadWithObjectKey:(NSString*)objectKey block:(OSSContinuationBlock)block;
-(void)uploadWithObjectKey:(NSString*)objectKey fileURL:(NSURL*)fileURL block:(OSSContinuationBlock)block;

-(NSString *)imageURLForObjectKey:(NSString *)objectKey imageSize:(CGSize)size;
-(NSString *)imageURLForObjectKey:(NSString *)objectKey imageSize:(CGSize)size cornerRadius:(CGFloat)cornerRadius;

-(NSString*)ObjectKeyForImage:(UIImage*)image;

-(NSString*)videoURLForObjectKey:(NSString *)objectKey;
-(NSString*)voiceURLForObjectKey:(NSString *)objectKey;
-(NSString*)ObjectKeyForVideoURL:(NSURL*)videoURL handler:(void (^)())handler;
-(NSString*)ObjectKeyForVoiceURL:(NSURL*)voiceURL;

-(BOOL)localFileExistsForObjectKey:(NSString *)objectKey;
-(NSString*)localFilePathForObjectKey:(NSString *)objectKey;

-(BOOL)localFileExistsForObjectUrl:(NSURL *)objectUrl;
//-(NSString*)localFilePathForObjectKey:(NSString *)objectKey;


-(void)downloadWithObjectKey:(NSString*)objectKey progress:(OSSNetworkingDownloadProgressBlock)progress block:(OSSContinuationBlock)block;



-(UIImage *)imageWithVideoURL:(NSURL *)videoURL;


@end
