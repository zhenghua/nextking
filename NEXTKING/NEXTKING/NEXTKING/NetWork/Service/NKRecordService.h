//
//  NKRecordService.h
//  WEIMI
//
//  Created by King on 10/24/12.
//  Copyright (c) 2012 ZUO.COM. All rights reserved.
//

#import "NKServiceBase.h"

extern NSString *const NKCachePathFeeds;

@interface NKRecordService : NKServiceBase

dshared(NKRecordService);

#pragma mark Add
-(NKRequest*)addRecordWithTitle:(NSString*)title content:(NSString*)content attachmentDescription:(NSString*)attachmentDescription objectKey:(NSString*)objectKey parentID:(NSString*)parentID type:(NSString*)type andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)addRecordWithRecord:(NKMRecord*)record andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)deleteRecordWithRecord:(NKMRecord*)record andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)getRecordWithID:(NSString*)recordID andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)addRecordWithTitle:(NSString*)title content:(NSString*)content attachmentDescription:(NSString*)attachmentDescription attachmentTitle:(NSString*)attachmentTitle attachmentType:(NSString*)attachmentType objectKey:(NSString*)objectKey parentID:(NSString*)parentID type:(NSString*)type andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listRecordWithUID:(NSString*)uid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)listRecordWithPID:(NSString*)pid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listAllWikiWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listRecordWithType:(NSString*)type offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)listRecordWithType:(NSString*)type offset:(NSInteger)offset size:(NSInteger)size parentID:(NSString*)parentID andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)likeWithRecordID:(NSString*)recordID sharesID:(NSString*)sharesID unlike:(BOOL)unlike  andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listLikeWithRecordID:(NSString*)recordID offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)approveWithRecordID:(NSString*)recordID approve:(BOOL)approve requestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)listAllRecordsWithoffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;

@end
