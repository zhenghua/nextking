//
//  NKOrderService.m
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKOrderService.h"

NSString *const NKCachePathFollowingShares = @"NKCachePathFollowingShares.NK";

NSString *const NKFollowSharesOKNotificationKey = @"NKFollowSharesOKNotification";

@implementation NKOrderService

$singleService(NKOrderService, @"order");


-(instancetype)init{
    
    self = [super init];
    if (self) {
        
        [self performSelector:@selector(fetchMySharesWithRequestDelegate:) withObject:nil afterDelay:0.2];
        
    }
    return self;
    
}

-(void)didLogout:(NSNotification*)notification{
    
    sharedNKOrderService = nil;
}

-(void)fetchMySharesWithRequestDelegate:(NKRequestDelegate*)rd{
    
    if (!rd) {
        rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:nil andFailedSelector:nil];
    }
    rd.finishInspector = self;
    rd.finishInspectorSelector = @selector(fetchSharesOK:);
    
    [self getSharesWithID:nil andRequestDelegate:rd];
}

-(void)fetchSharesOK:(NKRequest*)request{
    self.shares = [request.results lastObject];
    self.shares.user = [NKMUser me];
}

static NSString *const NKAPIAddMoney = @"/add_money";
-(NKRequest*)addMoneyWithMoney:(NSNumber*)money andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIAddMoney];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"money", money, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMWallet class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
}

static NSString *const NKAPIGetWallet = @"/get_wallet";
-(NKRequest*)getWalletWithRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIGetWallet];
    NSMutableDictionary *parameters = [self parameters];
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMWallet class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIApplyShares = @"/apply_shares";
-(NKRequest*)addSharesWithPrice:(NSNumber*)price amount:(NSNumber*)amount title:(NSString*)title link:(NSString*)link andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIApplyShares];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"price", price, parameters);
    NKBindValueToKeyForParameterToDic(@"amount", amount, parameters);
    NKBindValueToKeyForParameterToDic(@"title", title, parameters);
    NKBindValueToKeyForParameterToDic(@"link", link, parameters);
    NKBindValueToKeyForParameterToDic(@"name", [[NKMUser me] name], parameters);
    NKBindValueToKeyForParameterToDic(@"avatar", [[NKMUser me] avatarPath], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIGetShares = @"/get_shares";
-(NKRequest*)getSharesWithID:(NSString*)sharesID andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIGetShares];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", sharesID, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIApprove = @"/process_shares";
-(NKRequest*)approveWithSharesID:(NSString*)sharesID approve:(BOOL)approve requestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIApprove];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", sharesID, parameters);
    NKBindValueToKeyForParameterToDic(@"approve", [NSNumber numberWithBool:approve], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIDealSharesPrice = @"/deal_shares_price";
-(NKRequest*)dealSharesPriceWithOpen:(BOOL)open requestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIDealSharesPrice];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"open", [NSNumber numberWithBool:open], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIDiscoverShares = @"/discover";
-(NKRequest*)discoverWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIDiscoverShares];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIListApply = @"/list_apply";
-(NKRequest*)listApplyWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListApply];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIGetUsersOrders = @"/list_order";
-(NKRequest*)listOrdersWithUID:(NSString*)uid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIGetUsersOrders];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", uid, parameters);
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMOrder class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIListSharesOrder = @"/list_shares_order";
-(NKRequest*)listSharesOrdersWithSharesID:(NSString*)sharesID offset:(NSInteger)offset size:(NSInteger)size price:(NSNumber*)price type:(NSInteger)type andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListSharesOrder];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", sharesID, parameters);
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKBindValueToKeyForParameterToDic(@"price", price, parameters);
    NKBindValueToKeyForParameterToDic(@"type", [NSNumber numberWithInteger:type], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMOrder class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
}

-(void)updateWalletInfo{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(updateWalletOK:) andFailedSelector:nil];
    [self getWalletWithRequestDelegate:rd];
}

-(void)updateWalletOK:(NKRequest*)request{
    
    NKMWallet *wallet = [request.results lastObject];
    [NKMUser me].wallet = wallet;
    
}

static NSString *const NKAPIAddOrder = @"/add_order";
-(NKRequest*)addOrderWithSharesID:(NSString*)sharesID price:(CGFloat)price amount:(CGFloat)amount isBuy:(BOOL)isBuy andRequestDelegate:(NKRequestDelegate*)rd{
    
    rd.finishInspector = self;
    rd.finishInspectorSelector = @selector(updateWalletInfo);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIAddOrder];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"shares_id", sharesID, parameters);
    NKBindValueToKeyForParameterToDic(@"is_buy", [NSNumber numberWithBool:isBuy], parameters);
    NKBindValueToKeyForParameterToDic(@"price", [NSNumber numberWithFloat:price], parameters);
    NKBindValueToKeyForParameterToDic(@"amount", [NSNumber numberWithFloat:amount], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMOrder class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
    
}

static NSString *const NKAPIListShareholders = @"/shareholder_list";
-(NKRequest*)listShareholdersWithSharesID:(NSString*)sharesID offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListShareholders];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", sharesID, parameters);
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShareholder class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}
static NSString *const NKAPIGetShareholder = @"/get_shareholder";
-(NKRequest*)getShareholderWithSharesID:(NSString*)sharesID andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIGetShareholder];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", sharesID, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShareholder class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
}

static NSString *const NKAPIListPosition = @"/position_list";
-(NKRequest*)listPositionWithUID:(NSString*)uid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListPosition];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShareholder class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
    
}

static NSString *const NKAPIFollowShares = @"/follow_shares";
-(NKRequest*)followSharesWithID:(NSString*)sharesID ifUnfollow:(BOOL)isUnfollow requestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIFollowShares];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", sharesID, parameters);
    if (isUnfollow) {
        NKBindValueToKeyForParameterToDic(@"unfollow", [NSNumber numberWithInteger:isUnfollow], parameters);
    }
    
    if (!rd) {
        rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:nil andFailedSelector:nil];
    }
    
    rd.finishInspector = self;
    rd.finishInspectorSelector = @selector(followOK:);
    
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:nil resultType:NKResultTypeOrigin];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

-(void)followOK:(NKRequest*)request{
    [[NSNotificationCenter defaultCenter] postNotificationName:NKFollowSharesOKNotificationKey object:nil];
}

static NSString *const NKAPIListMyFollows = @"/list_follow";
-(NKRequest*)listMyFollowsWithRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListMyFollows];
    NSMutableDictionary *parameters = [self parameters];
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMShares class] resultType:NKResultTypeArray];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIListFlow = @"/list_flow";
-(NKRequest*)listFlowWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListFlow];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMFlow class] resultType:NKResultTypeResultSets];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPICancelOrder = @"/cancel_order";
-(NKRequest*)cancelOrderWithOrderID:(NSString*)orderID andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPICancelOrder];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", orderID, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMOrder class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
}

@end
