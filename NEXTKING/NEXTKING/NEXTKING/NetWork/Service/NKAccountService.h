//
//  NKAccountService.h
//  FANSZ
//
//  Created by King on 10/12/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKServiceBase.h"
#import "YTKNetworkPrivate.h"

@interface NKAccountService : NKServiceBase

dshared(NKAccountService);


#pragma mark Login/Logout

-(NKRequest*)loginWithUsername:(NSString*)username password:(NSString*)password andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)logoutWithRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)registerWithUsername:(NSString*)username password:(NSString*)password name:(NSString*)name avatar:(NSString*)avatar code:(NSString*)code gender:(NSString*)gender andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)checkMobileWithMobile:(NSString*)mobile password:(NSString*)password andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)verifyWithMobile:(NSString*)mobile code:(NSString*)code andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)sendSMSWithMobile:(NSString*)mobile andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)resetPasswordWithMobile:(NSString*)mobile password:(NSString*)password code:(NSString*)code andRequestDelegate:(NKRequestDelegate*)rd;


@end
