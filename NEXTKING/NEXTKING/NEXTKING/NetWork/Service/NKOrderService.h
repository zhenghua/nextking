//
//  NKOrderService.h
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKServiceBase.h"


extern NSString *const NKCachePathFollowingShares;

extern NSString *const NKFollowSharesOKNotificationKey;


@interface NKOrderService : NKServiceBase

@property (nonatomic, strong) NKMShares *shares;

dshared(NKOrderService);

-(NKRequest*)addMoneyWithMoney:(NSNumber*)money andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)getWalletWithRequestDelegate:(NKRequestDelegate*)rd;
-(void)updateWalletInfo;
-(void)fetchMySharesWithRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)addSharesWithPrice:(NSNumber*)price amount:(NSNumber*)amount title:(NSString*)title link:(NSString*)link andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)getSharesWithID:(NSString*)sharesID andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)discoverWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)listApplyWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)approveWithSharesID:(NSString*)sharesID approve:(BOOL)approve requestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)dealSharesPriceWithOpen:(BOOL)open requestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listOrdersWithUID:(NSString*)uid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)listSharesOrdersWithSharesID:(NSString*)sharesID offset:(NSInteger)offset size:(NSInteger)size price:(NSNumber*)price type:(NSInteger)type andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)addOrderWithSharesID:(NSString*)sharesID price:(CGFloat)price amount:(CGFloat)amount isBuy:(BOOL)isBuy andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)cancelOrderWithOrderID:(NSString*)orderID andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listShareholdersWithSharesID:(NSString*)sharesID offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)getShareholderWithSharesID:(NSString*)sharesID andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listPositionWithUID:(NSString*)uid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)followSharesWithID:(NSString*)sharesID ifUnfollow:(BOOL)isUnfollow requestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listMyFollowsWithRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listFlowWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;

@end
