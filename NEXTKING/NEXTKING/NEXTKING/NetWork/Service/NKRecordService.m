//
//  NKRecordService.m
//  WEIMI
//
//  Created by King on 10/24/12.
//  Copyright (c) 2012 ZUO.COM. All rights reserved.
//

#import "NKRecordService.h"

NSString *const NKCachePathFeeds = @"NKCachePathFeeds.NK";

@implementation NKRecordService

$singleService(NKRecordService, @"record");


#pragma mark Add
static NSString *const NKAPIAddRecord = @"/add";

-(NKRequest*)addRecordWithTitle:(NSString*)title content:(NSString*)content attachmentDescription:(NSString*)attachmentDescription objectKey:(NSString*)objectKey parentID:(NSString*)parentID type:(NSString*)type andRequestDelegate:(NKRequestDelegate*)rd{
    
    
    return [self addRecordWithTitle:title content:content attachmentDescription:attachmentDescription attachmentTitle:nil attachmentType:nil objectKey:objectKey parentID:parentID type:type andRequestDelegate:rd];
    
}

-(NKRequest*)addRecordWithRecord:(NKMRecord*)record andRequestDelegate:(NKRequestDelegate*)rd{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIAddRecord];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"title", record.title, parameters);
    [parameters setObject:record.type?record.type:NKRecordTypeFeed forKey:@"type"];
    NKBindValueToKeyForParameterToDic(@"content", record.content, parameters);
    NKBindValueToKeyForParameterToDic(@"parent_id", record.parentID, parameters);
    
    if ([record.attachments count]>0) {
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:record.attachments.count];
        for (NKMAttachment *attachment in record.attachments) {
            [array addObject:[attachment uploadString]];
        }
        NKBindValueToKeyForParameterToDic(@"attachments", array, parameters);
    }
    
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeSingleObject];
    request.requestDelegate = rd;
    
    
    [request start];
    return request;
}

-(NKRequest*)addRecordWithTitle:(NSString*)title content:(NSString*)content attachmentDescription:(NSString*)attachmentDescription attachmentTitle:(NSString*)attachmentTitle attachmentType:(NSString*)attachmentType objectKey:(NSString*)objectKey parentID:(NSString*)parentID type:(NSString*)type andRequestDelegate:(NKRequestDelegate*)rd;{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIAddRecord];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"title", title, parameters);
    [parameters setObject:type?type:NKRecordTypeFeed forKey:@"type"];
    NKBindValueToKeyForParameterToDic(@"content", content, parameters);
    NKBindValueToKeyForParameterToDic(@"parent_id", parentID, parameters);
    
    NKBindValueToKeyForParameterToDic(@"objectKey", objectKey, parameters);
    
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeSingleObject];
    request.requestDelegate = rd;
    

    [request start];
    return request;
    
    
}
static NSString *const NKAPIDeleteRecord = @"/delete";
-(NKRequest*)deleteRecordWithRecord:(NKMRecord*)record andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIDeleteRecord];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"id", record.modelID, parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeSingleObject];
    request.requestDelegate = rd;
    [request start];
    return request;
}

static NSString *const NKAPIGetRecord = @"/get_record";
-(NKRequest*)getRecordWithID:(NSString*)recordID andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIGetRecord];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"id", recordID, parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeSingleObject];
    request.requestDelegate = rd;
    [request start];
    return request;
}

static NSString *const NKAPIListRecordWithUID = @"/user_list";
-(NKRequest*)listRecordWithUID:(NSString*)uid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListRecordWithUID];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"id", uid, parameters);
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;

}

static NSString *const NKAPIListRecordWithPID = @"/user_list";
-(NKRequest*)listRecordWithPID:(NSString*)pid offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListRecordWithPID];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"id", pid, parameters);
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;

    
}
static NSString *const NKAPIListAllWiki = @"/wiki_list";
-(NKRequest*)listAllWikiWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListAllWiki];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}


static NSString *const NKAPIListType = @"/type_list";
-(NKRequest*)listRecordWithType:(NSString*)type offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    return [self listRecordWithType:type offset:offset size:size parentID:nil andRequestDelegate:rd];
}

-(NKRequest*)listRecordWithType:(NSString*)type offset:(NSInteger)offset size:(NSInteger)size parentID:(NSString*)parentID andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListType];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"type", type, parameters);
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKBindValueToKeyForParameterToDic(@"parent_id", parentID, parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;

}

static NSString *const NKAPILike= @"/like";
-(NKRequest*)likeWithRecordID:(NSString*)recordID sharesID:(NSString*)sharesID unlike:(BOOL)unlike  andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPILike];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"record_id", recordID, parameters);
    NKBindValueToKeyForParameterToDic(@"shares_id", sharesID, parameters);
    NKBindValueToKeyForParameterToDic(@"unlike", [NSNumber numberWithInteger:unlike], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeOrigin];
    request.requestDelegate = rd;
    [request start];
    return request;
}

static NSString *const NKAPIListLiker= @"/list_liker";
-(NKRequest*)listLikeWithRecordID:(NSString*)recordID offset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListLiker];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKBindValueToKeyForParameterToDic(@"id", recordID, parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}


static NSString *const NKAPIUserVerify = @"/user_verify";
-(NKRequest*)approveWithRecordID:(NSString*)recordID approve:(BOOL)approve requestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIUserVerify];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"id", recordID, parameters);
    NKBindValueToKeyForParameterToDic(@"approve", [NSNumber numberWithBool:approve], parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIListAll = @"/list_all";
-(NKRequest*)listAllRecordsWithoffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListAll];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMRecord class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}

@end
