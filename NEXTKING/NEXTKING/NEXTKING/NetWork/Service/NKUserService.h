//
//  NKUserService.h
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKServiceBase.h"

@interface NKUserService : NKServiceBase

@property (nonatomic, strong) NKMNotification *notification;

@property (nonatomic, strong) NKMNotification *local;

dshared(NKUserService);

-(void)fetchNotification;
-(void)updateLocalNotification;

-(void)postDeviceToken:(NSData*)deviceToken;


-(NKRequest*)bindAPNWithToken:(NSString*)token andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)updateUserInfoWithInfo:(NSDictionary*)info andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)getNotificationWithRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listUsersWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)processUserWithUID:(NSString*)uid status:(NSInteger)status andRequestDelegate:(NKRequestDelegate*)rd;

-(NKRequest*)listNoticesWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd;
-(NKRequest*)deleteAllNoticesWithandRequestDelegate:(NKRequestDelegate*)rd;

@end
