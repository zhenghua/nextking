//
//  NKUserService.m
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKUserService.h"
#import "NKDataStore.h"

NSString *const NKCachePathNotificaton = @"NKCachePathNotificaton.NK";

@implementation NKUserService

$singleService(NKUserService, @"user");

-(instancetype)init{
    
    self = [super init];
    if (self) {
        
        self.local = [[NKDataStore sharedDataStore] cachedObjectOf:NKCachePathNotificaton andClass:[NKMNotification class]];
        if (!self.local) {
            self.local = [NKMNotification new];
        }
    }
    return self;
}

-(void)updateLocalNotification{
    
    if ([self.notification.noticeTime longLongValue]>[self local].noticeTime.longLongValue) {
        self.local.noticeTime = self.notification.noticeTime;
        [self.local setNeedNoticeAlert:[NSNumber numberWithBool:NO]];
    }
    
    if ([self.notification.feedTime longLongValue]>[self local].feedTime.longLongValue) {
        self.local.feedTime = self.notification.feedTime;
        [self.local setNeedFeedAlert:[NSNumber numberWithBool:NO]];
    }
    
    [[NKDataStore sharedDataStore] cacheObject:self.local forCacheKey:NKCachePathNotificaton];

}

-(void)didLogout:(NSNotification*)notification{
    [NKRequestDelegate removeTarget:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fetchNotification) object:nil];
    
    sharedNKUserService = nil;
}

-(void)fetchNotification{
    
    [NKRequestDelegate removeTarget:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fetchNotification) object:nil];
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(getNotificationOK:) andFailedSelector:nil];
    [self getNotificationWithRequestDelegate:rd];
    
    [self performSelector:@selector(fetchNotification) withObject:nil afterDelay:self.local.interval.floatValue>1?self.local.interval.floatValue:60];
}

-(void)getNotificationOK:(NKRequest*)request{
    
    self.notification = [request.results lastObject];
    self.local.interval = self.notification.interval;
    
    self.local.needFeedAlert = [NSNumber numberWithBool:[self.notification.feedTime integerValue]>self.local.feedTime.integerValue];
    self.local.needNoticeAlert = [NSNumber numberWithBool:[self.notification.noticeTime integerValue]>self.local.noticeTime.integerValue];

}


#pragma mark APN
- (void)postDeviceToken:(NSData*)deviceToken
{
    NSString *deviceTokenStr = [NSString stringWithFormat:@"%@", deviceToken];
    deviceTokenStr = [deviceTokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceTokenStr = [deviceTokenStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    [self bindAPNWithToken:deviceTokenStr andRequestDelegate:nil];
}

static NSString *const NKAPIBindDeviceToken= @"/bind_token";
-(NKRequest*)bindAPNWithToken:(NSString*)token andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIBindDeviceToken];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"token", token, parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeOrigin];
    request.requestDelegate = rd;
    [request start];
    return request;
}

static NSString *const NKAPIUpdateInfo= @"/update_info";
-(NKRequest*)updateUserInfoWithInfo:(NSDictionary*)info andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIUpdateInfo];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValuesForKeysWithDictionary:info];
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeSingleObject];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}

static NSString *const NKAPIGetNotification = @"/get_notification";
-(NKRequest*)getNotificationWithRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIGetNotification];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMNotification class] resultType:NKResultTypeSingleObject];
    request.requestDelegate = rd;
    [request start];
    return request;
}

static NSString *const NKAPIListUsers = @"/list_all";
-(NKRequest*)listUsersWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListUsers];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;
}

static NSString *const NKAPIProcessUser = @"/process_user";
-(NKRequest*)processUserWithUID:(NSString*)uid status:(NSInteger)status andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIProcessUser];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"id", uid, parameters);
    NKBindValueToKeyForParameterToDic(@"status", [NSNumber numberWithInteger:status], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeOrigin];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}

static NSString *const NKAPIListNotice = @"/list_notice";
-(NKRequest*)listNoticesWithOffset:(NSInteger)offset size:(NSInteger)size andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIListNotice];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKBindValueToKeyForParameterToDic(@"offset", [NSNumber numberWithInteger:offset], parameters);
    NKBindValueToKeyForParameterToDic(@"size", [NSNumber numberWithInteger:size], parameters);
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMNotice class] resultType:NKResultTypeResultSets];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}

static NSString *const NKAPIDeleteAllNotices = @"/remove_notice";
-(NKRequest*)deleteAllNoticesWithandRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIDeleteAllNotices];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NKRequest *request = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeOrigin];
    request.requestDelegate = rd;
    [request start];
    return request;
    
}


@end
