//
//  NKAccountService.m
//  FANSZ
//
//  Created by King on 10/12/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKAccountService.h"

@implementation NKAccountService

$singleService(NKAccountService, @"account");


#pragma mark Login
static NSString *const NKAPILogin = @"/login";
-(NKRequest*)loginWithUsername:(NSString*)username password:(NSString*)password andRequestDelegate:(NKRequestDelegate*)rd{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPILogin];
    
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:@{@"account":username, @"password":password} methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}


static NSString *const NKAPILogout = @"/logout";
-(NKRequest*)logoutWithRequestDelegate:(NKRequestDelegate*)rd{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPILogout];
    
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:nil methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeOrigin];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPIRegister = @"/register";
-(NKRequest*)registerWithUsername:(NSString*)username password:(NSString*)password name:(NSString*)name avatar:(NSString*)avatar code:(NSString*)code gender:(NSString*)gender andRequestDelegate:(NKRequestDelegate*)rd;{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIRegister];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"account", username, parameters);
    NKBindValueToKeyForParameterToDic(@"password", password, parameters);
    NKBindValueToKeyForParameterToDic(@"name", name, parameters);
    NKBindValueToKeyForParameterToDic(@"avatar", avatar, parameters);
    NKBindValueToKeyForParameterToDic(@"code", code, parameters);
    NKBindValueToKeyForParameterToDic(@"gender", gender, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:[NKMUser class] resultType:NKResultTypeSingleObject];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

static NSString *const NKAPICheckMobile = @"/check_mobile";
-(NKRequest*)checkMobileWithMobile:(NSString*)mobile password:(NSString*)password andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPICheckMobile];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"mobile", mobile, parameters);
    NKBindValueToKeyForParameterToDic(@"password", password, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:nil resultType:NKResultTypeOrigin];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}
static NSString *const NKAPIVerifyMobileCode = @"/verify";
-(NKRequest*)verifyWithMobile:(NSString*)mobile code:(NSString*)code andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIVerifyMobileCode];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"mobile", mobile, parameters);
    NKBindValueToKeyForParameterToDic(@"code", code, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:nil resultType:NKResultTypeOrigin];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}
static NSString *const NKAPISendSMS = @"/send_sms";
-(NKRequest*)sendSMSWithMobile:(NSString*)mobile andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPISendSMS];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"mobile", mobile, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:nil resultType:NKResultTypeOrigin];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}
static NSString *const NKAPIResetPassword = @"/reset_password";
-(NKRequest*)resetPasswordWithMobile:(NSString*)mobile password:(NSString*)password code:(NSString*)code andRequestDelegate:(NKRequestDelegate*)rd{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[self serviceBaseURL], NKAPIResetPassword];
    NSMutableDictionary *parameters = [self parameters];
    NKBindValueToKeyForParameterToDic(@"mobile", mobile, parameters);
    NKBindValueToKeyForParameterToDic(@"code", code, parameters);
    NKBindValueToKeyForParameterToDic(@"password", password, parameters);
    NKRequest *reqeust = [NKRequest requestWithURL:urlString parameters:parameters methodName:nil resultClass:nil resultType:NKResultTypeOrigin];
    reqeust.requestDelegate = rd;
    [reqeust start];
    return reqeust;
    
}

@end
