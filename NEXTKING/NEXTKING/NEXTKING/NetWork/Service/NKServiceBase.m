//
//  NKServiceBase.m
//  NEXTKING
//
//  Created by King on 10/24/12.
//  Copyright (c) 2012 NK.COM. All rights reserved.
//

#import "NKServiceBase.h"
#import "NKConfig.h"
#import "NKAccountManager.h"

@implementation NKServiceBase

//-(void)addRequest:(NKRequest*)request{
//    
//    [request addRequestHeader:@"nkmac" value:[self makeMD5:[NKServiceBase macaddress]]];
//    
//    [[NKSDK sharedSDK] addRequest:request];
//    
//#ifdef DEBUG
//    NSLog(@"url:%@, cookie:%@, header:%@", request.url, [request requestCookies], [request requestHeaders]);
//#endif
//    // Start
//    [request.requestDelegate delegateStartWithRequest:request];
//}

-(void)dealloc{
    
    [NKRequestDelegate removeTarget:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NKWillLogoutNotificationKey object:nil];
}

-(instancetype)init{
    
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:NKWillLogoutNotificationKey object:nil];
    }
    return self;
}

-(void)didLogout:(NSNotification*)notification{
    
}

-(NSString *)serviceBaseURL{
    if (self.serviceName) {
        return [[[NKConfig sharedConfig] domainURL] stringByAppendingFormat:@"/%@",self.serviceName];
    }
    return [[NKConfig sharedConfig] domainURL];
    
}

-(NSString*)accessToken{
    
    NKMAccount *account = [[[[NKConfig sharedConfig] accountManagerClass] sharedAccountManager] currentAccount];
    if (account.accessToken) {
        return account.accessToken;
    }
    return @"demo";
}

-(NSString*)sn{
    
    NSString *sn = [[NKMUser me] modelID];
    if (sn) {
        return sn;
    }
    
    return @"demo";
}

-(NSMutableDictionary*)parametersWithSNAndAccessToken{
    
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:[self sn], @"sn", [self accessToken], @"access_token", nil];
    
}

-(NSMutableDictionary*)parametersWithMemberSNAndAccessToken{
    
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:[self sn], @"member_sn", [self accessToken], @"access_token", nil];
}
-(NSMutableDictionary*)parameters{
    
    return [NSMutableDictionary dictionary];
}

@end
