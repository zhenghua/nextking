//
//  NKRequest.m
//  FANZ
//
//  Created by King on 10/9/15.
//  Copyright © 2015 FANZ. All rights reserved.
//

#import "NKRequest.h"

@implementation NKRequest


+(id)requestWithURL:(NSString*)requestURL andParameters:(NSDictionary*)parameters{
    
    NKRequest *request = [[NKRequest alloc] init];
    
    request.requestURL = requestURL;
    request.parameters = parameters;
    request.method = YTKRequestMethodPost;
    request.serializerType = YTKRequestSerializerTypeJSON;

    return request;
}

+ (id)requestWithURL:(NSString *)requestURL andParameters:(NSDictionary *)parameters methodName:(NSString *)methodName
{
    
    NKRequest *request = [[NKRequest alloc] init];
    request.requestURL = requestURL;
    request.parameters = parameters;
    request.method = YTKRequestMethodPost;
    request.serializerType = YTKRequestSerializerTypeJSON;
    request.methodName = methodName;
    return request;

}

+(id)requestWithURL:(NSString*)requestURL parameters:(NSDictionary*)parameters methodName:(NSString *)methodName resultClass:(Class)resultClass resultType:(NKResultType)resultType{
    
    
    NKRequest *request = [[NKRequest alloc] init];
    request.requestURL = requestURL;
    request.parameters = parameters;
    request.method = YTKRequestMethodPost;
    request.serializerType = YTKRequestSerializerTypeHTTP;
    request.resultClass = resultClass;
    request.resultType = resultType;
    request.methodName = methodName;
    
    
    return request;
    
}


- (NSTimeInterval)requestTimeoutInterval {
    return 30;
}

- (NSString *)requestUrl {
    return self.requestURL;
}

- (YTKRequestMethod)requestMethod {
    return self.method;
}

- (id)requestArgument {
    
    if (self.methodName == nil) {
        return self.parameters;
    }
    return [self buildParameters];
//    return self.parameters;
}

- (YTKRequestSerializerType)requestSerializerType {
    return self.serializerType;
}

/**
 *  @author mazengyi, 15-12-03 15:28:12
 *
 *  转换参数
 *
 *  @param parameters 参数dic
 *
 *  @return           转换格式之后的dic
 */
- (NSDictionary *)buildParameters
{
    /*
     请求的格式
     {"header": {
     "local": "ZH_cn",
     "terminal_type": "IOS"
     },
     "request": [{
     "method": "getVerifyCodeForRegister",
     "params": {
     "mobile": "18506046205"
     }
     }]}
     */
   
    NSMutableDictionary *requestDic = [NSMutableDictionary dictionary];
    requestDic[@"header"] = @{@"local"  : @"ZH_cn" , @"terminal_type" : @"iOS"};
    requestDic[@"request"] = @[@{@"method" : self.methodName, @"params" : self.parameters}];
    return requestDic;
}

@end
