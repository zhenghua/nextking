//
//  NKRequestDelegate.h
//  NEXTKING
//
//  Created by King on 10/24/12.
//  Copyright (c) 2012 ZUO.COM. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

@interface NKRequestDelegate : NSObject{
    
}

@property (nonatomic, weak) id  delegateTarget;
@property (nonatomic, assign) SEL startSelector;
@property (nonatomic, assign) SEL finishSelector;
@property (nonatomic, assign) SEL failedSelector;

@property (nonatomic, weak) id  finishInspector;
@property (nonatomic, assign) SEL finishInspectorSelector;



+(instancetype)requestDelegateWithTarget:(id)atarget startSelector:(SEL)astartSelector finishSelector:(SEL)afinishSelector andFailedSelector:(SEL)afailedSelector;

+(instancetype)requestDelegateWithTarget:(id)atarget finishSelector:(SEL)afinishSelector andFailedSelector:(SEL)afailedSelector;



+(void)removeTarget:(id)tg;



// Tell the delegate
-(void)delegateStartWithRequest:(id)request;
-(void)delegateFinishWithRequest:(id)request;
-(void)delegateFailedWithRequest:(id)request;


// Default process if the sd has nil SEL
-(void)start;
-(void)success;
-(void)failed;

@end
