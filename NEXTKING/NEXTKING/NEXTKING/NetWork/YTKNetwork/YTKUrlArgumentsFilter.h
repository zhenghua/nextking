//
//  YTKUrlArgumentsFilter.h
//  FANSZ
//
//  Created by King on 10/14/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTKNetworkConfig.h"

@interface YTKUrlArgumentsFilter : NSObject <YTKUrlFilterProtocol>

@property (nonatomic, strong) NSDictionary *arguments;

+ (YTKUrlArgumentsFilter *)filterWithArguments:(NSDictionary *)arguments;

- (NSString *)filterUrl:(NSString *)originUrl withRequest:(YTKBaseRequest *)request;

@end