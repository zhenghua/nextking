//
//  YTKUrlArgumentsFilter.m
//  FANSZ
//
//  Created by King on 10/14/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "YTKUrlArgumentsFilter.h"
#import "YTKNetworkPrivate.h"

@implementation YTKUrlArgumentsFilter {
    
}

+ (YTKUrlArgumentsFilter *)filterWithArguments:(NSDictionary *)arguments {
    return [[self alloc] initWithArguments:arguments];
}

- (id)initWithArguments:(NSDictionary *)arguments {
    self = [super init];
    if (self) {
        self.arguments = arguments;
    }
    return self;
}

- (NSString *)filterUrl:(NSString *)originUrl withRequest:(YTKBaseRequest *)request {
    return [YTKNetworkPrivate urlStringWithOriginUrlString:originUrl appendParameters:_arguments];
}


@end
