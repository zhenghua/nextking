//
//  NKRequest.h
//  FANZ
//
//  Created by King on 10/9/15.
//  Copyright © 2015 FANZ. All rights reserved.
//

#import "YTKRequest.h"
#import "NKRequestDelegate.h"


typedef enum{
    NKResultTypeSingleObject,   // Single Object
    NKResultTypeArray,          // Array of objects
    NKResultTypeResultSets,     // totalCount and array of some objects
    NKResultTypeOriginArray,
    NKResultTypeDeapArray,    // [[a,a,a],[a,a],[a,a]]
    NKResultTypeOrigin
}NKResultType;

@interface NKRequest : YTKRequest

@property (nonatomic, strong) NSString                  *requestURL;
@property (nonatomic, strong) NSDictionary              *parameters;
@property (nonatomic, assign) YTKRequestMethod           method;
@property (nonatomic, assign) YTKRequestSerializerType   serializerType;
@property (nonatomic, assign) NKResultType               resultType;
@property (nonatomic, assign) Class                      resultClass;
@property (nonatomic, copy)   NSString                  *methodName;


@property (nonatomic, strong) NSMutableArray     *results;
@property (nonatomic, strong) NSNumber           *totalCount;
@property (nonatomic, strong) NSNumber           *pageNumber;
@property (nonatomic, strong) NSNumber           *hasMore;

+(id)requestWithURL:(NSString*)requestURL andParameters:(NSDictionary*)parameters;

+(id)requestWithURL:(NSString*)requestURL andParameters:(NSDictionary*)parameters methodName:(NSString *)methodName;

+(id)requestWithURL:(NSString*)requestURL parameters:(NSDictionary*)parameters methodName:(NSString *)methodName resultClass:(Class)resultClass resultType:(NKResultType)resultType;

@end
