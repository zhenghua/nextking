//
//  NKRequestDelegate.m
//  NEXTKING
//
//  Created by King on 10/24/12.
//  Copyright (c) 2012 ZUO.COM. All rights reserved.
//

#import "NKRequestDelegate.h"
#import "NKRequest.h"



@implementation NKRequestDelegate


static NSMutableArray *_requestDelegates = nil;

+(void)addRD:(NKRequestDelegate*)rd{
    
    if (!_requestDelegates) {
        _requestDelegates = [[NSMutableArray alloc] init];
    }
    
    if (rd) {
        [_requestDelegates addObject:rd];
    }
    
}

+(void)removeRD:(NKRequestDelegate*)rd{
    
    if (rd) {
        [_requestDelegates removeObject:rd];
    }
}

+(void)removeTarget:(id)tg{
    
    for (NKRequestDelegate *td in _requestDelegates) {
        if (td.delegateTarget == tg) {
            td.delegateTarget = nil;
        }
    }
    
}

+(instancetype)requestDelegateWithTarget:(id)target finishSelector:(SEL)finishSelector andFailedSelector:(SEL)failedSelector{
    return [self requestDelegateWithTarget:target startSelector:nil finishSelector:finishSelector andFailedSelector:failedSelector];
}

+(instancetype)requestDelegateWithTarget:(id)target startSelector:(SEL)startSelector finishSelector:(SEL)finishSelector andFailedSelector:(SEL)failedSelector{
    
    NKRequestDelegate *newRequestDelegate = [[NKRequestDelegate alloc] init];
    
    newRequestDelegate.delegateTarget = target;
    newRequestDelegate.startSelector = startSelector;
    newRequestDelegate.finishSelector = finishSelector;
    newRequestDelegate.failedSelector = failedSelector;
    
    [self addRD:newRequestDelegate];
    
    return newRequestDelegate;
    
}


-(void)delegateStartWithRequest:(id)request{
    if (self.delegateTarget && self.startSelector && [self.delegateTarget respondsToSelector:self.startSelector]) {
        SuppressPerformSelectorLeakWarning(
                                           [self.delegateTarget performSelector:self.startSelector withObject:request];
                                           );
    }
    else {
        [self start];
    }
    
}
-(void)delegateFinishWithRequest:(id)request{
    
    if (self.finishInspector && self.finishInspectorSelector && [self.finishInspector respondsToSelector:self.finishInspectorSelector]) {
        SuppressPerformSelectorLeakWarning(
                                           [_finishInspector performSelector:self.finishInspectorSelector withObject:request];
                                           );
    }
    
    if (self.delegateTarget && self.finishSelector) {
        SuppressPerformSelectorLeakWarning(
                                           [self.delegateTarget performSelector:self.finishSelector withObject:request];
                                           );
    }
    else {
        [self success];
    }
    
    [NKRequestDelegate removeRD:self];
    
}
-(void)delegateFailedWithRequest:(id)request{
    
    if (self.delegateTarget && self.failedSelector) {
        SuppressPerformSelectorLeakWarning(
                                           [self.delegateTarget performSelector:self.failedSelector withObject:request];
                                           );
    }
    else {
        [self failed];
    }
    
    [NKRequestDelegate removeRD:self];
    
}

-(void)start{

}

-(void)success{
    [NKRequestDelegate removeRD:self];
}

-(void)failed{
    [NKRequestDelegate removeRD:self];
}




@end
