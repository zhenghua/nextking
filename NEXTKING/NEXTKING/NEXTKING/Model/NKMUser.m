//
//  NKMUser.m
//  FANSZ
//
//  Created by King on 10/12/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKMUser.h"
#import "UIColor+HexString.h"
#import "NSDate+YYAdd.h"
#import "NKMRecord.h"

//NSString *const NKRelationFriend = @"friend";
//NSString *const NKRelationFollow = @"follow";
//NSString *const NKRelationFollower = @"follower";
//NSString *const NKRelationMySelf;
//NSString *const NKRelationUnknow;

NSString *const NKRelationFriend = @"2";
NSString *const NKRelationFollow = @"1";
NSString *const NKRelationFollower = @"3";
NSString *const NKRelationMySelf = @"4";
NSString *const NKRelationUnknow = @"5";


NSString *const NKGenderKeyMale = @"M";
NSString *const NKGenderKeyFemale = @"F";
NSString *const NKGenderKeyUnknown = @"unknown";


@implementation NKMWallet

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMWallet *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"uid", model.uid, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"money", model.money, dic);
    NKBindValueWithKeyForParameterFromDic(@"order_money", model.orderMoney, dic);
    NKBindValueWithKeyForParameterFromDic(@"shares_money", model.sharesMoney, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"status", model.status, dic);
    
    return model;
}

@end


@implementation NKMShares

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMShares *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"uid", model.uid, dic);
    NKBindValueWithKeyForParameterFromDic(@"name", model.name, dic);
    NKBindValueWithKeyForParameterFromDic(@"avatar", model.avatar, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"price", model.price, dic);
    NKBindValueWithKeyForParameterFromDic(@"status", model.status, dic);
    NKBindValueWithKeyForParameterFromDic(@"following", model.following, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"total_amount", model.totalAmount, dic);
    NKBindValueWithKeyForParameterFromDic(@"deal_amount", model.dealAmount, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"open_price", model.openPrice, dic);
    NKBindValueWithKeyForParameterFromDic(@"close_price", model.closePrice, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"launch_price", model.launchPrice, dic);
    NKBindValueWithKeyForParameterFromDic(@"launch_time", model.launchTime, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"title", model.title, dic);
    NKBindValueWithKeyForParameterFromDic(@"link", model.link, dic);
    
    NSDictionary *userDic = [dic objectOrNilForKey:@"user"];
    if (userDic) {
        model.user = [NKMUser modelFromCache:userDic];
    }
    
    return model;
}

-(NSString*)sharesID{
    
    return [NSString stringWithFormat:@"%@", [NSNumber numberWithInt:self.modelID.intValue]];
}

-(NKMSharesPriceStauts)priceStatus{
    
    CGFloat currentPrice = self.price.floatValue;
    CGFloat closePrice = self.closePrice.floatValue;
    if (closePrice == 0) {
        closePrice = self.launchPrice.floatValue;
    }
    
    if (currentPrice > closePrice) {
        return NKMSharesPriceStautsRise;
    }
    else if (currentPrice < closePrice){
        return NKMSharesPriceStautsFall;
    }
    else{
        return NKMSharesPriceStautsNormal;
    }
    
}

-(UIImage*)priceImage{
    
    switch ([self priceStatus]) {
        case NKMSharesPriceStautsNormal:
            return [UIImage imageNamed:@"back_normal"];
            break;
        case NKMSharesPriceStautsFall:
            return [UIImage imageNamed:@"back_fall"];
            break;
        case NKMSharesPriceStautsRise:
            return [UIImage imageNamed:@"back_rise"];
            break;
            
        default:
            break;
    }

    
}

-(UIColor*)priceColor{
    
    switch ([self priceStatus]) {
        case NKMSharesPriceStautsNormal:
            return [NKMShares normalColor];
            break;
        case NKMSharesPriceStautsFall:
            return [NKMShares fallColor];
            break;
        case NKMSharesPriceStautsRise:
            return [NKMShares riseColor];
            break;
            
        default:
            break;
    }
    
}

-(NSString*)priceText{
    return [NSString stringWithFormat:@"%.2f", self.price.floatValue];
}
-(NSString*)rangeText{
    
    CGFloat currentPrice = self.price.floatValue;
    CGFloat closePrice = self.closePrice.floatValue;
    if (closePrice == 0) {
        closePrice = self.launchPrice.floatValue;
    }
    
    if (currentPrice > closePrice) {
        return [NSString stringWithFormat:@"+%.2f", (currentPrice-closePrice)];
    }
    else{
        return [NSString stringWithFormat:@"%.2f", (currentPrice-closePrice)];
    }
    
}
-(NSString*)percentText{
    
    CGFloat currentPrice = self.price.floatValue;
    CGFloat closePrice = self.closePrice.floatValue;
    if (closePrice == 0) {
        closePrice = self.launchPrice.floatValue;
    }
    
    if (currentPrice > closePrice) {
        return [NSString stringWithFormat:@"+%.2f%%", (currentPrice-closePrice)/closePrice*100];
    }
    else{
        return [NSString stringWithFormat:@"%.2f%%", (currentPrice-closePrice)/closePrice*100];
    }
    
}

-(NSString*)marketValueText{
    
    CGFloat value = self.price.floatValue * self.totalAmount.floatValue;
    
    if (value>=100000000) {
        value = value/100000000.0;
        return [NSString stringWithFormat:@"%.2f亿", value];
    }
    else if (value>=10000){
        value = value/10000.0;
        return [NSString stringWithFormat:@"%.2f万", value];
    }
    
    return [NSString stringWithFormat:@"%.2f", value];
}

+(UIColor*)riseColor{
    return [UIColor colorWithHexString:@"FA5E28"];
}
+(UIColor*)fallColor{
    return [UIColor colorWithHexString:@"25BE83"];
}
+(UIColor*)normalColor{
    return [UIColor alphaGrayColor];
}
@end

@implementation NKMOrder

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMOrder *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"buyer_id", model.buyerID, dic);
    NKBindValueWithKeyForParameterFromDic(@"seller_id", model.sellerID, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"price", model.price, dic);
    NKBindValueWithKeyForParameterFromDic(@"amount", model.amount, dic);
    NKBindValueWithKeyForParameterFromDic(@"deal_amount", model.dealAmount, dic);
    NKBindValueWithKeyForParameterFromDic(@"deal_price", model.dealPrice, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"status", model.status, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"create_time", model.createTime, dic);
    NKBindValueWithKeyForParameterFromDic(@"deal_time", model.dealTime, dic);
    NKBindValueWithKeyForParameterFromDic(@"cancel_time", model.cancelTime, dic);
    
    NSDictionary *sharesDic = [dic objectOrNilForKey:@"shares"];
    if (sharesDic) {
        model.shares = [NKMShares modelFromDic:sharesDic];
    }
    
    NSDictionary *buyerDic = [dic objectOrNilForKey:@"buyer"];
    if (buyerDic) {
        model.buyer = [NKMUser modelFromCache:buyerDic];
    }
    
    NSDictionary *sellerDic = [dic objectOrNilForKey:@"seller"];
    if (sellerDic) {
        model.seller = [NKMUser modelFromCache:sellerDic];
    }
    

    return model;
}

-(NSString*)createTimeText{
    
    if (self.createTime) {
        
        NSDateFormatter *formatterFullDate = [[NSDateFormatter alloc] init];
        [formatterFullDate setDateFormat:@"yy-M-dd HH:mm:ss"];
        [formatterFullDate setLocale:[NSLocale currentLocale]];
        return [formatterFullDate stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.createTime.longLongValue]];
    }
    
    return @"--";
}
-(NSString*)dealTimeText{
    
    if (self.dealTime) {
        
        NSDateFormatter *formatterFullDate = [[NSDateFormatter alloc] init];
        [formatterFullDate setDateFormat:@"yy-M-dd HH:mm:ss"];
        [formatterFullDate setLocale:[NSLocale currentLocale]];
        return [formatterFullDate stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.dealTime.longLongValue]];
    }
    
    return @"--";
}
-(NSString*)cancelTimeText{
    
    if (self.cancelTime) {
        
        NSDateFormatter *formatterFullDate = [[NSDateFormatter alloc] init];
        [formatterFullDate setDateFormat:@"yy-M-dd HH:mm:ss"];
        [formatterFullDate setLocale:[NSLocale currentLocale]];
        return [formatterFullDate stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.cancelTime.longLongValue]];
    }
    
    return @"--";
}

-(NSNumber*)dealPrice{
    
    if (!_dealPrice) {
        
        if (self.dealAmount.floatValue>0) {
            return self.price;
        }
        return [NSNumber numberWithFloat:0.0];
    }
    return _dealPrice;
    
}


-(NSNumber*)dealAmount{
    
    if (!_dealAmount) {
        return [NSNumber numberWithFloat:0.0];
    }
    return _dealAmount;
    
}

-(NSString*)statusText{
    
    switch (self.status.intValue) {
        case NKMOrderStatusCreate:{
            if ([self.dealAmount floatValue]>0) {
                return @"部成";
            }
            return @"已报";
        }
            break;
        case NKMOrderStatusFinish:
            return @"已成";
            break;
        case NKMOrderStatusCancel:{
            
            if ([self.dealAmount floatValue]>0 && [self.dealAmount floatValue]<self.amount.floatValue) {
                return @"部撤";
            }
            return @"已撤";
        }
            
            break;
        default:
            break;
    }
    return @"";
}

-(NSString*)actionText{
    
    if ([self isBuy]) {
        return @"买入";
    }
    return @"卖出";
}

-(BOOL)isBuy{
    
    if ((self.buyerID || self.buyer) && !self.sellerID ) {
        return YES;
    }
    return NO;
}

@end


@implementation NKMShareholder

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMShareholder *model = [super modelFromDic:dic];
  
    NKBindValueWithKeyForParameterFromDic(@"shares_id", model.sharesID, dic);
    NKBindValueWithKeyForParameterFromDic(@"uid", model.uid, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"price", model.price, dic);
    NKBindValueWithKeyForParameterFromDic(@"amount", model.amount, dic);
    NKBindValueWithKeyForParameterFromDic(@"selling_amount", model.sellingAmount, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"status", model.status, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"create_time", model.createTime, dic);

    NSDictionary *userDic = [dic objectOrNilForKey:@"user"];
    if (userDic) {
        model.user = [NKMUser modelFromCache:userDic];
    }
    
    NSDictionary *sharesDic = [dic objectOrNilForKey:@"shares"];
    if (sharesDic) {
        model.shares = [NKMShares modelFromDic:sharesDic];
    }
    
    return model;
}

@end



@implementation NKMFlow

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMFlow *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"shares_id", model.sharesID, dic);
    NKBindValueWithKeyForParameterFromDic(@"from_id", model.fromID, dic);
    NKBindValueWithKeyForParameterFromDic(@"to_id", model.toID, dic);
    NKBindValueWithKeyForParameterFromDic(@"other_id", model.otherID, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"price", model.price, dic);
    NKBindValueWithKeyForParameterFromDic(@"amount", model.amount, dic);
    NKBindValueWithKeyForParameterFromDic(@"money", model.money, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"type", model.type, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"time", model.time, dic);
    
    NSDictionary *userDic = [dic objectOrNilForKey:@"user"];
    if (userDic) {
        model.user = [NKMUser modelFromCache:userDic];
    }
    
    NSDictionary *sharesDic = [dic objectOrNilForKey:@"shares"];
    if (sharesDic) {
        model.shares = [NKMShares modelFromDic:sharesDic];
    }
    
    return model;
}

-(NSString*)typeText{
    
    switch (self.type.integerValue) {
        case 1:{
            
            if ([self.fromID isEqualToString:[[NKMUser me] modelID]]) {
                return @"股票买入";
            }
            return @"股票卖出";
            
        }
            break;
        case 2:{
            return @"充值";
        }
            break;
        case 3:{
            return @"提现";
        }
            break;
            
        default:
            break;
    }
    
    return @"";
}

-(NSString*)timeText{
    
    return [NSDate stringWithNormalDate:[NSDate dateWithTimeIntervalSince1970:self.time.doubleValue]];
}

-(NSString*)moneyText{
    
    NSString *money = [NSString stringWithFormat:@"%.2f", self.money.floatValue];
    
    NSString *plus = @"+";
    
    if ([self.fromID isEqualToString:[[NKMUser me] modelID]]) {
        plus = @"-";
    }

    return [NSString stringWithFormat:@"%@%@", plus, money];
}

@end

@implementation NKMNotification

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMNotification *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"feed_time", model.feedTime, dic);
    NKBindValueWithKeyForParameterFromDic(@"notice_time", model.noticeTime, dic);
    NKBindValueWithKeyForParameterFromDic(@"shares_status", model.sharesStatus, dic);
    NKBindValueWithKeyForParameterFromDic(@"interval", model.interval, dic);
    
    return model;
}

-(NSDictionary*)cacheDic{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NKBindValueToKeyForParameterToDic(@"feed_time", self.feedTime, dic);
    NKBindValueToKeyForParameterToDic(@"shares_status", self.sharesStatus, dic);
    NKBindValueToKeyForParameterToDic(@"notice_time", self.noticeTime, dic);
    NKBindValueToKeyForParameterToDic(@"interval", self.interval, dic);
    
    return dic;
}

@end


@implementation NKMNotice

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMNotice *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"time", model.time, dic);
    NKBindValueWithKeyForParameterFromDic(@"type", model.type, dic);
    NKBindValueWithKeyForParameterFromDic(@"cotent", model.cotent, dic);
    
    NSDictionary *record = [dic objectOrNilForKey:@"record"];
    if (record) {
        model.record = [NKMRecord modelFromDic:record];
    }else if ([model.type isEqualToString:NKRecordTypeLike]){
        
        //model.record = [NKMRecord modelFromDic:record];
    }
    
    NSDictionary *parent = [dic objectOrNilForKey:@"parent"];
    if (parent) {
        model.parent = ([model.type isEqualToString:NKRecordTypeSharesComment]||[model.type isEqualToString:NKRecordTypeDeal])?[NKMShares modelFromDic:parent]:[NKMRecord modelFromDic:parent];
    }
    
    
    NSDictionary *order = [dic objectOrNilForKey:@"order"];
    if (order) {
        model.order = [NKMOrder modelFromDic:order];
        model.order.shares = model.parent;
    }
    
    return model;
}

@end


@implementation NKMUser

static NKMUser *_me = nil;
+(id)meFromUser:(NKMUser*)user{
    _me = user;
    return _me;
    
}
+(instancetype)me{
    return _me;
}

+(id)user{
    NKMUser *newUser = [[self alloc] init];
    newUser.createType = NKMUserCreateTypeFromContact;
    return newUser;
}
#if TARGET_OS_IPHONE
+(id)userWithName:(NSString*)tname phoneNumber:(NSString*)tphoneNumber andAvatar:(UIImage*)tavatar{
    
    NKMUser *newUser = [[self alloc] init];
    newUser.createType = NKMUserCreateTypeFromContact;
    newUser.name = tname;
    newUser.mobile = tphoneNumber;
    newUser.avatar = tavatar;
    //    newUser.invited = NO;
    return newUser;
}
#endif

+(id)modelFromDic:(NSDictionary*)dic{
    
    return [self userFromDic:dic];
}

/*
 -(void)logCount{
 
 NSLog(@"%d", [name retainCount]);
 [self performSelector:@selector(logCount) withObject:nil afterDelay:1.0];
 
 }
 */

-(NSString*)description{
    
    return [NSString stringWithFormat:@" <%@> name:%@, id:%@, city:%@, avatar:%@, sign:%@", NSStringFromClass([self class]), _name, self.modelID, _city, _avatarPath, _sign];
}

-(NSDictionary*)cacheDic{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    // ID  !!!
    NKBindValueToKeyForParameterToDic(@"id",self.modelID,dic);
    
    // Name
    NKBindValueToKeyForParameterToDic(@"name",_name,dic);
    NKBindValueToKeyForParameterToDic(@"search_key",self.searchKey,dic);
    
    // Contact
    NKBindValueToKeyForParameterToDic(@"mobile", self.mobile, dic);
    
    // Avatar
    NKBindValueToKeyForParameterToDic(@"avatar", self.avatarPath, dic);
    // Gender
    //    NKBindValueToKeyForParameterToDic(@"gender", self.gender, dic);
    //    NKBindValueToKeyForParameterToDic(@"company", self.company, dic);
    //    NKBindValueToKeyForParameterToDic(@"brief", self.brief, dic);
    NKBindValueToKeyForParameterToDic(@"city", self.city, dic);
    NKBindValueToKeyForParameterToDic(@"birthday", self.birthday, dic);
    
    NKBindValueToKeyForParameterToDic(@"sign", self.sign, dic);
    NKBindValueToKeyForParameterToDic(@"verify_status", self.verify, dic);
    NKBindValueToKeyForParameterToDic(@"online_time", self.onlineTime, dic);
    
    
    return dic;
}

-(void)setBirthday:(NSNumber *)birthday{
    
    _birthday = birthday;
    
    if (birthday) {
        self.constellation = [[NSDate dateWithTimeIntervalSince1970:[birthday longLongValue]] xingzuo];
    }
    
}

-(void)bindValuesForDic:(NSDictionary*)dic{
    
    // Name
    NKBindValueWithKeyForParameterFromDic(@"name", self.name, dic);
    NKBindValueWithKeyForParameterFromDic(@"search_key", self.searchKey, dic);
    
    // Contact
    NKBindValueWithKeyForParameterFromDic(@"mobile", self.mobile, dic);
    NKBindValueWithKeyForParameterFromDic(@"email", self.email, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"avatar", self.avatarPath, dic);
    
    NSDictionary *wallet = [dic objectOrNilForKey:@"wallet"];
    if (wallet) {
        self.wallet = [NKMWallet modelFromDic:wallet];
    }
    // Gender
    NKBindValueWithKeyForParameterFromDic(@"gender", self.gender, dic);
    NKBindValueWithKeyForParameterFromDic(@"company", self.company, dic);
    NKBindValueWithKeyForParameterFromDic(@"location", self.location, dic);
    NKBindValueWithKeyForParameterFromDic(@"sign", self.sign, dic);
    NKBindValueWithKeyForParameterFromDic(@"birthday", self.birthday, dic);
    NKBindValueWithKeyForParameterFromDic(@"city", self.city, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"verify_status", self.verify, dic);
    NKBindValueWithKeyForParameterFromDic(@"status", self.status, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"online_time", self.onlineTime, dic);
    if (self.onlineTime) {
        self.timeString = [NSDate stringWithTimelineDate:[NSDate dateWithTimeIntervalSince1970:[self.onlineTime longLongValue]]];
    }
    
    [self bindRelationWithDictionary:dic];
    
  
    
    
    
    
    /*
     // Friend info
     NKBindValueWithKeyForParameterFromDic(@"commonFriendCount", self.commonFriendCount, dic);
     NKBindValueWithKeyForParameterFromDic(@"followerCount", self.followerCount, dic);
     NKBindValueWithKeyForParameterFromDic(@"friendCount", self.friendCount, dic);
     NKBindValueWithKeyForParameterFromDic(@"friend", self.isFriend, dic);
     NKBindValueWithKeyForParameterFromDic(@"isBlocked", self.isBlocked, dic);
     NKBindValueWithKeyForParameterFromDic(@"connectionType", self.relationType, dic);
     */
    
    
    /*
     NSArray *circlesArray = [dic objectOrNilForKey:@"circles"];
     if ([circlesArray count]) {
     self.circles = [NSMutableArray arrayWithCapacity:[circlesArray count]];
     
     for (NSDictionary *circleDic in circlesArray) {
     [self.circles addObject:[NKMDCircle modelFromDic:circleDic]];
     }
     
     }
     else if (circlesArray) {
     self.circles = [NSMutableArray array];
     }
     
     // Login State and time
     if ([dic objectOrNilForKey:@"lastLoginTime"]!=nil) {
     self.lastLoginTime = [NSDate dateWithTimeIntervalSince1970:[[dic objectOrNilForKey:@"lastLoginTime"] longLongValue]/1000];
     }
     if ([dic objectOrNilForKey:@"createdAt"]) {
     self.createAt = [NSDate dateWithTimeIntervalSince1970:[[dic objectOrNilForKey:@"createdAt"] longLongValue]/1000];
     }
     
     // PostInfo
     NKBindValueWithKeyForParameterFromDic(@"postCount", self.postCount, dic);
     NKBindValueWithKeyForParameterFromDic(@"eventCount", self.eventCount, dic);
     
     // Recommend
     NKBindValueWithKeyForParameterFromDic(@"recommendReason", self.recommendReason, dic);
     NKBindValueWithKeyForParameterFromDic(@"recommendType", self.recommendType, dic);
     
     */
    
}

-(void)bindRelationWithDictionary:(NSDictionary*)dic{
    
    NSString *relation = [dic objectOrNilForKey:@"relationship"];
    if (!relation) {
        self.relation = [NKMUser me] == self? [NSNumber numberWithInteger:NKMUserRelationMySelf]:[NSNumber numberWithInteger:NKMUserRelationUnknow];
    }
    else{
        if ([relation isEqualToString:@"01"]) {
            self.relation = [NSNumber numberWithInteger:NKMUserRelationFollower];
        }
        else if ([relation isEqualToString:@"00"]) {
            self.relation = [NSNumber numberWithInteger:NKMUserRelationFollow];
        }
        else if ([relation isEqualToString:@"10"]) {
            self.relation = [NSNumber numberWithInteger:NKMUserRelationFriend];
        }
        else if ([relation isEqualToString:@"11"]) {
            self.relation = [NSNumber numberWithInteger:NKMUserRelationSpecialFriend];
        }
    }
}

+(id)modelFromCache:(NSDictionary*)dic{
    return [self userFromDic:dic isCache:YES];
}

-(void)bindValuesFromCache:(NSDictionary*)dic{
    
    // Name
    NKBindValueWithKeyForParameterFromCache(@"name",self.name,dic);
    NKBindValueWithKeyForParameterFromCache(@"search_key",self.searchKey,dic);
    
    // Contact
    NKBindValueWithKeyForParameterFromCache(@"mobile", self.mobile, dic);
    
    // Avatar
    NKBindValueWithKeyForParameterFromCache(@"avatar", self.avatarPath, dic);
    
    NKBindValueWithKeyForParameterFromCache(@"gender", self.gender, dic);

    NKBindValueWithKeyForParameterFromCache(@"sign", self.sign, dic);
    NKBindValueWithKeyForParameterFromCache(@"birthday", self.birthday, dic);
    NKBindValueWithKeyForParameterFromCache(@"city", self.city, dic);
    
    NKBindValueWithKeyForParameterFromCache(@"verify_status", self.verify, dic);
    NKBindValueWithKeyForParameterFromCache(@"status", self.status, dic);
    
    NKBindValueWithKeyForParameterFromCache(@"online_time", self.onlineTime, dic);
    if (self.onlineTime) {
        self.timeString = [NSDate stringWithTimelineDate:[NSDate dateWithTimeIntervalSince1970:[self.onlineTime longLongValue]]];
    }
    
    //[self bindRelationWithDictionary:dic];
    
}

+(id)userFromDic:(NSDictionary*)dic isCache:(BOOL)cache{
    if (!dic) {
        return [[self alloc] init];
    }
    
    if (![dic objectOrNilForKey:@"id"]) {
        return [[self alloc] init];
    }
    
    NSString *theUID = [NSString stringWithFormat:@"%@", [dic objectOrNilForKey:@"id"]];
    NKMUser *cachedUser = [[[NKMemoryCache sharedMemoryCache] cachedUsers] objectOrNilForKey:theUID];
    if (cachedUser) {
        
        cache?[cachedUser bindValuesFromCache:dic]:[cachedUser bindValuesForDic:dic];
        
        return cachedUser;
    }
    else{
        NKMUser *newUser = [[NKMUser alloc] init];
        newUser.createType = NKMUserCreateTypeFromWeb;
        newUser.jsonDic = dic;
        // ID
        newUser.modelID  = theUID;
        [newUser bindValuesForDic:dic];
        [[[NKMemoryCache sharedMemoryCache] cachedUsers] setObject:newUser forKey:theUID];
        return newUser;
    }
    
}

+(id)userFromDic:(NSDictionary*)dic{
    
    return [self userFromDic:dic isCache:NO];
}


-(NSData*)profileUpdateString{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObjectOrNil:self.name forKey:@"name"];
    [dic setObjectOrNil:self.sign forKey:@"sign"];
    [dic setObjectOrNil:self.gender forKey:@"gender"];
    
    /*
     NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
     self.name, @"name",
     self.brief, @"brief",
     self.city, @"city",
     self.company, @"company",
     self.gender, @"gender",
     self.birthday, @"birthday",
     nil];
     */
    
    return [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
}


+(NSPredicate*)predicateWithSearchString:(NSString*)searchString{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(mobile BEGINSWITH[cd] %@) OR (showName contains [cd] %@) OR (name contains [cd] %@) OR (searchKey contains [cd] %@)",searchString,searchString,searchString, searchString];
    
    return predicate;
    
}

-(BOOL)hasNickname{
    
    return (_name && ![_name isEqualToString:@""]);
}

-(NSString*)name{
    
    if (_name && ![_name isEqualToString:@""]) {
        return _name;
    }
    
    return _mobile;
    
    
}

-(NSString*)showName{
    if (_name) {
        return _name;
    }
    else
    {
        return _mobile;
    }
    
}




@end
