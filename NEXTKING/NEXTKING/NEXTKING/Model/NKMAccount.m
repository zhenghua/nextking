//
//  NKMAccount.m
//  FANSZ
//
//  Created by King on 10/12/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKMAccount.h"

NSString *const NKWillLogoutNotificationKey = @"NKWillLogoutNotificationKey";
NSString *const NKLoginFinishNotificationKey = @"NKLoginFinishNotificationKey";


@implementation NKMAccount


static NKMAccount *_reg = nil;

+(id)registerAccount{
    
    if (!_reg) {
        _reg = [[NKMAccount alloc] init];
    }
    
    return _reg;
}

+(id)cleanRegisterAccout{
    
    _reg = nil;
    return nil;
}

+(id)accountWithAccount:(NSString *)aloginName password:(NSString *)apassword andShouldAutoLogin:(NSNumber *)autoLogin{
    
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:aloginName, @"account", apassword, @"password", autoLogin, @"shouldAutoLogin", nil];
    
    return [NKMAccount accountFromLocalDic:dic];
    
    
}

+(id)accountFromLocalDic:(NSDictionary*)dic{
    
    NKMAccount *newAccount = [[self alloc] init];
    if (newAccount) {
        
        newAccount.jsonDic = dic;
        newAccount.modelID = [dic objectForKey:@"id"];
        newAccount.account = [dic objectForKey:@"account"];
        newAccount.password = [dic objectForKey:@"password"];
        newAccount.shouldAutoLogin = [dic objectForKey:@"shouldAutoLogin"];
        
        NKBindValueWithKeyForParameterFromDic(@"accessToken", newAccount.accessToken, dic);
        NKBindValueWithKeyForParameterFromDic(@"refreshToken", newAccount.refreshToken, dic);
    }
    
    return  newAccount;
    
}
-(NSDictionary*)persistentDic{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.modelID, @"id", self.account, @"account", self.password, @"password", self.shouldAutoLogin, @"shouldAutoLogin", nil];
    NKBindValueToKeyForParameterToDic(@"accessToken",self.accessToken,dic);
    NKBindValueToKeyForParameterToDic(@"refreshToken", self.refreshToken, dic);
    
    return dic;
}



@end
