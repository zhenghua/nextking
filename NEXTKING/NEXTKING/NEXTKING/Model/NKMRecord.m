//
//  NKMRecord.m
//  NEXTKING
//
//  Created by jinzhenghua on 28/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKMRecord.h"
#import "NSDate+YYAdd.h"


NSString *const NKAttachmentTypeMan = @"man";
NSString *const NKAttachmentTypePicture = @"image";

@implementation NKMAttachment

+(id)modelFromDic:(NSDictionary *)dic{
    
    NKMAttachment *attachment = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"type", attachment.type, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"title", attachment.title, dic);
    NKBindValueWithKeyForParameterFromDic(@"content", attachment.content, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"link", attachment.link, dic);
    NKBindValueWithKeyForParameterFromDic(@"play_link", attachment.playLink, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"picture", attachment.pictureURL, dic);
    NKBindValueWithKeyForParameterFromDic(@"thumbnail", attachment.thumbnailURL, dic);
    
    
    return attachment;
}

-(NSString*)uploadString{
    
    return [NSString stringWithFormat:@"%@|%@|%@|%@", self.type?self.type:@"image", self.title?self.title:@"", self.content?self.content:@"", self.pictureURL?self.pictureURL:@""];
}

-(void)calculate{
    NSArray *array = [self.content componentsSeparatedByString:@","];
    if ([array count] == 2 ) {
        _width = [array[0] floatValue];
        _height = [array[1] floatValue];
    }
    else{
        _width = 150;
        _height = 150;
    }
}

-(CGFloat)width{
    
    if (_width<=0) {
        [self calculate];
    }
    return _width;
}

-(CGFloat)height{
    
    if (_height<=0) {
        [self calculate];
    }
    return _height;
}

@end


NSString *const NKRecordTypeFeed = @"feed";
NSString *const NKRecordTypeGroup = @"group";
NSString *const NKRecordTypeComment = @"comment";
NSString *const NKRecordTypeLike = @"like";
NSString *const NKRecordTypeSharesComment = @"sc";
NSString *const NKRecordTypeUserVerify = @"verify";
NSString *const NKRecordTypeDeal = @"deal";

@implementation NKMRecord



+(id)modelFromDic:(NSDictionary*)dic{
    
    NKMRecord *record = [super modelFromDic:dic];
    
    record.sender = [NKMUser modelFromCache:[dic objectOrNilForKey:@"user"]];
    
    NKBindValueWithKeyForParameterFromDic(@"title", record.title, dic);
    NKBindValueWithKeyForParameterFromDic(@"content", record.content, dic);
    NKBindValueWithKeyForParameterFromDic(@"client", record.client, dic);
    NKBindValueWithKeyForParameterFromDic(@"type", record.type, dic);
    NKBindValueWithKeyForParameterFromDic(@"parent_id", record.parentID, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"status", record.status, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"comment_count", record.commentCount, dic);
    NKBindValueWithKeyForParameterFromDic(@"like_count", record.likeCount, dic);
    NKBindValueWithKeyForParameterFromDic(@"liked", record.liked, dic);
    NKBindValueWithKeyForParameterFromDic(@"create_time", record.createTime, dic);

    record.timeString = [NSDate stringWithTimelineDate:[NSDate dateWithTimeIntervalSince1970:[record.createTime longLongValue]]];
    
    // Attachments
    NSArray *tattach = [dic objectOrNilForKey:@"attachments"];
    if ([tattach count]) {
        NSMutableArray *attacht = [NSMutableArray arrayWithCapacity:[tattach count]];
        
        for (NSDictionary *tatt in tattach) {
            [attacht addObject:[NKMAttachment modelFromDic:tatt]];
        }
        record.attachments = attacht;
    }
    
    return record;
}

-(NSString*)timeString{
    
    if (!_timeString) {
        _timeString = [NSDate stringWithTimelineDate:[NSDate dateWithTimeIntervalSince1970:[self.createTime longLongValue]]];
    }
    
    return _timeString;
}

-(NSString*)commentText{

    return [NSString stringWithFormat:@"评论 %@", self.commentCount];
}

@end
