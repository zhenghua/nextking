//
//  NKMRecord.h
//  NEXTKING
//
//  Created by jinzhenghua on 28/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKModel.h"
#import "NKMUser.h"


extern NSString *const NKAttachmentTypeMan;
extern NSString *const NKAttachmentTypePicture;


@interface NKMAttachment : NKModel{
    
}

@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *playLink;

@property (nonatomic, strong) NSString *pictureURL;
@property (nonatomic, strong) NSString *thumbnailURL;

@property (nonatomic, strong) UIImage  *image;

@property (nonatomic, assign) CGFloat  width;
@property (nonatomic, assign) CGFloat  height;

@property (nonatomic) BOOL uploaded;

-(NSString*)uploadString;

@end




extern NSString *const NKRecordTypeFeed;
extern NSString *const NKRecordTypeGroup;
extern NSString *const NKRecordTypeComment;
extern NSString *const NKRecordTypeLike;
extern NSString *const NKRecordTypeSharesComment;
extern NSString *const NKRecordTypeUserVerify;
extern NSString *const NKRecordTypeDeal;

@interface NKMRecord : NKModel{

}

@property (nonatomic, strong) NKMUser  *sender;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *client;
@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *timeString;

@property (nonatomic, strong) NSNumber *status;

@property (nonatomic, strong) NSString *parentID;

@property (nonatomic, strong) NSMutableArray  *attachments;

@property (nonatomic, strong) NSNumber *commentCount;
@property (nonatomic, strong) NSNumber *likeCount;
@property (nonatomic, strong) NSNumber *liked;

-(NSString*)commentText;

@end