//
//  NKModel.m
//  NEXTKING
//
//  Created by King on 14/10/28.
//  Copyright (c) 2014年 King. All rights reserved.
//

#import "NKModel.h"
#import "NKMAccount.h"


@implementation NKModel

+(id)modelFromDic:(NSDictionary*)dic{
    
    NKModel *model = [[self alloc] init];
    model.jsonDic = dic;

    if ([dic objectOrNilForKey:@"id"]) {
        model.modelID = [NSString stringWithFormat:@"%@", [dic objectOrNilForKey:@"id"]];
    }
    return model;
}

-(NSDictionary*)cacheDic{
    return self.jsonDic;
}

+(id)modelFromCache:(NSDictionary*)cache{
    return [self modelFromDic:cache];
}

@end


@implementation NSDictionary (ForJsonNull)

- (id)objectOrNilForKey:(id)key {
    id object = [self objectForKey:key];
    if(object == [NSNull null]) {
        return nil;
    }
    return object;
}

@end

@implementation NSMutableDictionary (SetNilForKey)

- (void)setObjectOrNil:(id)object forKey:(id)key {
    if(!object) {
        [self removeObjectForKey:key];
        return;
    }
    [self setObject:object forKey:key];
}

@end


@implementation NKMemoryCache


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NKWillLogoutNotificationKey object:nil];
    
}

static NKMemoryCache *_sharedMemoryCache = nil;

+(id)sharedMemoryCache{
    
    if (!_sharedMemoryCache) {
        @synchronized(self){
            _sharedMemoryCache = [[self alloc] init];
        }
    }
    
    return _sharedMemoryCache;
}

-(void)logout:(NSNotification*)noti{
    
    [self.cachedUsers removeAllObjects];
    [NKMUser meFromUser:nil];
    
}

-(id)init{
    self = [super init];
    if (self) {
        self.cachedUsers = [NSMutableDictionary dictionary];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:NKWillLogoutNotificationKey object:nil];
        
    }
    return self;
}

@end


@implementation NKMActionResult

+(id)modelFromDic:(NSDictionary*)dic{
    
    
    NKMActionResult *result = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"unique", result.unique, dic);
    
    return result;
}

@end
