//
//  NKMUser.h
//  FANSZ
//
//  Created by King on 10/12/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKModel.h"

typedef enum{
    NKMUserCreateTypeFromWeb   =  5,
    NKMUserCreateTypeFromContact  =  0
}NKMUserCreateType;

typedef enum{
    NKMUserRelationFollow   =  1,
    NKMUserRelationFriend   =  2,
    NKMUserRelationFollower =  3,
    NKMUserRelationMySelf   =  4,
    NKMUserRelationUnknow   =  5,
    NKMUserRelationSpecialFriend = 6
}NKMUserRelation;

extern NSString *const NKRelationFriend;
extern NSString *const NKRelationFollow;
extern NSString *const NKRelationFollower;
extern NSString *const NKRelationMySelf;
extern NSString *const NKRelationUnknow;

extern NSString *const NKGenderKeyMale;
extern NSString *const NKGenderKeyFemale;
extern NSString *const NKGenderKeyUnknown;


@interface NKMWallet : NKModel

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSNumber *money;
@property (nonatomic, strong) NSNumber *orderMoney;
@property (nonatomic, strong) NSNumber *sharesMoney;
@property (nonatomic, strong) NSNumber *status;

@end






@interface NKMUser : NKModel

@property (nonatomic, assign) NKMUserCreateType createType;

@property (nonatomic, strong) NSString *showName;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *searchKey;

@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSNumber *birthday;
@property (nonatomic, strong) NSString *constellation;
@property (nonatomic, strong) NSString *sign;
@property (nonatomic, strong) NSString *city;


@property (nonatomic, strong) NSString *avatarPath;

@property (nonatomic, strong) UIImage  *avatar;
@property (nonatomic, strong) NSNumber *relation;

@property (nonatomic, strong) NKMWallet *wallet;
@property (nonatomic, strong) NSNumber  *verify;
@property (nonatomic, strong) NSNumber  *status;

@property (nonatomic, strong) NSNumber  *onlineTime;
@property (nonatomic, strong) NSString  *timeString;


+(id)userFromDic:(NSDictionary*)dic;

+(id)user;
+(id)userWithName:(NSString*)tname phoneNumber:(NSString*)tphoneNumber andAvatar:(UIImage*)tavatar;

+(id)meFromUser:(NKMUser*)user;
+(instancetype)me;

-(NSData*)profileUpdateString;

+(NSPredicate*)predicateWithSearchString:(NSString*)searchString;

//判断是否有设置过nickname
-(BOOL)hasNickname;

@end

typedef enum{
    NKMSharesPriceStautsNormal   =  0,
    NKMSharesPriceStautsRise,
    NKMSharesPriceStautsFall
}NKMSharesPriceStauts;

@interface NKMShares : NKModel

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *totalAmount;
@property (nonatomic, strong) NSNumber *dealAmount;
@property (nonatomic, strong) NSNumber *openPrice;
@property (nonatomic, strong) NSNumber *closePrice;
@property (nonatomic, strong) NSNumber *status;

@property (nonatomic, strong) NSNumber *launchPrice;
@property (nonatomic, strong) NSNumber *launchTime;

@property (nonatomic, strong) NSNumber *value;

@property (nonatomic, strong) NKMUser  *user;
@property (nonatomic, strong) NSNumber *following;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;

-(NSString*)sharesID;

-(NKMSharesPriceStauts)priceStatus;
-(UIColor*)priceColor;
-(UIImage*)priceImage;
-(NSString*)priceText;
-(NSString*)rangeText;
-(NSString*)percentText;

-(NSString*)marketValueText;


+(UIColor*)riseColor;
+(UIColor*)fallColor;
+(UIColor*)normalColor;

@end

typedef enum{
    NKMOrderStatusCreate   =  0,
    NKMOrderStatusFinish,
    NKMOrderStatusCancel
}NKMOrderStatus;

@interface NKMOrder : NKModel

@property (nonatomic, strong) NSNumber *buyerID;
@property (nonatomic, strong) NSNumber *sellerID;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *dealAmount;
@property (nonatomic, strong) NSNumber *dealPrice;
@property (nonatomic, strong) NSNumber *status;

@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSNumber *dealTime;
@property (nonatomic, strong) NSNumber *cancelTime;

@property (nonatomic, strong) NKMShares *shares;

@property (nonatomic, strong) NKMUser   *buyer;
@property (nonatomic, strong) NKMUser   *seller;

-(NSString*)statusText;
-(NSString*)actionText;
-(BOOL)isBuy;

-(NSString*)createTimeText;
-(NSString*)dealTimeText;
-(NSString*)cancelTimeText;

@end

@interface NKMShareholder : NKModel

@property (nonatomic, strong) NSString *sharesID;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NKMUser  *user;
@property (nonatomic, strong) NKMShares *shares;

@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *sellingAmount;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSNumber *createTime;

@end

typedef NS_ENUM(NSInteger, NKFlowType) {
    NKFlowTypeTrade = 1, //股票交易
    NKFlowTypeCharge = 2, //充值
    NKFlowTypeWithdraw = 3, //提现
    NKFlowTypeOther
    
};


@interface NKMFlow : NKModel

@property (nonatomic, strong) NSString *sharesID;
@property (nonatomic, strong) NSString *otherID;
@property (nonatomic, strong) NSString *fromID;
@property (nonatomic, strong) NSString *toID;
@property (nonatomic, strong) NKMUser  *user;
@property (nonatomic, strong) NKMShares *shares;

@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *money;
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSNumber *time;

-(NSString*)typeText;
-(NSString*)timeText;
-(NSString*)moneyText;

@end

@interface NKMNotification : NKModel;

@property (nonatomic, strong) NSNumber *feedTime;
@property (nonatomic, strong) NSNumber *noticeTime;
@property (nonatomic, strong) NSNumber *sharesStatus;
@property (nonatomic, strong) NSNumber *interval;

@property (nonatomic, strong) NSNumber *needFeedAlert;
@property (nonatomic, strong) NSNumber *needNoticeAlert;

@end


@class NKMRecord;
@interface NKMNotice : NKModel

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSNumber *time;

@property (nonatomic, strong) NSString  *cotent;
@property (nonatomic, strong) NKMRecord *record;
@property (nonatomic, strong) id parent;
@property (nonatomic, strong) NKMOrder *order;

@end


