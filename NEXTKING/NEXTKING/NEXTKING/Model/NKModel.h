//
//  NKModel.h
//  NEXTKING
//
//  Created by King on 14/10/28.
//  Copyright (c) 2014年 King. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define NKBindValueForParameter(value, param) \
if(value!=nil) {\
param = value;\
}\


#define NKBindValueWithKeyForParameterFromDic(key, param, dic) \
if([dic objectOrNilForKey:key]!=nil) {\
param = [dic objectOrNilForKey:key];\
}\

#define NKBindValueWithKeyForParameterFromCache(key, param, dic) \
if(!param) {\
param = [dic objectOrNilForKey:key];\
}\


#define NKBindValueToKeyForParameterToDic(key, param, dic) \
if(param != nil) {\
[dic setObject:param forKey:key];\
}\



@interface NKModel : NSObject{
    
}

@property (nonatomic, strong) NSDictionary *jsonDic;
@property (nonatomic, strong) NSString     *modelID;
@property (nonatomic, assign) CGFloat       cellHeight;

+(id)modelFromDic:(NSDictionary*)dic;
-(NSDictionary*)cacheDic;
+(id)modelFromCache:(NSDictionary*)cache;

@end

@interface NKMemoryCache : NSObject {
    
}
@property (nonatomic, strong) NSMutableDictionary *cachedUsers;

+(id)sharedMemoryCache;


@end


@interface NSDictionary (ForJsonNull)

- (id)objectOrNilForKey:(id)key;

@end

@interface NSMutableDictionary (SetNilForKey)

- (void)setObjectOrNil:(id)object forKey:(id)key;

@end


@interface NKMActionResult : NKModel

@property (nonatomic, strong) NSNumber *unique;

@end


