//
//  NKMAccount.h
//  FANSZ
//
//  Created by King on 10/12/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKMUser.h"

extern NSString *const NKWillLogoutNotificationKey;
extern NSString *const NKLoginFinishNotificationKey;

@interface NKMAccount : NKMUser{
    
}


@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, strong) NSNumber *shouldSavePassword;
@property (nonatomic, strong) NSNumber *shouldAutoLogin;

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *refreshToken;

@property (nonatomic, strong) NSNumber *registerState;
@property (nonatomic, strong) NSString *verifyCode;
@property (nonatomic, strong) NSDate   *codeSendDate;


+(id)accountFromLocalDic:(NSDictionary*)dic;
+(id)accountWithAccount:(NSString*)aloginName password:(NSString*)apassword andShouldAutoLogin:(NSNumber*)autoLogin;

-(NSDictionary*)persistentDic;


+(id)registerAccount;

@end
