//
//  NKDataStore.m
//  NEXTKING
//
//  Created by jinzhenghua on 16/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKDataStore.h"
#import "NKModel.h"
#import "NKAccountManager.h"
#import "NKConfig.h"

NSString *const NKCachePathProfile = @"NKCachePathProfile.NK";

@implementation NKDataStore

+(id)sharedDataStore{
    
    static NKDataStore *_instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[NKDataStore alloc] init];
    });
    
    return _instance;
    
}


#pragma mark Methods

-(NSString*)rootPath{
    
    return [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

-(NSString*)accountsPath{
    
    return [[self rootPath] stringByAppendingPathComponent:@"NKAccountsPath.NK"];
}

-(NSString*)pathForUserID:(NSString*)userID{
    
    if (!userID) {
        return nil;
    }
    
    [[NSFileManager defaultManager] createDirectoryAtPath:[[self rootPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", userID]] withIntermediateDirectories:YES attributes:nil error:nil];
    return[[self rootPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", userID]];
}

-(NSString*)cachedPathOf:(NSString*)cacheKey forUserID:(NSString*)userID;{
    
    if (!userID) {
        userID = @"NKUserID";
    }
    
    return [[self pathForUserID:userID] stringByAppendingPathComponent:cacheKey];
    
}
-(NSString*)cachedPathOf:(NSString*)cacheKey{
    
    // Need add config here
    return [self cachedPathOf:cacheKey forUserID:[[[[NKConfig sharedConfig] accountManagerClass] sharedAccountManager] currentUserID]];
}


-(NSMutableArray*)cachedArray:(NSArray*)array withClass:(Class)cc{
    
    if ([array count]) {
        NSMutableArray *cachedArray = [NSMutableArray arrayWithCapacity:[array count]];
        for (id object in array) {
            
            if ([object isKindOfClass:[NSArray class]]) {
                NSMutableArray *newArray = [self cachedArray:object withClass:cc];
                if (newArray) {
                    [cachedArray addObject:newArray];
                }
            }
            else{
                id newModel = [cc modelFromCache:object];
                if (newModel) {
                    [cachedArray addObject:newModel];
                }
                
            }
            
        }
        return cachedArray;
    }
    
    return [NSMutableArray array];
    
}


-(NSMutableArray *)cachedArrayOf:(NSString*)cacheKey andClass:(Class)cc{
    
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:[self cachedPathOf:cacheKey]];
    if (!data) {
        return nil;
    }
    
    NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    if (error) {
        return nil;
    }
    
    NSMutableArray *cachedArray = [self cachedArray:array withClass:cc];
    
    if ([cachedArray count]) {
        return cachedArray;
    }
    return nil;
    
}

-(id)cachedObjectOf:(NSString*)cacheKey andClass:(Class)cc{
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[self cachedPathOf:cacheKey]];
    
    if (dic) {
        if (cc && (cc != [NSDictionary class] || cc !=[NSMutableDictionary class])) {
            return [cc modelFromCache:dic];
        }
        else
            return dic;
        
    }
    return nil;
}

-(NSMutableArray*)arrayToCache:(NSArray*)array{
    
    NSMutableArray *arrayToCache = [NSMutableArray arrayWithCapacity:[array count]];
    
    for (id object in array) {
        
        if ([object isKindOfClass:[NSArray class]]) {
            [arrayToCache addObject:[self arrayToCache:object]];
        }
        else{
            [arrayToCache addObject:[object cacheDic]];
        }
        
    }
    
    return arrayToCache;
}

-(BOOL)cacheArray:(NSArray*)array forCacheKey:(NSString*)cacheKey{
    
    if (!array /*|| [array count]<=0*/) {
        return NO;
    }
    
    NSMutableArray *arrayToCache = [self arrayToCache:array];
    NSError *error = nil;
    BOOL suc = [[NSJSONSerialization dataWithJSONObject:arrayToCache options:NSJSONWritingPrettyPrinted error:&error] writeToFile:[self cachedPathOf:cacheKey] options:NSDataWritingAtomic error:&error];
    
    return suc;
}

-(BOOL)cacheObject:(id)object forCacheKey:(NSString*)cacheKey{
    
    
    if (!object) {
        return [[NSFileManager defaultManager] removeItemAtPath:[self cachedPathOf:cacheKey] error:nil];
    }
    else if ([object respondsToSelector:@selector(cacheDic)]) {
        return [[object cacheDic] writeToFile:[self cachedPathOf:cacheKey] atomically:YES];
    }
    else
        return [object writeToFile:[self cachedPathOf:cacheKey] atomically:YES];
    
}



@end