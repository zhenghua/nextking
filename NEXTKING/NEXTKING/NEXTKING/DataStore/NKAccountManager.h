//
//  NKAccountManager.h
//  NEXTKING
//
//  Created by King on 14/10/28.
//  Copyright (c) 2014年 King. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NKRequestDelegate.h"
#import "NKMAccount.h"

@interface NKAccountManager : NSObject

@property (nonatomic, strong) NSString *currentUserID;
@property (nonatomic, strong) NKMAccount *currentAccount;
@property (nonatomic, strong) NSMutableArray *allAccounts;

+(id)sharedAccountManager;
+(BOOL)isKing;

// Login and Logout
-(BOOL)canAutoLogin;
-(void)loginWithAccount:(id)account andRequestDelegate:(NKRequestDelegate*)rd;
-(BOOL)autoLogin;
-(void)logOut;

-(void)saveAccountsFile;
-(void)loginWithAccount:(NKMAccount*)account;


@end
