//
//  NKAccountManager.m
//  NEXTKING
//
//  Created by King on 14/10/28.
//  Copyright (c) 2014年 King. All rights reserved.
//

#import "NKAccountManager.h"
#import "NKRequest.h"
#import "NKDataStore.h"
#import "NKAccountService.h"
#import "NKConfig.h"



@implementation NKAccountManager

+(id)sharedAccountManager{
    
    static NKAccountManager *_instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[NKAccountManager alloc] init];
    });
    
    return _instance;
}

+(BOOL)isKing{
    
    return [[[[NKAccountManager sharedAccountManager] currentAccount] account] isEqualToString:@"18602195219"];
}

-(id)init{
    self = [super init];
    if (self) {
        
        self.allAccounts = [NSMutableArray arrayWithContentsOfFile:[[NKDataStore sharedDataStore] accountsPath]];
        if ([self.allAccounts count]>0) {
            NSDictionary *firstDic = [self.allAccounts objectAtIndex:0];
            self.currentAccount = [NKMAccount accountFromLocalDic:firstDic];
            
        }
        else{
            self.allAccounts = [NSMutableArray array];
        }
    }
    return self;
}


-(void)saveAccountsFile{
    
    if (!self.currentAccount) {
        return;
    }
    
    NSDictionary *dic = [self.currentAccount persistentDic];
    
    NKMAccount *tempA = nil;
    for (NSDictionary *alreadyhave in self.allAccounts) {
        
        tempA = [NKMAccount accountFromLocalDic:alreadyhave];
        if ([tempA.modelID isEqualToString:self.currentAccount.modelID]) {
            [self.allAccounts removeObject:alreadyhave];
            break;
        }
    }
    [self.allAccounts insertObject:dic atIndex:0];
    [self.allAccounts writeToFile:[[NKDataStore sharedDataStore] accountsPath] atomically:YES];
}



#pragma mark Login and Logout
-(BOOL)canAutoLogin{
    //return YES;
    return [self.currentAccount.shouldAutoLogin boolValue]&&self.currentAccount.password;
}
-(void)logOut{
    // Will set the current account to nil if we do not want to show the email and password again
    
    [[NKAccountService sharedNKAccountService] logoutWithRequestDelegate:nil];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NKWillLogoutNotificationKey object:nil];
    
    self.currentAccount.shouldAutoLogin = [NSNumber numberWithBool:NO];
    self.currentAccount.password = @"";
    [self saveAccountsFile];
    
}

-(BOOL)autoLogin{
    
    // Should AutoLogin?
    if ([self canAutoLogin]) {
        [self loginWithAccount:self.currentAccount];
        return YES;
    }
    
    return NO;
}

-(void)loginFinish:(NKRequest*)request{
    
    NKMUser *me = [NKMUser meFromUser:[request.results lastObject]];
    
    self.currentAccount.modelID = me.modelID;
    [self saveAccountsFile];
    [[NKDataStore sharedDataStore] cacheObject:me forCacheKey:NKCachePathProfile];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NKLoginFinishNotificationKey object:nil];
    
    if ([[NKConfig sharedConfig] needAPN]) {

        UIUserNotificationSettings *settings = [UIUserNotificationSettings
                                                settingsForTypes:(UIUserNotificationTypeBadge |
                                                                  UIUserNotificationTypeSound |
                                                                  UIUserNotificationTypeAlert)
                                                categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    }
    
}

-(void)loginWithAccount:(id)account andRequestDelegate:(NKRequestDelegate*)rd{
    
    self.currentAccount = account;
    
    rd.finishInspector = self;
    rd.finishInspectorSelector = @selector(loginFinish:);
    
    [NKMUser meFromUser:[[NKDataStore sharedDataStore] cachedObjectOf:NKCachePathProfile andClass:[NKMUser class]]];
    
    [[NKAccountService sharedNKAccountService] loginWithUsername:self.currentAccount.account password:self.currentAccount.password andRequestDelegate:rd];
    
}

-(void)loginWithAccount:(NKMAccount*)account{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:nil finishSelector:nil andFailedSelector:nil];
    
    [self loginWithAccount:account andRequestDelegate:rd];
    
}

-(NSString*)currentUserID{
    
    return self.currentAccount.modelID;
}



@end
