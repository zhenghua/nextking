//
//  NKDataStore.h
//  NEXTKING
//
//  Created by jinzhenghua on 16/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const NKCachePathProfile;

@interface NKDataStore : NSObject{
    
    
}


+(id)sharedDataStore;

-(NSString*)rootPath;
-(NSString*)accountsPath;

-(NSString*)cachedPathOf:(NSString*)cacheKey forUserID:(NSString*)userID;
-(NSString*)cachedPathOf:(NSString*)cacheKey;

-(NSMutableArray *)cachedArrayOf:(NSString*)cacheKey andClass:(Class)cc;
-(id)cachedObjectOf:(NSString*)cacheKey andClass:(Class)cc;

-(BOOL)cacheArray:(NSArray*)array forCacheKey:(NSString*)cacheKey;
-(BOOL)cacheObject:(id)object forCacheKey:(NSString*)cacheKey;

@end