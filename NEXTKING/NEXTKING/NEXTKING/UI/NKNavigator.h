//
//  NKNavigator.h
//  FANSZ
//
//  Created by King on 10/10/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKSegmentControl.h"
#import "NKConfig.h"

#define NKNavigatorHeight 48

@interface NKNavigator : UIView <NKSegmentControlDelegate>{
    
}

@property (nonatomic, strong) NKSegmentControl *tabs;
@property (nonatomic, strong) NSArray *tabSource;

+(id)sharedNavigator;

-(void)showLoginOKView;

-(void)addTabs:(NSArray*)tab;

@end
