//
//  NKConfig.h
//  NEXTKING
//
//  Created by King on 10/8/15.
//  Copyright © 2015 King. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NKConfig : NSObject

+(instancetype)sharedConfig;

@property (nonatomic, strong) NSString *domainURL;
@property (nonatomic, strong) NSString *baseImageURL;
@property (nonatomic, strong) NSString *ImageUploadURL;

@property (nonatomic, weak)   id  errorTarget;
@property (nonatomic, assign) SEL errorMethod;

@property (nonatomic, assign) Class accountManagerClass;

@property (nonatomic) UIStatusBarStyle statusBarStyle;

@property (nonatomic, strong) UIColor *themeColorText;
@property (nonatomic, strong) UIColor *themeColorBackground;
@property (nonatomic, strong) UIColor *navigatorBackgroundColor;

@property (nonatomic, assign) BOOL needAPN;

@property (nonatomic, assign) NSInteger uiStyle;


-(NSString*)imageURLForImageID:(NSString*)imageID;

- (NSString *)imageURLForImageID:(NSString *)imageID imageSize:(CGSize)size;

@end
