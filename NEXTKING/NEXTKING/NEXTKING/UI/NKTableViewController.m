//
//  NKTableViewController.m
//  FANSZ
//
//  Created by King on 10/16/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKTableViewController ()

@end

@implementation NKTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    BOOL pushed = [self.navigationController.viewControllers indexOfObject:self]>=1;
    
    self.showTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, NKMainWidth, pushed?(NKMainHeight-64):(NKContentHeight-64)) style:[self tableViewStyle]];
    _showTableView.dataSource = self;
    _showTableView.delegate = self;
    [self.contentView insertSubview:_showTableView atIndex:0];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self modifyTableViewStyle];
    
}

-(NSMutableArray*)dataSource{
    
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //self.showTableView = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(UITableViewStyle)tableViewStyle{
    
    return UITableViewStylePlain;
}

-(void)modifyTableViewStyle{
    
    if ([self tableViewStyle] == UITableViewStyleGrouped) {
        self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 10)];
        self.showTableView.backgroundColor = [self normalBackgroundColor];
        self.showTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.showTableView.separatorColor = [UIColor lineColor];
        self.showTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 10)];
    }
    else{
        _showTableView.backgroundColor = [UIColor clearColor];
        _showTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
}


#pragma mark Data
-(void)getMoreData{
    
}

-(void)refreshData{
    
}

-(void)getMoreDataOK:(NKRequest*)request{
    
    if ([request.results count]>0) {
        [self.showTableView.footer endRefreshing];
        [self.dataSource addObjectsFromArray:request.results];
        [self.showTableView reloadData];
    }
    else{
        [self.showTableView.footer endRefreshingWithNoMoreData];
    }
}


-(void)refreshDataOK:(NKRequest*)request{
    [self.showTableView.header endRefreshing];
   
    self.dataSource = request.results;
    
    if ([self cachePathKey]) {
        [[NKDataStore sharedDataStore] cacheArray:self.dataSource forCacheKey:[self cachePathKey]];
    }
    [self.showTableView reloadData];
}

-(void)getMoreDataFailed:(NKRequest*)request{
    [self.showTableView.footer endRefreshing];
}
-(void)refreshDataFailed:(NKRequest*)request{
    [self.showTableView.header endRefreshing];
    
    
}

-(void)addRefreshHeader{
    
    self.showTableView.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
}

-(void)addGetMoreFooter{
    
    self.showTableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
}

-(NSString*)cachePathKey{
    return nil;
}


#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

-(Class)cellClass{
    return [NKTableViewCell class];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKCellIdentifier";
    
    NKTableViewCell *cell = (NKTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[self cellClass] alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    id object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[self cellClass] cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    if ([self respondsToSelector:object.action]) {
        SuppressPerformSelectorLeakWarning(
                                           [self performSelector:object.action withObject:object];
                                           );
    }
}

@end


@implementation NKRequestDelegate (RefreshAndGetMore)

+(id)refreshRequestDelegateWithTarget:(id)atarget{
    
    return [NKRequestDelegate requestDelegateWithTarget:atarget finishSelector:@selector(refreshDataOK:) andFailedSelector:@selector(refreshDataFailed:)];
    
}
+(id)getMoreRequestDelegateWithTarget:(id)atarget{
    return [NKRequestDelegate requestDelegateWithTarget:atarget finishSelector:@selector(getMoreDataOK:) andFailedSelector:@selector(getMoreDataFailed:)];
}

@end
