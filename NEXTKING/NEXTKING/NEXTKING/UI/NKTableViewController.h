//
//  NKTableViewController.h
//  FANSZ
//
//  Created by King on 10/16/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKViewController.h"
#import "MJRefresh.h"
#import "NKTableViewCell.h"

@interface NKTableViewController : NKViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *showTableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@property (nonatomic) NSInteger currentPage;

-(UITableViewStyle)tableViewStyle;
-(void)modifyTableViewStyle;
-(void)getMoreData;
-(void)getMoreDataOK:(NKRequest*)request;

-(void)refreshData;
-(void)refreshDataOK:(NKRequest*)request;

-(void)getMoreDataFailed:(NKRequest*)request;
-(void)refreshDataFailed:(NKRequest*)request;

-(void)addRefreshHeader;
-(void)addGetMoreFooter;

-(Class)cellClass;
-(NSString*)cachePathKey;

@end


@interface NKRequestDelegate (RefreshAndGetMore)

+(id)refreshRequestDelegateWithTarget:(id)atarget;
+(id)getMoreRequestDelegateWithTarget:(id)atarget;

@end