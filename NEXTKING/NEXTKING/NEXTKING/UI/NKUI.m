//
//  NKUI.m
//  FANZ
//
//  Created by King on 10/9/15.
//  Copyright © 2015 FANZ. All rights reserved.
//

#import "NKUI.h"
#import "NKModelDefine.h"
#import "NKConfig.h"
#import "NKAccountService.h"
#import "NKAccountManager.h"

@implementation NKUI

static NKUI * _NKUI = nil;

+(instancetype)sharedNKUI{
    
    if (!_NKUI) {
        _NKUI = [[NKUI alloc] init];
    }
    
    return _NKUI;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nkBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, NKMainHeight)];
    self.nkBackgroundView.userInteractionEnabled = YES;
    [self.view addSubview:self.nkBackgroundView];
    
    self.navigator = [NKNavigator sharedNavigator];
    [self.view addSubview:self.navigator];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    
    return [[NKConfig sharedConfig] statusBarStyle];
}


#pragma mark NaviTabAnimate

-(CGFloat)appHeight{
    
    if (!_appHeight) {
        _appHeight = UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])?[[UIScreen mainScreen] bounds].size.height:[[UIScreen mainScreen] bounds].size.width;
    }
    return _appHeight;
}
-(CGFloat)appWidth{
    if (!_appWidth) {
        _appWidth = UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])?[[UIScreen mainScreen] bounds].size.width:[[UIScreen mainScreen] bounds].size.height;
    }
    return _appWidth;
}

-(void)showNaviTab{
    
    if (self.navigatorOnTop) {
        self.navigator.frame = CGRectMake(0, 20, NKMainWidth, NKNavigatorHeight);
    }
    else{
       self.navigator.frame = CGRectMake(0, NKMainHeight-NKNavigatorHeight, NKMainWidth, NKNavigatorHeight);
    }
    
    
}

-(void)hideNaviTab{
    
    //[UIView animateWithDuration:0.3 animations:^{
        self.navigator.frame = CGRectMake(0, NKMainHeight, NKMainWidth, NKNavigatorHeight);
    //}];
}

-(void)addTabs:(NSArray*)tab{
    [self view];
    
    
    [self.navigator addTabs:tab];
    
    
    if (self.navigatorOnTop) {
        self.navigator.backgroundColor = [UIColor clearColor];
        
        UIView *scroller = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth/self.navigator.tabs.segments.count, 2)];
        scroller.backgroundColor = [[NKConfig sharedConfig] themeColorText];
        
        [self.navigator.tabs addScroller:scroller];
        
        
    }
    [self showDefaultViewController];
    
}


#pragma mark Show

-(void)showWelcome{
    [self showViewControllerWithClass:_welcomeClass];
    [self hideNaviTab];
}
-(void)showHome{
    
    [NKNC popToRootViewControllerAnimated:NO];
    
    [[[self navigator] tabs] selectSegment:0 shouldTellDelegate:YES];
    [self showNaviTab];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NKLoginFinishNotificationKey object:nil];
    
}
-(void)showDefaultViewController{
    
    // Wit Account System
    if (_needLogin) {
        
        if ([NKMUser me]) {
            
        }
        else {
            if (![[[[NKConfig sharedConfig] accountManagerClass] sharedAccountManager] canAutoLogin]) {
                //[self showLoginView];
            }
            else {
                [[[[NKConfig sharedConfig] accountManagerClass] sharedAccountManager] autoLogin];
            }
        }
        
        if ([[[[NKConfig sharedConfig] accountManagerClass] sharedAccountManager] canAutoLogin]) {
            [self showViewControllerWithClass:_homeClass andIndex:0];
            [self showNaviTab];
        }
        else {
            [self showWelcome];
        }
    }
    
    else{
        [self showViewControllerWithClass:_homeClass andIndex:0];
        [self showNaviTab];
    }
    
}

// Show ViewController
-(UIViewController*)showViewControllerWithClass:(Class)ControllerToShow{
    
    return [self showViewControllerWithClass:ControllerToShow andIndex:-1];
}

-(UIViewController*)showViewControllerWithClass:(Class)ControllerToShow andIndex:(NSInteger)index{
    
    if ([NSStringFromClass([_currentController class]) isEqualToString:NSStringFromClass(ControllerToShow)]) {
        return _currentController;
    }
    
    [_currentController dismissViewControllerAnimated:NO completion:nil];
    
    UIViewController *newController = nil;
    
    if (self.needStoreViewControllers&&index>=0) {
        
        if (!self.allViewControllers) {
            self.allViewControllers = [NSMutableArray arrayWithCapacity:[[self.navigator.tabSource objectAtIndex:0] count]];
            for (int i=0; i<[[self.navigator.tabSource objectAtIndex:0] count]; i++) {
                [self.allViewControllers addObject:[NSNull null]];
            }
        }
        
        if ([self.allViewControllers objectAtIndex:index] == [NSNull null]) {
            newController = [[ControllerToShow alloc] init];
            [self.allViewControllers replaceObjectAtIndex:index withObject:newController];
        }
        
        newController = [self.allViewControllers objectAtIndex:index];
        
    }
    else{
        newController = [[ControllerToShow alloc] init];
    }
    
    [_currentController.view removeFromSuperview];
    [_nkBackgroundView addSubview:newController.view];
    
    self.currentController = newController;
    
    return newController;

}


-(void)reloadViewControllers{
    
    for (int i=0; i<self.allViewControllers.count; i++) {
        
        if ([self.allViewControllers objectAtIndex:i] != self.currentController) {
            [self.allViewControllers replaceObjectAtIndex:i withObject:[NSNull null]];
        }
    }
    
}

-(void)removeAllViewControllers{
    
    [NKNC popToRootViewControllerAnimated:NO];
    [self.allViewControllers removeAllObjects];
    [self.currentController.view removeFromSuperview];
    self.allViewControllers = nil;
    self.currentController = nil;
    
}

-(void)logout{
    
    [[NKAccountManager sharedAccountManager] logOut];
    [[NKUI sharedNKUI] removeAllViewControllers];
    [[NKUI sharedNKUI] showWelcome];
    
}


@end
