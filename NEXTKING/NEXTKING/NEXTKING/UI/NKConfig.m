//
//  NKConfig.m
//  NEXTKING
//
//  Created by King on 10/8/15.
//  Copyright © 2015 King. All rights reserved.
//

#import "NKConfig.h"
#import "NKAccountManager.h"
#import "YTKNetworkConfig.h"
#import "UIColor+HexString.h"

@implementation NKConfig


+ (instancetype)sharedConfig
{
    static NKConfig *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        
    });
    return instance;
}

-(id)init{
    
    self = [super init];
    if (self) {
        self.domainURL = @"http://zhenghua.sinaapp.com/index.php";
        self.baseImageURL = @"http://192.168.88.6:8080";
        self.ImageUploadURL = @"http://192.168.88.6:2000";
        self.accountManagerClass = [NKAccountManager class];
        
        self.needAPN = YES;
        
        self.themeColorBackground = [UIColor whiteColor];
        self.themeColorText = [UIColor colorWithHexString:@"#00b2ae"];
        
        self.uiStyle = [[NSUserDefaults standardUserDefaults] integerForKey:@"NKUIStyleKey"];
    }
    return self;
}



-(NSString*)imageURLForImageID:(NSString*)imageID{
   
    return [self imageURLForImageID:imageID imageSize:CGSizeMake(0, 0)];
}

- (NSString *)imageURLForImageID:(NSString *)imageID imageSize:(CGSize)size
{

    if (size.height == 0 && size.width == 0) {
        return [NSString stringWithFormat:@"%@/%@", self.baseImageURL, imageID];
    }
    
    CGFloat width = size.width * 2;
    CGFloat height = size.height * 2;
    
    if (width<=80) {
        width = 80;
        height = 80;
    }
    else if (width <= 100) {
        width = 100;
        height = 100;
    }
    else if (width <= 140) {
        width = 140;
        height = 140;
    }
    else  {
        width = 400;
        height = 400;
    }
    
    
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", self.baseImageURL, imageID]];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@_%lux%lu.%@", self.baseImageURL, imageID, (unsigned long)width, (unsigned long)height, imageUrl.pathExtension];
    return url;
}

@end
