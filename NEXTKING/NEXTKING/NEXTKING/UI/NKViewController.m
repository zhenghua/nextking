//
//  NKViewController.m
//  FANZ
//
//  Created by King on 10/8/15.
//  Copyright © 2015 FANZ. All rights reserved.
//

#import "NKViewController.h"
#import "NKRequestDelegate.h"

@implementation NKViewController

-(void)dealloc{
    
    [NKRequestDelegate removeTarget:self];
    
    NSLog(@"%@ -----------Dealloced", NSStringFromClass([self class]));
    
}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)rightButtonClick:(id)sender
{
    
}

-(IBAction)leftButtonClick:(id)sender{
    
}

-(void)nkLongPressed:(UILongPressGestureRecognizer*)gesture{
    
    [self popToHome:nil];
}

-(void)popToHome:(id)sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(UIButton*)addBackButton{
    
    UIButton *button = [self styleButton];
    
    button.frame = CGRectMake(0, 0, 54, 44);
    [button setImage:[UIImage imageNamed:@"navi_back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(nkLongPressed:)];
    [button addGestureRecognizer:longPress];
    
    
    return button;
    
}

-(UIButton*)addLeftCancelButton{
    return [self addleftButtonWithTitle:[NSArray arrayWithObjects:[UIImage imageNamed:@"xbutton.png"], [UIImage imageNamed:@"xbutton.png"], nil]];
    
}

-(UIButton*)addleftButtonWithTitle:(id)title{
    
    UIButton *button = [self styleButton];
    [button addTarget:self action:@selector(leftButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    if ([title isKindOfClass:[NSString class]]) {
        [button setTitle:title forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, 60, 43);
    }
    else if ([title isKindOfClass:[NSArray class]]){
        [button setImage:[title objectAtIndex:0] forState:UIControlStateNormal];
        [button setImage:[title objectAtIndex:1] forState:UIControlStateHighlighted];
        button.frame = CGRectMake(0, 0, 55, 43);
    }
    return button;
}

-(UIButton*)styleButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 60, 44);
    //button.tintColor = [UIColor lightGrayColor];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [button setTitleColor:self.themeColorText forState:UIControlStateNormal];
    [self.headBar addSubview:button];
    return button;
    
}

-(UIButton*)addRightButtonWithTitle:(id)title{
    
    UIButton *button = [self styleButton];
    [button addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    if ([title isKindOfClass:[NSString class]]) {
        [button setTitle:title forState:UIControlStateNormal];
        button.frame = CGRectMake(NKMainWidth-60, 0, 60, 43);
    }
    else if ([title isKindOfClass:[NSArray class]]){
        [button setImage:[title objectAtIndex:0] forState:UIControlStateNormal];
        if ([title count]>1) {
            [button setImage:[title objectAtIndex:1] forState:UIControlStateHighlighted];
        }
        button.frame = CGRectMake(NKMainWidth-55, 0, 55, 43);
    }
    else if ([title isKindOfClass:[UIImage class]]){
        [button setImage:title forState:UIControlStateNormal];
        button.frame = CGRectMake(NKMainWidth-55, 0, 55, 43);
    }
    
    self.nkRightButton = button;
    return button;
    
}


-(void)modifyHeaderColor{
    
    self.headBar.backgroundColor = self.themeColorBackground;
    UIView *upper = [[UIView alloc] initWithFrame:CGRectMake(0, -20, self.headBar.frame.size.width, 20)];
    [self.headBar insertSubview:upper atIndex:0];
    upper.backgroundColor = self.headBar.backgroundColor;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.headBar.frame.size.height, self.headBar.frame.size.width, 0.5)];
    [self.headBar insertSubview:line atIndex:0];
    line.backgroundColor = [UIColor lineColor];
    
    
    _titleLabel.textColor = self.themeColorText;
    _titleLabel.font = [UIFont systemFontOfSize:18];
    _titleLabel.shadowColor = [UIColor clearColor];
    _titleLabel.shadowOffset = CGSizeMake(0, -1);
    
}

-(UIColor*)normalBackgroundColor{
    
    return [UIColor colorWithWhite:0.973 alpha:1.000];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.themeColorBackground = [[NKConfig sharedConfig] themeColorBackground];
        self.themeColorText = [[NKConfig sharedConfig] themeColorText];
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    
    return [[NKConfig sharedConfig] statusBarStyle];
}

-(void)modifySubviewsFromXib{
    
    NSArray *subviews = [self.view subviews];
    for (UIView *view in subviews) {
        if (view.frame.origin.y<=44 && view.frame.origin.y+view.frame.size.height<=self.headBar.bounds.size.height) {
            [self.headBar addSubview:view];
        }
        else {
            [self.contentView addSubview:view];
        }
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, NKMainHeight);
    
    UIView *tempView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.contentView = tempView;
    [self.view addSubview:_contentView];
    
    self.headBar = [[UIView alloc] initWithFrame:CGRectMake(0, 20, NKMainWidth, 44)];
    [self.view addSubview:_headBar];
    _headBar.backgroundColor = [UIColor clearColor];
    
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth-120, 44)];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    
    _titleLabel.font = [UIFont boldSystemFontOfSize:20];
    
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.center = CGPointMake(NKMainWidth/2, 22);
    _titleLabel.shadowColor = [UIColor whiteColor];
    _titleLabel.shadowOffset = CGSizeMake(0, 1);
    [_headBar addSubview:_titleLabel];
    
    [self modifyHeaderColor];

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }

    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    return YES;
}

- (void)viewDidUnload

{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)showAlertWithMessage:(NSString*)message{
    
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)requestFailed:(NKRequest*)request{
    [self showAlertWithMessage:request.errorDescription];

}

-(NKRequestDelegate*)requestDelegateWithSuccess:(SEL)selector{
    
    return [NKRequestDelegate requestDelegateWithTarget:self finishSelector:selector andFailedSelector:@selector(requestFailed:)];
    
}

@end

