//
//  NKNavigationController.m
//  FANSZ
//
//  Created by King on 10/10/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKNavigationController.h"
#import "NKViewController.h"

@implementation NKNavigationController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.delegate = self;
    
}

-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    NSLog(@"%@ --------ViewContorllerShowed", NSStringFromClass([viewController class]));
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)] && navigationController.viewControllers.count>1) {
        navigationController.interactivePopGestureRecognizer.delegate = (NKViewController*)viewController;
    }
}


@end
