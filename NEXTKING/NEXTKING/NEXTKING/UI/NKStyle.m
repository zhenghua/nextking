//
//  NKStyle.m
//  NEXTKING
//
//  Created by King on 16/9/8.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKStyle.h"
#import "NKConfig.h"
#import "NKUI.h"

@implementation NKStyle

+(NKUIStyle)uiStyle{
    
    return [[NKConfig sharedConfig] uiStyle];
}

+(void)updateStyleWithStyle:(NKUIStyle)style{
    
    [[NKConfig sharedConfig] setUiStyle:style];
    [[NSUserDefaults standardUserDefaults] setInteger:style forKey:@"NKUIStyleKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NKUI sharedNKUI] reloadViewControllers];
    
    
}

@end
