//
//  NKNavigationController.h
//  FANSZ
//
//  Created by King on 10/10/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NKNavigationController : UINavigationController <UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end
