//
//  NKNavigator.m
//  FANSZ
//
//  Created by King on 10/10/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKNavigator.h"

#import "NKUI.h"



@implementation NKNavigator



static NKNavigator *_navigator = nil;

+(id)sharedNavigator{
    
    if (!_navigator) {
        
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        
        _navigator = [[NKNavigator alloc] initWithFrame:CGRectMake(0, screenBounds.size.height-NKNavigatorHeight, NKMainWidth, NKNavigatorHeight)];
        
    }
    return _navigator;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [[NKConfig sharedConfig] navigatorBackgroundColor];
        
        
    }
    return self;
}

-(void)addTabs:(NSArray*)tab{
    
    
    
    self.tabSource = tab;
    
    self.tabs = [NKSegmentControl segmentControlViewWithSegments:[tab objectAtIndex:0]
                                                     andDelegate:self];
    [self addSubview:self.tabs];
    //self.tabs.noBackgroundImage = YES;
    
    self.tabs.frame = CGRectMake(0, 0, NKMainWidth, NKNavigatorHeight);
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, -0.5, self.tabs.frame.size.width, 0.5)];
    [self.tabs insertSubview:line atIndex:0];
    line.backgroundColor = [UIColor lineColor];
    
    
    for (int i=0; i<[self.tabs.segments count]; i++) {
        
        UIButton *button = self.tabs.segments[i];
        button.frame = CGRectMake(NKMainWidth/[self.tabs.segments count]*i, button.frame.origin.y, NKMainWidth/[self.tabs.segments count], NKNavigatorHeight);
        
    }
    
    
}


-(void)showLoginOKView{
    
//    [NKNC popToRootViewControllerAnimated:NO];
//    
//    [self.tabs selectSegment:0 shouldTellDelegate:NO];
//    
//    [[NKUI sharedNKUI] showViewControllerWithClass:[[tabSource objectAtIndex:1] objectAtIndex:0]];
//    [[NKUI sharedNKUI] showNaviTab];
}


-(void)segmentControl:(NKSegmentControl*)control didChangeIndex:(NSInteger)index{
    
    
    [[NKUI sharedNKUI] showViewControllerWithClass:[[_tabSource objectAtIndex:1] objectAtIndex:index] andIndex:index];
    
    
}

@end
