//
//  NKViewController.h
//  FANZ
//
//  Created by King on 10/8/15.
//  Copyright © 2015 FANZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKDataStore.h"
#import "NKUI.h"
#import "NKRequest.h"
#import "NKModelDefine.h"
#import "NKProgressHUD.h"

@interface NKViewController : UIViewController <YTKRequestDelegate, UIGestureRecognizerDelegate>


@property (nonatomic, strong) UIView  *placeHolderView;
@property (nonatomic, strong) UIView  *headBar;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView  *contentView;
@property (nonatomic, strong) UIButton *nkRightButton;

@property (nonatomic, strong) UIColor *themeColorText;
@property (nonatomic, strong) UIColor *themeColorBackground;

-(IBAction)goBack:(id)sender;
-(IBAction)rightButtonClick:(id)sender;
-(IBAction)leftButtonClick:(id)sender;

-(UIButton*)addRightButtonWithTitle:(id)title;
-(UIButton*)addleftButtonWithTitle:(id)title;
-(UIButton*)addBackButton;
-(UIButton*)addLeftCancelButton;

-(UIColor*)normalBackgroundColor;

-(void)modifyHeaderColor;

-(void)showAlertWithMessage:(NSString*)message;

-(void)requestFailed:(NKRequest*)request;

-(NKRequestDelegate*)requestDelegateWithSuccess:(SEL)selector;

@end

