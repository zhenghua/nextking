//
//  NKStyle.h
//  NEXTKING
//
//  Created by King on 16/9/8.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NKUIStyle) {
    NKUIStyleNormal = 0, //正常
    NKUIStyleLiterature = 1, //文艺简单
    NKUIStyleCool = 2, //妖艳
    NKUIStyleOther
};

@interface NKStyle : NSObject

+(NKUIStyle)uiStyle;

+(void)updateStyleWithStyle:(NKUIStyle)style;

@end
