//
//  NKStepView.h
//  NEXTKING
//
//  Created by King on 16/7/30.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NKStepView : UIView

@property (nonatomic, strong) NSMutableArray *steps;
@property (nonatomic, assign) NSUInteger currentStep;

-(void)showWithSteps:(NSArray*)steps andCurrentStep:(NSUInteger)currentStep;

@end
