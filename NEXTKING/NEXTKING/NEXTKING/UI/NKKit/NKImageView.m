//
//  NKImageView.m
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKImageView.h"

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

@implementation NKImageView


-(void)dealloc{
    
    [_modelObject removeObserver:self forKeyPath:_keyPath];
    
}

-(void)bindValueOfModel:(id)model forKeyPath:(NSString*)key{
    [_modelObject removeObserver:self forKeyPath:_keyPath];
    
    self.modelObject = model;
    self.keyPath = key;
    
    [self renderImage];
    
    [_modelObject addObserver:self forKeyPath:_keyPath options:NSKeyValueObservingOptionNew context:nil];
    
}

-(void)renderImage{
    id value = [_modelObject valueForKeyPath:_keyPath];
    
    if (_target&&_renderMethod&&[_target respondsToSelector:_renderMethod]) {
        SuppressPerformSelectorLeakWarning(

                                           [_target performSelector:_renderMethod withObject:value withObject:self];
                                           
                                           );
    }

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    [self renderImage];
}


-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.clipsToBounds = YES;
    }

    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setSingleTapped:(SEL)singleTapped{
    
    _singleTapped = singleTapped;
    
    self.userInteractionEnabled = YES;
    
    if (!self.tap) {
        self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:_tap];
        
    }
    
}

-(void)tapped:(UITapGestureRecognizer*)gesture{
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        if ([_target respondsToSelector:_singleTapped]) {
            SuppressPerformSelectorLeakWarning(
                                               [_target performSelector:_singleTapped withObject:gesture];
                                               );
        }
    }
    
}




@end
