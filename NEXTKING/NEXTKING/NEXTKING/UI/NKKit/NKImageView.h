//
//  NKImageView.h
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKFileManager.h"

@interface NKImageView : UIImageView

@property (nonatomic, strong) id        modelObject;
@property (nonatomic, strong) NSString *keyPath;

@property (nonatomic, weak)   id        target;
@property (nonatomic, assign) SEL       renderMethod;
@property (nonatomic, assign) SEL       singleTapped;

@property (nonatomic, strong)   UITapGestureRecognizer *tap;

-(void)bindValueOfModel:(id)model forKeyPath:(NSString*)key;

@end
