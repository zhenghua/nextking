//
//  NKCellDataObject.h
//  NEXTKING
//
//  Created by King on 6/16/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NKCellDataObject : NSObject


@property (nonatomic, strong) NSString      *title;
@property (nonatomic, strong) UIImage       *titleImage;
@property (nonatomic, strong) id            accessoryView;
@property (nonatomic, strong) NSString      *detailText;

@property (nonatomic, assign) SEL           action;

@property (nonatomic, weak)   id            target;
@property (nonatomic, assign) SEL           detailTextRenderMethod;

@property (nonatomic, strong) NSString      *keyPath;

@property (nonatomic, strong) UIColor       *backgroudColor;
@property (nonatomic, assign) CGFloat       cellHeight;

@property (nonatomic, strong) NSString      *cellName;
@property (nonatomic, strong) NSString      *viewControllerName;

+(instancetype)cellDataObjectWithTitle:(NSString*)title detailText:(NSString*)detailText accessoryView:(id)accessoryView action:(SEL)action;
+(instancetype)cellDataObjectWithTitle:(NSString*)title titleImage:(UIImage*)titleImage detailText:(NSString*)detailText accessoryView:(id)accessoryView action:(SEL)action;

-(void)addSwithWithKeyPath:(NSString*)key;


-(void)runWithTarget:(id)target;


@end
