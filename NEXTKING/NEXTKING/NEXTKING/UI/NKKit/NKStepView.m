//
//  NKStepView.m
//  NEXTKING
//
//  Created by King on 16/7/30.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKStepView.h"
#import "UIColor+HexString.h"
#import "NKMUser.h"

@implementation NKStepView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
    
}

-(void)showWithSteps:(NSArray*)steps andCurrentStep:(NSUInteger)currentStep{
    
    self.steps = [NSMutableArray arrayWithArray:steps];
    self.currentStep = currentStep;
    NSInteger count = steps.count;
    CGFloat stepWidth = [[UIScreen mainScreen] bounds].size.width / (count+1);
    UIView *circle = nil;
    UILabel *label = nil;
    CGFloat lineHeight = 3;
    CGFloat circleWidth = 12;
    CGFloat centerY = self.height-40;
    
    UIProgressView *progress = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, lineHeight)];
    [progress setTintColor:[NKMShares riseColor]];
    [self addSubview:progress];
    progress.centerY = centerY;
    [progress setProgress:(currentStep+1.0)/(count+1.0) animated:NO];
    
    
    for (int i=0; i<count; i++) {
        
        circle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, circleWidth, circleWidth)];
        circle.center = CGPointMake(stepWidth*(i+1), centerY);
        [self addSubview:circle];
        circle.layer.cornerRadius = circleWidth/2;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 15)];
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        label.text = steps[i];
        label.center = CGPointMake(circle.centerX, centerY+20);
        label.adjustsFontSizeToFitWidth = YES;
        
        if (i<=currentStep) {
            circle.backgroundColor = [NKMShares riseColor];
            label.textColor = [NKMShares riseColor];
        }
        else{
            circle.backgroundColor = [UIColor lightGrayColor];
            label.textColor = [UIColor lightGrayColor];
        }
    }

}

@end
