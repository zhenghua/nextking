//
//  NKSegmentControl.h
//  FANSZ
//
//  Created by King on 10/10/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NKSegmentControlDirectionPortrait,
    NKSegmentControlDirectionLandscape
} NKSegmentControlDirection;


@interface NKSegment : NSObject{
    
}

@property (nonatomic, strong) UIImage *normalBackground;
@property (nonatomic, strong) UIImage *selectedBackground;

@property (nonatomic, strong) id title;

@property (nonatomic, assign) CGSize segmentSize;
@property (nonatomic, strong) UIColor *segmentColor;
@property (nonatomic, strong) UIColor *highlightSegmentColor;

@property (nonatomic, strong) UIColor *normalTextColor;
@property (nonatomic, strong) UIColor *highlightTextColor;

+(id)segmentWithNormalBack:(UIImage*)normal selectedBack:(UIImage*)selected;
+(id)segmentWithNormalBack:(UIImage*)normal selectedBack:(UIImage*)selected andTitle:(id)atitle;
+(id)segmentWithNormalBack:(UIImage*)normal selectedBack:(UIImage*)selected title:(id)atitle normalTextColor:(UIColor*)nColor andHighlightTextColor:(UIColor*)hColor;
+(id)segmentWithSize:(CGSize)size color:(UIColor*)color andTitle:(id)atitle;
+(id)segmentWithSize:(CGSize)size normalColor:(UIColor*)ncolor highlightColor:(UIColor*)hColor andTitle:(id)atitle;

@end


@protocol NKSegmentControlDelegate;

@interface NKSegmentControl : UIView{
    
}
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, strong) NSArray  *segments;
@property (nonatomic, strong) NSArray  *segmentImages;

@property (nonatomic, weak) id <NKSegmentControlDelegate> delegate;

@property (nonatomic) BOOL shouldAnimate;
@property (nonatomic) BOOL noBackgroundImage;
@property (nonatomic, strong) UIView *scroller;

@property (nonatomic) BOOL selectedSegmentClickable;


-(void)selectSegment:(NSInteger)indexToSelect;
-(void)selectSegment:(NSInteger)indexToSelect shouldTellDelegate:(BOOL)should;

+(instancetype)segmentControlViewWithSegments:(NSArray*)tempSegments;
+(instancetype)segmentControlViewWithSegments:(NSArray*)tempSegments andDelegate:(id<NKSegmentControlDelegate>)adelegate;
+(instancetype)segmentControlViewWithSegments:(NSArray*)tempSegments direction:(NKSegmentControlDirection)direction andDelegate:(id<NKSegmentControlDelegate>)adelegate;

-(void)addScroller:(UIView*)scroller;

@end



@protocol NKSegmentControlDelegate <NSObject>
@optional
-(void)segmentControl:(NKSegmentControl*)control didChangeIndex:(NSInteger)index;

@end
