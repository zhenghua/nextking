//
//  NKProgressHUD.m
//  NEXTKING
//
//  Created by King on 16/7/2.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKProgressHUD.h"
#import "MBProgressHUD.h"

@implementation NKProgressHUD

+ (BOOL)hideHUDForView:(UIView *)view animated:(BOOL)animated{
    
    return [MBProgressHUD hideHUDForView:view animated:animated];
    
}

+ (void)showTextHudInView:(UIView *)view text:(NSString *)text{
    
    if (!view) {
        return;
    }
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    [hud setAnimationType:MBProgressHUDAnimationFade];
    [hud setMode:MBProgressHUDModeIndeterminate];
    hud.label.text = text;
    [hud setMargin:10.f];
    [hud setUserInteractionEnabled:YES];
    [hud showAnimated:YES];
}


+ (void)showTextHudInView:(UIView *)view animate:(BOOL)animate text:(NSString *)text fontSize:(float)fontSize detailText:(NSString*)detailText duration:(NSTimeInterval)duration completion:(void (^)(void))completion{
    
    if (!view) {
        return;
        //view = [[UIView alloc] init]; // if the view is nil, NSAssert(view, @"View must not be nil.");
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animate];
    [hud setMode:MBProgressHUDModeText];
    
    if (text) {
        hud.label.text = text;
    }
    if (fontSize>0) {
        hud.label.font = [UIFont systemFontOfSize:fontSize];
    }
    if (detailText) {
        hud.detailsLabel.text = detailText;
    }
    
    [hud setMargin:10.f];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud setUserInteractionEnabled:YES];
    [hud setCompletionBlock:^
     {
         if (completion)
         {
             completion();
         }
     }];
    
    [hud hideAnimated:YES afterDelay:duration];
    
    
}

@end
