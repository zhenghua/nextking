//
//  NKTableViewCell.m
//  ZUO
//
//  Created by King on 9/25/12.
//  Copyright (c) 2012 ZUO.COM. All rights reserved.
//

#import "NKTableViewCell.h"
#import "NKMUser.h"

@implementation NKTableViewCell


-(void)dealloc{
    
    [NKRequestDelegate removeTarget:self];
}

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    if (![object respondsToSelector:@selector(cellHeight)] || !object.cellHeight) {
        return 60;
    }
    
    return object.cellHeight;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKCellDataObject *model = (NKCellDataObject*)object;
    
    self.imageView.image = model.titleImage;
    self.textLabel.text = model.title;
    if (!model.accessoryView) {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    else if ([model.accessoryView isKindOfClass:[NSNumber class]]) {
        self.accessoryType = [model.accessoryView integerValue];
    }
    
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(NKKVOLabel*)detailLabel{
    
    if (!_detailLabel) {
        
        _detailLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(NKMainWidth-200-40, 0, 200, [NKTableViewCell cellHeightForObject:nil])];
        [self.contentView addSubview:_detailLabel];
        _detailLabel.textColor = [UIColor lightGrayColor];
        _detailLabel.textAlignment = NSTextAlignmentRight;
        [self setValue:_detailLabel forKeyPath:@"detailTextLabel"];
    }
    return _detailLabel;
}

-(NKBadgeView*)badgeView{
    
    if (!_badgeView) {
        
        _badgeView = [[NKBadgeView alloc] initWithImage:[UIImage imageNamed:@"icon_unread"]];
        [self.contentView addSubview:_badgeView];
        
    }

    return _badgeView;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    if (_badgeView) {
        _badgeView.center = CGPointMake(self.contentView.width-10, ceilf(self.contentView.height/2));
    }
    
}

@end


@implementation UIView (NK)

-(void)addBottomLine{
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.height-0.5, NKMainWidth, 0.5)];
    line.backgroundColor = [UIColor lineColor];
    [self addSubview:line];
}

@end
