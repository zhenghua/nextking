//
//  NKCellDataObject.m
//  NEXTKING
//
//  Created by King on 6/16/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKCellDataObject.h"

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

@implementation NKCellDataObject


+(instancetype)cellDataObjectWithTitle:(NSString*)title detailText:(NSString*)detailText accessoryView:(id)accessoryView action:(SEL)action{
   
    return [self cellDataObjectWithTitle:title titleImage:nil detailText:detailText accessoryView:accessoryView action:action];
}
+(instancetype)cellDataObjectWithTitle:(NSString*)title titleImage:(UIImage*)titleImage detailText:(NSString*)detailText accessoryView:(id)accessoryView action:(SEL)action{
    
    NKCellDataObject *object = [[NKCellDataObject alloc] init];
    
    object.title = title;
    object.detailText = detailText;
    object.accessoryView = accessoryView;
    object.action = action;
    object.titleImage = titleImage;
    
    return object;
    
}

-(void)addSwithWithKeyPath:(NSString*)key{
    
    
}

-(void)runWithTarget:(id)target{
    
    if ([target respondsToSelector:self.action]) {
        SuppressPerformSelectorLeakWarning(
                                           [target performSelector:self.action withObject:self];
                                           );
        
    }
    
}

@end
