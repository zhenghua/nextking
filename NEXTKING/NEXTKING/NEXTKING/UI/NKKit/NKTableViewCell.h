//
//  NKTableViewCell.h
//  ZUO
//
//  Created by King on 9/25/12.
//  Copyright (c) 2012 ZUO.COM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKUI.h"
#import "NKCellDataObject.h"
#import "NKKVOLabel.h"
#import "NKImageView.h"
#import "NKBadgeView.h"

@interface NKTableViewCell : UITableViewCell{

}

@property (nonatomic, strong) id showedObject;
@property (nonatomic, strong) UIImageView *bottomLine;

@property (nonatomic, strong) NKKVOLabel *detailLabel;
@property (nonatomic, strong) NKBadgeView *badgeView;


+(CGFloat)cellHeightForObject:(id)object;

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath;


@end



@interface UIView (NK)

-(void)addBottomLine;

@end