//
//  NKBadgeView.m
//  NEXTKING
//
//  Created by King on 16/7/31.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKBadgeView.h"

@implementation NKBadgeView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.image = [UIImage imageNamed:@"icon_unread"];
    }
    return self;
}

-(void)renderImage{
    
    NSNumber *number = [self.modelObject valueForKeyPath:self.keyPath];
    
    //numberLabel.text = [NSString stringWithFormat:@"%@", [number intValue]>99?@"99":number];
    //self.text = @"";
    
    self.hidden = [number intValue]<=0 ? YES: NO;

}

@end
