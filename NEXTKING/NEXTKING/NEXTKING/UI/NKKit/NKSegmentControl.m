//
//  NKSegmentControl.m
//  FANSZ
//
//  Created by King on 10/10/15.
//  Copyright © 2015 FANSZ. All rights reserved.
//

#import "NKSegmentControl.h"


#define NormalTextColor [UIColor blackColor]
#define HighlightTextColor [UIColor whiteColor]

@implementation NKSegment


+(id)segmentWithNormalBack:(UIImage*)normal selectedBack:(UIImage*)selected{
    return [self segmentWithNormalBack:normal selectedBack:selected andTitle:nil];
}

+(id)segmentWithNormalBack:(UIImage*)normal selectedBack:(UIImage*)selected andTitle:(id)atitle{
    
    return [self segmentWithNormalBack:normal selectedBack:selected title:atitle normalTextColor:nil andHighlightTextColor:nil];
    
}

+(id)segmentWithNormalBack:(UIImage*)normal selectedBack:(UIImage*)selected title:(id)atitle normalTextColor:(UIColor*)nColor andHighlightTextColor:(UIColor*)hColor{
    
    NKSegment *newSegment = [[self alloc] init];
    
    newSegment.normalBackground = normal;
    newSegment.selectedBackground = selected;
    newSegment.title = atitle;
    
    if (nColor) {
        newSegment.normalTextColor = nColor;
    }
    if (hColor) {
        newSegment.highlightTextColor = hColor;
    }
    
    return newSegment;
    
}


+(id)segmentWithSize:(CGSize)size color:(UIColor*)color andTitle:(id)atitle{
    
    return [self segmentWithSize:size normalColor:color highlightColor:nil andTitle:atitle];
}

+(id)segmentWithSize:(CGSize)size normalColor:(UIColor*)ncolor highlightColor:(UIColor*)hColor andTitle:(id)atitle{
    
    NKSegment *newSegment = [[self alloc] init];
    newSegment.segmentSize = size;
    newSegment.title = atitle;
    
    if (ncolor) {
        newSegment.segmentColor = ncolor;
    }
    if (hColor) {
        newSegment.highlightSegmentColor = hColor;
    }
    
    return newSegment;
    
    
}

-(id)init{
    
    self = [super init];
    if (self) {
        
        self.normalTextColor = NormalTextColor;
        self.highlightTextColor = HighlightTextColor;
        
        self.segmentColor = [UIColor clearColor];
        self.highlightSegmentColor = [UIColor clearColor];
        
    }
    
    return self;
}

@end


@implementation NKSegmentControl


+(instancetype)segmentControlViewWithSegments:(NSArray*)tempSegments{
    return [self segmentControlViewWithSegments:tempSegments andDelegate:nil];
}
+(instancetype)segmentControlViewWithSegments:(NSArray*)tempSegments andDelegate:(id<NKSegmentControlDelegate>)adelegate{
    
    return [self segmentControlViewWithSegments:tempSegments direction:NKSegmentControlDirectionLandscape andDelegate:adelegate];
    
}

+(instancetype)segmentControlViewWithSegments:(NSArray*)tempSegments direction:(NKSegmentControlDirection)direction andDelegate:(id<NKSegmentControlDelegate>)adelegate{
    
    NKSegmentControl *newSC = [[NKSegmentControl alloc] init];
    newSC.selectedIndex = 0;
    
    NSMutableArray *buttonSegments = [NSMutableArray arrayWithCapacity:[tempSegments count]];
    
    UIButton *newSegment = nil;
    UIImage *normalImage = nil;
    CGFloat totalWidth = 0.0;
    CGFloat totalHeight = 0.0;
    NSInteger currentIndex = 0;
    
    for (NKSegment *segment in tempSegments) {
        normalImage = segment.normalBackground;
        
        newSegment = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if ([segment.title isKindOfClass:[NSString class]]) {
            [newSegment setTitle:segment.title forState:UIControlStateNormal];
        }
        else if ([segment.title isKindOfClass:[UIImage class]]) {
            [newSegment setImage:segment.title forState:UIControlStateNormal];
        }
        
        // Set Style
        newSegment.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        //newSegment.titleLabel.shadowColor = [UIColor whiteColor];
        //newSegment.titleLabel.shadowOffset = CGSizeMake(0, 1);
        //newSegment.reversesTitleShadowWhenHighlighted = YES;
        [newSegment setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [newSegment setTitleShadowColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [newSegment setTitleColor:NormalTextColor forState:UIControlStateNormal];
        [newSegment setTitleColor:HighlightTextColor forState:UIControlStateHighlighted];
        
        newSegment.tag = currentIndex;
        [newSegment setBackgroundImage:currentIndex==0?segment.selectedBackground:segment.normalBackground forState:UIControlStateNormal];
        [newSegment addTarget:newSC action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
        [newSC addSubview:newSegment];
        newSegment.adjustsImageWhenHighlighted = NO;
        
        [buttonSegments addObject:newSegment];
        
        
        
        switch (direction) {
            case NKSegmentControlDirectionLandscape:{
                newSegment.frame = CGRectMake(totalWidth, 0, normalImage?normalImage.size.width:segment.segmentSize.width, normalImage?normalImage.size.height:segment.segmentSize.height);
                currentIndex++;
                totalWidth+=newSegment.frame.size.width;
                if (newSegment.frame.size.height>totalHeight) {
                    totalHeight = newSegment.frame.size.height;
                }
            }
                break;
            case NKSegmentControlDirectionPortrait:{
                newSegment.frame = CGRectMake(0, totalHeight, normalImage?normalImage.size.width:segment.segmentSize.width, normalImage?normalImage.size.height:segment.segmentSize.height);
                currentIndex++;
                totalHeight+=newSegment.frame.size.height;
                if (newSegment.frame.size.width>totalWidth) {
                    totalWidth = newSegment.frame.size.width;
                }
            }
                break;
            default:
                break;
        }
        
        
        
    }
    
    
    newSC.frame = CGRectMake(0, 0, totalWidth, totalHeight);
    newSC.segments = buttonSegments;
    newSC.segmentImages = tempSegments;
    
    newSC.delegate = adelegate;
    
    [newSC selectSegment:0 shouldTellDelegate:NO];
    
    return newSC;
    
}

-(void)deSelectAll{
    for (UIButton *segment in self.segments) {
        
        NKSegment *nkseg = [self.segmentImages objectAtIndex:segment.tag];
        [[segment subviews] makeObjectsPerformSelector:@selector(setHighlighted:) withObject:nil];
        if (self.noBackgroundImage) {
            [segment setImage:nkseg.normalBackground forState:UIControlStateNormal];
        }
        else{
            [segment setBackgroundImage:nkseg.normalBackground forState:UIControlStateNormal];
        }
        
        [segment setTitleColor:nkseg.normalTextColor forState:UIControlStateNormal];
        
        if (!nkseg.selectedBackground && !nkseg.normalBackground) {
            segment.backgroundColor = nkseg.segmentColor;
        }
        
    }
    
}

-(void)selectSegment:(NSInteger)indexToSelect shouldTellDelegate:(BOOL)should{
    
    [self deSelectAll];
    self.selectedIndex = indexToSelect;
    
    if (indexToSelect<0 || indexToSelect>=[self.segments count]) {
        return;
    }
    
    UIButton *nkseg = [self.segments objectAtIndex:self.selectedIndex];
    
    [[nkseg subviews] makeObjectsPerformSelector:@selector(setHighlighted:) withObject:nil];
    
    NKSegment *theSeg = [self.segmentImages objectAtIndex:self.selectedIndex];
    
    if (self.noBackgroundImage) {
        [nkseg setImage:[theSeg selectedBackground] forState:UIControlStateNormal];
    }
    else{
        [nkseg setBackgroundImage:[theSeg selectedBackground] forState:UIControlStateNormal];
    }
    
    [nkseg setTitleColor:[theSeg highlightTextColor] forState:UIControlStateNormal];
    
    if (!theSeg.selectedBackground && !theSeg.normalBackground) {
        nkseg.backgroundColor = theSeg.highlightSegmentColor;
    }
    
    if (self.shouldAnimate) {
        nkseg.alpha = 0.3;
        [UIView animateWithDuration:0.3 animations:^{
            nkseg.alpha = 1.0;
        }];
        
    }
    
    if (should && [self.delegate respondsToSelector:@selector(segmentControl:didChangeIndex:)]) {
        [self.delegate segmentControl:self didChangeIndex:self.selectedIndex];
    }
    
    if (self.scroller) {
        [UIView animateWithDuration:0.3 animations:^{
            self.scroller.center = CGPointMake(nkseg.center.x, self.scroller.center.y);
        }];
    }
    
}

-(void)selectSegment:(NSInteger)indexToSelect{
    
    [self selectSegment:indexToSelect shouldTellDelegate:YES];
}


-(void)addScroller:(UIView*)scroller{
    
    self.scroller = scroller;
    [self addSubview:_scroller];
    UIButton *nkseg = [self.segments objectAtIndex:self.selectedIndex];
    _scroller.center = CGPointMake(nkseg.center.x, self.bounds.size.height-(self.scroller.bounds.size.height/2));
    
}

-(void)buttonClicked:(UIButton*)button{
    
    if (button.tag == self.selectedIndex && !self.selectedSegmentClickable) {
        return;
    }
    
    [self selectSegment:button.tag];
    
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.noBackgroundImage = NO;
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
