//
//  NKWebViewController.h
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKViewController.h"

@interface NKWebViewController : NKViewController

@property (nonatomic, strong) NSString *url;

@end
