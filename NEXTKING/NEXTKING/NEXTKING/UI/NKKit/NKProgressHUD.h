//
//  NKProgressHUD.h
//  NEXTKING
//
//  Created by King on 16/7/2.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NKProgressHUD : NSObject

+ (void)showTextHudInView:(UIView *)view text:(NSString *)text;

+ (void)showTextHudInView:(UIView *)view animate:(BOOL)animate text:(NSString *)text fontSize:(float)fontSize detailText:(NSString*)detailText duration:(NSTimeInterval)duration completion:(void (^)(void))completion;

+ (BOOL)hideHUDForView:(UIView *)view animated:(BOOL)animated;

@end
