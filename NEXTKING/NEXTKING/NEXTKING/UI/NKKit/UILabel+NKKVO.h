//
//  UILabel+NKKVO.h
//  NEXTKING
//
//  Created by King on 16/7/8.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (NKKVO)

-(void)bindValueOfModel:(id)model forKeyPath:(NSString*)key;

@end
