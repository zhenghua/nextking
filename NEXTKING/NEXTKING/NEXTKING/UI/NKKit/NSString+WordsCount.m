//
//  NSString+WordsCount.m
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NSString+WordsCount.h"

@implementation NSString (WordsCount)

- (NSUInteger)wordsCount
{
    __block NSUInteger count = 0;
    [self enumerateSubstringsInRange:NSMakeRange(0, self.length)
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop)
     {
         if ([substring canBeConvertedToEncoding:NSASCIIStringEncoding])
         {
             count++;
         }
         else
         {
             count += [substring lengthOfBytesUsingEncoding:NSUTF32StringEncoding] / 2;
         }
     }];
    return (NSUInteger) ceilf((float) (count) / 2.0f);
}

- (NSInteger)wordsCountForHans
{
    __block NSUInteger count = 0;
    [self enumerateSubstringsInRange:NSMakeRange(0, self.length)
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop)
     {
         if ([substring canBeConvertedToEncoding:NSASCIIStringEncoding])
         {
             count++;
         }
         else
         {
             count += [substring lengthOfBytesUsingEncoding:NSUTF16StringEncoding];
         }
     }];
    return count;
}

@end
