//
//  NKWebViewController.m
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKWebViewController.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"

@interface NKWebViewController ()<NJKWebViewProgressDelegate, UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;

@property (nonatomic, strong) NJKWebViewProgressView *progressView;

@property (nonatomic, strong) NJKWebViewProgress *progressProxy;

@end

@implementation NKWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addBackButton];
    
    if (![_url hasPrefix:@"http"]) {
        _url = [NSString stringWithFormat:@"http://%@", _url];
    }
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 44, NKMainWidth, NKMainHeight-44)];
    [self.contentView addSubview:_webView];
    
//    NSString* secretAgent = [_webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
//    NSString *newUagent = [NSString stringWithFormat:@"%@ MicroMessenger/5.0.1",secretAgent];
//    NSDictionary *dictionary = [[NSDictionary alloc]
//                                initWithObjectsAndKeys:newUagent, @"UserAgent", nil];
//    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    self.webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigationBarBounds = self.headBar.bounds;
    CGRect barFrame = CGRectMake(0, navigationBarBounds.size.height, navigationBarBounds.size.width, progressBarHeight);
    
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.headBar addSubview:_progressView];
    _progressView.progress = 0;
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", _url]]];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
    self.titleLabel.text = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
