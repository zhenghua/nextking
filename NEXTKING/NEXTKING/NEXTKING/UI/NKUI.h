//
//  NKUI.h
//  FANZ
//
//  Created by King on 10/9/15.
//  Copyright © 2015 FANZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKNavigator.h"
#import "UIColor+HexString.h"
#import "NKStyle.h"
#import "WebImage.h"
#import "NKRequest.h"
#import "NKNavigationController.h"

#define NKMainHeight [[NKUI sharedNKUI] appHeight]
#define NKMainWidth [[NKUI sharedNKUI] appWidth]

#define NKContentHeight NKMainHeight-NKNavigatorHeight

#define NKNC [[NKUI sharedNKUI] navigationController]

@interface NKUI : UIViewController

@property (nonatomic, assign) Class welcomeClass;
@property (nonatomic, assign) Class homeClass;
@property (nonatomic, assign) BOOL  needLogin;

@property (nonatomic, strong) UIImageView *nkBackgroundView;
@property (nonatomic, strong) NKNavigator *navigator;

@property (nonatomic, strong) UIViewController *currentController;

@property (nonatomic, assign) BOOL needStoreViewControllers;
@property (nonatomic, strong) NSMutableArray *allViewControllers;

@property (nonatomic, assign) BOOL  navigatorOnTop;

@property (nonatomic) CGFloat appHeight;
@property (nonatomic) CGFloat appWidth;

+(instancetype)sharedNKUI;

-(CGFloat)appHeight;
-(CGFloat)appWidth;

-(void)showNaviTab;
-(void)showWelcome;
-(void)showHome;

-(void)addTabs:(NSArray*)tab;

-(UIViewController*)showViewControllerWithClass:(Class)ControllerToShow;
-(UIViewController*)showViewControllerWithClass:(Class)ControllerToShow andIndex:(NSInteger)index;
-(void)removeAllViewControllers;

-(void)logout;

-(void)reloadViewControllers;

@end
