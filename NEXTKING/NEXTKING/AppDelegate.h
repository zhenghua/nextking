//
//  AppDelegate.h
//  NEXTKING
//
//  Created by jinzhenghua on 12/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

