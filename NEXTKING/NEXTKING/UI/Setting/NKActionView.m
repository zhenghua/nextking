//
//  NKActionView.m
//  NEXTKING
//
//  Created by King on 16/9/1.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKActionView.h"
#import "NKUI.h"


@interface NKActionView ()

@property (nonatomic, strong) UIButton *backgroundButton;


@end

@implementation NKActionView



-(instancetype)init{
    
    self = [super initWithFrame:CGRectMake(0, 0, NKMainWidth, NKMainHeight)];
    if (self) {
        
        self.needsFullScreen = YES;
        
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, NKMainHeight-266, NKMainWidth, 276)];
        [self addSubview:_contentView];
        self.contentView.layer.shadowOffset = CGSizeMake(0, -1);
        self.contentView.layer.shadowOpacity = 0.2;
        self.contentView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.contentView.layer.shadowRadius = 7.0;
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 0.5)];
        [self.contentView addSubview:line];
        line.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 41, 45)];
        [self.contentView addSubview:closeButton];
        [closeButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setImage:[UIImage imageNamed:@"trade_icon_close"] forState:UIControlStateNormal];
        
        UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(NKMainWidth-45, 0, 41, 45)];
        [self.contentView addSubview:saveButton];
        [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [saveButton setImage:[UIImage imageNamed:@"trade_icon_ok"] forState:UIControlStateNormal];
        
    }
    return self;
    
    
}

-(id)pickedData{
    
    return nil;
}

-(void)saveButtonClicked:(id)sender{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionView:didPickData:)]) {
        [self.delegate actionView:self didPickData:[self pickedData]];
    }
    
    [self closeButtonClicked:nil];
    
}

-(void)showInView:(UIView*)view{
    CGRect frame = self.contentView.frame;
    frame.origin.y = NKMainHeight;
    self.contentView.frame = frame;
    
    if (self.needsFullScreen) {
        self.backgroundButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, NKMainHeight)];
        [self insertSubview:_backgroundButton atIndex:0];
        [_backgroundButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _backgroundButton.backgroundColor = [UIColor blackColor];
        _backgroundButton.alpha = 0.0;
    }
    
    [view addSubview:self];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.contentView.frame;
        frame.origin.y = NKMainHeight-self.contentView.height;
        self.contentView.frame = frame;
        _backgroundButton.alpha = 0.4;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame = self.contentView.frame;
            frame.origin.y = NKMainHeight-self.contentView.height+10;
            self.contentView.frame = frame;
        }];
    }];
}

-(void)closeButtonClicked:(id)sender{
    
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.contentView.frame;
        frame.origin.y = NKMainHeight;
        self.contentView.frame = frame;
        self.backgroundButton.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}


@end
