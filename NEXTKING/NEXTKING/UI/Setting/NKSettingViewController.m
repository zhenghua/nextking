//
//  NKSettingViewController.m
//  NEXTKING
//
//  Created by jinzhenghua on 25/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKSettingViewController.h"
#import "NKAccountManager.h"
#import "NKOrderService.h"
#import "NKUserService.h"
#import "NKSystemSettingViewController.h"

#import "NKSettingViewCell.h"
#import "NKSettingViewMeCell.h"
#import "NKSettingViewWalletCell.h"
#import "NKMyWalletViewController.h"
#import "NKMySharesViewController.h"
#import "NKMyProfileViewController.h"
#import "NKMyPositionViewController.h"

#import "NKSharesDetailViewController.h"
#import "NKNoticeViewController.h"


#import "NKManagerViewController.h"
#import "KYCViewController.h"


@interface NKSettingViewController ()

@end

@implementation NKSettingViewController


-(UITableViewStyle)tableViewStyle{
    
    return UITableViewStyleGrouped;
}

-(void)rightButtonClick:(id)sender{
    NKNoticeViewController *viewController = [NKNoticeViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.titleLabel.text = @"我";
    [self addRightButtonWithTitle:[UIImage imageNamed:@"nav_icon_notice"]];
    
    NKBadgeView *badge = [[NKBadgeView alloc] initWithImage:[UIImage imageNamed:@"icon_unread"]];
    [self.nkRightButton addSubview:badge];
    [badge bindValueOfModel:[NKUserService sharedNKUserService].local forKeyPath:@"needNoticeAlert"];
    badge.center = CGPointMake(37, 14);
    
    
    NKCellDataObject *me = [NKCellDataObject cellDataObjectWithTitle:@"我" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(meClicked:)];
    me.cellName = NSStringFromClass([NKSettingViewMeCell class]);
    
    NKCellDataObject *wallet = [NKCellDataObject cellDataObjectWithTitle:@"钱包" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(walletClicked:)];
    wallet.cellName = NSStringFromClass([NKSettingViewWalletCell class]);
    
    NKCellDataObject *myShares = [NKCellDataObject cellDataObjectWithTitle:@"我的时空" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(mySharesClicked:)];
    
    NKCellDataObject *position = [NKCellDataObject cellDataObjectWithTitle:@"我的持仓" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(postionClicked:)];
    
    NKCellDataObject *logout = [NKCellDataObject cellDataObjectWithTitle:@"设置" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(settingClicked:)];
    
    NKCellDataObject *kyc = [NKCellDataObject cellDataObjectWithTitle:@"KYC" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(kycClicked:)];
 
    
    self.dataSource = [NSMutableArray arrayWithObjects:
                       @[me],
                       @[wallet, myShares, position],
                       @[logout],
                       @[kyc],
                       nil];
    
    
    if ([NKAccountManager isKing]) {
  
        NKCellDataObject *uerVerify = [NKCellDataObject cellDataObjectWithTitle:@"管理后台" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(userVerifyClicked:)];
        uerVerify.viewControllerName = NSStringFromClass([NKManagerViewController class]);
        [self.dataSource addObject:@[uerVerify]];
    }
}

-(void)kycClicked:(NKCellDataObject*)object{
    
    KYCViewController *viewController = [KYCViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)userVerifyClicked:(NKCellDataObject*)object{
    id viewController = [NSClassFromString(object.viewControllerName) new];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)postionClicked:(NKCellDataObject*)object{
    
    NKMyPositionViewController *viewController = [NKMyPositionViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)meClicked:(NKCellDataObject*)object{
    
    NKMyProfileViewController *viewController = [NKMyProfileViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)walletClicked:(NKCellDataObject*)object{
    
    NKMyWalletViewController *viewController = [NKMyWalletViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)mySharesClicked:(NKCellDataObject*)sender{
    
    if ([[NKOrderService sharedNKOrderService] shares] && [[[[NKOrderService sharedNKOrderService] shares] status] integerValue]>=1) {
        NKSharesDetailViewController *viewController = [NKSharesDetailViewController new];
        viewController.shares = [[NKOrderService sharedNKOrderService] shares];
        [NKNC pushViewController:viewController animated:YES];
    }
    else{
        NKMySharesViewController *viewController = [NKMySharesViewController new];
        if ([[NKOrderService sharedNKOrderService] shares]) {
            viewController.shares = [[NKOrderService sharedNKOrderService] shares];
        }
        [NKNC pushViewController:viewController animated:YES];
    }
    
   
    
}

-(void)settingClicked:(NKCellDataObject*)sender{
    
    NKSystemSettingViewController *viewController = [NKSystemSettingViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    if (!object.cellName) {
        object.cellName = NSStringFromClass([NKSettingViewCell class]);
    }
    return [NSClassFromString(object.cellName) cellHeightForObject:object];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    if (!object.cellName) {
        object.cellName = NSStringFromClass([NKSettingViewCell class]);
    }
    
    NSString * cellIdentifier = object.cellName;
    
    NKSettingViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NSClassFromString(object.cellName) alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    [cell showForObject:object withIndex:indexPath];

    return cell;
}


@end
