//
//  NKFlowViewController.m
//  NEXTKING
//
//  Created by King on 16/7/26.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKFlowViewController.h"
#import "NKOrderService.h"
#import "NKFlowCell.h"

@implementation NKFlowViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    self.titleLabel.text = @"资金流水";
    
    self.showTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    [self.showTableView.header beginRefreshing];
    
    self.showTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 1)];
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listFlowWithOffset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)getMoreData{
    
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listFlowWithOffset:self.dataSource.count size:20 andRequestDelegate:rd];
}



#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKFlowCellIdentifier";
    
    NKFlowCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKFlowCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NKMFlow *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKFlowCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
