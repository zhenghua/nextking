//
//  NKPositionCell.m
//  NEXTKING
//
//  Created by King on 16/7/13.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKPositionCell.h"
#import "NKMUser.h"

@interface NKPositionCell ()

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *amount;

@end

@implementation NKPositionCell

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    return 60;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMShareholder *model = (NKMShareholder*)object;
    
    self.name.text = model.shares.name;
    self.amount.text = [NSString stringWithFormat:@"%@股", model.amount];
    if (model.sellingAmount) {
        self.amount.text = [self.amount.text stringByAppendingFormat:@" 出售中:%@股", model.sellingAmount];
    }
    
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:model.shares.avatar imageSize:_avatar.frame.size]];
    
    [_avatar sd_setImageWithURL:imageURL];
    
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        [self.contentView addSubview:_avatar];
        
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+15, CGRectGetMinY(_avatar.frame), NKMainWidth-CGRectGetMaxX(_avatar.frame)-15-15, CGRectGetHeight(_avatar.frame)/2)];
        [self.contentView addSubview:_name];
        _name.font = [UIFont systemFontOfSize:14];
        
        CGRect nameFrame = _name.frame;
        nameFrame.origin.y+=nameFrame.size.height;
        
        self.amount = [[UILabel alloc] initWithFrame:nameFrame];
        [self.contentView addSubview:_amount];
        _amount.textColor = [UIColor lightGrayColor];
        _amount.font = [UIFont systemFontOfSize:12];
        
    }
    return self;
}


@end
