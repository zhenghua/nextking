//
//  NKFlowCell.m
//  NEXTKING
//
//  Created by King on 16/7/26.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKFlowCell.h"
#import "NKMUser.h"

#define kFlowCellHeight 64.0f

@interface NKFlowCell ()

@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *moneyLabel;

@end

@implementation NKFlowCell

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    return kFlowCellHeight;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMFlow *model = (NKMFlow*)object;
    
    self.typeLabel.text = [model typeText];
    self.timeLabel.text = [model timeText];
    self.moneyLabel.text = [model moneyText];
    self.moneyLabel.textColor = [UIColor blackColor];
    if ([_moneyLabel.text rangeOfString:@"+"].length>0) {
        self.moneyLabel.textColor = [NKMShares riseColor];
    }

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 200, 18)];
        [self.contentView addSubview:_typeLabel];
        _typeLabel.font = [UIFont systemFontOfSize:17];
        
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, _typeLabel.bottom+5, 200, 15)];
        [self.contentView addSubview:_timeLabel];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B"];

        self.moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(NKMainWidth-200-15, 0, 200, kFlowCellHeight)];
        [self.contentView addSubview:_moneyLabel];
        _moneyLabel.font = [UIFont systemFontOfSize:17];
        _moneyLabel.textAlignment = NSTextAlignmentRight;
        
    }
    return self;
}

@end
