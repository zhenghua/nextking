//
//  NKOrderDetailViewController.m
//  NEXTKING
//
//  Created by King on 16/8/14.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKOrderDetailViewController.h"

@implementation NKOrderDetailViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self addBackButton];
    self.titleLabel.text = @"订单详情";
    
    self.showTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    NKCellDataObject *object = [NKCellDataObject cellDataObjectWithTitle:@"时空名称" detailText:self.order.shares.name  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"时空代码" detailText:[self.order.shares sharesID]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"委托状态" detailText:[self.order statusText]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"买卖方向" detailText:[self.order actionText]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"委托价格" detailText:[NSString stringWithFormat:@"%.2f", self.order.price.floatValue]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"委托数量" detailText:[NSString stringWithFormat:@"%.0f", self.order.amount.floatValue]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"成交价格" detailText:[NSString stringWithFormat:@"%.2f", self.order.dealPrice.floatValue]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"成交数量" detailText:[NSString stringWithFormat:@"%.0f", self.order.dealAmount.floatValue]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"成交时间" detailText:[self.order dealTimeText]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"撤单时间" detailText:[self.order cancelTimeText]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    object = [NKCellDataObject cellDataObjectWithTitle:@"委托时间" detailText:[self.order createTimeText]  accessoryView:nil action:nil];
    [self.dataSource addObject:object];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKCellIdentifier";
    
    NKTableViewCell *cell = (NKTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[self cellClass] alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor darkGrayColor];
    }
    NKCellDataObject *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    cell.detailTextLabel.text = object.detailText;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
}

@end
