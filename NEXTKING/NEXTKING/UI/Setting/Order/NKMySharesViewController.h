//
//  NKMySharesViewController.h
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKMySharesViewController : NKTableViewController

@property (nonatomic, strong) NKMShares *shares;

@end
