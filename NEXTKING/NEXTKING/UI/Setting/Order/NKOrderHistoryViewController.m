//
//  NKOrderHistoryViewController.m
//  NEXTKING
//
//  Created by King on 16/7/14.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKOrderHistoryViewController.h"
#import "NKOrderService.h"
#import "NKOrderCell.h"

#import "NKOrderDetailViewController.h"

@implementation NKOrderHistoryViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    self.titleLabel.text = @"历史订单";
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    [self.showTableView.header beginRefreshing];
    
}

-(void)refreshData{
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listOrdersWithUID:nil offset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listOrdersWithUID:nil offset:self.dataSource.count size:20 andRequestDelegate:rd];
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKOrderCellIdentifier";
    
    NKOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKOrderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.type = NKOrderCellTypeHistory;
    }
    NKMOrder *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKOrderCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKMOrder *object = [self.dataSource objectAtIndex:indexPath.row];
    
    if ([object.status integerValue] == NKMOrderStatusCreate) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"撤单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self cancelOrderWithOrder:object];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
        [NKNC presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        
        NKOrderDetailViewController *viewController = [NKOrderDetailViewController new];
        viewController.order = object;
        [NKNC pushViewController:viewController animated:YES];
    }
    
}

-(void)cancelOrderWithOrder:(NKMOrder*)order{
    
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(cancelOK:)];
    [[NKOrderService sharedNKOrderService] cancelOrderWithOrderID:order.modelID andRequestDelegate:rd];
}

-(void)cancelOK:(NKRequest*)request{
    
    [self refreshData];
    [self showAlertWithMessage:@"已撤单"];
    
}

@end
