//
//  NKMyPositionViewController.m
//  NEXTKING
//
//  Created by King on 16/7/11.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKMyPositionViewController.h"
#import "NKOrderService.h"
#import "NKPositionCell.h"
#import "NKOrderHistoryViewController.h"
#import "NKSharesDetailViewController.h"

NSString *const NKCachePathMyPosition = @"NKCachePathMyPosition.NK";

@implementation NKMyPositionViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addBackButton];
    
    self.titleLabel.text = @"我的持仓";
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    [self addRightButtonWithTitle:@"订单"];
    
    self.dataSource = [[NKDataStore sharedDataStore] cachedArrayOf:[self cachePathKey] andClass:[NKMShareholder class]];
    
    [self.showTableView.header beginRefreshing];
    
}

-(void)rightButtonClick:(id)sender{
    
    NKOrderHistoryViewController *viewController = [NKOrderHistoryViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listPositionWithUID:nil offset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
}

-(NSString*)cachePathKey{
    return NKCachePathMyPosition;
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listPositionWithUID:nil offset:self.dataSource.count size:20 andRequestDelegate:rd];
}


#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKSharesCellIdentifier";
    
    NKPositionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKPositionCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NKMShareholder *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKPositionCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKSharesDetailViewController *viewController = [NKSharesDetailViewController new];
    NKMShareholder *object = [self.dataSource objectAtIndex:indexPath.row];
    viewController.shares = object.shares;
    [NKNC pushViewController:viewController animated:YES];
    
}

@end
