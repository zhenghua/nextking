//
//  NKOrderDetailViewController.h
//  NEXTKING
//
//  Created by King on 16/8/14.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKOrderDetailViewController : NKTableViewController

@property (nonatomic, strong) NKMOrder *order;

@end
