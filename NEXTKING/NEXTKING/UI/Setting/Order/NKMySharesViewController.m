//
//  NKMySharesViewController.m
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKMySharesViewController.h"
#import "NKOrderService.h"
#import "NKTextInputViewController.h"
#import "NKStepView.h"

@interface NKMySharesViewController ()



@end

@implementation NKMySharesViewController

-(UITableViewStyle)tableViewStyle{
    return UITableViewStyleGrouped;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.shares = [[NKMShares alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    
    self.titleLabel.text = @"我的时空";
    [self addRightButtonWithTitle:@"发行"];
    
    NKCellDataObject *title = [NKCellDataObject cellDataObjectWithTitle:@"主营内容" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(titleClicked:)];
    title.keyPath = @"title";
    
    NKCellDataObject *link = [NKCellDataObject cellDataObjectWithTitle:@"服务详情" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(linkClicked:)];
    link.keyPath = @"link";
    
    NKCellDataObject *amount = [NKCellDataObject cellDataObjectWithTitle:@"发行数量" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(amountClicked:)];
    amount.keyPath = @"totalAmount";
    
    NKCellDataObject *price = [NKCellDataObject cellDataObjectWithTitle:@"发行价" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(priceClicked:)];
    price.keyPath = @"price";
    
    NKCellDataObject *total = [NKCellDataObject cellDataObjectWithTitle:@"总市值" titleImage:nil detailText:nil accessoryView:nil action:nil];
    total.keyPath = @"value";
    
    self.dataSource = [NSMutableArray arrayWithObjects:
                       @[title, link, amount, price, total],
                       nil];
    [self fetchShares];
    [self loadStatusHeader];
}

-(void)loadStatusHeader{
    
    if ([self.shares.status integerValue]>=1) {
        self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 1)];
        self.nkRightButton.hidden = YES;
        
    }
    else{
        self.nkRightButton.hidden = NO;
        NKStepView *header = [[NKStepView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 100)];
        self.showTableView.tableHeaderView = header;
        NSInteger step = self.shares.modelID?([self.shares.status integerValue]==0?1:2):0;
        [header showWithSteps:@[@"提交资料", @"等待审核", [self.shares.status integerValue]==-1?@"请求被拒,重新提交":@"发行成功"] andCurrentStep:step];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, NKMainWidth-30, 20)];
        title.font = [UIFont systemFontOfSize:15];
        [header addSubview:title];
        title.text = @"发行时空:";
        
    }

}

-(void)fetchShares{
    
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(fetchOK:)];
    [[NKOrderService sharedNKOrderService] getSharesWithID:nil andRequestDelegate:rd];
}

-(void)fetchOK:(NKRequest*)request{
    
    if ([request.results count]) {
        self.shares = [request.results lastObject];
        self.shares.user = [NKMUser me];
        [[NKOrderService sharedNKOrderService] setShares:self.shares];
        [self.showTableView reloadData];
        [self loadStatusHeader];
    }
}

-(void)linkClicked:(NKCellDataObject*)object{
    
    NKTextInputViewController *viewController = [NKTextInputViewController new];
    viewController.model = self.shares;
    viewController.keyPath = object.keyPath;
    viewController.titleText = object.title;
    viewController.placeholder = @"服务的链接";
    [NKNC pushViewController:viewController animated:YES];
}

-(void)titleClicked:(NKCellDataObject*)object{
    
    NKTextInputViewController *viewController = [NKTextInputViewController new];
    viewController.model = self.shares;
    viewController.keyPath = object.keyPath;
    viewController.titleText = object.title;
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)amountClicked:(NKCellDataObject*)object{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"10000股" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shares.totalAmount = [NSNumber numberWithFloat:10000];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"50000股" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shares.totalAmount = [NSNumber numberWithFloat:50000];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"100000股" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shares.totalAmount = [NSNumber numberWithFloat:100000];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)priceClicked:(NKCellDataObject*)object{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"0.1元/股" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shares.price = [NSNumber numberWithFloat:0.1];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"0.5元/股" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shares.price = [NSNumber numberWithFloat:0.5];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"1元/股" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shares.price = [NSNumber numberWithFloat:1.0];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];

}

-(void)calculate{
    
    if (self.shares.totalAmount && self.shares.price) {
        CGFloat value = self.shares.totalAmount.floatValue * self.shares.price.floatValue;
        if (value != self.shares.value.floatValue) {
            self.shares.value = [NSNumber numberWithFloat:value];
        }
    }
}

-(NSString*)render:(NSNumber*)value label:(NKKVOLabel*)label{
    
    if (!value) {
        return nil;
    }
    
    if ([label.keyPath isEqualToString:@"value"]) {
        return [NSString stringWithFormat:@"%@元",value];
    }
    
    [self calculate];
    
    if ([label.keyPath isEqualToString:@"totalAmount"]) {
        return [NSString stringWithFormat:@"%@股",value];
    }
    else if ([label.keyPath isEqualToString:@"price"]) {
        return [NSString stringWithFormat:@"%@元/股",value];
    }
    
    return [NSString stringWithFormat:@"%@", value];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)rightButtonClick:(id)sender{
    
    if (!self.shares.totalAmount) {
        [self amountClicked:nil];
        return;
    }
    
    if (!self.shares.price) {
        [self priceClicked:nil];
        return;
    }
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(addSharesOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKOrderService sharedNKOrderService] addSharesWithPrice:self.shares.price amount:self.shares.totalAmount title:self.shares.title link:self.shares.link andRequestDelegate:rd];
    
}

-(void)addSharesOK:(NKRequest*)request{
    self.shares = [request.results lastObject];
    self.shares.user = [NKMUser me];
    [[NKOrderService sharedNKOrderService] setShares:self.shares];
    [self loadStatusHeader];
    [self showAlertWithMessage:@"已经提交发行请求，请等待"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NKTableViewCell cellHeightForObject:nil];;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"SettingCellIdentifier";
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.detailLabel.target = self;
        cell.detailLabel.renderMethod = @selector(render:label:);
    }
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    [cell showForObject:object withIndex:indexPath];
    [cell.detailLabel bindValueOfModel:self.shares forKeyPath:object.keyPath];
    
    return cell;
}


@end
