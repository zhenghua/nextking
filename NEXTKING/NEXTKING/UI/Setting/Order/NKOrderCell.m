//
//  NKOrderCell.m
//  NEXTKING
//
//  Created by King on 16/7/14.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKOrderCell.h"
#import "NKMUser.h"

@interface NKOrderCell ()

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) UILabel *name;

@property (nonatomic, strong) UILabel *status;

@property (nonatomic, strong) UILabel *amount;

@end

@implementation NKOrderCell

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    return 60;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMOrder *model = (NKMOrder*)object;
    
    NKMUser *user = nil;
    if (self.type == NKOrderCellTypeHandicap) {
        if (model.buyer){
            user = model.buyer;
        }
        if (model.seller){
            user = model.seller;
        }
    }
    else{
        user = model.shares.user;
    }
    
    if (self.type == NKOrderCellTypeHandicap) {
        
        self.name.text = [NSString stringWithFormat:@"%@", user.name];
        self.amount.text = [NSString stringWithFormat:@"%.2f\n%.0f", model.price.floatValue, model.amount.floatValue-model.dealAmount.floatValue];
        self.status.text = [NSString stringWithFormat:@"%@", [model actionText]];
        
    }else{
        self.name.text = [NSString stringWithFormat:@"%@\n%@", user.name, [model.shares sharesID]];
        self.amount.text = [NSString stringWithFormat:@"%.2f\n%.0f", model.price.floatValue, model.amount.floatValue];
        self.status.text = [NSString stringWithFormat:@"%@\n%@", [model statusText], [model actionText]];
        
    }
    
    BOOL isBuy = [model isBuy];
    UIColor *color = isBuy?[NKMShares riseColor]:[NKMShares fallColor];
    self.name.textColor = color;
    self.amount.textColor = color;
    self.status.textColor = color;
    
    
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:user.avatarPath imageSize:_avatar.frame.size]];
    [_avatar sd_setImageWithURL:imageURL];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        [self.contentView addSubview:_avatar];
        
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+10, CGRectGetMinY(_avatar.frame), 100, 30)];
        [self.contentView addSubview:_name];
        _name.font = [UIFont systemFontOfSize:12];
        _name.numberOfLines = 2;
        
        CGRect nameFrame = _name.frame;
        nameFrame.origin.x = NKMainWidth-100-15;

        self.amount = [[UILabel alloc] initWithFrame:nameFrame];
        [self.contentView addSubview:_amount];
        _amount.font = _name.font;
        _amount.numberOfLines = 2;
        _amount.textAlignment = NSTextAlignmentRight;
        
        nameFrame.origin.x = NKMainWidth/2-20;
        self.status = [[UILabel alloc] initWithFrame:nameFrame];
        [self.contentView addSubview:_status];
        _status.font = _name.font;
        _status.numberOfLines = 2;
        _status.textAlignment = NSTextAlignmentCenter;
        
    }
    return self;
}


@end
