//
//  NKMyWalletViewController.m
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKMyWalletViewController.h"
#import "NKOrderService.h"
#import "NKKVOLabel.h"

#import "NKFlowViewController.h"

@interface NKMyWalletViewController ()

@property (nonatomic, strong) NKKVOLabel *moneyLabel;
@property (nonatomic, strong) NKKVOLabel *positionMoneyLabel;

@property (nonatomic, strong) UILabel    *total;
@property (nonatomic, strong) UILabel    *usable;

@property (nonatomic, strong) UIButton   *charge;
@property (nonatomic, strong) UIButton   *withdraw;

@end

@implementation NKMyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"钱包";
    [self addRightButtonWithTitle:@"明细"];
    
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 500)];
    
    UILabel *zongzichan = [[UILabel alloc] initWithFrame:CGRectMake(10, 12, NKMainWidth-20, 20)];
    [self.showTableView.tableHeaderView addSubview:zongzichan];
    zongzichan.font = [UIFont boldSystemFontOfSize:19];
    zongzichan.textColor = [UIColor colorWithHexString:@"#989899"];
    zongzichan.text = @"总资产:";
    
    self.total = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(zongzichan.frame)+15, CGRectGetWidth(zongzichan.frame), 30)];
    [self.showTableView.tableHeaderView addSubview:_total];
    _total.textColor = [NKMShares riseColor];
    _total.font = [UIFont boldSystemFontOfSize:28];
    _total.textAlignment = NSTextAlignmentCenter;
    
    self.moneyLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_total.frame)+15, NKMainWidth/3, 40)];
    [self.showTableView.tableHeaderView addSubview:_moneyLabel];
    
    [self normalizeLabel:_moneyLabel];
    
    _moneyLabel.target = self;
    _moneyLabel.renderMethod = @selector(render:label:);
    _moneyLabel.useAttributedText = YES;
    
    CGRect moneyLabelFrame = _moneyLabel.frame;
    moneyLabelFrame.origin.x += NKMainWidth/3;
    
    self.positionMoneyLabel = [[NKKVOLabel alloc] initWithFrame:moneyLabelFrame];
    [self.showTableView.tableHeaderView addSubview:_positionMoneyLabel];
    [self normalizeLabel:_positionMoneyLabel];
    
    _positionMoneyLabel.target = self;
    _positionMoneyLabel.renderMethod = @selector(render:label:);
    _positionMoneyLabel.useAttributedText = YES;
    
    
    CGRect positionFrame = _positionMoneyLabel.frame;
    positionFrame.origin.x += NKMainWidth/3;
    self.usable = [[UILabel alloc] initWithFrame:positionFrame];
    [self.showTableView.tableHeaderView addSubview:_usable];
    
    [self normalizeLabel:_usable];
    
    [_moneyLabel bindValueOfModel:[NKMUser me] forKeyPath:@"wallet.money"];
    [_positionMoneyLabel bindValueOfModel:[NKMUser me] forKeyPath:@"wallet.sharesMoney"];
    
    self.charge = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_moneyLabel.frame)+20, NKMainWidth-20, 42)];
    [self.showTableView.tableHeaderView addSubview:_charge];
    [_charge setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#3b7eee"]] forState:UIControlStateNormal];
    [_charge addTarget:self action:@selector(chargeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_charge setTitle:@"充值" forState:UIControlStateNormal];
    _charge.layer.cornerRadius = 2.0;
    _charge.clipsToBounds = YES;
    
    self.withdraw = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_charge.frame)+10, NKMainWidth-20, 42)];
    [self.showTableView.tableHeaderView addSubview:_withdraw];
    [_withdraw setBackgroundImage:[UIImage yy_imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [_withdraw setTitleColor:[NKMShares normalColor] forState:UIControlStateNormal];
    [_withdraw setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [_withdraw addTarget:self action:@selector(withdrawClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_withdraw setTitle:@"提现" forState:UIControlStateNormal];
    _withdraw.layer.cornerRadius = 2.0;
    _withdraw.layer.borderWidth = 0.5;
    _withdraw.layer.borderColor = [[NKMShares normalColor] CGColor];
    _withdraw.clipsToBounds = YES;
}

-(void)rightButtonClick:(id)sender{
    
    NKFlowViewController *viewController = [NKFlowViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)withdrawClicked:(id)sender{
    
    [self showAlertWithMessage:@"暂不支持"];
}

-(void)normalizeLabel:(UILabel*)label{
    label.font = [UIFont boldSystemFontOfSize:13];
    label.numberOfLines = 2;
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
}

-(void)setTotalText{
    NKMWallet *wallet = [[NKMUser me] wallet];
    _total.text = [NSString stringWithFormat:@"%.2f", wallet.money.floatValue+wallet.sharesMoney.floatValue];
}

-(id)render:(NSNumber*)money label:(NKKVOLabel*)label{
    
    if (label==_moneyLabel) {
        NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"账户资金\n%.2f", [money floatValue]]];
        [attribute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#989899"] range:NSMakeRange(0, 4)];
        [attribute addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:NSMakeRange(0, 4)];
        [self setTotalText];
        NKMWallet *wallet = [[NKMUser me] wallet];
        NSMutableAttributedString *usable = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"可用资金\n%.2f", [money floatValue]- wallet.orderMoney.floatValue]];
        [usable addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#989899"] range:NSMakeRange(0, 4)];
        [usable addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:NSMakeRange(0, 4)];
        _usable.attributedText = usable;
        
        
        return attribute;
    }
    else if (label == _positionMoneyLabel){
        NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"股票市值\n%.2f", [money floatValue]]];
        [attribute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#989899"] range:NSMakeRange(0, 4)];
        [attribute addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:NSMakeRange(0, 4)];
        [self setTotalText];
        return attribute;
        
    }
    
    
    return [NSString stringWithFormat:@"%.2f", money.floatValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NKOrderService sharedNKOrderService] updateWalletInfo];
}

-(void)chargeClicked:(id)sender{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(addOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKOrderService sharedNKOrderService] addMoneyWithMoney:[NSNumber numberWithFloat:100] andRequestDelegate:rd];
    
}
-(void)addOK:(NKRequest*)request{
    
    NKMWallet *wallet = [request.results lastObject];
    NKMUser *me = [NKMUser me];
    me.wallet = wallet;
    [self showAlertWithMessage:@"100元充值成功"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
