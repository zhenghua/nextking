//
//  NKOrderCell.h
//  NEXTKING
//
//  Created by King on 16/7/14.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewCell.h"

typedef NS_ENUM(NSInteger, NKOrderCellType) {
    NKOrderCellTypeHistory,
    NKOrderCellTypeHandicap
};


@interface NKOrderCell : NKTableViewCell

@property (nonatomic, assign) NKOrderCellType type;

@end
