//
//  KYCModel.m
//  NEXTKING
//
//  Created by King on 17/3/9.
//  Copyright © 2017年 jinzhenghua. All rights reserved.
//

#import "KYCModel.h"
#import "NSDate+YYAdd.h"



NSString *const KYCRecordTypeMessage = @"message";
NSString *const KYCRecordTypeConversation = @"conversation";
NSString *const KYCRecordTypeGroup = @"group";
NSString *const KYCRecordTypeCalculate = @"calculate";

NSString *const KYCRecordKeyYear = @"year";
NSString *const KYCRecordKeyIncome = @"income";
NSString *const KYCRecordKeyMobile = @"mobile";
NSString *const KYCRecordKeyPin = @"pin";

@implementation KYCModel

@end

@implementation KYCUser

+(instancetype)modelFromDic:(NSDictionary *)dic{
    KYCUser *model = [super modelFromDic:dic];
    NKBindValueWithKeyForParameterFromDic(@"name", model.name, dic);
    NKBindValueWithKeyForParameterFromDic(@"avatar", model.avatarPath, dic);
    return model;
}

-(NSString*)showName{
    return [self name];
}

@end

@implementation KYCAttachment


@end

@implementation KYCOption

+(instancetype)modelFromDic:(NSDictionary *)dic{
    
    KYCOption *model = [super modelFromDic:dic];
    
    NKBindValueWithKeyForParameterFromDic(@"score", model.score, dic);
    NKBindValueWithKeyForParameterFromDic(@"response", model.response, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"min", model.min, dic);
    NKBindValueWithKeyForParameterFromDic(@"max", model.max, dic);
    
    return model;

}

-(NSNumber*)min{
    
    if (!_min) {
        _min = [NSNumber numberWithFloat:CGFLOAT_MIN];
    }
    return _min;
}

-(NSNumber*)max{
    
    if (!_max) {
        _max = [NSNumber numberWithFloat:CGFLOAT_MAX];
    }
    return _max;
}



@end


@implementation KYCRecord

+(instancetype)modelFromDic:(NSDictionary *)dic{
    
    KYCRecord *model = [super modelFromDic:dic];
    
    NSDictionary *userDic = [dic objectOrNilForKey:@"user"];
    model.sender = [KYCUser modelFromDic:userDic];
    
    NKBindValueWithKeyForParameterFromDic(@"title", model.title, dic);
    NKBindValueWithKeyForParameterFromDic(@"content", model.content, dic);
    NKBindValueWithKeyForParameterFromDic(@"type", model.type, dic);
    NKBindValueWithKeyForParameterFromDic(@"parent_id", model.parentID, dic);
    NKBindValueWithKeyForParameterFromDic(@"next_id", model.nextID, dic);
    NKBindValueWithKeyForParameterFromDic(@"think_time", model.thinkTime, dic);
    NKBindValueWithKeyForParameterFromDic(@"type_time", model.typeTime, dic);
    NKBindValueWithKeyForParameterFromDic(@"input_type", model.inputType, dic);
    NKBindValueWithKeyForParameterFromDic(@"key", model.key, dic);
    NKBindValueWithKeyForParameterFromDic(@"relation_key", model.relationKey, dic);
    NKBindValueWithKeyForParameterFromDic(@"ratio", model.ratio, dic);
    NKBindValueWithKeyForParameterFromDic(@"calculate_keys", model.calculateKeys, dic);
    
    NKBindValueWithKeyForParameterFromDic(@"status", model.status, dic);

    NKBindValueWithKeyForParameterFromDic(@"create_time", model.createTime, dic);
    
    if (model.createTime) {
        model.timeString = [NSDate stringWithTimelineDate:[NSDate dateWithTimeIntervalSince1970:[model.createTime longLongValue]]];
    }
    
    NKBindValueWithKeyForParameterFromDic(@"error_answer_response", model.errorAnswerResponse, dic);
    
    // Attachments
    NSArray *tattach = [dic objectOrNilForKey:@"attachments"];
    if ([tattach count]) {
        NSMutableArray *attacht = [NSMutableArray arrayWithCapacity:[tattach count]];
        
        for (NSDictionary *tatt in tattach) {
            [attacht addObject:[KYCAttachment modelFromDic:tatt]];
        }
        model.attachments = attacht;
    }
    
    NSArray *optionsArray = [dic objectOrNilForKey:@"options"];
    if ([optionsArray count]) {
        
        model.options = [NSMutableArray arrayWithCapacity:[optionsArray count]];
        for(NSDictionary *optionDic in optionsArray){
            [model.options addObject:[KYCOption modelFromDic:optionDic]];
            
        }
    }
    
    NSArray *recordsArray = [dic objectOrNilForKey:@"records"];
    if ([recordsArray count]) {
        
        model.records = [NSMutableArray arrayWithCapacity:[recordsArray count]];
        for (NSDictionary *recordDic in recordsArray) {
            [model.records addObject:[KYCRecord modelFromDic:recordDic]];
        }
        
    }
    
    return model;
    
}

-(NSMutableArray*)errorAnswerResponse{
    
    if (!_errorAnswerResponse) {
        _errorAnswerResponse = [NSMutableArray arrayWithObjects:@"别闹!", @"请乖乖回答Neo的问题", @"SB", nil];
    }
    
    return _errorAnswerResponse;
}

-(NSNumber*)thinkTime{
    
    if (!_thinkTime) {
        _thinkTime = [NSNumber numberWithFloat:1.0];
    }
    return _thinkTime;
}

-(NSNumber*)typeTime{
    
    if (!_typeTime) {
        _typeTime = [NSNumber numberWithFloat:2.0];
    }
    return _typeTime;
}

-(BOOL)needShowOption{
    
    if ([self.inputType integerValue] == KYCInputTypeChoice && self.option == nil && self.options.count>0) {
        return YES;
    }
    
    return NO;
}

+(instancetype)messageRecordWithMessage:(NSString*)message{
    
    NSDictionary *neo = @{@"id":@"1", @"name":@"Neo", @"avatar":@""};
    return [self modelFromDic:@{@"id":@"10001",
                                @"user":neo,
                                @"content":message,
                                @"input_type":[NSNumber numberWithInteger:KYCInputTypeNoInput],
                                @"think_time":@0.3,
                                @"type_time":@0.5
                                }];
    
}

@end
