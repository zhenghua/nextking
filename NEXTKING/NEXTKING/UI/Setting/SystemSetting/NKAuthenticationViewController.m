//
//  NKAuthenticationViewController.m
//  NEXTKING
//
//  Created by King on 16/7/26.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKAuthenticationViewController.h"
#import "NKAccountService.h"

#import "NKTextInputViewController.h"
#import "NKRecordService.h"


@interface NKMAuthentication : NKModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *identityCode;

@property (nonatomic, strong) UIImage  *photoOfID;

@end

@implementation NKMAuthentication

@end



@interface NKAuthenticationViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) NKMAuthentication *authentication;
@property (nonatomic, strong) NKImageView *photo;

@end


@implementation NKAuthenticationViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.authentication = [NKMAuthentication new];
    }
    return self;
    
}

-(UITableViewStyle)tableViewStyle{
    return UITableViewStyleGrouped;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = @"实名认证";
   
    [self addRightButtonWithTitle:@"取消"];
    self.nkRightButton.hidden = YES;
    
    
    [self setupView];
    
}

-(void)resubmintButtonClick:(id)sender{
    
    [[NKMUser me] setVerify:[NSNumber numberWithInteger:0]];
    [self setupView];
    
}

-(void)setupView{
    
    NSInteger status = [[[NKMUser me] verify] integerValue];
    
    if (status==0) {
    
        self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 10)];
        NKCellDataObject *name = [NKCellDataObject cellDataObjectWithTitle:@"姓名" titleImage:nil detailText:@"请输入真实姓名" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(cellClicked:)];
        name.keyPath = @"name";
        
        NKCellDataObject *idCode = [NKCellDataObject cellDataObjectWithTitle:@"身份证号" titleImage:nil detailText:@"请输入身份证号码" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(cellClicked:)];
        idCode.keyPath = @"identityCode";
        
        self.dataSource = [NSMutableArray arrayWithObjects:
                           @[name, idCode],
                           nil];
        [self.showTableView reloadData];
        [self setupFooter];
        
    }
    else if (status == 1 || status == -1) {
    
        self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, self.showTableView.height)];
        UILabel *label = [[UILabel alloc] initWithFrame:self.showTableView.tableHeaderView.bounds];
        [self.showTableView.tableHeaderView addSubview:label];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = status ==1 ? @"等待审核中，一般为2个工作日":@"认证失败, 请提交正确的资料";
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 42)];
        [self.showTableView.tableHeaderView addSubview:button];
        [button addTarget:self action:@selector(resubmintButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"点击重新提交审核" forState:UIControlStateNormal];
        button.center = CGPointMake(button.centerX, ceilf(self.showTableView.height/2)-40);
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        self.dataSource = nil;
        [self.showTableView reloadData];
        self.showTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 1)];
        
    }
    else if (status == 2){
    
        self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, self.showTableView.height)];
        UILabel *label = [[UILabel alloc] initWithFrame:self.showTableView.tableHeaderView.bounds];
        [self.showTableView.tableHeaderView addSubview:label];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"已实名认证";
        
    }
    
}

-(void)setupFooter{
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 362)];
    self.showTableView.tableFooterView = footer;
    
    UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, NKMainWidth, 246)];
    photoView.backgroundColor = [UIColor whiteColor];
    [footer addSubview:photoView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 18, NKMainWidth, 15)];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"点击上传本人手持身份证照片";
    [photoView addSubview:label];
    
    NKImageView *photo = [[NKImageView alloc] initWithImage:[UIImage imageNamed:@"verify_placeholder"]];
    [photoView addSubview:photo];
    photo.contentMode = UIViewContentModeScaleAspectFit;
    photo.clipsToBounds = YES;
    photo.center = CGPointMake(NKMainWidth/2, 140);
    photo.target = self;
    photo.singleTapped = @selector(photoTapped:);
    self.photo = photo;
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, photoView.bottom+14, NKMainWidth-20, 14)];
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = [UIColor darkGrayColor];
    label.text = @"照片需保证所有信息清晰可见";
    [footer addSubview:label];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, photoView.bottom+42, NKMainWidth-20, 42)];
    [footer addSubview:button];
    [button setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#0077ee"]] forState:UIControlStateNormal];
    [button setTitle:@"提交资料" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)submitClicked:(id)sender{
    
    if (!self.authentication.name) {
        [self showAlertWithMessage:@"请输入真实姓名"];
        return;
    }
    if (!self.authentication.identityCode) {
        [self showAlertWithMessage:@"请输入身份证号码"];
        return;
    }
    if (!self.authentication.photoOfID) {
        [self showAlertWithMessage:@"请选择本人手持身份证照片"];
        return;
    }
    
    [NKProgressHUD showTextHudInView:self.view text:@"正在提交"];
    
    NSString *objectKey = [[NKFileManager fileManager] ObjectKeyForImage:self.authentication.photoOfID];
    
    [[NKFileManager fileManager] uploadWithObjectKey:objectKey block:^id(OSSTask *task) {
        
        if (!task.error) {
            
            NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(submitOK:)];
            [[NKRecordService sharedNKRecordService] addRecordWithTitle:self.authentication.name content:self.authentication.identityCode attachmentDescription:nil objectKey:objectKey parentID:nil type:NKRecordTypeUserVerify andRequestDelegate:rd];
        }
        else{
            [self performSelectorOnMainThread:@selector(showAlertWithMessage:) withObject:task.error waitUntilDone:NO];
        }
        return nil;
    }];
}

-(void)submitOK:(NKRequest*)request{
    
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    [[NKMUser me] setVerify:[NSNumber numberWithInteger:1]];
    [self setupView];
    
}

-(void)photoTapped:(UIGestureRecognizer*)gesture{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"从手机相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
}

-(void)showImagePickerWithType:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerControllerSourceType sourceType = [UIImagePickerController isSourceTypeAvailable:type]?type:UIImagePickerControllerSourceTypePhotoLibrary;
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
    imagePicker.delegate = self;
    imagePicker.sourceType = sourceType;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    self.authentication.photoOfID = image;
    self.photo.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)cellClicked:(NKCellDataObject*)object{
    
    NKTextInputViewController *viewController = [NKTextInputViewController new];
    viewController.model = self.authentication;
    viewController.keyPath = object.keyPath;
    viewController.titleText = object.title;
    viewController.placeholder = object.detailText;
    [NKNC pushViewController:viewController animated:YES];
    
}

-(NSString*)render:(NSString*)value label:(NKKVOLabel*)label{
    
    if (!value) {
        label.textColor = [UIColor lightGrayColor];
        
        for (NKCellDataObject *object in self.dataSource[0]) {
            if ([label.keyPath isEqualToString:object.keyPath]) {
                return object.detailText;
            }
        }
    }
    
    label.textColor = [UIColor blackColor];
    return value;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NKTableViewCell cellHeightForObject:nil];;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"SettingCellIdentifier";
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.detailLabel.target = self;
        cell.detailLabel.renderMethod = @selector(render:label:);
    }
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    [cell showForObject:object withIndex:indexPath];
    [cell.detailLabel bindValueOfModel:self.authentication forKeyPath:object.keyPath];
    
    return cell;
}


@end
