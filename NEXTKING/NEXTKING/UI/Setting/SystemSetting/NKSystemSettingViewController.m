//
//  NKSystemSettingViewController.m
//  NEXTKING
//
//  Created by King on 16/7/5.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSystemSettingViewController.h"
#import "NKAccountManager.h"

#import "NKAuthenticationViewController.h"
#import "NKAboutViewController.h"


@interface NKSystemSettingViewController ()

@end

@implementation NKSystemSettingViewController

-(UITableViewStyle)tableViewStyle{
    
    return UITableViewStyleGrouped;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"设置";

    
    NKCellDataObject *logout = [NKCellDataObject cellDataObjectWithTitle:@"退出" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(logoutButtonClicked:)];
    
    NKCellDataObject *about = [NKCellDataObject cellDataObjectWithTitle:@"关于" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(aboutClicked:)];
    
    NKCellDataObject *style = [NKCellDataObject cellDataObjectWithTitle:@"显示设置" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(styleClicked:)];
    
    NKCellDataObject *verify = [NKCellDataObject cellDataObjectWithTitle:@"身份认证" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(verifyClicked:)];
    verify.keyPath = @"verify";
    
    self.dataSource = [NSMutableArray arrayWithObjects:
                       @[verify],
                       @[style],
                       @[about],
                       @[logout],
                       nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)styleClicked:(id)sender{
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"标准" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [NKStyle updateStyleWithStyle:0];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"极简" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [NKStyle updateStyleWithStyle:1];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
}

-(void)logoutButtonClicked:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定退出" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[NKUI sharedNKUI] logout];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
}

-(void)aboutClicked:(id)sender{
    
    NKAboutViewController *viewController = [NKAboutViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)verifyClicked:(id)sender{
    
    NKAuthenticationViewController *viewController = [NKAuthenticationViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(NSString*)render:(NSNumber*)value label:(NKKVOLabel*)label{
    
    NSInteger status = [value integerValue];
    
    if (status==0) {
        return @"未认证";
    }
    else if (status == -1) {
        return @"认证失败";
    }
    else if (status == 1) {
        return @"等待审核";
    }
    else if (status == 2){
        return @"已认证";
    }
    
    return nil;
}

#pragma mark TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NKTableViewCell cellHeightForObject:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"SettingCellIdentifier";
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
    }
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    [cell showForObject:object withIndex:indexPath];
    if (object.keyPath) {
        cell.detailLabel.target = self;
        cell.detailLabel.renderMethod = @selector(render:label:);
        [cell.detailLabel bindValueOfModel:[NKMUser me] forKeyPath:object.keyPath];
    }
    
    
    return cell;
}


@end
