//
//  NKAboutViewController.m
//  NEXTKING
//
//  Created by King on 16/7/26.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKAboutViewController.h"

@implementation NKAboutViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addBackButton];
    self.titleLabel.text = @"关于";
    
    NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
    NSString *icons = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
    UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icons]];
    [self.contentView addSubview:icon];
    icon.center = CGPointMake(NKMainWidth/2, NKMainHeight/2-100);
    
    NSString *version = [NSString stringWithFormat:@"V %@", [infoPlist valueForKey:@"CFBundleShortVersionString"]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, icon.bottom+10, NKMainWidth, 15)];
    label.text = version;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:label];
    
}

@end
