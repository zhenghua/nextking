//
//  KYCAI.m
//  NEXTKING
//
//  Created by King on 2017/3/10.
//  Copyright © 2017年 jinzhenghua. All rights reserved.
//

#import "KYCAI.h"



@interface NSArray (Random)

-(id)randomObject;

@end

@implementation NSArray (Random)

-(id)randomObject{
    
    if (self.count>0) {
        return [self objectAtIndex:arc4random()%self.count];
    }
    return nil;
    
}

@end



@interface KYCAI ()

//@property (nonatomic, strong) KYCRecord *yearRecord;

@end



@implementation KYCAI


-(instancetype)init{
    
    self = [super init];
    if (self) {
        
        self.status = KYCNeoStatusIdle;
        
        NSDictionary *neo = @{@"id":@"1", @"name":@"Neo", @"avatar":@""};
        
        NSArray *json = @[@{@"id":@"1",
                            @"user":neo,
                            @"content":@"Boss，你好，我是机器人Neo。开启财富新时代",
                            @"type_time":@1.0,
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNoInput],
                            @"next_id":@"2"
                            },
                          @{@"id":@"2",
                            @"user":neo,
                            @"think_time":@0.5,
                            @"type_time":@1,
                            @"content":@"是哪一年出生的？（回复数字，比如88代表1988年）",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNumber],
                            @"next_id":@"3",
                            @"key":KYCRecordKeyYear,
                            @"error_answer_response":@[@"到底是哪一年？", @"请输入是哪一年", @"回复数字，比如88代表1988年"],
                            @"options":@[@{@"min":@1981.9, @"max":@2002.0, @"response":@[@"正是合适的年纪"]},
                                         @{@"min":@1952.0, @"max":@1982.0, @"response":@[@"比Neo资深！"]}
                                         ]
                            },
                          @{@"id":@"3",
                            @"user":neo,
                            @"content":@"收入一年大约多少万？（回复数字表示几万）",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNumber],
                            @"next_id":@"4",
                            @"key":KYCRecordKeyIncome,
                            @"error_answer_response":@[@"是钱！"],
                            @"options":@[@{@"max":@50.0, @"response":@[@"潜力还很大！"]},
                                         @{@"min":@49.9, @"max":@10000.0, @"response":@[@"土豪！"]},
                                         @{@"min":@9999.9, @"response":@[@"一定是王健林吧！"]}
                                         ]
                            },
                          @{@"id":@"24",
                            @"user":neo,
                            @"content":@"职业？             ",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                            @"next_id":@"5",
                            @"options":@[@{@"title":@"金融行业"}, @{@"title":@"非金融行业"}]
                            },
                          @{@"id":@"4",
                            @"user":neo,
                            @"content":@"自我评价风险偏好？",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                            @"next_id":@"5",
                            @"options":@[@{@"title":@"保守型", @"score":@1}, @{@"title":@"稳健型", @"score":@2}, @{@"title":@"成长型", @"score":@1}, @{@"title":@"激进型", @"score":@3}]
                            },
                          @{@"id":@"5",
                            @"user":neo,
                            @"content":@"邀请参加专业的风险偏好测试",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNoInput],
                            @"next_id":@"6"
                            },
                          @{@"id":@"6", @"relation_key":KYCRecordKeyYear, @"type":KYCRecordTypeGroup, @"records":@[
                                    @{@"id":@"6",
                                      @"user":neo,
                                      @"key":@"IE",
                                      @"content":@"下面哪些名词是接触过或者熟悉的？",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"7",
                                      @"ratio":@0.25,
                                      @"options":@[@{@"title":@"陆金所", @"score":@2}, @{@"title":@"雪球社区", @"score":@2}, @{@"title":@"蛋卷基金", @"score":@2}, @{@"title":@"纳斯达克", @"score":@2}, @{@"title":@"景顺", @"score":@2}]
                                      },
                                    @{@"id":@"6",
                                      @"user":neo,
                                      @"key":@"IE",
                                      @"content":@"下面哪些名词是接触过或者熟悉的？",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"7",
                                      @"ratio":@0.25,
                                      @"options":@[@{@"title":@"信托", @"score":@2}, @{@"title":@"飞乐音响(600651)", @"score":@2}, @{@"title":@"交银施罗德", @"score":@2}, @{@"title":@"标普", @"score":@2}, @{@"title":@"黑石资本", @"score":@2}]
                                      }
                                    ]},
                          @{@"id":@"9",
                            @"user":neo,
                            @"content":@"投资动因                                          ",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                            @"next_id":@"10",
                            @"key":@"IM",
                            @"ratio":@0.25,
                            @"options":@[@{@"title":@"获取投资组合最高回报", @"score":@10}, @{@"title":@"收益高于银行中长期理财产品", @"score":@8}, @{@"title":@"跑赢通胀", @"score":@4}, @{@"title":@"收益高于银行存款利率", @"score":@2}]
                            },
                          @{@"id":@"6", @"relation_key":@"IM", @"type":KYCRecordTypeGroup, @"records":@[
                                    @{@"id":@"6",
                                      @"user":neo,
                                      @"key":@"P",
                                      @"content":@"投资时长              ",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"7",
                                      @"ratio":@0.25,
                                      @"options":@[@{@"title":@"1-3", @"score":@10}, @{@"title":@"3-5", @"score":@8}, @{@"title":@"5-10", @"score":@6}, @{@"title":@">10", @"score":@0}]
                                      },
                                    @{@"id":@"6",
                                      @"user":neo,
                                      @"key":@"P",
                                      @"content":@"投资时长              ",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"7",
                                      @"ratio":@0.25,
                                      @"options":@[@{@"title":@"1-3", @"score":@10}, @{@"title":@"3-5", @"score":@8}, @{@"title":@"5-10", @"score":@6}, @{@"title":@">10", @"score":@0}]
                                      },
                                    @{@"id":@"6",
                                      @"user":neo,
                                      @"key":@"P",
                                      @"content":@"投资时长              ",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"7",
                                      @"ratio":@0.25,
                                      @"options":@[@{@"title":@"<1", @"score":@10}, @{@"title":@"1-3", @"score":@8}, @{@"title":@"3-5", @"score":@6}, @{@"title":@">5", @"score":@0}]
                                      },
                                    @{@"id":@"6",
                                      @"user":neo,
                                      @"key":@"P",
                                      @"content":@"投资时长              ",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"7",
                                      @"ratio":@0.25,
                                      @"options":@[@{@"title":@"<1", @"score":@10}, @{@"title":@"1-3", @"score":@8}, @{@"title":@"3-5", @"score":@6}, @{@"title":@">5", @"score":@0}]
                                      }
                                    ]},
                          @{@"id":@"6", @"relation_key":KYCRecordKeyYear, @"type":KYCRecordTypeGroup, @"records":@[
                                    @{@"id":@"8",
                                      @"user":neo,
                                      @"key":@"IC",
                                      @"content":@"在2015年那场著名的A股股灾中，你和市场的表现如图（两条曲线图），你的选择是？",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"9",
                                      @"ratio":@0.125,
                                      @"options":@[@{@"title":@"全部卖出", @"score":@0}, @{@"title":@"卖出一部分", @"score":@4}, @{@"title":@"买入更多", @"score":@6}, @{@"title":@"不动", @"score":@8}],
                                      @"attachments":@[@{@"thumbnail":@"http://shareskong.oss-cn-shanghai.aliyuncs.com/run.png"}]
                                      },
                                    @{@"id":@"8",
                                      @"user":neo,
                                      @"key":@"IC",
                                      @"content":@"在2008年那场股灾中，你和市场的表现如图(两条曲线图），你的选择是？",
                                      @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                                      @"next_id":@"9",
                                      @"ratio":@0.125,
                                      @"options":@[@{@"title":@"全部卖出", @"score":@0}, @{@"title":@"卖出一部分", @"score":@4}, @{@"title":@"买入更多", @"score":@6}, @{@"title":@"不动", @"score":@8}],
                                      @"attachments":@[@{@"thumbnail":@"http://shareskong.oss-cn-shanghai.aliyuncs.com/run.png"}]
                                      }
                                    ]},
                          @{@"id":@"7",
                            @"user":neo,
                            @"content":@"假设现在操盘1亿元的基金，你的投资风格会如何？",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                            @"next_id":@"8",
                            @"options":@[@{@"title":@"彼得林奇式", @"content":@"彼得林奇式为投资者指出了分析基金收益对比的一个基本原则，就是投资者比较基金收益的差异，要基于同一投资风格或投资类型，而不能简单的只看收益率", @"score":@1}, @{@"title":@"巴菲特式", @"content":@"彼得林奇式为投资者指出了分析基金收益对比的一个基本原则，就是投资者比较基金收益的差异，要基于同一投资风格或投资类型，而不能简单的只看收益率", @"score":@2}, @{@"title":@"索罗斯式", @"content":@"彼得林奇式为投资者指出了分析基金收益对比的一个基本原则，就是投资者比较基金收益的差异，要基于同一投资风格或投资类型，而不能简单的只看收益率", @"score":@1}]
                            },
                          @{@"id":@"9",
                            @"user":neo,
                            @"key":@"IB",
                            @"content":@"下面几种方案选择一种最喜欢的？",
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeChoice],
                            @"next_id":@"10",
                            @"ratio":@0.125,
                            @"options":@[@{@"title":@"7%，16%，-5%", @"score":@0}, @{@"title":@"9%，25%，-12%", @"score":@4}, @{@"title":@"10%，33%，-18%", @"score":@6}, @{@"title":@"12%，50%，-28%", @"score":@8}]
                            },
                          @{@"id":@"10",
                            @"user":neo,
                            @"content":@"你的风险测评结果正在计算中...",
                            @"type_time":@0,
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNoInput],
                            @"next_id":@"11",
                            
                            },
                          @{@"id":@"10",
                            @"user":neo,
                            @"type":KYCRecordTypeCalculate,
                            @"content":@"",
                            @"calculate_keys":@[@"IE", @"IM", @"P", @"IC", @"IB"],
                            @"type_time":@0,
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNoInput],
                            @"next_id":@"11",
                            
                            },
                          @{@"id":@"11",
                            @"user":neo,
                            @"content":@"投顾已配置好方案，邀请注册，手机号是？",
                            @"key":KYCRecordKeyMobile,
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNumber],
                            @"next_id":@"12",
                            
                            },
                          
                          @{@"id":@"12",
                            @"user":neo,
                            @"content":@"验证码是？",
                            @"key":KYCRecordKeyPin,
                            @"type_time":@0.5,
                            @"input_type":[NSNumber numberWithInteger:KYCInputTypeNumber],
                            @"next_id":@"13",
                            
                            }
                          ];
        
//        NSString *string = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
//        
//        NSLog(@"%@", string);
        
        self.messagesArray = [NSMutableArray array];
        for (NSDictionary *recordDic in json) {
            [self.messagesArray addObject:[KYCRecord modelFromDic:recordDic]];
        }
        
        
        
    }
    return self;
}


-(void)start{

    self.status = KYCNeoStatusThinking;
    
    if (!self.currentRecord) {
        self.currentRecord = [self.messagesArray objectAtIndex:0];
    }
    
    [self tellDelegate];
    
}

-(void)tellDelegate{
    
    [self tellDelegateWithRecord:self.currentRecord];
}

-(void)tellDelegateWithRecord:(KYCRecord*)record{
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(kyc:didResponseWithRecord:)]) {
        [self.delegate kyc:self didResponseWithRecord:record];
    }
    
}

- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

//判断是否为浮点形：

- (BOOL)isPureFloat:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

-(void)processMessage:(NSString*)message{
    
    //处理消息逻辑
    if ([self.currentRecord.inputType integerValue] == KYCInputTypeChoice) {
        if (!self.currentRecord.option) {
            
            for (KYCOption *option in self.currentRecord.options) {
                if ([message isEqualToString:option.title] ) {
                    self.currentRecord.option = option;
                    if (self.delegate && [self.delegate respondsToSelector:@selector(kyc:didChooseOptionWithRecord:)]) {
                        [self.delegate kyc:self didChooseOptionWithRecord:self.currentRecord];
                    }
                    break;
                }
            }
            
            if (!self.currentRecord.option) {
                [self tellDelegateWithRecord:[KYCRecord messageRecordWithMessage:[self.currentRecord.errorAnswerResponse randomObject]]];
                return;
            }
        }
        else{
            
        }
        
    }
    else if ([self.currentRecord.inputType integerValue] == KYCInputTypeNumber){
        
        if ([self.currentRecord.key isEqualToString:KYCRecordKeyYear]) {
            if (message.length == 2) {
                if (message.floatValue<20) {
                    message = [NSString stringWithFormat:@"20%@", message];
                }
                else{
                    message = [NSString stringWithFormat:@"19%@", message];
                }
            }
            
        }else if ([self.currentRecord.key isEqualToString:KYCRecordKeyIncome]) {
            
            if( ![self isPureInt:message] || ![self isPureFloat:message]){
                [self tellDelegateWithRecord:[KYCRecord messageRecordWithMessage:[self.currentRecord.errorAnswerResponse randomObject]]];
                return;
            }
        }
        
        for (KYCOption *option in self.currentRecord.options) {
            CGFloat year = [message floatValue];
            if (year>option.min.floatValue && year<option.max.floatValue ) {
                self.currentRecord.option = option;
                break;
            }
        }
        if (!self.currentRecord.option) {
            [self tellDelegateWithRecord:[KYCRecord messageRecordWithMessage:[self.currentRecord.errorAnswerResponse randomObject]]];
            return;
        }
        else{
            if (self.currentRecord.option.response) {
                [self tellDelegateWithRecord:[KYCRecord messageRecordWithMessage:self.currentRecord.option.response.randomObject]];
            }
        }

    }
    
    
    self.currentRecord.answer = message;
    
    if ([self.messagesArray lastObject] == self.currentRecord) {
        return;
    }
    
    self.currentRecord = [self.messagesArray objectAtIndex:[self.messagesArray indexOfObject:self.currentRecord]+1];
    
    //Group
    if ([self.currentRecord.type isEqualToString:KYCRecordTypeGroup]) {
        
        KYCRecord *relationRecord = nil;
        NSString *key = self.currentRecord.relationKey;
        for (KYCRecord *record in self.messagesArray) {
            if ([record.key isEqualToString:key]) {
                relationRecord = record;
                break;
            }
        }
        
        KYCRecord *realRecord = [self.currentRecord.records objectAtIndex:relationRecord?[relationRecord.options indexOfObject:relationRecord.option]:0];
        [self.messagesArray replaceObjectAtIndex:[self.messagesArray indexOfObject:self.currentRecord] withObject:realRecord];
        self.currentRecord = realRecord;
        
    }
    else if ([self.currentRecord.type isEqualToString:KYCRecordTypeCalculate]){
        
        CGFloat result = 0.0;
        
        for (KYCRecord *record in self.messagesArray) {
            for (NSString *key in self.currentRecord.calculateKeys) {
                
                if ([record.key isEqualToString:key]) {
                    result += (record.ratio.floatValue * record.option.score.floatValue);
                    break;
                }
            }
        }
        
        self.currentRecord.content = [NSString stringWithFormat:@"%.2f", result];
    }
    
    self.status = KYCNeoStatusThinking;
    [self performSelector:@selector(tellDelegate) withObject:nil afterDelay:[self.currentRecord.thinkTime floatValue]];
    
}


-(void)setStatus:(KYCNeoStatus)status{
    
    _status = status;
    NSLog(@"Status %ld", (long)self.status);
    
}

@end
