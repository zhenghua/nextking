//
//  NKSettingViewMeCell.m
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSettingViewMeCell.h"
#import "NKImageView.h"
#import "NKKVOLabel.h"
#import "NKMUser.h"

@interface NKSettingViewMeCell ()

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) NKKVOLabel  *nameLabel;
@property (nonatomic, strong) UILabel     *idLabel;


@end



@implementation NKSettingViewMeCell

+(CGFloat)cellHeightForObject:(id)object{
    
    return 100;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKCellDataObject *model = (NKCellDataObject*)object;
    
    if ([model.accessoryView isKindOfClass:[NSNumber class]]) {
        self.accessoryType = [model.accessoryView integerValue];
    }
    
    NKMUser *me = [NKMUser me];
    [_nameLabel bindValueOfModel:me forKeyPath:@"name"];
    
    _idLabel.text = [NSString stringWithFormat:@"时空号: %@", me.modelID];
    
    [self.avatar bindValueOfModel:me forKeyPath:@"avatarPath"];
    
}

-(void)renderImage:(NSString*)path imageView:(NKImageView*)imageView{
    
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:path imageSize:_avatar.frame.size]];
    
    [self.avatar sd_setImageWithURL:imageURL placeholderImage:[UIImage yy_imageWithColor:[UIColor colorWithWhite:0.973 alpha:1.000]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image || error) {
            [self.avatar sd_setImageWithURL:[NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:path imageSize:CGSizeZero]]];
        }
    }];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(20, 20, 60, 60)];
        [self.contentView addSubview:_avatar];
        _avatar.target = self;
        _avatar.renderMethod = @selector(renderImage:imageView:);
        
        self.nameLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+20, CGRectGetMinY(_avatar.frame), 120, CGRectGetHeight(_avatar.frame)/2)];
        [self.contentView addSubview:_nameLabel];
        
        self.idLabel = [[UILabel alloc] initWithFrame:CGRectMake(_nameLabel.left, _nameLabel.bottom, _nameLabel.width, _nameLabel.height)];
        [self.contentView addSubview:_idLabel];
        _idLabel.font = [UIFont systemFontOfSize:15];
        _idLabel.textColor = [UIColor lightGrayColor];
        
    }
    return self;
}


@end
