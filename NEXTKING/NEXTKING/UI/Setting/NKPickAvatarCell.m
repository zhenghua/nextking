//
//  NKPickAvatarCell.m
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKPickAvatarCell.h"
#import "NKPhotoGroupView.h"
#import "NKMUser.h"

@interface NKPickAvatarCell ()

@property (nonatomic, strong) NKImageView *avatar;

@end

@implementation NKPickAvatarCell

+(CGFloat)cellHeightForObject:(id)object{
    
    return 80;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    [super showForObject:object withIndex:indexPath];
    
    self.detailLabel.target = self;
    self.detailLabel.renderMethod = @selector(render:label:);
    
    
}

-(NSString*)render:(NSString*)value label:(NKKVOLabel*)lable{
    
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:value imageSize:_avatar.frame.size]];
    
    [self.avatar sd_setImageWithURL:imageURL placeholderImage:[UIImage yy_imageWithColor:[UIColor colorWithWhite:0.973 alpha:1.000]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image || error) {
            [self.avatar sd_setImageWithURL:[NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:value imageSize:CGSizeZero]]];
        }
    }];
    _avatar.centerX = self.detailLabel.right-_avatar.width/2+4;
    
    return nil;
}


-(void)avatarTapped:(UIGestureRecognizer*)gesture{
    
    NKPhotoGroupItem *item = [NKPhotoGroupItem new];
    item.thumbView = [gesture view];
    item.largeImageSize = CGSizeMake(512, 512);
    item.largeImageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:[[NKMUser me] avatarPath] imageSize:CGSizeZero]];
    NKPhotoGroupView *v = [[NKPhotoGroupView alloc] initWithGroupItems:@[item]];
    [v presentFromImageView:[gesture view] toContainer:NKNC.view animated:YES completion:^{
        
    }];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //self.detailLabel.hidden = YES;
        
        self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
        [self.contentView addSubview:_avatar];
        
        self.avatar.target = self;
        self.avatar.singleTapped = @selector(avatarTapped:);

        
    }
    return self;
}


@end
