//
//  KYCViewController.h
//  NEXTKING
//
//  Created by King on 17/3/9.
//  Copyright © 2017年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"
#import "KYCAI.h"



@interface KYCViewController : NKTableViewController <UITextFieldDelegate, KYCAIDelegate>

@property (nonatomic, strong) UIView *typeView;
@property (nonatomic, strong) UITextField *inputField;

@property (nonatomic, strong) KYCAI *kyc;
@property (nonatomic, strong) KYCRecord *currentRecord;

-(void)chooseOption:(KYCOption*)option;

@end






