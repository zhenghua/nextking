//
//  NKFeedsManagerViewController.m
//  NEXTKING
//
//  Created by King on 16/8/15.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKFeedsManagerViewController.h"
#import "NKRecordCell.h"
#import "NKRecordService.h"

#import "NKFeedDetailViewController.h"

@implementation NKFeedsManagerViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self addBackButton];
    self.titleLabel.text = @"帖子管理";
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    [self.showTableView.header beginRefreshing];
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listAllRecordsWithoffset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)refreshDataOK:(NKRequest *)request{
    [super refreshDataOK:request];
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listAllRecordsWithoffset:self.dataSource.count size:20 andRequestDelegate:rd];
}


#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKRecordCell *cell = (NKRecordCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    cell.nameLabel.text = [cell.nameLabel.text stringByAppendingFormat:@" %@", object.status];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKFeedDetailViewController *viewController = [NKFeedDetailViewController new];
    viewController.record = self.dataSource[indexPath.row];
    //viewController.delegate = self;
    [NKNC pushViewController:viewController animated:YES];
}


@end
