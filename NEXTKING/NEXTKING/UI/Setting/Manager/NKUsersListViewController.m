//
//  NKUsersListViewController.m
//  NEXTKING
//
//  Created by King on 16/8/5.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKUsersListViewController.h"
#import "NKUserService.h"
#import "NKUserCell.h"
#import "NKProcessUserViewController.h"

@implementation NKUsersListViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = @"新用户";
    [self addRefreshHeader];
    [self addGetMoreFooter];
    [self.showTableView.header beginRefreshing];
    
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKUserService sharedNKUserService] listUsersWithOffset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKUserService sharedNKUserService] listUsersWithOffset:self.dataSource.count size:20 andRequestDelegate:rd];
}


#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKUserCell *cell = (NKUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKUserCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    NKMUser *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    cell.detailLabel.text = [NSString stringWithFormat:@"%@", object.status];
    if (object.timeString) {
        cell.detailLabel.text = [object.timeString stringByAppendingFormat:@" %@", cell.detailLabel.text];
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKUserCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKProcessUserViewController *viewController = [NKProcessUserViewController new];
    viewController.user = [self.dataSource objectAtIndex:indexPath.row];
    [NKNC pushViewController:viewController animated:YES];
    
}

@end
