//
//  NKManagerViewController.m
//  NEXTKING
//
//  Created by King on 16/8/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKManagerViewController.h"
#import "NKUserVerifyViewController.h"
#import "NKUsersListViewController.h"
#import "NKApplicantsViewController.h"
#import "NKOrderService.h"

#import "NKFeedsManagerViewController.h"

@implementation NKManagerViewController


-(UITableViewStyle)tableViewStyle{
    
    return UITableViewStyleGrouped;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"管理后台";
    
    NKCellDataObject *applicant = [NKCellDataObject cellDataObjectWithTitle:@"发行审批" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(applyClicked:)];
    NKCellDataObject *dealPrice = [NKCellDataObject cellDataObjectWithTitle:@"开收盘价" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(dealSharesPrice:)];
    NKCellDataObject *uerVerify = [NKCellDataObject cellDataObjectWithTitle:@"实名审核" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(userVerifyClicked:)];
    uerVerify.viewControllerName = NSStringFromClass([NKUserVerifyViewController class]);
    NKCellDataObject *listUser = [NKCellDataObject cellDataObjectWithTitle:@"用户" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(userVerifyClicked:)];
    listUser.viewControllerName = NSStringFromClass([NKUsersListViewController class]);
    
    NKCellDataObject *listFeeds = [NKCellDataObject cellDataObjectWithTitle:@"帖子管理" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(userVerifyClicked:)];
    listFeeds.viewControllerName = NSStringFromClass([NKFeedsManagerViewController class]);
    
    [self.dataSource addObject:@[applicant, dealPrice, uerVerify, listUser, listFeeds]];
    
}

-(void)userVerifyClicked:(NKCellDataObject*)object{
    id viewController = [NSClassFromString(object.viewControllerName) new];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)dealSharesPrice:(NKCellDataObject*)object{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"开盘价" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dealPriceWithOpen:YES];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"收盘价" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dealPriceWithOpen:NO];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)dealPriceWithOpen:(BOOL)open{
    
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(dealPriceOK:)];
    [[NKOrderService sharedNKOrderService] dealSharesPriceWithOpen:open requestDelegate:rd];
    
}

-(void)dealPriceOK:(NKRequest*)request{
    
    [self showAlertWithMessage:@"成功"];
}

-(void)applyClicked:(NKCellDataObject*)object{
    
    NKApplicantsViewController *viewController = [NKApplicantsViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}



#pragma mark TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NKTableViewCell cellHeightForObject:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"ManagerCellIdentifier";
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
    }
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
}

@end
