//
//  NKApplicantsViewController.m
//  NEXTKING
//
//  Created by King on 16/7/19.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKApplicantsViewController.h"
#import "NKOrderService.h"
#import "NKApplicantCell.h"
#import "NKWebViewController.h"


@implementation NKApplicantsViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addBackButton];
    self.titleLabel.text = @"发行审批";
    [self addRefreshHeader];
    
    [self.showTableView.header beginRefreshing];
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listApplyWithOffset:0 size:20 andRequestDelegate:rd];
    
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKSharesCellIdentifier";
    
    NKApplicantCell *cell = (NKApplicantCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKApplicantCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NKMShares *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKApplicantCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NKMShares *object = [self.dataSource objectAtIndex:indexPath.row];
    if ([object.status integerValue] == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"查看服务" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NKWebViewController *viewController = [NKWebViewController new];
            viewController.url = object.link;
            [NKNC pushViewController:viewController animated:YES];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"通过" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self makeApproveWithSharesID:object.modelID approve:YES];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"拒绝" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self makeApproveWithSharesID:object.modelID approve:NO];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
       
    }
}


-(void)makeApproveWithSharesID:(NSString*)sharesID approve:(BOOL)approve{
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(approveOK:)];
    [[NKOrderService sharedNKOrderService] approveWithSharesID:sharesID approve:approve requestDelegate:rd];
}

-(void)approveOK:(NKRequest*)request{
    
    [self refreshData];
}


@end
