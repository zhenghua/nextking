//
//  NKApplicantCell.m
//  NEXTKING
//
//  Created by King on 16/7/19.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKApplicantCell.h"
#import "NKMUser.h"

@interface NKApplicantCell ()

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *amount;

@end

@implementation NKApplicantCell

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    return 60;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMShares *model = (NKMShares*)object;
    
    self.name.text = model.name;
    self.amount.text = [NSString stringWithFormat:@"%@股 %.2f元/股 %@", model.totalAmount, model.price.floatValue, model.title];
    
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:model.avatar imageSize:_avatar.frame.size]];
    
    [_avatar sd_setImageWithURL:imageURL];
    
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        [self.contentView addSubview:_avatar];
        
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+15, CGRectGetMinY(_avatar.frame), NKMainWidth-CGRectGetMaxX(_avatar.frame)-15-15, CGRectGetHeight(_avatar.frame)/2)];
        [self.contentView addSubview:_name];
        _name.font = [UIFont systemFontOfSize:14];
        
        CGRect nameFrame = _name.frame;
        nameFrame.origin.y+=nameFrame.size.height;
        
        self.amount = [[UILabel alloc] initWithFrame:nameFrame];
        [self.contentView addSubview:_amount];
        _amount.textColor = [UIColor lightGrayColor];
        _amount.font = [UIFont systemFontOfSize:12];
        
    }
    return self;
}

@end