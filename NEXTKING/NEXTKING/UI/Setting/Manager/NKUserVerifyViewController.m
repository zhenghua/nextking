//
//  NKUserVerifyViewController.m
//  NEXTKING
//
//  Created by King on 16/8/4.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKUserVerifyViewController.h"
#import "NKRecordCell.h"
#import "NKRecordService.h"

@implementation NKUserVerifyViewController

-(void)setupHeader{
    self.titleLabel.text = @"实名审核";
}

-(NSString*)recordType{
    return NKRecordTypeUserVerify;
}

-(NSString*)parentID{
    return nil;
}

-(BOOL)needCommentButton{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NKRecordCell *cell = (NKRecordCell*)[super tableView:tableView cellForRowAtIndexPath:indexPath];

    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
 
    cell.nameLabel.text = object.title;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
 
    [alert addAction:[UIAlertAction actionWithTitle:@"通过" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self makeApproveWithRecordID:object.modelID approve:YES];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"拒绝" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self makeApproveWithRecordID:object.modelID approve:NO];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];

}

-(void)makeApproveWithRecordID:(NSString*)recordID approve:(BOOL)approve{
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(approveOK:)];
    [[NKRecordService sharedNKRecordService] approveWithRecordID:recordID approve:approve requestDelegate:rd];
}

-(void)approveOK:(NKRequest*)request{
    
    [self refreshData];
}


@end
