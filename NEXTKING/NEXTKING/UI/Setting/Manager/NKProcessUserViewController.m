//
//  NKProcessUserViewController.m
//  NEXTKING
//
//  Created by King on 16/8/8.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKProcessUserViewController.h"
#import "NKUserService.h"

@implementation NKProcessUserViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addRightButtonWithTitle:[UIImage imageNamed:@"button_more"]];
    
}

-(void)rightButtonClick:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"激活" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self processUserWithStatus:1];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"屏蔽" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self processUserWithStatus:0];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
}

-(void)processUserWithStatus:(NSInteger)status{
    
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(processOK:)];
    [[NKUserService sharedNKUserService] processUserWithUID:self.user.modelID status:status andRequestDelegate:rd];
    
}

-(void)processOK:(NKRequest*)request{
    [self showAlertWithMessage:@"处理成功"];
}

@end
