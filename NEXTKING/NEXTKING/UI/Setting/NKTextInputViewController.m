//
//  NKTextInputViewController.m
//  NEXTKING
//
//  Created by King on 16/7/20.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTextInputViewController.h"

@implementation NKTextInputViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    }
    return self;
}

-(void)textFieldDidChange:(NSNotification*)notification{
    
    if (notification.object != _field) {
        return;
    }
    self.nkRightButton.enabled = NO;
    
    if ([_field hasText] && ![_field.text isEqualToString:[_model valueForKeyPath:_keyPath]]) {
        self.nkRightButton.enabled = YES;
    }
    
    
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self addleftButtonWithTitle:@"取消"];
    [self addRightButtonWithTitle:@"保存"];
    self.view.backgroundColor = [self normalBackgroundColor];
    
    self.titleLabel.text = self.titleText;
    
    self.field = [[UITextField alloc] initWithFrame:CGRectMake(0, 64+10, NKMainWidth, 60)];
    [self.contentView addSubview:_field];
    _field.clearButtonMode = UITextFieldViewModeWhileEditing;
    _field.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 60)];
    _field.leftViewMode = UITextFieldViewModeAlways;
    _field.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 60)];
    _field.rightViewMode = UITextFieldViewModeAlways;
    _field.font = [UIFont systemFontOfSize:20];
    _field.backgroundColor = [UIColor whiteColor];
    _field.placeholder = self.placeholder;
    
    
    _field.text = [_model valueForKeyPath:_keyPath];
    [_field becomeFirstResponder];
    
    [self.nkRightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    self.nkRightButton.enabled = NO;
    
}

-(void)rightButtonClick:(id)sender{
    
    [_model setValue:_field.text forKeyPath:_keyPath];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewController:didClickSaveButtonWithText:)]) {
        [self.delegate viewController:self didClickSaveButtonWithText:_field.text];
    }
    
    [self leftButtonClick:nil];
}

-(void)leftButtonClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
