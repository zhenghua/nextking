//
//  NKNoticeViewController.m
//  NEXTKING
//
//  Created by King on 16/8/10.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKNoticeViewController.h"
#import "NKUserService.h"
#import "NKFeedDetailViewController.h"
#import "NKRecordCell.h"
#import "NKSharesCommentViewController.h"
#import "NKOrderDetailViewController.h"
#import "NKAuthenticationViewController.h"


NSString *const NKCachePathNotice = @"NKCachePathNotice.NK";

@interface NKNoticeCell : NKRecordCell

@property (nonatomic, strong) UILabel *parentLabel;
@property (nonatomic, strong) NKImageView *parentImageView;

@end


@implementation NKNoticeCell

+(CGFloat)contentLabelWidth{
    return NKMainWidth-20-62-80;
}

+(CGFloat)cellHeightForObject:(id)object{

    NKMNotice *notice = object;
    
    return [super cellHeightForObject:notice.record];
    
}


-(void)showForObject:(id)object withIndex:(NSIndexPath *)indexPath{
    self.showedObject = object;
    NKMNotice *notice = object;
    
    [super showForObject:notice.record withIndex:indexPath];
    
    _parentImageView.hidden = YES;
    _parentLabel.hidden = YES;
    
    if ([notice.type isEqualToString:NKRecordTypeComment] || [notice.type isEqualToString:NKRecordTypeLike] ||[notice.type isEqualToString:NKRecordTypeUserVerify]) {
        NKMRecord *parent = notice.parent;
        if (parent.attachments.count>0) {
            NKMAttachment *attachment = parent.attachments[0];
            NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:attachment.pictureURL imageSize:_parentImageView.frame.size]];
            [self.parentImageView sd_setImageWithURL:imageURL];
            _parentImageView.hidden = NO;
        }else{
            
            _parentLabel.text = parent.content;
            _parentLabel.hidden = NO;
        }
        
    }
    else if ([notice.type isEqualToString:NKRecordTypeSharesComment]){
        NKMShares *parent = notice.parent;
        NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:parent.avatar imageSize:_parentImageView.frame.size]];
        [self.parentImageView sd_setImageWithURL:imageURL];
        _parentImageView.hidden = NO;
    }
    else if ([notice.type isEqualToString:NKRecordTypeDeal]){
        
        NKMShares *parent = notice.parent;
        NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:parent.avatar imageSize:_parentImageView.frame.size]];
        [self.parentImageView sd_setImageWithURL:imageURL];
        _parentImageView.hidden = NO;
        
    }
    
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.likeButton.hidden = YES;
        
        self.parentImageView = [[NKImageView alloc] initWithFrame:CGRectMake(NKMainWidth-76, 16, 66, 66)];
        [self.contentView addSubview:_parentImageView];
        
        self.parentLabel = [[UILabel alloc] initWithFrame:_parentImageView.frame];
        [self.contentView addSubview:_parentLabel];
        _parentLabel.numberOfLines = 0;
        _parentLabel.textColor = [UIColor darkGrayColor];
        _parentLabel.font = [UIFont systemFontOfSize:14];
    }
    return self;
}


@end



@implementation NKNoticeViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = @"通知";
    [self addRightButtonWithTitle:@"清空"];
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    self.dataSource = [[NKDataStore sharedDataStore] cachedArrayOf:[self cachePathKey] andClass:[NKMNotice class]];
    if ([[[[NKUserService sharedNKUserService] local] needNoticeAlert] boolValue]) {
        [self.showTableView.header beginRefreshing];
    }
    
}

-(void)rightButtonClick:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"清空所有消息" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteAll];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
}

-(void)deleteAll{
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(deleteOK:)];
    [[NKUserService sharedNKUserService] deleteAllNoticesWithandRequestDelegate:rd];
}

-(void)deleteOK:(NKRequest*)request{
    [self refreshData];
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKUserService sharedNKUserService] listNoticesWithOffset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)refreshDataOK:(NKRequest *)request{
    [super refreshDataOK:request];
    [[NKUserService sharedNKUserService] updateLocalNotification];
}


-(void)getMoreData{
    
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKUserService sharedNKUserService] listNoticesWithOffset:self.dataSource.count size:20 andRequestDelegate:rd];
}

-(NSString*)cachePathKey{
    
    return NKCachePathNotice;
}

-(Class)cellClass{
    return [NKNoticeCell class];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKMNotice *notice = self.dataSource[indexPath.row];
    if ([notice.type isEqualToString:NKRecordTypeComment] || [notice.type isEqualToString:NKRecordTypeLike]) {
        NKFeedDetailViewController *viewController = [NKFeedDetailViewController new];
        viewController.record = notice.parent;
        [NKNC pushViewController:viewController animated:YES];
    }
    else if ([notice.type isEqualToString:NKRecordTypeSharesComment]){
        
        NKSharesCommentViewController *viewController = [NKSharesCommentViewController new];
        viewController.shares = notice.parent;
        [NKNC pushViewController:viewController animated:YES];
        
    }
    else if ([notice.type isEqualToString:NKRecordTypeDeal]){
        
        NKOrderDetailViewController *viewController = [NKOrderDetailViewController new];
        viewController.order = notice.order;
        [NKNC pushViewController:viewController animated:YES];
    
    }
    else if ([notice.type isEqualToString:NKRecordTypeUserVerify]){
        NKAuthenticationViewController *viewController = [NKAuthenticationViewController new];
        [NKNC pushViewController:viewController animated:YES];
    }

}




@end
