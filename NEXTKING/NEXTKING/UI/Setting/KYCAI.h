//
//  KYCAI.h
//  NEXTKING
//
//  Created by King on 2017/3/10.
//  Copyright © 2017年 jinzhenghua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KYCModel.h"

typedef NS_ENUM(NSInteger, KYCNeoStatus) {
    KYCNeoStatusIdle = 0, //空闲
    KYCNeoStatusThinking = 1, //思考中
    KYCNeoStatusTyping = 2, //输入中
    KYCNeoStatusWaiting = 3, //等待回答中
    KYCNeoStatusOther
};


@protocol KYCAIDelegate;

@interface KYCAI : NSObject

@property (nonatomic, weak) id<KYCAIDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *messagesArray;
@property (nonatomic, strong) KYCRecord *currentRecord;

@property (nonatomic, assign) KYCNeoStatus status;

-(void)start;

-(void)processMessage:(NSString*)message;


@end


@protocol KYCAIDelegate <NSObject>

@optional
-(void)kyc:(KYCAI*)kyc didResponseWithRecord:(KYCRecord*)record;
-(void)kyc:(KYCAI*)kyc didChooseOptionWithRecord:(KYCRecord*)record;

@end
