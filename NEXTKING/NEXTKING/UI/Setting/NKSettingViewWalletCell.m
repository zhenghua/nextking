//
//  NKSettingViewWalletCell.m
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSettingViewWalletCell.h"
#import "NKKVOLabel.h"
#import "NKMUser.h"

@interface NKSettingViewWalletCell ()

@property (nonatomic, strong) NKKVOLabel *moneyLabel;

@end

@implementation NKSettingViewWalletCell

+(CGFloat)cellHeightForObject:(id)object{
    
    return 60;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKCellDataObject *model = (NKCellDataObject*)object;
    
    self.imageView.image = model.titleImage;
    self.textLabel.text = model.title;
    if ([model.accessoryView isKindOfClass:[NSNumber class]]) {
        self.accessoryType = [model.accessoryView integerValue];
    }
    
    _moneyLabel.target = self;
    _moneyLabel.renderMethod = @selector(render:);
    
    [_moneyLabel bindValueOfModel:[NKMUser me] forKeyPath:@"wallet.money"];

    
}

-(NSString*)render:(NSNumber*)money{
    
    return [NSString stringWithFormat:@"%.2f", money.floatValue];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.moneyLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(NKMainWidth-100-40, 0, 100, [NKSettingViewWalletCell cellHeightForObject:nil])];
        [self.contentView addSubview:_moneyLabel];
        _moneyLabel.textColor = [UIColor lightGrayColor];
        _moneyLabel.textAlignment = NSTextAlignmentRight;
        //_moneyLabel.backgroundColor = [UIColor redColor];
        
    }
    return self;
}

@end
