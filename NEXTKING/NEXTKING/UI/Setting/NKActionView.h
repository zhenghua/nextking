//
//  NKActionView.h
//  NEXTKING
//
//  Created by King on 16/9/1.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NKActionViewDelegate;

@interface NKActionView : UIView

@property (nonatomic, weak) id<NKActionViewDelegate> delegate;
@property (nonatomic, assign) BOOL needsFullScreen;
@property (nonatomic, strong) UIView   *contentView;

-(void)showInView:(UIView*)view;

-(id)pickedData;

@end


@protocol NKActionViewDelegate <NSObject>
@optional
-(void)actionView:(NKActionView*)actionView didPickData:(id)data;
@end