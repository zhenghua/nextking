//
//  NKMyProfileViewController.m
//  NEXTKING
//
//  Created by King on 16/7/7.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKMyProfileViewController.h"
#import "NKTextInputViewController.h"
#import "NKUserService.h"
#import "NKPickAvatarCell.h"
#import "NKDatePickerView.h"
#import "NKCityPickerView.h"

@interface NKMyProfileViewController ()<NKTextInputViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NKDatePickerViewDelegate, NKActionViewDelegate>

@end

@implementation NKMyProfileViewController

-(UITableViewStyle)tableViewStyle{
    return UITableViewStyleGrouped;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"个人信息";
    
    NKCellDataObject *avatar = [NKCellDataObject cellDataObjectWithTitle:@"头像" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(avatarClicked:)];
    avatar.keyPath = @"avatarPath";
    avatar.cellName = NSStringFromClass([NKPickAvatarCell class]);
    
    NKCellDataObject *name = [NKCellDataObject cellDataObjectWithTitle:@"名字" titleImage:nil detailText:@"请输入真实姓名" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(cellClicked:)];
    name.keyPath = @"name";
    
    NKCellDataObject *gender = [NKCellDataObject cellDataObjectWithTitle:@"性别" titleImage:nil detailText:@"性别" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(genderClicked:)];
    gender.keyPath = @"gender";
    
    NKCellDataObject *birthday = [NKCellDataObject cellDataObjectWithTitle:@"年龄" titleImage:nil detailText:@"年龄" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(birthdayClicked:)];
    birthday.keyPath = @"birthday";
    
    NKCellDataObject *constellation = [NKCellDataObject cellDataObjectWithTitle:@"星座" titleImage:nil detailText:@"星座" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(birthdayClicked:)];
    constellation.keyPath = @"constellation";
    
    NKCellDataObject *city = [NKCellDataObject cellDataObjectWithTitle:@"城市" titleImage:nil detailText:@"城市" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(cityClicked:)];
    city.keyPath = @"city";
    
    NKCellDataObject *sign = [NKCellDataObject cellDataObjectWithTitle:@"个性签名" titleImage:nil detailText:@"个性签名" accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(cellClicked:)];
    sign.keyPath = @"sign";
    
    self.dataSource = [NSMutableArray arrayWithObjects:
                       @[avatar, name],
                       @[gender, birthday, constellation, city, sign],
                       nil];
    
}

-(void)actionView:(NKActionView*)actionView didPickData:(id)data{
    
    [[NKMUser me] setCity:data];
    [[NKUserService sharedNKUserService] updateUserInfoWithInfo:[NSDictionary dictionaryWithObjectsAndKeys:data, @"city", nil] andRequestDelegate:nil];
}

-(void)cityClicked:(NKCellDataObject*)object{
    
    NKCityPickerView *picker = [[NKCityPickerView alloc] init];
    picker.delegate = self;
    [picker showInView:self.view];

}

-(void)birthdayClicked:(NKCellDataObject*)object{
    
    NKDatePickerView *view = [[NKDatePickerView alloc] init];
    view.delegate = self;
    [view showInView:self.view];
    if ([[NKMUser me] birthday]) {
        [view.picker setDate:[NSDate dateWithTimeIntervalSince1970:[[[NKMUser me] birthday] longLongValue]] animated:YES];
    }

}

-(void)pickerView:(NKDatePickerView*)pickerView didPickDate:(NSDate*)date{
    
    [[NKMUser me] setBirthday:[NSNumber numberWithLongLong:[date timeIntervalSince1970]]];
    [[NKUserService sharedNKUserService] updateUserInfoWithInfo:[NSDictionary dictionaryWithObjectsAndKeys:[[NKMUser me] birthday], @"birthday", nil] andRequestDelegate:nil];
}

-(void)avatarClicked:(NKCellDataObject*)object{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"从手机相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
}

-(void)showImagePickerWithType:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerControllerSourceType sourceType = [UIImagePickerController isSourceTypeAvailable:type]?type:UIImagePickerControllerSourceTypePhotoLibrary;
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
    imagePicker.delegate = self;
    imagePicker.sourceType = sourceType;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    NSString *objectKey = [[NKFileManager fileManager] ObjectKeyForImage:image];
    
    [[NKFileManager fileManager] uploadWithObjectKey:objectKey block:^id(OSSTask *task) {
       
        if (!task.error) {
            [[NKUserService sharedNKUserService] updateUserInfoWithInfo:[NSDictionary dictionaryWithObjectsAndKeys:objectKey, @"avatar", nil] andRequestDelegate:nil];
            [[NKMUser me] setAvatarPath:objectKey];
        }
        
        return nil;
    }];
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)genderClicked:(NKCellDataObject*)object{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setGenderWithGender:NKGenderKeyMale];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setGenderWithGender:NKGenderKeyFemale];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)setGenderWithGender:(NSString*)gender{
    [NKMUser me].gender = gender;
    [[NKUserService sharedNKUserService] updateUserInfoWithInfo:[NSDictionary dictionaryWithObjectsAndKeys:gender, @"gender", nil] andRequestDelegate:nil];
}

-(void)cellClicked:(NKCellDataObject*)object{
    
    NKTextInputViewController *viewController = [NKTextInputViewController new];
    viewController.model = [NKMUser me];
    viewController.keyPath = object.keyPath;
    viewController.titleText = object.title;
    viewController.placeholder = object.detailText;
    viewController.delegate = self;
    [NKNC pushViewController:viewController animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    if (!object.cellName) {
        object.cellName = NSStringFromClass([NKTableViewCell class]);
    }
    return [NSClassFromString(object.cellName) cellHeightForObject:object];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    if (!object.cellName) {
        object.cellName = NSStringFromClass([NKTableViewCell class]);
    }
    
    NSString * cellIdentifier = object.cellName;
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NSClassFromString(object.cellName) alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.detailLabel.target = self;
        cell.detailLabel.renderMethod = @selector(render:label:);
    }
    
    [cell showForObject:object withIndex:indexPath];
    [cell.detailLabel bindValueOfModel:[NKMUser me] forKeyPath:object.keyPath];
    
    return cell;
}

-(NSString*)render:(NSString*)value label:(NKKVOLabel*)label{
    
    if (!value) {
        label.textColor = [UIColor lightGrayColor];
        
        for (NSArray *array in self.dataSource) {
            for (NKCellDataObject *object in array) {
                if ([label.keyPath isEqualToString:object.keyPath]) {
                    return object.detailText;
                }
            }
        }
    }

    label.textColor = [UIColor darkGrayColor];
    
    if ([label.keyPath isEqualToString:@"gender"]) {
        if ([value isEqualToString:NKGenderKeyMale] ) {
            return @"男";
        }
        else if ([value isEqualToString:NKGenderKeyFemale]){
            return @"女";
        }
    }
    else if ([label.keyPath isEqualToString:@"birthday"]){
        
        return [NSString stringWithFormat:@"%.f", floor([[NSDate date] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:[value longLongValue]]]/(365*24*3600))] ;
        
    }
    
    return value;
    
}

-(void)viewController:(NKTextInputViewController*)viewController didClickSaveButtonWithText:(NSString*)text{
    
    [[NKUserService sharedNKUserService] updateUserInfoWithInfo:[NSDictionary dictionaryWithObjectsAndKeys:text, viewController.keyPath, nil] andRequestDelegate:nil];
    
}

@end
