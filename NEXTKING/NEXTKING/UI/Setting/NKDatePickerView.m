//
//  NKDatePickerView.m
//  NEXTKING
//
//  Created by King on 16/8/28.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKDatePickerView.h"
#import "NKUI.h"

@interface NKDatePickerView ()

@property (nonatomic, strong) UIButton *backgroundButton;

@end

@implementation NKDatePickerView

-(instancetype)init{
    
    self = [super initWithFrame:CGRectMake(0, NKMainHeight-266, NKMainWidth, 276)];
    if (self) {

        self.layer.shadowOffset = CGSizeMake(0, -1);
        self.layer.shadowOpacity = 0.2;
        self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.layer.shadowRadius = 7.0;
        
        self.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 0.5)];
        [self addSubview:line];
        line.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 41, 45)];
        [self addSubview:closeButton];
        [closeButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setImage:[UIImage imageNamed:@"trade_icon_close"] forState:UIControlStateNormal];
        
        UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(NKMainWidth-45, 0, 41, 45)];
        [self addSubview:saveButton];
        [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [saveButton setImage:[UIImage imageNamed:@"trade_icon_ok"] forState:UIControlStateNormal];
        
        
        self.picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, NKMainWidth, 216)];
        [self addSubview:_picker];
        _picker.datePickerMode = UIDatePickerModeDate;
        _picker.maximumDate = [NSDate date];
        _picker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-365*24*3600*100.0];
        
        [_picker setDate:[NSDate dateWithTimeIntervalSince1970:365*24*3600*18.0+24*3600*30*8.0-16*24*3600.0]];
    }
    return self;

    
}

-(void)saveButtonClicked:(id)sender{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(pickerView:didPickDate:)]) {
        [self.delegate pickerView:self didPickDate:self.picker.date];
    }
    
    [self closeButtonClicked:nil];
    
}

-(void)showInView:(UIView*)view{
    CGRect frame = self.frame;
    frame.origin.y = NKMainHeight;
    self.frame = frame;
    
    self.backgroundButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, NKMainHeight)];
    [view addSubview:_backgroundButton];
    [_backgroundButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    _backgroundButton.backgroundColor = [UIColor blackColor];
    _backgroundButton.alpha = 0.0;
    
    [view addSubview:self];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = NKMainHeight-self.height;
        self.frame = frame;
        _backgroundButton.alpha = 0.4;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame = self.frame;
            frame.origin.y = NKMainHeight-self.height+10;
            self.frame = frame;
        }];
    }];
}

-(void)closeButtonClicked:(id)sender{
    
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = NKMainHeight;
        self.frame = frame;
        self.backgroundButton.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.backgroundButton removeFromSuperview];
        [self removeFromSuperview];
    }];
    
}

@end
