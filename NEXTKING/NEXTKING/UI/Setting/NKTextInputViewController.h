//
//  NKTextInputViewController.h
//  NEXTKING
//
//  Created by King on 16/7/20.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKViewController.h"


@protocol NKTextInputViewControllerDelegate;
@interface NKTextInputViewController : NKViewController

@property (nonatomic, strong) id model;
@property (nonatomic, strong) NSString *keyPath;
@property (nonatomic, strong) NSString *titleText;
@property (nonatomic, strong) NSString *placeholder;

@property (nonatomic, strong) UITextField *field;

@property (nonatomic, weak) id<NKTextInputViewControllerDelegate> delegate;

@end

@protocol NKTextInputViewControllerDelegate <NSObject>

@optional
-(void)viewController:(NKTextInputViewController*)viewController didClickSaveButtonWithText:(NSString*)text;

@end

