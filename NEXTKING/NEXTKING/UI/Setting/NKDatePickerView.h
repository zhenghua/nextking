//
//  NKDatePickerView.h
//  NEXTKING
//
//  Created by King on 16/8/28.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NKDatePickerViewDelegate;

@interface NKDatePickerView : UIView

@property (nonatomic, strong) UIDatePicker *picker;

@property (nonatomic, weak) id<NKDatePickerViewDelegate> delegate;

-(void)showInView:(UIView*)view;

@end

@protocol NKDatePickerViewDelegate <NSObject>
@optional
-(void)pickerView:(NKDatePickerView*)pickerView didPickDate:(NSDate*)date;
@end