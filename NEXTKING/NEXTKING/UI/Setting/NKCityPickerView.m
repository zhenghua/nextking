//
//  NKCityPickerView.m
//  NEXTKING
//
//  Created by King on 16/9/1.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKCityPickerView.h"
#import "NKUI.h"

@interface NKCityPickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIPickerView *picker;
@property (nonatomic, strong) NSArray      *cityArray;

@end

@implementation NKCityPickerView

-(instancetype)init{
    
    self = [super init];
    if (self) {
        
        NSURL *plistUrl = [[NSBundle mainBundle] URLForResource:@"City" withExtension:@"plist"];
        self.cityArray = [[NSArray alloc] initWithContentsOfURL:plistUrl];

        
        self.picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, NKMainWidth, 216)];
        [self.contentView addSubview:_picker];
        _picker.delegate = self;
        _picker.dataSource = self;
        
    }
    
    return self;
}

-(id)pickedData{
    
    return [[[[self.cityArray objectAtIndex:[self.picker selectedRowInComponent:0]] objectForKey:@"children"] objectAtIndex:[self.picker selectedRowInComponent:1]] objectForKey:@"name"];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    switch (component) {
        case 0:{
            return self.cityArray.count;
        }
            
            break;
        case 1:{
            return [[[self.cityArray objectAtIndex:[pickerView selectedRowInComponent:0]] objectForKey:@"children"] count];
        }
            
        default:
            break;
    }
    return 0;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    switch (component) {
        case 0:{
            return [[self.cityArray objectAtIndex:row] objectForKey:@"name"];
        }
            
            break;
        case 1:{
            return [[[[self.cityArray objectAtIndex:[pickerView selectedRowInComponent:0]] objectForKey:@"children"] objectAtIndex:row] objectForKey:@"name"];
        }
            
        default:
            break;
    }
    return @"";
    
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (component == 0) {
        [pickerView reloadComponent:1];
    }
}


@end
