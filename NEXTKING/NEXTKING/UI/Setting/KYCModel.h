//
//  KYCModel.h
//  NEXTKING
//
//  Created by King on 17/3/9.
//  Copyright © 2017年 jinzhenghua. All rights reserved.
//

#import "NKModel.h"
#import "NKMRecord.h"

@interface KYCModel : NKModel

@end

@interface KYCUser : KYCModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatarPath;


-(NSString*)showName;

@end

@interface KYCAttachment : NKMAttachment

@end


@interface KYCOption : KYCAttachment

@property (nonatomic, strong) NSNumber *score;
@property (nonatomic, strong) NSMutableArray *response;

@property (nonatomic, strong) NSNumber *min;
@property (nonatomic, strong) NSNumber *max;


@end


extern NSString *const KYCRecordTypeMessage;
extern NSString *const KYCRecordTypeConversation;
extern NSString *const KYCRecordTypeGroup;
extern NSString *const KYCRecordTypeCalculate;

extern NSString *const KYCRecordKeyYear;
extern NSString *const KYCRecordKeyIncome;
extern NSString *const KYCRecordKeyMobile;
extern NSString *const KYCRecordKeyPin;

typedef NS_ENUM(NSInteger, KYCInputType) {
    KYCInputTypeText = 0, //标准键盘输入
    KYCInputTypeNumber = 1, //数字键盘输入
    KYCInputTypeChoice = 2, //选择题
    KYCInputTypeNoInput = 3, //无需输入
    KYCInputTypeOther
};


@interface KYCRecord : KYCModel

@property (nonatomic, strong) KYCUser  *sender;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *timeString;

@property (nonatomic, strong) NSNumber *status;

@property (nonatomic, strong) NSString *parentID;
@property (nonatomic, strong) NSString *nextID;

@property (nonatomic, strong) NSNumber *inputType;
@property (nonatomic, strong) NSNumber *thinkTime;
@property (nonatomic, strong) NSNumber *typeTime;

@property (nonatomic, strong) NSString *answer;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *relationKey;
@property (nonatomic, strong) NSArray  *calculateKeys;

@property (nonatomic, strong) NSMutableArray *attachments;
@property (nonatomic, strong) NSMutableArray *options;
@property (nonatomic, strong) KYCOption *option;
@property (nonatomic, strong) NSNumber *ratio;

@property (nonatomic, strong) NSMutableArray *rightAnswerResponse;
@property (nonatomic, strong) NSMutableArray *errorAnswerResponse;

@property (nonatomic, strong) NSMutableArray *records;


-(BOOL)needShowOption;

+(instancetype)messageRecordWithMessage:(NSString*)message;

@end




