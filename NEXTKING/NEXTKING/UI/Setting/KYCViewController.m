//
//  KYCViewController.m
//  NEXTKING
//
//  Created by King on 17/3/9.
//  Copyright © 2017年 jinzhenghua. All rights reserved.
//

#import "KYCViewController.h"

#import "NKTableViewCell.h"

#define kOptionContentLabelFont [UIFont systemFontOfSize:14]
#define kOptionContentLabelWidth NKMainWidth - kContentLabelMargin - 40 -10

#define kContentLabelFont [[self class] contentLabelFont]
#define kContentLabelWidth [[self class] contentLabelWidth]
#define kContentLabelMargin 20
#define kContentBubbleMargin 10
#define kMaxOptionCount 6
#define kImageHeight 160


@class KYCRecordCell;
@class KYCViewController;
@interface KYCOptionView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, weak)   id        target;
@property (nonatomic, assign) SEL       singleTapped;

@property (nonatomic, strong)   UITapGestureRecognizer *tap;

@end

@implementation KYCOptionView


-(void)setSingleTapped:(SEL)singleTapped{
    
    _singleTapped = singleTapped;
    
    self.userInteractionEnabled = YES;
    
    if (!self.tap) {
        self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:_tap];
        
    }
    
}

-(void)tapped:(UITapGestureRecognizer*)gesture{
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        if ([_target respondsToSelector:_singleTapped]) {
            SuppressPerformSelectorLeakWarning(
                                               [_target performSelector:_singleTapped withObject:gesture];
                                               );
        }
    }
}

-(UILabel*)titleLabel{
    
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, NKMainWidth - kContentLabelMargin - 40 -10, 20)];
        [self addSubview:_titleLabel];
        _titleLabel.font = [UIFont systemFontOfSize:18];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#0b74e2"];
    }
    return _titleLabel;
    
}

-(UILabel*)contentLabel{
    
    
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, kOptionContentLabelWidth, 20)];
        [self addSubview:_contentLabel];
        _contentLabel.font = kOptionContentLabelFont;
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor lightGrayColor];
    }
    return _contentLabel;
}


@end

@interface KYCRecordCell : NKTableViewCell

@property (nonatomic, strong) UIView      *bubble;
@property (nonatomic, strong) UILabel     *contentLabel;

@property (nonatomic, strong) NKImageView *imagePlace;

@property (nonatomic, strong) UIView      *optionsView;
@property (nonatomic, strong) NSMutableArray *optionViewArray;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak)   KYCViewController *controller;



@end

@implementation KYCRecordCell

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    self.indexPath = indexPath;
    
    KYCRecord *record = object;
    
    CGFloat height = [[self class] startHeight];
    CGFloat yMargin = 10;
    
    _contentLabel.text = record.content;
    CGSize size = [[self class] contentLabelHeightWithText:record.content];
    
    _contentLabel.height = size.height;
    height+= (yMargin+ _contentLabel.height);
    
    if ([record.sender.modelID isEqualToString:@"1"]) {
        _contentLabel.left = kContentLabelMargin;
    }
    else{
        _contentLabel.left = NKMainWidth-kContentLabelMargin-size.width;

    }
    
    _contentLabel.width = size.width;
    
    if (record.attachments.count>0) {
        
        _imagePlace.hidden = NO;
        _imagePlace.top = height+yMargin;
        
        height += yMargin+kImageHeight;
        KYCAttachment *attachment = [record.attachments lastObject];
        NSURL *imageURL = [NSURL URLWithString:attachment.thumbnailURL];
        [_imagePlace sd_setImageWithURL:imageURL placeholderImage:nil options:SDWebImageLowPriority progress:nil completed:nil
         ];
        
    }
    else{
        _imagePlace.hidden = YES;
    }
    
    if ([record needShowOption]) {
        height += yMargin;
        CGFloat optionsViewHeight = 0;
        _optionsView.hidden = NO;
        KYCOptionView *optionView = nil;
        KYCOption *option = nil;
        for (int i=0; i<kMaxOptionCount; i++) {
            optionView = self.optionViewArray[i];
            if (i>record.options.count-1) {
                optionView.hidden = YES;
            }
            else{
                optionView.hidden = NO;
                option = record.options[i];
                optionView.titleLabel.text = option.title;
                optionView.contentLabel.hidden = YES;
                
                if (option.content.length>0) {
                    optionView.contentLabel.hidden = NO;
                    optionView.contentLabel.height = [[self class] optionContentLabelHeightWithText:option.content].height;
                    optionView.contentLabel.text = option.content;
                }
                optionView.frame = CGRectMake(0, optionsViewHeight, _contentLabel.width, 30+(option.content.length>0?10+optionView.contentLabel.height:0));
                optionsViewHeight += optionView.height;
            }
        }
        _optionsView.frame = CGRectMake(kContentLabelMargin+5, height, _contentLabel.width, optionsViewHeight);
        
    }
    else{
        _optionsView.hidden = YES;
        _optionsView.height = 0;
    }
    
    _bubble.frame = CGRectMake(_contentLabel.left-kContentBubbleMargin, _contentLabel.top-kContentBubbleMargin, _contentLabel.width+kContentBubbleMargin*2, _contentLabel.height+ ([record needShowOption]?yMargin+_optionsView.height:0) + (record.attachments.count>0?yMargin+kImageHeight:0) + kContentBubbleMargin*2);
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
       
        
        self.contentView.backgroundColor = [UIColor colorWithWhite:0.973 alpha:1.000];
        
        self.bubble = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.contentView addSubview:_bubble];
        _bubble.backgroundColor = [UIColor whiteColor];
        
        self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(kContentLabelMargin, [[self class] startHeight]+9, kContentLabelWidth, 0)];
        [self.contentView addSubview:_contentLabel];
        self.contentLabel.backgroundColor = [UIColor whiteColor];
        _contentLabel.font = kContentLabelFont;
        _contentLabel.numberOfLines = 0;
        
        self.imagePlace = [[NKImageView alloc] initWithFrame:CGRectMake(kContentLabelMargin, 0, kContentLabelWidth-10, kImageHeight)];
        [self.contentView addSubview:_imagePlace];
        
        self.optionsView = [[UIView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_optionsView];
        _optionsView.backgroundColor = [UIColor whiteColor];
        
        self.optionViewArray = [[NSMutableArray alloc] initWithCapacity:kMaxOptionCount];
        
        KYCOptionView *optionView = nil;
 
        for (int i=0; i<kMaxOptionCount; i++) {
            optionView = [[KYCOptionView alloc] initWithFrame:CGRectZero];
            optionView.target = self;
            optionView.singleTapped = @selector(optionTapped:);
            optionView.tag = i+100;
            [self.optionsView addSubview:optionView];
            [self.optionViewArray addObject:optionView];
        }
        
    }
    return self;
}

-(void)optionTapped:(UIGestureRecognizer*)gesture{
    
    NSUInteger index = [[gesture view] tag]-100;
    KYCRecord *record = self.showedObject;

    KYCOption *option = [record.options objectAtIndex:index];
    NSLog(@"%@", option.title);
    record.option = option;
    record.cellHeight = 0;
    
    [self.controller.showTableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.controller chooseOption:option];
    
}


-(void)avatarTapped:(UIGestureRecognizer*)gesture{
    
}

-(CGRect)avatarFrame{
    
    return CGRectMake(10, 16, 43, 43);
    
}


+(CGFloat)cellHeightForObject:(id)object{
    
    KYCRecord *record = object;
    if (record.cellHeight>0) {
        return record.cellHeight;
    }
    
    CGFloat height = [self startHeight];
    CGFloat yMargin = 10;
    
    if ([record.content length]>0) {
        
        height += (yMargin+ [self contentLabelHeightWithText:record.content].height);
    }
    
    if ([record.attachments count]>0) {
        
        height += (yMargin+kImageHeight);
        
    }
    
    if ([record needShowOption]) {
        
        for (KYCOption *option in record.options) {
            height += yMargin+20;
            if ([option.content length]>0) {
                height += ([self optionContentLabelHeightWithText:option.content].height + 10);
            }
        }
    }
    
    height += yMargin+10;
    
    record.cellHeight = ceilf(height);
    
    return height;
    
}

+(UIFont*)contentLabelFont{
    
    return [UIFont systemFontOfSize:16];
}

+(CGFloat)startHeight{
    
    return 10;
    
}


+(CGFloat)contentLabelWidth{
    
    return NKMainWidth - kContentLabelMargin - 40;
}

+(CGSize)optionContentLabelHeightWithText:(NSString*)text{
    
    if (!text) {
        return CGSizeMake(0, 0);
    }
    
    CGSize size = [text boundingRectWithSize:CGSizeMake(kOptionContentLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:kOptionContentLabelFont, NSFontAttributeName,nil] context:nil].size;
    
    return CGSizeMake(ceilf(size.width), ceilf(size.height));
    
}


+(CGSize)contentLabelHeightWithText:(NSString*)text{
    
    if (!text) {
        return CGSizeMake(0, 0);
    }
    
    CGSize size = [text boundingRectWithSize:CGSizeMake(kContentLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:kContentLabelFont,NSFontAttributeName,nil] context:nil].size;
    
    return CGSizeMake(ceilf(size.width), ceilf(size.height));

}

//static inline CGFloat CGFloatPixelRound(CGFloat value) {
//    CGFloat scale = [[UIScreen mainScreen] scale];
//    return round(value * scale) / scale;
//}

@end


@interface KYCViewController ()

@property (nonatomic, strong) NSMutableArray *messagesArray;

@end


@implementation KYCViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note{
    
    if (![_inputField isFirstResponder]) {
        return;
    }
    
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    CGRect frame = self.typeView.frame;
    frame.origin.y = NKMainHeight - (keyboardBounds.size.height + frame.size.height);
    
    [UIView animateWithDuration:[duration doubleValue] delay:0 options:UIViewAnimationOptionBeginFromCurrentState|[curve intValue] animations:^{
        self.typeView.frame = frame;
        self.showTableView.height = NKMainHeight - (keyboardBounds.size.height + frame.size.height)-self.showTableView.top;

    } completion:^(BOOL finished) {
        if (self.dataSource.count>0) {
            NSIndexPath *path = [NSIndexPath indexPathForRow:self.dataSource.count-1 inSection:0];
            [self.showTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }];
    
    
    
}

- (void)keyboardWillHide:(NSNotification *)note{
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect frame = self.typeView.frame;
    frame.origin.y = NKMainHeight-(frame.size.height);
    
    [UIView animateWithDuration:[duration doubleValue] delay:0 options:UIViewAnimationOptionBeginFromCurrentState|[curve intValue] animations:^{
        self.typeView.frame = frame;
        
    } completion:^(BOOL finished) {
        
    }];
    self.showTableView.height = NKMainHeight - _typeView.height-self.showTableView.top;
    
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        self.kyc = [[KYCAI alloc] init];
        _kyc.delegate = self;
        
    }
    return self;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (self.kyc.status == KYCNeoStatusWaiting || self.kyc.status == KYCNeoStatusIdle){
        [_inputField resignFirstResponder];
        NSString *message = _inputField.text;
        
        [self showMyMessage:message delay:0.3  animated:YES];
        
        _inputField.text = @"";
        
        return YES;
    }
    
    return NO;
}


-(void)chooseOption:(KYCOption*)option{
    
    [_inputField resignFirstResponder];
    [self showMyMessage:option.title delay:0.3 animated:YES];
    
    
}

-(void)showMyMessage:(NSString*)message delay:(CGFloat)delay animated:(BOOL)animated{
    
    [self.dataSource addObject:[KYCRecord modelFromDic:@{@"id":@"1", @"user":@{@"id":@"2", @"name":@"Neo", @"avatar":@""}, @"content":message}]];
    [self performSelector:@selector(insertMyMessageAnimated:) withObject:[NSNumber numberWithBool:animated] afterDelay:delay];
    [self.kyc performSelector:@selector(processMessage:) withObject:message afterDelay:delay];
}



-(void)insertMyMessageAnimated:(NSNumber*)animated{
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:self.dataSource.count-1 inSection:0];
    [self.showTableView insertRowsAtIndexPaths:@[path] withRowAnimation:animated.boolValue?UITableViewRowAnimationFade:UITableViewRowAnimationNone];
    [self.showTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:animated.boolValue];

}

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self addBackButton];
    self.titleLabel.text = @"KYC";
    
    self.showTableView.backgroundColor = [self normalBackgroundColor];
    self.showTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 20)];
    
    self.typeView = [[UIView alloc] initWithFrame:CGRectMake(0, NKMainHeight-50, NKMainWidth, 50)];
    [self.contentView addSubview:_typeView];
    _typeView.backgroundColor = [UIColor whiteColor];
    
    self.inputField = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, _typeView.width-20, _typeView.height)];
    [_typeView addSubview:_inputField];
    _inputField.font = [UIFont systemFontOfSize:16];
    _inputField.placeholder = @"写消息...";
    _inputField.delegate = self;
    _inputField.returnKeyType = UIReturnKeySend;
    _inputField.enablesReturnKeyAutomatically = YES;

    
    [self.kyc start];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"WMFeedCellIdentifier";
    
    KYCRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    if ([object.sender.modelID isEqualToString:@"1"]) {

    }
    else{
        cellIdentifier = @"WMFeedCellIdentifierMe";
        
    }
    
    KYCRecordCell *cell = (KYCRecordCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[KYCRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.controller = self;
    }
    
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [KYCRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
}

-(void)kyc:(KYCAI*)kyc didResponseWithRecord:(KYCRecord*)record{
    self.currentRecord = record;
    [self showTyping];
}

-(void)kyc:(KYCAI*)kyc didChooseOptionWithRecord:(KYCRecord*)record{
    
    for (KYCRecordCell *cell in self.showTableView.visibleCells) {
        if (cell.showedObject == record) {
            record.cellHeight = 0;
            [self.showTableView reloadRowsAtIndexPaths:@[cell.indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }

}

-(void)showTyping{
    
    self.kyc.status = KYCNeoStatusTyping;
    KYCRecord *typingRecord = [[KYCRecord alloc] init];
    typingRecord.content = @"typing...";
    typingRecord.sender = [KYCUser modelFromDic:@{@"id":@"1"}];
    [self.dataSource addObject:typingRecord];
    NSIndexPath *path = [NSIndexPath indexPathForRow:self.dataSource.count-1 inSection:0];
    [self.showTableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
    [self.showTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    [self performSelector:@selector(processResponse) withObject:nil afterDelay:[self.currentRecord.typeTime floatValue]];
}
-(void)processResponse{
    
    self.kyc.status = KYCNeoStatusWaiting;
    [self.dataSource removeObjectAtIndex:self.dataSource.count-1];
    [self.dataSource addObject:self.currentRecord];
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:self.dataSource.count-1 inSection:0];
    [self.showTableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    [self.showTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    switch ([self.kyc.currentRecord.inputType integerValue]) {
        case KYCInputTypeText:
            _inputField.keyboardType = UIKeyboardTypeDefault;
            break;
        case KYCInputTypeNumber:
            _inputField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            break;
        case KYCInputTypeChoice:
            _inputField.keyboardType = UIKeyboardTypeDefault;
            break;
        case KYCInputTypeNoInput:
            _inputField.keyboardType = UIKeyboardTypeDefault;
            break;
        default:
            _inputField.keyboardType = UIKeyboardTypeDefault;
            break;
    }
    
    if ([self.kyc.currentRecord.inputType integerValue] == KYCInputTypeNoInput) {
        [self.kyc processMessage:nil];
    }
}

@end
