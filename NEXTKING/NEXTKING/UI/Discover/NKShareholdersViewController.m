//
//  NKShareholdersViewController.m
//  NEXTKING
//
//  Created by King on 16/7/12.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKShareholdersViewController.h"
#import "NKOrderService.h"
#import "NKShareholderCell.h"

@implementation NKShareholdersViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addBackButton];
    
    
    self.titleLabel.text = self.shares.name;
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    [self.showTableView.header beginRefreshing];
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listShareholdersWithSharesID:self.shares.modelID offset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)getMoreData{
    
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listShareholdersWithSharesID:self.shares.modelID offset:self.dataSource.count size:20 andRequestDelegate:rd];
    
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKSharesCellIdentifier";
    
    NKShareholderCell *cell = (NKShareholderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKShareholderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NKMShareholder *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKShareholderCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



@end
