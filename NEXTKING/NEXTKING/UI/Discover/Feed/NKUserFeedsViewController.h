//
//  NKUserFeedsViewController.h
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKUserFeedsViewController : NKTableViewController

@property (nonatomic, strong) NKMUser *user;

@end
