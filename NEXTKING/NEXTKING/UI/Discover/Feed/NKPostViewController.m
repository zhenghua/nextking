//
//  NKPostViewController.m
//  NEXTKING
//
//  Created by jinzhenghua on 29/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKPostViewController.h"
#import "NKRecordService.h"
#import "NKFileManager.h"
#import "UITextView+Placeholder.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define kImageWidth 65

@interface NKPostViewController () <DNPhotoBrowserDelegate>


@property (nonatomic, strong) UITextView *textView;

@property (nonatomic, strong) UIButton *addButton;

@property (nonatomic, strong) NSMutableArray *assetsArray;

@property (nonatomic, strong) ALAssetsLibrary *library;


@end

@implementation NKPostViewController

-(UITableViewStyle)tableViewStyle{
    return UITableViewStyleGrouped;
}

-(ALAssetsLibrary*)library{
    
    if (!_library) {
        _library = [ALAssetsLibrary new];
    }
    
    return _library;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.record = [[NKMRecord alloc] init];
        self.record.attachments = [NSMutableArray arrayWithCapacity:9];
        self.assetsArray = [NSMutableArray arrayWithCapacity:9];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addleftButtonWithTitle:@"取消"];
    [self addRightButtonWithTitle:@"发送"];
    
    CGFloat margin = 15.0;
    
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 175)];
    self.showTableView.height = NKMainHeight - self.showTableView.top;
    
    UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, -300, NKMainWidth, 300+self.showTableView.tableHeaderView.height)];
    [self.showTableView.tableHeaderView addSubview:background];
    background.backgroundColor = [UIColor whiteColor];
    
    self.addButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.showTableView.tableHeaderView addSubview:self.addButton];
    
    [_addButton addTarget:self action:@selector(headButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    _addButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _addButton.layer.borderWidth = 0.5;
    [_addButton setImage:[UIImage imageNamed:@"btn_nav_newfeed"] forState:UIControlStateNormal];
    _addButton.tintColor = [UIColor lightGrayColor];
    
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(margin, 10, NKMainWidth-margin*2, 75)];
    [self.showTableView.tableHeaderView addSubview:_textView];
    _textView.placeholder = self.isComment?@"添加评论":@"这一刻的想法...";
    _textView.placeholderColor = [UIColor lightGrayColor];
    _textView.font = [UIFont systemFontOfSize:17];
    
    if (self.isComment) {
        [_textView becomeFirstResponder];
    }
    
    [self reloadHeader];
}

-(void)reloadHeader{
    
    CGFloat margin = 15.0;
    CGFloat yMargin = 10.0;
    CGFloat yStart = 95.0;
    CGFloat totalHeight = 175.0;
    CGFloat nextX = margin;
    if (self.assetsArray.count>3) {
        totalHeight += yMargin+kImageWidth;
    }
    if (self.assetsArray.count>7){
        totalHeight += (yMargin+kImageWidth);
    }
    
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, totalHeight)];
    
    UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, -300, NKMainWidth, 300+self.showTableView.tableHeaderView.height)];
    [self.showTableView.tableHeaderView addSubview:background];
    background.backgroundColor = [UIColor whiteColor];
    
    [self.showTableView.tableHeaderView addSubview:_textView];
    
    for (int i=0; i<self.assetsArray.count; i++) {
        DNAsset *asset = self.assetsArray[i];
        
        [self.library assetForURL:asset.url resultBlock:^(ALAsset *asset) {
            
            NKImageView *imageView = [[NKImageView alloc] initWithFrame:CGRectMake(nextX, yStart, kImageWidth, kImageWidth)];
            imageView.target = self;
            imageView.singleTapped = @selector(imageTapped:);
            imageView.tag = 100+i;
            [self.showTableView.tableHeaderView addSubview:imageView];
            imageView.image = [UIImage imageWithCGImage:asset.aspectRatioThumbnail];
        } failureBlock:^(NSError *error) {
            
        }];
        nextX = margin + ((i+1)%4)*(kImageWidth+yMargin);
        if (i==3 || i==7) {
            yStart += yMargin+kImageWidth;
        }
    }
    
    if (self.assetsArray.count<9) {
        self.addButton.frame = CGRectMake(nextX, yStart, kImageWidth, kImageWidth);
        [self.showTableView.tableHeaderView addSubview:_addButton];
    }

}

-(void)imageTapped:(UIGestureRecognizer*)gesture{
    
    NSUInteger index = [[gesture view] tag]-100;
    NSMutableArray *array = [NSMutableArray array];
    
    for (DNAsset *asset in self.assetsArray) {
        [array addObject:asset.asset];
    }
    
    DNPhotoBrowser *browser = [[DNPhotoBrowser alloc] initWithPhotos:array
                                                        currentIndex:index
                                                           fullImage:NO];
    browser.delegate = self;
    browser.allowFullImagePick = NO;
    browser.isDelete = YES;
    browser.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:browser animated:YES];
    
}

-(void)rightButtonClick:(id)sender{
    self.record.content = self.textView.text;
    [self.textView resignFirstResponder];
    [self.record.attachments removeAllObjects];
    
    if (self.assetsArray.count<=0) {
        
        if (![self.textView hasText]) {
            [self showAlertWithMessage:@"请输入内容"];
            return;
        }
        
        [NKProgressHUD showTextHudInView:self.view text:@"发送中..."];
        NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(addOK:) andFailedSelector:@selector(addFailed:)];
        [[NKRecordService sharedNKRecordService] addRecordWithRecord:self.record andRequestDelegate:rd];
        return;
    }
    
    [NKProgressHUD showTextHudInView:self.view text:@"发送中..."];
     __block int callbackCount = 0;
    for (int i=0; i<self.assetsArray.count; i++) {
        DNAsset *asset = self.assetsArray[i];
        [self.library assetForURL:asset.url resultBlock:^(ALAsset *asset) {
            
            UIImage *image = [UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
            NSString *objectKey = [[NKFileManager fileManager] ObjectKeyForImage:image];
            NSString *size = [NSString stringWithFormat:@"%@,%@", [NSNumber numberWithFloat:ceilf(image.size.width)], [NSNumber numberWithFloat:ceilf(image.size.height)]];
    
            NKMAttachment *object = [[NKMAttachment alloc] init];
            object.content = size;
            object.pictureURL = objectKey;
            [self.record.attachments addObject:object];
            
            OSSContinuationBlock block = ^id(OSSTask *task) {
                callbackCount++;
                if (!task.error) {
                    
                    for (NKMAttachment *attachment in self.record.attachments) {
                        if ([attachment.pictureURL isEqualToString:task.result]) {
                            attachment.uploaded = YES;
                        }
                    }
                    if (callbackCount>=self.assetsArray.count) {
                        for (NKMAttachment *attachment in self.record.attachments) {
                            if (attachment.uploaded == NO) {
                                //失败
                                NKRequest *request = [[NKRequest alloc] init];
                                request.errorDescription = @"发送失败";
                                [self performSelectorOnMainThread:@selector(addFailed:) withObject:request waitUntilDone:NO];
                                return nil;
                            }
                        }
                        
                        NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(addOK:) andFailedSelector:@selector(addFailed:)];
                        [[NKRecordService sharedNKRecordService] addRecordWithRecord:self.record andRequestDelegate:rd];
                        
                    }
                }
                else{
                    if (callbackCount>=self.assetsArray.count) {
                        //失败
                        NKRequest *request = [[NKRequest alloc] init];
                        request.errorDescription = @"发送失败";
                        [self performSelectorOnMainThread:@selector(addFailed:) withObject:request waitUntilDone:NO];
                    }
                }
                
                return nil;
            };
            
            if (object.uploaded == YES) {
                block(nil);
                block = nil;
                return;
            }

            [[NKFileManager fileManager] uploadWithObjectKey:object.pictureURL block:block];
            
        } failureBlock:^(NSError *error) {
        }];
    }
    
}

-(void)addOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(postViewController:didAddRecordWithRecord:)]) {
        [self.delegate postViewController:self didAddRecordWithRecord:[request.results lastObject]];
    }
    
    [self leftButtonClick:nil];
}

-(void)addFailed:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlertWithMessage:request.errorDescription];
    
}

-(void)headButtonClicked:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"从手机相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)showImagePickerWithType:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerControllerSourceType sourceType = [UIImagePickerController isSourceTypeAvailable:type]?type:UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        
        DNImagePickerController *imagePicker = [[DNImagePickerController alloc] init];
        imagePicker.imagePickerDelegate = self;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
        return;
    }
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.allowsEditing = NO;
    [self presentViewController:imagePicker animated:YES completion:nil];
    imagePicker.delegate = self;
    imagePicker.sourceType = sourceType;
}

-(void)leftButtonClick:(id)sender{
    [_textView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self.library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
        
        DNAsset *asset = [[DNAsset alloc] init];
        asset.url = assetURL;
        NSArray *imageAssets = @[asset];
        [self dnImagePickerController:nil sendImages:imageAssets isFullImage:NO];
        
    }];

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)dnImagePickerController:(DNImagePickerController *)imagePickerController sendImages:(NSArray *)imageAssets isFullImage:(BOOL)fullImage{
    
    [self.assetsArray addObjectsFromArray:imageAssets];
    [self reloadHeader];
    
}

- (void)dnImagePickerControllerDidCancel:(DNImagePickerController *)imagePicker{
    [imagePicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - DNPhotoBrowserDelegate
- (void)sendImagesFromPhotobrowser:(DNPhotoBrowser *)photoBrowser currentAsset:(ALAsset *)asset{
  
}

- (NSUInteger)seletedPhotosNumberInPhotoBrowser:(DNPhotoBrowser *)photoBrowser{
    return 0;
}

- (BOOL)photoBrowser:(DNPhotoBrowser *)photoBrowser currentPhotoAssetIsSeleted:(ALAsset *)asset{
    return NO;
}

- (BOOL)photoBrowser:(DNPhotoBrowser *)photoBrowser seletedAsset:(ALAsset *)asset{
    return NO;
}

- (void)photoBrowser:(DNPhotoBrowser *)photoBrowser deletePhotoAtIndex:(NSUInteger)index{
    
    [self.assetsArray removeObjectAtIndex:index];
    [self reloadHeader];
}

@end
