//
//  NKFeedListViewController.m
//  NEXTKING
//
//  Created by King on 16/7/18.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKFeedListViewController.h"
#import "NKFeedDetailViewController.h"
#import "NKPostViewController.h"
#import "NKRecordCell.h"
#import "NKRecordService.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "NKUserService.h"

#import "YYFPSLabel.h"

@interface NKFeedListViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, DNImagePickerControllerDelegate, NKPostViewControllerDelegate, NKFeedDetailViewControllerDelegate>
@property (nonatomic, strong) YYFPSLabel *fpsLabel;

@end

@implementation NKFeedListViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = @"动态";
    [self addRightButtonWithTitle:[UIImage imageNamed:@"btn_nav_newfeed"]];
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    NSMutableArray *cachedArray = [[NKDataStore sharedDataStore] cachedArrayOf:NKCachePathFeeds andClass:[NKMRecord class]];
    if ([cachedArray count]) {
        self.dataSource = cachedArray;
        if ([[[[NKUserService sharedNKUserService] local] needFeedAlert] boolValue]) {
            [self refreshData];
        }
    }
    else{
        [self.showTableView.header beginRefreshing];
    }
    
    
    _fpsLabel = [YYFPSLabel new];
    [_fpsLabel sizeToFit];
    _fpsLabel.bottom = self.view.height - 15;
    _fpsLabel.left = 15;
    [self.view addSubview:_fpsLabel];
    
}

-(void)rightButtonClick:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"从手机相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];

}

-(void)showImagePickerWithType:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerControllerSourceType sourceType = [UIImagePickerController isSourceTypeAvailable:type]?type:UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        
        DNImagePickerController *imagePicker = [[DNImagePickerController alloc] init];
        imagePicker.imagePickerDelegate = self;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
        return;
    }
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.allowsEditing = NO;
    [self presentViewController:imagePicker animated:YES completion:nil];
    imagePicker.delegate = self;
    imagePicker.sourceType = sourceType;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
        
        DNAsset *asset = [[DNAsset alloc] init];
        asset.url = assetURL;
        NSArray *imageAssets = @[asset];
        
        [self showPostWithImages:imageAssets];
        
    }];
}

-(void)showPostWithImages:(NSArray*)imageAssets{
    NKPostViewController *postViewController = [[NKPostViewController alloc] init];
    postViewController.delegate = self;
    postViewController.view.backgroundColor = [UIColor whiteColor];
    NKNavigationController *navigation = [[NKNavigationController alloc] initWithRootViewController:postViewController];
    navigation.navigationBarHidden = YES;
    [postViewController dnImagePickerController:nil sendImages:imageAssets isFullImage:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    [NKNC presentViewController:navigation animated:YES completion:nil];
}

-(void)postViewController:(NKPostViewController*)viewController didAddRecordWithRecord:(NKMRecord*)record{
    
    [self.dataSource insertObject:record atIndex:0];
    [self.showTableView reloadData];
    [[NKDataStore sharedDataStore] cacheArray:self.dataSource forCacheKey:[self cachePathKey]];
    
    
    [self updateLocalFetchTimeWithTime:record.createTime];
    
}

- (void)dnImagePickerController:(DNImagePickerController *)imagePickerController sendImages:(NSArray *)imageAssets isFullImage:(BOOL)fullImage{
    
    [self showPostWithImages:imageAssets];
    
}

- (void)dnImagePickerControllerDidCancel:(DNImagePickerController *)imagePicker{
    [imagePicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithType:NKRecordTypeFeed offset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)updateLocalFetchTimeWithTime:(NSNumber*)time{
    
    if (time) {
        [NKUserService sharedNKUserService].notification.feedTime = time;
    }
    
    [[NKUserService sharedNKUserService] updateLocalNotification];
}

-(void)refreshDataOK:(NKRequest *)request{
    [super refreshDataOK:request];
    
    [self updateLocalFetchTimeWithTime:nil];
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithType:NKRecordTypeFeed offset:self.dataSource.count size:20 andRequestDelegate:rd];
}

-(NSString*)cachePathKey{
    return NKCachePathFeeds;
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKRecordCell *cell = (NKRecordCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKFeedDetailViewController *viewController = [NKFeedDetailViewController new];
    viewController.record = self.dataSource[indexPath.row];
    viewController.delegate = self;
    [NKNC pushViewController:viewController animated:YES];
}


-(void)feedDetailViewController:(NKFeedDetailViewController*)viewController didDeleteRecord:(NKMRecord*)record{
    
    if ([self.dataSource containsObject:record]) {
        [self.dataSource removeObject:record];
        [self.showTableView reloadData];
        [[NKDataStore sharedDataStore] cacheArray:self.dataSource forCacheKey:[self cachePathKey]];
    }
}

@end
