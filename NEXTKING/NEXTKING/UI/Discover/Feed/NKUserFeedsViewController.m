//
//  NKUserFeedsViewController.m
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKUserFeedsViewController.h"
#import "NKFeedDetailViewController.h"
#import "NKRecordCell.h"
#import "NKRecordService.h"

@interface NKUserFeedsViewController ()<NKFeedDetailViewControllerDelegate>

@end

@implementation NKUserFeedsViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = self.user.name;
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    [self.showTableView.header beginRefreshing];
    
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithUID:self.user.modelID offset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithUID:self.user.modelID offset:self.dataSource.count size:20 andRequestDelegate:rd];
}

//-(NSString*)cachePathKey{
//    return NKCachePathFeeds;
//}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"NKFeedCellIdentifier";
    
    NKRecordCell *cell = (NKRecordCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKFeedDetailViewController *viewController = [NKFeedDetailViewController new];
    viewController.record = self.dataSource[indexPath.row];
    viewController.delegate = self;
    [NKNC pushViewController:viewController animated:YES];
}


-(void)feedDetailViewController:(NKFeedDetailViewController*)viewController didDeleteRecord:(NKMRecord*)record{
    
    if ([self.dataSource containsObject:record]) {
        [self.dataSource removeObject:record];
        [self.showTableView reloadData];
    }
}


@end
