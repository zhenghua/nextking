//
//  NKUserViewController.h
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKUserViewController : NKTableViewController

@property (nonatomic, strong) NKMUser *user;

@end
