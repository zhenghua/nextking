//
//  NKRecordCell.h
//  NEXTKING
//
//  Created by jinzhenghua on 29/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKTableViewCell.h"

@interface NKRecordCell : NKTableViewCell

@property (nonatomic, strong) NKKVOLabel     *nameLabel;
@property (nonatomic, strong) UIButton    *likeButton;

@end
