//
//  NKFeedDetailViewController.h
//  NEXTKING
//
//  Created by King on 5/30/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@protocol NKFeedDetailViewControllerDelegate;

@interface NKFeedDetailViewController : NKTableViewController

@property (nonatomic, strong) NKMRecord *record;
@property (nonatomic, weak) id<NKFeedDetailViewControllerDelegate> delegate;

-(NSString*)recordType;
-(NSString*)parentID;

@end


@protocol NKFeedDetailViewControllerDelegate <NSObject>

@optional
-(void)feedDetailViewController:(NKFeedDetailViewController*)viewController didDeleteRecord:(NKMRecord*)record;

@end