//
//  NKFeedDetailViewController.m
//  NEXTKING
//
//  Created by King on 5/30/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKFeedDetailViewController.h"
#import "NKRecordService.h"
#import "NKRecordCell.h"
#import "NKPostViewController.h"
#import "NKLikersViewController.h"

#import "NKAccountManager.h"

@interface NKFeedDetailViewController ()<NKPostViewControllerDelegate,  NKFeedDetailViewControllerDelegate>

@property (nonatomic, strong) UIButton *commentButton;

@end

@implementation NKFeedDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupHeader];
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    [self refreshData];
    
    if ([self needCommentButton]) {
        self.commentButton = [[UIButton alloc] initWithFrame:CGRectMake(0, NKMainHeight-44, NKMainWidth, 44)];
        [self.contentView addSubview:_commentButton];
        [_commentButton addTarget:self action:@selector(commentButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_commentButton setTitle:@"评论" forState:UIControlStateNormal];
        [_commentButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_commentButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithWhite:0.9 alpha:0.8]] forState:UIControlStateHighlighted];
        [_commentButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 0.5)];
        line.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        [_commentButton addSubview:line];
        
        self.showTableView.height = self.showTableView.height-44;
    }
    
}

-(BOOL)needCommentButton{
    return YES;
}

-(void)setupHeader{
    
    [self addRightButtonWithTitle:[UIImage imageNamed:@"button_more"]];
    
    NKRecordCell *cell = [[NKRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    if (self.record.cellHeight<=0) {
        [NKRecordCell cellHeightForObject:self.record];
    }
    [cell showForObject:self.record withIndex:nil];
  
    cell.height = self.record.cellHeight;
    cell.width = NKMainWidth;
    cell.bottomLine.height = cell.bottomLine.height+12;
    cell.bottomLine.backgroundColor = [UIColor colorWithHexString:@"#f2f2f2"];
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, self.record.cellHeight+12)];
    [self.showTableView.tableHeaderView addSubview:cell];
}

-(void)commentButtonClicked:(id)sender{
    
    NKPostViewController *postViewController = [[NKPostViewController alloc] init];
    postViewController.delegate = self;
    postViewController.isComment = YES;
    postViewController.record.type = [self recordType];
    postViewController.record.parentID = [self parentID];
    postViewController.view.backgroundColor = [UIColor whiteColor];
    NKNavigationController *navigation = [[NKNavigationController alloc] initWithRootViewController:postViewController];
    navigation.navigationBarHidden = YES;
    [NKNC presentViewController:navigation animated:YES completion:nil];
    
}

-(void)postViewController:(NKPostViewController*)viewController didAddRecordWithRecord:(NKMRecord*)record{
    [self.dataSource insertObject:record atIndex:0];
    [self.showTableView reloadData];
    
    self.record.commentCount = [NSNumber numberWithInteger:MAX(0, self.record.commentCount.integerValue+1)];
}


-(void)refreshData{
    
    if (self.record) {
        
        NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(getRecordOK:)];
        [[NKRecordService sharedNKRecordService] getRecordWithID:self.record.modelID andRequestDelegate:rd];
        
    }

    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithType:[self recordType] offset:0 size:20 parentID:[self parentID] andRequestDelegate:rd];
     [self.showTableView.footer resetNoMoreData];
}

-(void)getRecordOK:(NKRequest*)request{
    
    if (request.results.count<=0) {
        return;
    }
    NKMRecord *record = [request.results lastObject];
    self.record.liked = record.liked;
    self.record.likeCount = record.likeCount;
    self.record.commentCount = record.commentCount;
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithType:[self recordType] offset:self.dataSource.count size:20 parentID:[self parentID] andRequestDelegate:rd];
}

-(NSString*)recordType{
    return NKRecordTypeComment;
}

-(NSString*)parentID{
    return self.record.modelID;
}

-(void)deleteRecord{
    
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(deleteOK:)];
    [[NKRecordService sharedNKRecordService] deleteRecordWithRecord:self.record andRequestDelegate:rd];
}

-(void)deleteOK:(NKRequest*)request{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(feedDetailViewController:didDeleteRecord:)]) {
        [self.delegate feedDetailViewController:self didDeleteRecord:self.record];
    }
    
    [self goBack:nil];
}

-(void)addCommentOK:(NKRequest*)request{
    
    self.record.commentCount = [NSNumber numberWithInteger:1+self.record.commentCount.integerValue];
    
}

-(void)rightButtonClick:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"粉丝" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showLikers];
    }]];
    
    if (self.record.sender == [NKMUser me] || [NKAccountManager isKing] ) {
        [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self deleteRecord];
        }]];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
}

-(void)showLikers{
    
    NKLikersViewController *viewController = [NKLikersViewController new];
    viewController.record = self.record;
    [NKNC pushViewController:viewController animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKRecordCell *cell = (NKRecordCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKFeedDetailViewController *viewController = [NKFeedDetailViewController new];
    viewController.record = self.dataSource[indexPath.row];
    viewController.delegate = self;
    [NKNC pushViewController:viewController animated:YES];
}

-(void)feedDetailViewController:(NKFeedDetailViewController*)viewController didDeleteRecord:(NKMRecord*)record{
    
    if ([self.dataSource containsObject:record]) {
        [self.dataSource removeObject:record];
        self.record.commentCount = [NSNumber numberWithInteger:MAX(0, self.record.commentCount.integerValue-1)];
        [self.showTableView reloadData];
    }
}

@end
