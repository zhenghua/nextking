//
//  NKLikersViewController.h
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKLikersViewController : NKTableViewController

@property (nonatomic, strong) NKMRecord *record;

@end
