//
//  NKRecordCell.m
//  NEXTKING
//
//  Created by jinzhenghua on 29/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKRecordCell.h"
#import "NKMRecord.h"
#import "NKFileManager.h"

#import "NKPhotoGroupView.h"
#import "NKRecordService.h"
#import "NKUserViewController.h"

@interface NKRecordCell ()

@property (nonatomic, strong) NKImageView *avatar;

@property (nonatomic, strong) UILabel     *contentLabel;
@property (nonatomic, strong) UILabel     *timeLabel;

@property (nonatomic, strong) NKKVOLabel  *commentLabel;
@property (nonatomic, strong) NKKVOLabel  *likeLabel;


@property (nonatomic, strong) NSMutableArray     *pictures;

@end

#define kContentLabelFont [[self class] contentLabelFont]
#define kContentLabelWidth [[self class] contentLabelWidth]
#define kWBCellPaddingPic 5

@implementation NKRecordCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(UIFont*)contentLabelFont{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return [UIFont systemFontOfSize:15];
            break;
        case NKUIStyleLiterature:
            return [UIFont systemFontOfSize:14];
            break;
            
        default:
            break;
    }
    
    return [UIFont systemFontOfSize:15];
}

+(CGFloat)contentLabelWidth{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return NKMainWidth-20-62;
            break;
        case NKUIStyleLiterature:
            return NKMainWidth-24-64;
            break;
            
        default:
            break;
    }
    return NKMainWidth-20-62;
}

+(CGFloat)contentLabelHeightWithText:(NSString*)text{
    
    if (!text) {
        return 0;
    }

    return ceilf([text boundingRectWithSize:CGSizeMake(kContentLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:kContentLabelFont,NSFontAttributeName,nil] context:nil].size.height);
}

static inline CGFloat CGFloatPixelRound(CGFloat value) {
    CGFloat scale = [[UIScreen mainScreen] scale];
    return round(value * scale) / scale;
}

static inline CGFloat NKPicWidth() {
    return MAX(75, CGFloatPixelRound((NKMainWidth - 64*2 - 2*kWBCellPaddingPic)/3));
}

+(CGFloat)imageHeightWithAttachments:(NSArray*)attachments{
    
    CGFloat height = 0;
    CGFloat picWidth = NKPicWidth();
    
    if ([attachments count]>0) {
        
        if (attachments.count==1) {
            
            NKMAttachment *attachment = [attachments lastObject];
            if (attachment.width<=attachment.height) {
                height = picWidth*2;
            }
            else{
                height = MAX(picWidth, attachment.height/attachment.width*picWidth*2);
            }
            
        }
        else if (attachments.count<4){
            height = picWidth ;
        }
        else if (attachments.count<7){
            height = picWidth+kWBCellPaddingPic+picWidth;
        }
        else{
            height = picWidth*3 + kWBCellPaddingPic*2;
        }
    }
    
    return ceilf(height);
    
}

+(CGFloat)startHeight{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return 33;
            break;
        case NKUIStyleLiterature:
            return 36;
            break;
            
        default:
            break;
    }
    return 33;
    
}

+(CGFloat)cellHeightForObject:(id)object{
    
    NKMRecord *record = object;
    if (record.cellHeight>0) {
        return record.cellHeight;
    }
    
    CGFloat height = [self startHeight];
    CGFloat yMargin = 10;
    
    if ([record.content length]>0) {
        
        height += (yMargin+ [self contentLabelHeightWithText:record.content]);
    }
    
    if ([record.attachments count]>0) {
        
        height += (yMargin+[self imageHeightWithAttachments:record.attachments]);
        
    }
    
    height += yMargin+27;
    
    record.cellHeight = ceilf(height);
    
    return height;
    
}

-(NSURL*)imageURLWithRecord:(NKMRecord*)record{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:record.sender.avatarPath imageSize:_avatar.frame.size]];
            break;
        case NKUIStyleLiterature:
            return [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:record.sender.avatarPath imageSize:_avatar.frame.size cornerRadius:_avatar.frame.size.width]];
            break;
            
        default:
            break;
    }

    
    return [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:record.sender.avatarPath imageSize:_avatar.frame.size]];
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMRecord *record = object;
    
    NSURL *imageURL = [self imageURLWithRecord:record];
    [self.avatar sd_setImageWithURL:imageURL];
    self.nameLabel.text = record.sender.showName;
    
    CGFloat height = [[self class] startHeight];
    CGFloat yMargin = 10;
    
    if ([record.content length]>0) {
        _contentLabel.hidden = NO;
        _contentLabel.text = record.content;
        _contentLabel.height = [[self class] contentLabelHeightWithText:record.content];
        height+= (yMargin+ _contentLabel.height);
    }
    else{
        _contentLabel.hidden = YES;
    }
    
    if (record.attachments.count>0) {
        CGFloat imageTop = height+yMargin;
        [self setImageViewWithTop:imageTop];
        height += (yMargin+[NKRecordCell imageHeightWithAttachments:record.attachments]);
    }
    else{
        [self hideImageView];
    }
    
    height+=10;
    self.timeLabel.top = height;
    _timeLabel.text = record.timeString;
    height+=27;
    
    _likeButton.center = CGPointMake(NKMainWidth-45, _timeLabel.centerY);
    _likeLabel.center = CGPointMake(_likeButton.centerX+35, _timeLabel.centerY);
    
    _commentLabel.center = CGPointMake(_likeButton.centerX-50, _timeLabel.centerY);
    
    [_likeLabel bindValueOfModel:record forKeyPath:@"likeCount"];
    [_commentLabel bindValueOfModel:record forKeyPath:@"commentCount"];
    
    self.bottomLine.top = record.cellHeight-0.5;
  
}

- (void)setImageViewWithTop:(CGFloat)imageTop{
    NKMRecord *record = self.showedObject;
    CGFloat xStart = _nameLabel.left;
    CGFloat picWidth = NKPicWidth();
    CGSize picSize = CGSizeMake(picWidth, picWidth);
    NSUInteger picsCount = record.attachments.count;
    for (int i=0; i<9; i++) {
        NKImageView *imageView = _pictures[i];
        if (i>=picsCount) {
            [imageView sd_cancelCurrentImageLoad];
            imageView.hidden = YES;
        }
        else{
            CGPoint origin = {0};
            switch (picsCount) {
                case 1: {
                    origin.x = xStart;
                    origin.y = imageTop;
                    
                    NKMAttachment *attachment = [record.attachments lastObject];
                    if (attachment.width<=attachment.height) {
                        picSize = CGSizeMake(picWidth*2*attachment.width/attachment.height, picWidth*2);
                    }
                    else{
                        picSize = CGSizeMake(picWidth*2, MAX(picWidth, attachment.height/attachment.width*picWidth*2));
                    }
                    
                } break;
                case 4: {
                    origin.x = xStart + (i % 2) * (picSize.width + kWBCellPaddingPic);
                    origin.y = imageTop + (int)(i / 2) * (picSize.height + kWBCellPaddingPic);
                } break;
                default: {
                    origin.x = xStart + (i % 3) * (picSize.width + kWBCellPaddingPic);
                    origin.y = imageTop + (int)(i / 3) * (picSize.height + kWBCellPaddingPic);
                } break;
            }
            imageView.frame = (CGRect){.origin = origin, .size = picSize};
            imageView.hidden = NO;
            
            NKMAttachment *attachment = [record.attachments objectAtIndex:i];
            NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:[attachment pictureURL] imageSize:picSize]];
            [imageView sd_setImageWithURL:imageURL placeholderImage:nil options:SDWebImageLowPriority progress:nil completed:nil
            ];
        }
    }

}

-(void)hideImageView{
    [self.pictures enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setHidden:YES];
    }];
}

-(void)avatarTapped:(UIGestureRecognizer*)gesture{
    
    NKUserViewController *viewController = [NKUserViewController new];
    NKMRecord *record = self.showedObject;
    viewController.user = record.sender;
    [NKNC pushViewController:viewController animated:YES];
}

-(CGRect)avatarFrame{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return  CGRectMake(10, 16, 43, 43);
            break;
        case NKUIStyleLiterature:
            return CGRectMake(24, 16, 32, 32);
            break;
            
        default:
            break;
    }
    
    return CGRectMake(10, 16, 43, 43);
    
}

-(NKKVOLabel*)nameLabel{
    
    if (!_nameLabel) {
        
        switch ([NKStyle uiStyle]) {
            case NKUIStyleNormal:{
                _nameLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+10, _avatar.top+2, 200, 16)];
                _nameLabel.font = [UIFont boldSystemFontOfSize:15];
                _nameLabel.textColor = [UIColor colorWithHexString:@"#5d64b0"];
            }
                break;
            case NKUIStyleLiterature:{
                _nameLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+8, _avatar.top, 200, 24)];
                _nameLabel.font = [UIFont systemFontOfSize:16];
                _nameLabel.textColor = [UIColor alphaGrayColor];
            }
                break;
                
            default:
                break;
        }
        
    }
    
    return _nameLabel;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 0.5)];
        [self.contentView addSubview:line];
        line.backgroundColor = [UIColor lineColor];
        self.bottomLine = (UIImageView*)line;
        
        self.avatar = [[NKImageView alloc] initWithFrame:[self avatarFrame]];
        [self.contentView addSubview:_avatar];
        _avatar.target = self;
        _avatar.singleTapped = @selector(avatarTapped:);
        

        [self.contentView addSubview:self.nameLabel];
        _nameLabel.target = self;
        _nameLabel.singleTapped = @selector(avatarTapped:);
        
        self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_nameLabel.left, [[self class] startHeight]+9, kContentLabelWidth, 0)];
        [self.contentView addSubview:_contentLabel];
        _contentLabel.font = kContentLabelFont;
        _contentLabel.numberOfLines = 0;
        
        self.pictures = [NSMutableArray arrayWithCapacity:9];
        
        NKImageView *imageView = nil;
        for (int i=0; i<9; i++) {
            imageView = [[NKImageView alloc] initWithFrame:CGRectZero];
            imageView.target = self;
            imageView.singleTapped = @selector(imageTapped:);
            imageView.tag = i+100;
            [self.contentView addSubview:imageView];
            [self.pictures addObject:imageView];
        }

        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_nameLabel.left, 0, 200, 14)];
        [self.contentView addSubview:_timeLabel];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = [UIColor lightGrayColor];
        
        self.likeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [self.contentView addSubview:_likeButton];
        [_likeButton addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        self.likeLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(0, 0, 40, 14)];
        [self.contentView addSubview:_likeLabel];
        _likeLabel.font = [UIFont systemFontOfSize:13];
        _likeLabel.textColor = _timeLabel.textColor;
        _likeLabel.target = self;
        _likeLabel.renderMethod = @selector(render:label:);
        
        self.commentLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(0, 0, 40, 14)];
        [self.contentView addSubview:_commentLabel];
        _commentLabel.font = [UIFont systemFontOfSize:13];
        _commentLabel.textColor = _timeLabel.textColor;
        _commentLabel.target = self;
        _commentLabel.renderMethod = @selector(render:label:);
        
    }
    return self;
}

-(NSString*)render:(NSNumber*)value label:(NKKVOLabel*)label{
    
    if (value.integerValue>0) {
        label.hidden = NO;
    }
    else{
        label.hidden = YES;
    }
    NKMRecord *record = self.showedObject;
    if (label==_likeLabel) {
        
        
        if ([record.liked boolValue]) {
            [_likeButton setImage:[UIImage imageNamed:@"list_like_red"] forState:UIControlStateNormal];
        }else{
            [_likeButton setImage:[UIImage imageNamed:@"list_like"] forState:UIControlStateNormal];
        }
        
        return [NSString stringWithFormat:@"%@", value];
    }
    else{
        
        if (label.hidden == YES) {
            return nil;
        }
        
        NSString *text = [record commentText];
        
        CGFloat width = [text boundingRectWithSize:CGSizeMake(100, _commentLabel.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_commentLabel.font, NSFontAttributeName, nil] context:nil].size.width;
        
        _commentLabel.left = _likeButton.centerX - width - 25;
        _commentLabel.width = width+10;
        
        return text;
    }
    return @"";
    
}

-(void)likeButtonClicked:(id)sender{
    
    NKMRecord *record = self.showedObject;
    
    BOOL liked = [record.liked boolValue];
    
    BOOL like = !liked;
    record.liked = [NSNumber numberWithBool:like];
    record.likeCount = [NSNumber numberWithInteger:MAX(0, record.likeCount.integerValue+(like?1:-1))];
    
    if (like) {
        
        CGRect frame = _likeButton.imageView.frame;
        _likeButton.imageView.frame = CGRectMake(-44, -44, 44*3, 44*3);
        
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _likeButton.imageView.frame = frame;
        } completion:nil];

    }
    
    [[NKRecordService sharedNKRecordService] likeWithRecordID:[self.showedObject modelID] sharesID:nil unlike:liked andRequestDelegate:nil];
}

-(void)imageTapped:(UIGestureRecognizer*)gesture{
    UIView *fromView = nil;
    NSMutableArray *items = [NSMutableArray new];
    NKMRecord *record = self.showedObject;
    for (NSUInteger i = 0, max = record.attachments.count; i < max; i++) {
        UIView *imgView = self.pictures[i];
        NKMAttachment *attachment = record.attachments[i];
        NKPhotoGroupItem *item = [NKPhotoGroupItem new];
        item.thumbView = imgView;
        item.largeImageSize = CGSizeMake(attachment.width, attachment.height);
        item.largeImageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:[attachment pictureURL] imageSize:CGSizeZero]];
        
        [items addObject:item];
        if (i == [gesture view].tag-100) {
            fromView = imgView;
        }
    }
    NKPhotoGroupView *v = [[NKPhotoGroupView alloc] initWithGroupItems:items];
    [v presentFromImageView:fromView toContainer:NKNC.view animated:YES completion:^{
        
    }];
}

@end
