//
//  NKUserViewController.m
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKUserViewController.h"
#import "NKPhotoGroupView.h"
#import "NKUserFeedsViewController.h"

@implementation NKUserViewController

//-(UITableViewStyle)tableViewStyle{
//    return UITableViewStyleGrouped;
//}

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = self.user.name;
    
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 100)];
    NKImageView *avatar = [[NKImageView alloc] initWithFrame:CGRectMake(0, 0, 72, 72)];
    avatar.center = CGPointMake(NKMainWidth/2, 60);
    [avatar sd_setImageWithURL:[NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:self.user.avatarPath imageSize:avatar.size cornerRadius:72]]];
    [self.showTableView.tableHeaderView addSubview:avatar];
    self.showTableView.tableHeaderView.backgroundColor = [UIColor whiteColor];
    self.showTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 0.5)];
    footer.backgroundColor = [UIColor lightGrayColor];
    footer.alpha = 0.5;
    self.showTableView.tableFooterView = footer;
    footer.layer.shadowOffset = CGSizeMake(0, 1);
    footer.layer.shadowOpacity = 0.2;
    footer.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    footer.layer.shadowRadius = 7.0;
    
    self.showTableView.backgroundColor = [self normalBackgroundColor];
    avatar.target = self;
    avatar.singleTapped = @selector(avatarTapped:);
    
    UIImageView *gender = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[self.user.gender isEqualToString:NKGenderKeyMale]?@"icon_male":@"icon_female"]];
    [self.showTableView.tableHeaderView addSubview:gender];
    gender.center = CGPointMake(avatar.centerX+26, avatar.centerY+26);
    
    
    NKCellDataObject *feeds = [NKCellDataObject cellDataObjectWithTitle:@"个人动态" titleImage:nil detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(feedsClicked:)];
    
 
    self.dataSource = [NSMutableArray arrayWithObjects:
                       @[feeds],
                       nil];
    
    if (self.user.sign) {
        NKCellDataObject *sign = [NKCellDataObject cellDataObjectWithTitle:@"签名" titleImage:nil detailText:self.user.sign accessoryView:@(UITableViewCellAccessoryNone) action:nil];
        [self.dataSource addObject:@[sign]];
    }
    
    if (self.user.timeString) {
        NKCellDataObject *time = [NKCellDataObject cellDataObjectWithTitle:@"最近在线" titleImage:nil detailText:self.user.timeString accessoryView:@(UITableViewCellAccessoryNone) action:nil];
        [self.dataSource addObject:@[time]];
    }
    
    if (self.user.birthday) {
        
        NKCellDataObject *age = [NKCellDataObject cellDataObjectWithTitle:@"年龄" titleImage:nil detailText:[NSString stringWithFormat:@"%.f", floor([[NSDate date] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:[self.user.birthday longLongValue]]]/(365*24*3600.0))]  accessoryView:@(UITableViewCellAccessoryNone) action:nil];
        NKCellDataObject *constellation = [NKCellDataObject cellDataObjectWithTitle:@"星座" titleImage:nil detailText:self.user.constellation accessoryView:@(UITableViewCellAccessoryNone) action:nil];
        
        [self.dataSource addObject:@[age, constellation]];
    }
    
    if (self.user.city) {
        
        NKCellDataObject *city = [NKCellDataObject cellDataObjectWithTitle:@"城市" titleImage:nil detailText:self.user.city accessoryView:@(UITableViewCellAccessoryNone) action:nil];
        [self.dataSource addObject:@[city]];
    }
    
}

-(void)feedsClicked:(NKCellDataObject*)object{
    NKUserFeedsViewController *viewController = [NKUserFeedsViewController new];
    viewController.user = self.user;
    [NKNC pushViewController:viewController animated:YES];
}

-(void)avatarTapped:(UIGestureRecognizer*)gesture{
    
    NKPhotoGroupItem *item = [NKPhotoGroupItem new];
    item.thumbView = [gesture view];
    item.largeImageSize = CGSizeMake(512, 512);
    item.largeImageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:[self.user avatarPath] imageSize:CGSizeZero]];
    NKPhotoGroupView *v = [[NKPhotoGroupView alloc] initWithGroupItems:@[item]];
    [v presentFromImageView:[gesture view] toContainer:NKNC.view animated:YES completion:^{
        
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    if (!object.cellName) {
        object.cellName = NSStringFromClass([NKTableViewCell class]);
    }
    return [NSClassFromString(object.cellName) cellHeightForObject:object];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 10;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 10;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    if (!object.cellName) {
        object.cellName = NSStringFromClass([NKTableViewCell class]);
    }
    
    NSString * cellIdentifier = object.cellName;
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NSClassFromString(object.cellName) alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    [cell showForObject:object withIndex:indexPath];
    cell.detailLabel.text = object.detailText;
    return cell;
}



@end
