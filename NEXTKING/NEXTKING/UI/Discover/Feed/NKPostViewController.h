//
//  NKPostViewController.h
//  NEXTKING
//
//  Created by jinzhenghua on 29/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"
#import "DNImagePickerController.h"


@protocol NKPostViewControllerDelegate;
@interface NKPostViewController : NKTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, DNImagePickerControllerDelegate>

@property (nonatomic, weak) id<NKPostViewControllerDelegate> delegate;
@property (nonatomic, assign) BOOL isComment;
@property (nonatomic, strong) NKMRecord *record;

@end

@protocol NKPostViewControllerDelegate <NSObject>

@optional
-(void)postViewController:(NKPostViewController*)viewController didAddRecordWithRecord:(NKMRecord*)record;

@end