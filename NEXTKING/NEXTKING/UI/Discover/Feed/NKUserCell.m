//
//  NKUserCell.m
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKUserCell.h"
#import "NKMUser.h"

@interface NKUserCell ()

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) NKKVOLabel *name;

@end

@implementation NKUserCell

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    return 60;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMUser *model = (NKMUser*)object;
    
    self.name.text = model.showName;
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:model.avatarPath imageSize:_avatar.frame.size]];
    
    [_avatar sd_setImageWithURL:imageURL];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        [self.contentView addSubview:_avatar];
        
        self.name = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+10, CGRectGetMinY(_avatar.frame)+5, 200, 20)];
        [self.contentView addSubview:_name];
        _name.font = [UIFont systemFontOfSize:18];
        
    }
    return self;
}


@end