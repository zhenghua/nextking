//
//  NKLikersViewController.m
//  NEXTKING
//
//  Created by King on 16/7/29.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKLikersViewController.h"
#import "NKRecordService.h"
#import "NKUserCell.h"
#import "NKUserViewController.h"

@implementation NKLikersViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = @"粉丝";
    [self addRefreshHeader];
    [self addGetMoreFooter];
    [self.showTableView.header beginRefreshing];
    
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listLikeWithRecordID:self.record.modelID offset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
    
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listLikeWithRecordID:self.record.modelID offset:self.dataSource.count size:20 andRequestDelegate:rd];
}


#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKUserCell *cell = (NKUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKUserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMUser *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [NKUserCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKUserViewController *viewController = [NKUserViewController new];
    viewController.user = [self.dataSource objectAtIndex:indexPath.row];
    [NKNC pushViewController:viewController animated:YES];

}


@end
