//
//  NKSharesCell.m
//  NEXTKING
//
//  Created by King on 16/7/9.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSharesCell.h"
#import "NKMUser.h"

@interface NKSharesCell ()

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) NKKVOLabel *name;
@property (nonatomic, strong) UILabel *code;
@property (nonatomic, strong) UIImageView *priceBack;
@property (nonatomic, strong) NKKVOLabel *price;
@property (nonatomic, strong) UILabel *percent;

@end

@implementation NKSharesCell

+(CGFloat)cellHeightForObject:(NKCellDataObject*)object{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return 60;
            break;
        case NKUIStyleLiterature:
            return 80;
            break;
            
        default:
            break;
    }
    
    return 80;
}

-(void)showForObject:(id)object withIndex:(NSIndexPath*)indexPath{
    
    self.showedObject = object;
    
    NKMShares *model = (NKMShares*)object;
    
    [self.name bindValueOfModel:model forKeyPath:@"name"];
   
    [self.price bindValueOfModel:model forKeyPath:@"price"];
    
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:{
            self.code.text = [model sharesID];
            NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:model.avatar imageSize:_avatar.frame.size]];
            
            [_avatar sd_setImageWithURL:imageURL];
        }
            break;
        case NKUIStyleLiterature:{
            
        }
            break;
            
        default:
            break;
    }
    
    
    
}

-(NSString*)render:(NSNumber*)value label:(NKKVOLabel*)label{
    
    NKMShares *model = (NKMShares*)self.showedObject;
    _percent.text = model.percentText;
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:{
            _percent.backgroundColor = [model priceColor];
            return [model priceText];
        }
            break;
        case NKUIStyleLiterature:{
            self.priceBack.image = [model priceImage];
            _percent.hidden = [model priceStatus]==NKMSharesPriceStautsNormal;
            return [NSString stringWithFormat:@"￥%@", [model priceText]];
        }
            break;
            
        default:
            break;
    }
    return [model priceText];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        switch ([NKStyle uiStyle]) {
            case NKUIStyleNormal:{
                self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
                [self.contentView addSubview:_avatar];
                
                self.name = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+10, CGRectGetMinY(_avatar.frame), 120, 20)];
                [self.contentView addSubview:_name];
                _name.font = [UIFont systemFontOfSize:18];
                
                CGRect nameFrame = _name.frame;
                nameFrame.origin.y+=nameFrame.size.height;
                nameFrame.size.height = 10;
                
                self.code = [[UILabel alloc] initWithFrame:nameFrame];
                [self.contentView addSubview:_code];
                _code.textColor = [UIColor darkGrayColor];
                _code.font = [UIFont systemFontOfSize:9];
                
                self.percent = [[UILabel alloc] initWithFrame:CGRectMake(NKMainWidth-75-15, 12, 75, 36)];
                [self.contentView addSubview:_percent];
                _percent.textColor = [UIColor whiteColor];
                _percent.textAlignment = NSTextAlignmentCenter;
                _percent.layer.cornerRadius = 2.0;
                _percent.font = [UIFont boldSystemFontOfSize:18];
                _percent.adjustsFontSizeToFitWidth = YES;
                
                CGRect percentFrame = _percent.frame;
                percentFrame.origin.x -= (20+100);
                percentFrame.size.width = 100;
                self.price = [[NKKVOLabel alloc] initWithFrame:percentFrame];
                _price.textAlignment = NSTextAlignmentRight;
                _price.font = [UIFont boldSystemFontOfSize:18];
                [self.contentView addSubview:_price];
                _price.target = self;
                _price.renderMethod = @selector(render:label:);
            }
                break;
            case NKUIStyleLiterature:{
                
                self.name = [[NKKVOLabel alloc] initWithFrame:CGRectMake(24, 28, 120, 24)];
                [self.contentView addSubview:_name];
                _name.font = [UIFont systemFontOfSize:16];
                _name.textColor = [UIColor alphaGrayColor];
                
                
                self.priceBack = [[UIImageView alloc] initWithFrame:CGRectMake(NKMainWidth-115, 16, 102, 48)];
                [self.contentView addSubview:_priceBack];
                
                self.price = [[NKKVOLabel alloc] initWithFrame:_priceBack.bounds];
                [_priceBack addSubview:_price];
                _price.textAlignment = NSTextAlignmentCenter;
                _price.font = [UIFont systemFontOfSize:18];
                _price.target = self;
                _price.renderMethod = @selector(render:label:);
                
                self.percent = [[UILabel alloc] initWithFrame:CGRectMake(_priceBack.left-75-8, 29, 75, 24)];
                [self.contentView addSubview:_percent];
                _percent.textColor = [UIColor alphaGrayColor];
                _percent.textAlignment = NSTextAlignmentRight;
                _percent.font = [UIFont systemFontOfSize:14];
                _percent.adjustsFontSizeToFitWidth = YES;
                
                self.bottomLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, [NKSharesCell cellHeightForObject:nil]-0.5, NKMainWidth, 0.5)];
                [self.contentView addSubview:self.bottomLine];
                self.bottomLine.backgroundColor = [UIColor lineColor];

                
            }
                break;
                
            default:
                break;
        }
        
        
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:{
            NKMShares *model = (NKMShares*)self.showedObject;
            _percent.backgroundColor = [model priceColor];
        }
            
            break;
        case NKUIStyleLiterature:
            break;
            
        default:
            break;
    }
    
}

@end
