//
//  NKSharesDetailViewController.m
//  NEXTKING
//
//  Created by King on 16/7/9.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSharesDetailViewController.h"
#import "NKOrderService.h"
#import "NKImageView.h"
#import "NKShareholdersViewController.h"
#import "NKKVOLabel.h"
#import "NKTradeView.h"
#import "NKSegmentControl.h"
#import "NKWebViewController.h"
#import "NKOrderCell.h"
#import "NKShareholderCell.h"
#import "NKRecordCell.h"

#import "NKUserViewController.h"
#import "NKSharesCommentViewController.h"
#import "NKSharesDetailLiteratureViewController.h"


#define kSectionHeight 30

@interface NKSharesDetailViewController ()<NKSegmentControlDelegate, NKTradeViewDelegate>

@property (nonatomic, strong) UIButton *buyButton;
@property (nonatomic, strong) UIButton *sellButton;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UIButton *commentButton;

@property (nonatomic, strong) UILabel  *rangeLabel;
@property (nonatomic, strong) UILabel  *percentLabel;

@property (nonatomic, strong) UILabel  *detailInfoLabel;

@property (nonatomic, strong) NKSegmentControl *segmentControl;

@property (nonatomic, strong) NSMutableArray *shareholdersArray;
@property (nonatomic, strong) NSMutableArray *commentsArray;

@property (nonatomic, weak) NKTradeView *tradeView;

@end

@implementation NKSharesDetailViewController

+(instancetype)new{
    
    switch ([NKStyle uiStyle]) {
        case NKUIStyleNormal:
            return [super new];
            break;
        case NKUIStyleLiterature:
            return (NKSharesDetailViewController*)[NKSharesDetailLiteratureViewController new];
            break;
        default:
            break;
    }
    
    return [super new];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addBackButton];
    
    self.titleLabel.text = self.shares.name;
    
    [self addRefreshHeader];
    [self addRightButtonWithTitle:[UIImage imageNamed:@"button_more"]];
    
    [self addTableViewHeader];
    
    CGFloat buttonHeight = 42;
    
    CGRect tableViewFrame = self.showTableView.frame;
    tableViewFrame.size.height -= buttonHeight;
    self.showTableView.frame = tableViewFrame;
    
    self.buyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, NKMainHeight-buttonHeight, NKMainWidth/4, buttonHeight)];
    [self.contentView addSubview:_buyButton];
    [_buyButton addTarget:self action:@selector(buyButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_buyButton setTitle:@"买入" forState:UIControlStateNormal];
    _buyButton.backgroundColor = [UIColor colorWithHexString:@"#3b7eee"];
    
    
    self.sellButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_buyButton.frame), NKMainHeight-buttonHeight, NKMainWidth/4, buttonHeight)];
    [self.contentView addSubview:_sellButton];
    [_sellButton addTarget:self action:@selector(sellButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_sellButton setTitle:@"卖出" forState:UIControlStateNormal];
    _sellButton.backgroundColor = [UIColor colorWithHexString:@"#ff7700"];
    
    self.commentButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_sellButton.frame), NKMainHeight-buttonHeight, NKMainWidth/4, buttonHeight)];
    [self.contentView addSubview:_commentButton];
    [_commentButton addTarget:self action:@selector(commentButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_commentButton setBackgroundColor:[UIColor whiteColor]];
    [_commentButton setTitle:@"评论" forState:UIControlStateNormal];
    [_commentButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_commentButton setBackgroundImage:[UIImage yy_imageWithColor:[self normalBackgroundColor]] forState:UIControlStateHighlighted];
    

    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_commentButton.frame), NKMainHeight-buttonHeight, NKMainWidth/4, buttonHeight)];
    [self.contentView addSubview:_followButton];
    [_followButton addTarget:self action:@selector(followButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_followButton setBackgroundColor:[UIColor whiteColor]];
    [_followButton setTitle:@"关注" forState:UIControlStateNormal];
    [_followButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_followButton setBackgroundImage:[UIImage yy_imageWithColor:[self normalBackgroundColor]] forState:UIControlStateHighlighted];
    
    [self.contentView addSubview:_commentButton];
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth/2, 0.5)];
    [_commentButton addSubview:topLine];
    topLine.backgroundColor = [self normalBackgroundColor];
    
    UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(_commentButton.width, 5, 0.5, _commentButton.height-10)];
    [_commentButton addSubview:verticalLine];
    verticalLine.backgroundColor = [self normalBackgroundColor];
    
    [self checkButton];

    [self refreshData];
}

-(void)commentButtonClicked:(id)sender{
    
    NKSharesCommentViewController *viewController = [NKSharesCommentViewController new];
    viewController.shares = self.shares;
    [NKNC pushViewController:viewController animated:YES];
}

-(void)checkButton{
    
    if ([self.shares.following boolValue]) {
        [_followButton setTitle:@"更多" forState:UIControlStateNormal];
    }
    else{
        [_followButton setTitle:@"关注" forState:UIControlStateNormal];
    }
    
}

-(void)followButtonClicked:(id)sender{
    
    if ([self.shares.following boolValue]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消关注" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self unfollow];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        
        [NKProgressHUD showTextHudInView:self.view text:@"正在关注"];
        NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(followOK:)];
        [[NKOrderService sharedNKOrderService] followSharesWithID:self.shares.modelID ifUnfollow:NO requestDelegate:rd];
    }
    
    
}

-(void)unfollow{
    [NKProgressHUD showTextHudInView:self.view text:@"正在取消关注"];
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(unfollowOK:)];
    [[NKOrderService sharedNKOrderService] followSharesWithID:self.shares.modelID ifUnfollow:YES requestDelegate:rd];
}

-(void)unfollowOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    self.shares.following = [NSNumber numberWithBool:NO];
    [self checkButton];
    
}

-(void)followOK:(NKRequest*)request{
    
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    self.shares.following = [NSNumber numberWithBool:YES];
    [self checkButton];
}

-(void)avatarTapped:(UIGestureRecognizer*)gesture{
    
    NKUserViewController *viewController = [NKUserViewController new];
    viewController.user = self.shares.user;
    [NKNC pushViewController:viewController animated:YES];
}

-(void)addTableViewHeader{
    
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 130)];
    
    CGFloat margin = 12;
    
    NKImageView *avatar = [[NKImageView alloc] initWithFrame:CGRectMake(margin, margin, 80, 80)];
    avatar.target = self;
    avatar.singleTapped = @selector(avatarTapped:);
    
    NSURL *imageURL = [NSURL URLWithString:[[NKFileManager fileManager] imageURLForObjectKey:self.shares.avatar imageSize:avatar.frame.size]];
    [avatar sd_setImageWithURL:imageURL];

    [self.showTableView.tableHeaderView addSubview:avatar];
    
    
    NKKVOLabel *priceLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(avatar.frame)+margin, CGRectGetMinY(avatar.frame), 120, 50)];
    [self.showTableView.tableHeaderView addSubview:priceLabel];
    priceLabel.font = [UIFont boldSystemFontOfSize:50];
    priceLabel.textColor = [UIColor whiteColor];
    priceLabel.target = self;
    priceLabel.renderMethod = @selector(priceWithValue:label:);
    priceLabel.textAlignment = NSTextAlignmentCenter;
    
    self.rangeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(priceLabel.frame)+10, CGRectGetWidth(priceLabel.frame)/2, 20)];
    [priceLabel addSubview:_rangeLabel];
    _rangeLabel.font = [UIFont systemFontOfSize:14];
    _rangeLabel.textAlignment = NSTextAlignmentCenter;
    _rangeLabel.adjustsFontSizeToFitWidth = YES;
    
    CGRect frame = _rangeLabel.frame;
    frame.origin.x = frame.size.width;
    
    self.percentLabel = [[UILabel alloc] initWithFrame:frame];
    [priceLabel addSubview:_percentLabel];
    _percentLabel.font = [UIFont systemFontOfSize:14];
    _percentLabel.textAlignment = NSTextAlignmentCenter;
    _percentLabel.adjustsFontSizeToFitWidth = YES;
    
    
    self.detailInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(avatar.frame), CGRectGetMaxY(avatar.frame), NKMainWidth-2*CGRectGetMinX(avatar.frame), 40)];
    [self.showTableView.tableHeaderView addSubview:_detailInfoLabel];
    _detailInfoLabel.font = [UIFont systemFontOfSize:14];
    
    [priceLabel bindValueOfModel:self.shares forKeyPath:@"price"];
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.showTableView.tableHeaderView.frame.size.height-0.5, NKMainWidth, 0.5)];
//    line.backgroundColor = [self normalBackgroundColor];
//    [self.showTableView.tableHeaderView addSubview:line];
    
}

-(NSString*)priceWithValue:(NSNumber*)price label:(NKKVOLabel*)label{
    
    _rangeLabel.text = [self.shares rangeText];
    _percentLabel.text = [self.shares percentText];
    
    label.textColor = [self.shares priceColor];
    _rangeLabel.textColor = label.textColor;
    _percentLabel.textColor = label.textColor;
    
    NSString *detailString = [NSString stringWithFormat:@"昨收 %.2f   成交量 %.0f   总市值 %@", [self.shares.closePrice floatValue], [self.shares.dealAmount floatValue], [self.shares marketValueText]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:detailString];
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#989899"] range:[detailString rangeOfString:@"昨收"]];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#989899"] range:[detailString rangeOfString:@"成交量"]];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#989899"] range:[detailString rangeOfString:@"总市值"]];
    _detailInfoLabel.attributedText = attributedString;
    
    return [self.shares priceText];
}

-(void)rightButtonClick:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"服务信息" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NKWebViewController *viewController = [NKWebViewController new];
        viewController.url = self.shares.link;
        [NKNC pushViewController:viewController animated:YES];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"股东信息" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NKShareholdersViewController *viewController = [NKShareholdersViewController new];
        viewController.shares = self.shares;
        [NKNC pushViewController:viewController animated:YES];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alert animated:YES completion:nil];
    
}


-(void)buyButtonClicked:(id)sender{
    
    NKTradeView *view = [[NKTradeView alloc] initWithShares:self.shares isBuy:YES];
    view.delegate = self;
    [view showInView:self.view];
    self.tradeView = view;
    
}

-(void)sellButtonClicked:(id)sender{
    
    NKTradeView *view = [[NKTradeView alloc] initWithShares:self.shares isBuy:NO];
    view.delegate = self;
    [view showInView:self.view];
    self.tradeView = view;
    
}

-(void)tradeView:(NKTradeView*)tradeView didAddOrderWithOrder:(NKMOrder*)order{
    
    [self refreshData];
}

-(void)refreshData{
    
    [[NKOrderService sharedNKOrderService] getSharesWithID:self.shares.modelID andRequestDelegate:[self requestDelegateWithSuccess:@selector(fetchSharesOK:)]];
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    
    switch (self.segmentControl.selectedIndex) {
        case 0:{
            [[NKOrderService sharedNKOrderService] listSharesOrdersWithSharesID:self.shares.modelID offset:0 size:5 price:self.shares.price type:0 andRequestDelegate:rd];
        }
            break;
        case 1:{
             [[NKOrderService sharedNKOrderService] listShareholdersWithSharesID:self.shares.modelID offset:0 size:20 andRequestDelegate:rd];
        }
        case 2:{
            
        }
            break;
        default:
            break;
    }
}

-(void)fetchSharesOK:(NKRequest*)request{
    
    NKMShares *shares = [request.results lastObject];
    if ([shares isKindOfClass:[NKMShares class]]) {
        
        self.shares.price = shares.price;
        self.shares.dealAmount = shares.dealAmount;
        
    }
}

-(void)refreshDataOK:(NKRequest *)request{
    
    [self.showTableView.header endRefreshing];
    
    switch (self.segmentControl.selectedIndex) {
        case 0:
            self.dataSource = request.results;
            break;
        case 1:
            self.shareholdersArray = request.results;
            break;
        case 2:{
            self.commentsArray = request.results;
        }
            break;
        default:
            break;
    }
    
    
    [self.showTableView reloadData];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kSectionHeight;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, kSectionHeight)];
    [view addSubview:self.segmentControl];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}


-(NKSegment*)segmentWithTitle:(NSString*)title{
    
    NKSegment *segment = [NKSegment segmentWithSize:CGSizeMake(NKMainWidth/2, kSectionHeight) color:[UIColor clearColor] andTitle:title];
    segment.normalTextColor = [UIColor lightGrayColor];
    segment.highlightTextColor = self.themeColorText;
    return segment;
}

- (NKSegmentControl *)segmentControl{
    
    if (!_segmentControl){
        
        _segmentControl = [NKSegmentControl segmentControlViewWithSegments:@[[self segmentWithTitle:@"买卖单"],[self segmentWithTitle:@"股东"]] andDelegate:self];
        UIView *bottomLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, NKMainWidth/5, 1)];
        bottomLineView.backgroundColor = self.themeColorText;
        [_segmentControl addScroller:bottomLineView];
        
    }
    return _segmentControl;
}

#pragma mark - segmentDelegate -

-(void)segmentControl:(NKSegmentControl*)control didChangeIndex:(NSInteger)index{
    
    [NKRequestDelegate removeTarget:self];
   
    switch (index){
        case 0:{
            [self refreshData];
        }
            break;
        case 1:
        case 2:{
            if (![[self dataArray] count]) {
                [self refreshData];
            }
            else{
                [self.showTableView reloadData];
            }

        }
            break;
        default:
            break;
    }
    
}


-(NSMutableArray*)dataArray{
    
    switch (self.segmentControl.selectedIndex) {
        case 0:
            return self.dataSource;
            break;
        case 1:
            return self.shareholdersArray;
            break;
        case 2:
            return self.commentsArray;
            break;
        default:
            break;
    }
    return self.dataSource;
}

-(Class)cellClass{
    
    switch (self.segmentControl.selectedIndex) {
        case 0:
            return [NKOrderCell class];
            break;
        case 1:
            return [NKShareholderCell class];
            break;
        case 2:
            return [NKRecordCell class];
            break;
        default:
            break;
    }
    return [NKOrderCell class];
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self dataArray] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NKTableViewCell *cell = nil;
    switch (self.segmentControl.selectedIndex) {
        case 0:{
            static NSString * cellIdentifier = @"NKSharesCellIdentifier";
            cell = (NKOrderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                cell = [[NKOrderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
                [(NKOrderCell*)cell setType:NKOrderCellTypeHandicap];
            }
        }
            break;
        case 1:{
            static NSString * cellIdentifier = @"NKShareholderCellIdentifier";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                cell = [[NKShareholderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            }
        }
            break;
            
        default:
            break;
    }
    
    id object = [[self dataArray] objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return [[self cellClass] cellHeightForObject:[self dataArray][indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (self.segmentControl.selectedIndex) {
        case 0:{
            NKMOrder *object = [[self dataArray] objectAtIndex:indexPath.row];
            
            if (!self.tradeView) {
                if ([object isBuy]) {
                    [self sellButtonClicked:nil];
                }
                else{
                    [self buyButtonClicked:nil];
                }
            }
            self.tradeView.price.text = [NSString stringWithFormat:@"%.2f", object.price.floatValue];
            
        }
            break;
        case 1:{
            NKMShareholder *object = [[self dataArray] objectAtIndex:indexPath.row];
            NKUserViewController *viewController = [NKUserViewController new];
            viewController.user = object.user;
            [NKNC pushViewController:viewController animated:YES];
        }
            break;
        case 2:
            break;
        default:
            break;
    }

    
    
}


@end
