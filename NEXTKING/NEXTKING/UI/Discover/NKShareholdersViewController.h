//
//  NKShareholdersViewController.h
//  NEXTKING
//
//  Created by King on 16/7/12.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKShareholdersViewController : NKTableViewController

@property (nonatomic, strong) NKMShares *shares;

@end
