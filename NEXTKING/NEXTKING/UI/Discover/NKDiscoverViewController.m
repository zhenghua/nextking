//
//  NKDiscoverViewController.m
//  NEXTKING
//
//  Created by King on 5/30/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKDiscoverViewController.h"
#import "NKDiscoverSharesViewController.h"
#import "NKFeedListViewController.h"

#import "NKUserService.h"

@interface NKDiscoverViewController ()

@end

@implementation NKDiscoverViewController

-(UITableViewStyle)tableViewStyle{
    
    return UITableViewStyleGrouped;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.titleLabel.text = @"探索";
    
    NKCellDataObject *feeds = [NKCellDataObject cellDataObjectWithTitle:@"动态" titleImage:[UIImage imageNamed:@"explor_timeline"] detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(feedsClicked:)];
    feeds.keyPath = @"needFeedAlert";
    feeds.target = [NKUserService sharedNKUserService].local;

    NKCellDataObject *discoverShares = [NKCellDataObject cellDataObjectWithTitle:@"时空" titleImage:[UIImage imageNamed:@"explor_near_man"] detailText:nil accessoryView:@(UITableViewCellAccessoryDisclosureIndicator) action:@selector(discoverSharesClicked:)];
    
    
    self.dataSource = [NSMutableArray arrayWithObjects:
                       @[discoverShares],
                       @[feeds],
                       nil];
    
}

-(void)feedsClicked:(NKCellDataObject*)object{
    
    NKFeedListViewController *viewController = [NKFeedListViewController new];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)peopleNearbyClicked:(NKCellDataObject*)object{
    
    
}

-(void)discoverSharesClicked:(NKCellDataObject*)object{
    
    NKDiscoverSharesViewController *viewController = [NKDiscoverSharesViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NKTableViewCell cellHeightForObject:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"DiscoverCellIdentifier";
    
    NKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    NKCellDataObject *object = self.dataSource[indexPath.section][indexPath.row];
    
    [cell showForObject:object withIndex:indexPath];
    [cell.badgeView bindValueOfModel:object.target forKeyPath:object.keyPath];


    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
