//
//  NKTradeView.m
//  NEXTKING
//
//  Created by King on 16/7/12.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTradeView.h"
#import "NKUI.h"
#import "NKOrderService.h"
#import "NKTradeKeyboard.h"
#import "NKProgressHUD.h"

@interface NKTradeView () <UITextFieldDelegate, NKTradeKeyboardDelegate>

@property (nonatomic) CGFloat lastY;

@property (nonatomic, strong) UILabel *priceLabel;


@property (nonatomic, strong) UITextField *amount;

@property (nonatomic, strong) UIButton   *priceAdd;
@property (nonatomic, strong) UIButton   *priceMinus;

@property (nonatomic, strong) UIButton   *amountAdd;
@property (nonatomic, strong) UIButton   *amountMinus;

@property (nonatomic, strong) UIButton   *actionButton;
@property (nonatomic, strong) UILabel    *moneyLabel;

@property (nonatomic, strong) NSNumber   *allowSellAmount;

@end

@implementation NKTradeView

-(void)dealloc{
    [NKRequestDelegate removeTarget:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note{

    if (![_price isFirstResponder] && ![_amount isFirstResponder]) {
        return;
    }
    
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    CGRect frame = self.frame;
    frame.origin.y = NKMainHeight - (keyboardBounds.size.height + self.frame.size.height-68);
    
    [UIView animateWithDuration:[duration doubleValue] delay:0 options:UIViewAnimationOptionBeginFromCurrentState|[curve intValue] animations:^{
        self.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)note{
    
    CGRect frame = self.frame;
    frame.origin.y = NKMainHeight-(self.frame.size.height-10);
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState|0 animations:^{
        self.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
}
-(instancetype)initWithShares:(NKMShares*)shares isBuy:(BOOL)isBuy{
    
    self = [super initWithFrame:CGRectMake(0, NKMainHeight-270, NKMainWidth, 280)];
    if (self) {
        
        self.shares = shares;
        self.isBuy = isBuy;
        
        self.tintColor = self.isBuy?[UIColor colorWithHexString:@"#3b7eee"]:[UIColor colorWithHexString:@"#ff7700"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        self.layer.shadowOffset = CGSizeMake(0, -1);
        self.layer.shadowOpacity = 0.2;
        self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.layer.shadowRadius = 7.0;
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        [self addGestureRecognizer:pan];
        
        self.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 0.5)];
        [self addSubview:line];
        line.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 41, 45)];
        [self addSubview:closeButton];
        [closeButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setImage:[UIImage imageNamed:@"trade_icon_close"] forState:UIControlStateNormal];
        
        
        CGFloat xMargin = 13.0;
        
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, 40, NKMainWidth-26, 15)];
        [self addSubview:_priceLabel];
        _priceLabel.font = [UIFont systemFontOfSize:14];
        _priceLabel.textColor = [self.shares priceColor];
        
        NSString *text = [NSString stringWithFormat:@"最新 %@(%@)" ,[self.shares priceText], [self.shares percentText]];
        NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:text];
        
        [attribute addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, 2)];
        
        _priceLabel.attributedText = attribute;
        
        
        self.price = [[UITextField alloc] initWithFrame:CGRectMake(xMargin, 60, NKMainWidth-106-xMargin, 42)];
        [self addSubview:_price];
        [self styleField:_price leftText: @"     价格"];
        _price.placeholder = @"0.00";
        _price.text = [NSString stringWithFormat:@"%.2f", self.shares.price.floatValue];
        
        self.amount = [[UITextField alloc] initWithFrame:CGRectMake(xMargin, CGRectGetMaxY(_price.frame)+10, CGRectGetWidth(_price.frame), CGRectGetHeight(_price.frame))];
        [self addSubview:_amount];
        [self styleField:_amount leftText: @"     仓位"];
        _amount.placeholder = @"输入数量";
        
        self.priceAdd = [self styleButton];
        [self addSubview:_priceAdd];
        [_priceAdd setImage:[UIImage imageNamed:@"trade_icon_add"] forState:UIControlStateNormal];
        _priceAdd.center = CGPointMake(NKMainWidth-33, CGRectGetMidY(_price.frame));
        [_priceAdd addTarget:self action:@selector(priceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        self.priceMinus = [self styleButton];
        [self addSubview:_priceMinus];
        [_priceMinus setImage:[UIImage imageNamed:@"trade_icon_minus"] forState:UIControlStateNormal];
        _priceMinus.center = CGPointMake(NKMainWidth-80, CGRectGetMidY(_price.frame));
        [_priceMinus addTarget:self action:@selector(priceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        self.amountAdd = [self styleButton];
        [self addSubview:_amountAdd];
        [_amountAdd setImage:[UIImage imageNamed:@"trade_icon_add"] forState:UIControlStateNormal];
        _amountAdd.center = CGPointMake(NKMainWidth-33, CGRectGetMidY(_amount.frame));
        [_amountAdd addTarget:self action:@selector(amountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        self.amountMinus = [self styleButton];
        [self addSubview:_amountMinus];
        [_amountMinus setImage:[UIImage imageNamed:@"trade_icon_minus"] forState:UIControlStateNormal];
        _amountMinus.center = CGPointMake(NKMainWidth-80, CGRectGetMidY(_amount.frame));
        [_amountMinus addTarget:self action:@selector(amountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        self.actionButton = [[UIButton alloc] initWithFrame:CGRectMake(xMargin, 270-52, NKMainWidth-xMargin*2, 42)];
        [self addSubview:_actionButton];
        
        [_actionButton setBackgroundImage:[UIImage yy_imageWithColor:self.isBuy?[UIColor colorWithHexString:@"#3b7eee"]:[UIColor colorWithHexString:@"#ff7700"]] forState:UIControlStateNormal];
        [_actionButton addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_actionButton setTitle:self.isBuy?@"买入":@"卖出" forState:UIControlStateNormal];
        _actionButton.layer.cornerRadius = 2.0;
        _actionButton.clipsToBounds = YES;
        
        CGRect priceLabelFrame = self.priceLabel.frame;
        priceLabelFrame.origin.y = CGRectGetMaxY(_amount.frame) + 41;
        
        self.moneyLabel = [[UILabel alloc] initWithFrame:priceLabelFrame];
        [self addSubview:_moneyLabel];
        _moneyLabel.font = [UIFont systemFontOfSize:14];
        _moneyLabel.textColor = [UIColor darkGrayColor];
        [self setMoneyLabelText];
        _moneyLabel.adjustsFontSizeToFitWidth = YES;
        
        UIButton *amountButton = nil;
        for (int i=0; i<4; i++) {
            amountButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [self addSubview:amountButton];
            amountButton.frame = CGRectMake(xMargin+i*52, CGRectGetMaxY(_amount.frame)+8, 46, 26);
            [amountButton addTarget:self action:@selector(positionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            amountButton.tag = 1000+i;
            [amountButton setTitle:[self buttonTitleWithTag:amountButton.tag] forState:UIControlStateNormal];
            amountButton.titleLabel.font = [UIFont systemFontOfSize:12];
            
            [self addButtonBorder:amountButton];
            
        }
        
        if (!self.isBuy) {
            [self fetchAllowSellAmount];
        }
    }
    return self;
}

-(void)fetchAllowSellAmount{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(fetchOK:) andFailedSelector:@selector(fetchFailed:)];
    [[NKOrderService sharedNKOrderService] getShareholderWithSharesID:self.shares.modelID andRequestDelegate:rd];
    
}
-(void)fetchOK:(NKRequest*)request{
    
    NKMShareholder *holder = [request.results lastObject];
    self.allowSellAmount = [NSNumber numberWithFloat:holder.amount.floatValue-holder.sellingAmount.floatValue];
    [self setMoneyLabelText];
    
}
-(void)fetchFailed:(NKRequest*)request{
    
}

-(NSString*)buttonTitleWithTag:(NSInteger)tag{
    
    switch (tag) {
        case 1000:
            return @"全仓";
            break;
        case 1001:
            return @"1/2";
            break;
        case 1002:
            return @"1/3";
            break;
        case 1003:
            return @"1/4";
            break;
        default:
            break;
    }
    return @"";
    
}

-(void)setMoneyLabelText{
    
    NSString *orderMoney = @"--";
    if ([_amount.text length]>0) {
        orderMoney = [NSString stringWithFormat:@"%.2f", _price.text.floatValue*_amount.text.floatValue];
    }
    
    NKMWallet *wallet = [[NKMUser me] wallet];
    float money = [wallet.money floatValue]-[wallet.orderMoney floatValue];
    NSString *walletMoney = [NSString stringWithFormat:@"%.2f", money];
    _moneyLabel.text = [NSString stringWithFormat:@"订单金额: %@   可用资金: %@", orderMoney, walletMoney];
    if (self.isBuy) {
        _moneyLabel.text = [_moneyLabel.text stringByAppendingFormat:@"   可买: %@", [self allowAmount]];
    }else{
        _moneyLabel.text = [_moneyLabel.text stringByAppendingFormat:@"   可卖: %@", [self allowAmount]];
    }
    
}


-(NSNumber *)allowAmount{
    if (self.isBuy) {
        NKMWallet *wallet = [[NKMUser me] wallet];
        float money = [wallet.money floatValue]-[wallet.orderMoney floatValue];
        float price = MAX(_price.text.floatValue, 0.01);
        NSNumber *allowAmount =[NSNumber numberWithInt:money/price];
        return allowAmount;
    }
    else{
        return self.allowSellAmount?self.allowSellAmount:[NSNumber numberWithInteger:0];
    }
}

-(void)actionButtonClicked:(UIButton*)button{
    
    if (_price.text.floatValue<0.01) {
        [self showMessage:@"请输入价格"];
        return;
    }
    
    if (_amount.text.floatValue<=0) {
        [self showMessage:@"请输入数量"];
        return;
    }
    
    if (!self.isBuy && _amount.text.floatValue>[self allowSellAmount].floatValue) {
        [self showMessage:@"超出可卖数量"];
        return;
    }
  
    [NKProgressHUD showTextHudInView:self text:@"提交中"];
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(addOK:) andFailedSelector:@selector(addFailed:)];
    [[NKOrderService sharedNKOrderService] addOrderWithSharesID:self.shares.modelID price:_price.text.floatValue amount:_amount.text.floatValue isBuy:self.isBuy andRequestDelegate:rd];
}

-(void)showMessage:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil]];
    [NKNC presentViewController:alertController animated:YES completion:nil];
}

-(void)addOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self animated:YES];
    [self showMessage:@"委托成功"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tradeView:didAddOrderWithOrder:)]) {
        [self.delegate tradeView:self didAddOrderWithOrder:[request.results lastObject]];
    }
    
    [self closeButtonClicked:nil];
}

-(void)addFailed:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self animated:YES];
    [self showMessage:request.errorDescription];
}

-(void)positionButtonClicked:(UIButton*)button{
    NSInteger i = button.tag-1000+1;
    [self setPostionWithInteger:i];
}

-(void)setPostionWithInteger:(NSInteger)integer{
    NSInteger amount = [[self allowAmount] intValue]/integer;
    _amount.text = [NSString stringWithFormat:@"%ld", (long)amount];
    
    [self setMoneyLabelText];
}

-(void)priceButtonClicked:(UIButton*)button{
    
    CGFloat value = -0.01;
    
    if (button==_priceAdd) {
        value = 0.01;
    }
    
    if ([_price isFirstResponder] || [_amount isFirstResponder]) {
        [_price becomeFirstResponder];
    }
    
    
    
    self.price.text = [NSString stringWithFormat:@"%.2f", MAX(self.price.text.floatValue + value, 0.01)];
    [self setMoneyLabelText];
    
}

-(void)amountButtonClicked:(UIButton*)button{
    
    CGFloat value = -1;
    
    if (button==_amountAdd) {
        value = 1;
    }
    
    if ([_price isFirstResponder] || [_amount isFirstResponder]) {
        [_amount becomeFirstResponder];
    }
    
    self.amount.text = [NSString stringWithFormat:@"%.0f", MAX(self.amount.text.floatValue + value, 0)];
    [self setMoneyLabelText];
}

-(void)textFieldDidChange:(NSNotification*)notification{
    
    if (notification.object != _price && notification.object != _amount) {
        return;
    }
    
    if (notification.object == _amount) {
        _amount.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:_amount.text.integerValue]];
    }
    
    [self setMoneyLabelText];

}

-(UIButton*)styleButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, 42, 42);
    button.backgroundColor = [UIColor colorWithHexString:@"#edf0f5"];
    [self addButtonBorder:button];
    return button;
}

-(void)addButtonBorder:(UIButton*)button{
    button.layer.borderWidth = 0.5;
    button.layer.cornerRadius = 2.0;
    button.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.8].CGColor;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (!textField.inputView) {
        
        if (textField == _price) {
            _price.inputView = [[NKTradeKeyboard alloc] initWithType:NKTradeKeyboardTypePrice delegate:self];
        }
        else if (textField == _amount){
            _amount.inputView = [[NKTradeKeyboard alloc] initWithType:NKTradeKeyboardTypeAmount delegate:self];
        }
    }
    
    return YES;
}


-(void)keyboard:(NKTradeKeyboard*)keyboard didClickedButton:(UIButton*)button{
    
    UITextField *field = nil;
    
    if (keyboard.keyboardType == NKTradeKeyboardTypePrice) {
        field = _price;
    }
    else{
        field = _amount;
    }
    
    switch (button.tag) {
        case 201:
            [field resignFirstResponder];
            break;
        case 202:
            field.text = nil;
            break;
        case 203:{
            [field deleteBackward];
        }
            break;
        case 204:{
            if (field == _price) {
                [_amount becomeFirstResponder];
            }
            else if (field == _amount){
                [field resignFirstResponder];
                [self actionButtonClicked:button];
            }
        }
            break;
        case 301: case 302:
        case 303: case 304:{
            [self setPostionWithInteger:button.tag-300];
        }
            break;
        
        case 100: case 101: case 102: case 103: case 104:
        case 105: case 106: case 107: case 108: case 109:{
            
            BOOL canAdd = NO;
            if (field == _amount && field.text.length<7) {
                canAdd = YES;
            }
            else if (field == _price && field.text.floatValue<100000){
                if ([field.text rangeOfString:@"."].length==1) {
                    if ([[[field.text componentsSeparatedByString:@"."] lastObject] length]<2) {
                        canAdd = YES;
                    }
                    else if ([field offsetFromPosition:field.selectedTextRange.start toPosition:field.endOfDocument] >2) {
                        canAdd = YES;
                    }
                }
                else{
                    canAdd = YES;
                }
            }
            if (canAdd) {
                [field insertText:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:button.tag-100]]];
            }

        }
            break;
        case 110:{
            if (field == _price && field.text.floatValue<100000){
                if ([field.text rangeOfString:@"."].length==0){
                    [field insertText:@"."];
                }
            }
        }
            break;
            
        default:
            break;
    }
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.layer.borderColor = self.tintColor.CGColor;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    textField.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.8].CGColor;
}

-(void)styleField:(UITextField*)field leftText:(NSString*)text{
    
    field.font = [UIFont systemFontOfSize:14];
    field.layer.borderWidth = 0.5;
    field.layer.cornerRadius = 2.0;
    field.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.8].CGColor;
    field.leftViewMode = UITextFieldViewModeAlways;
    UILabel *label = [self leftLabelForField:field];
    label.text = text;
    field.leftView = label;
    
    field.delegate = self;
}

-(UILabel *)leftLabelForField:(UITextField*)field{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 72, CGRectGetHeight(field.frame))];
    label.font = [UIFont boldSystemFontOfSize:12];
    label.textColor = [UIColor darkGrayColor];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 18)];
    [label addSubview:line];
    line.backgroundColor = [UIColor colorWithCGColor:field.layer.borderColor];
    line.center = CGPointMake(60, CGRectGetHeight(field.frame)/2);
    
    return label;
}

-(void)pan:(UIPanGestureRecognizer*)gesture{
    
    if ([_price isFirstResponder] || [_amount isFirstResponder]) {
        [_price resignFirstResponder];
        [_amount resignFirstResponder];
        //return;
    }
    
    switch (gesture.state) {
        case 1:{
            _lastY = [gesture locationInView:self].y;
        }
            break;
        case 2:{
            //Move
            CGRect frame = self.frame;
            frame.origin.y += [gesture locationInView:self].y - _lastY;
            if (frame.origin.y<NKMainHeight-270) {
                frame.origin.y = NKMainHeight-270;
            }
            self.frame = frame;
            _lastY = [gesture locationInView:self].y;
        }
            break;
        default:{
            CGRect frame = self.frame;
            if (frame.origin.y<NKMainHeight-270+80) {
                [UIView animateWithDuration:0.2 animations:^{
                    CGRect frame = self.frame;
                    frame.origin.y = NKMainHeight-270;
                    self.frame = frame;
                }];
            }
            else{
                [self closeButtonClicked:nil];
            }
        }
            break;
    }
    
}

-(void)showInView:(UIView*)view{
    CGRect frame = self.frame;
    frame.origin.y = NKMainHeight;
    self.frame = frame;
    
    [view addSubview:self];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = NKMainHeight-280;
        self.frame = frame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame = self.frame;
            frame.origin.y = NKMainHeight-270;
            self.frame = frame;
        }];
    }];
}

-(void)closeButtonClicked:(id)sender{
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = NKMainHeight;
        self.frame = frame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}


@end
