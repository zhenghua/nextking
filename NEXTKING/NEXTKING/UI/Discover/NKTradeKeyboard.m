//
//  NKTradeKeyboard.m
//  NEXTKING
//
//  Created by King on 16/7/15.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTradeKeyboard.h"
#import "NKUI.h"

@interface NKKeyboardObject : NSObject

@property (nonatomic, strong) id title;
@property (nonatomic) NSInteger tag;
@property (nonatomic) BOOL notBold;

+(instancetype)objectWithTitle:(id)title tag:(NSInteger)tag;
+(instancetype)objectWithTitle:(id)title tag:(NSInteger)tag notBold:(BOOL)notBold;

@end

@implementation NKKeyboardObject

+(instancetype)objectWithTitle:(id)title tag:(NSInteger)tag notBold:(BOOL)notBold{
    
    NKKeyboardObject *object = [NKKeyboardObject new];
    
    object.title = title;
    object.tag = tag;
    object.notBold = notBold;
    
    return object;
}

+(instancetype)objectWithTitle:(id)title tag:(NSInteger)tag{
    
    return [self objectWithTitle:title tag:tag notBold:NO];
}

@end

@implementation NKTradeKeyboard

-(instancetype)initWithType:(NKTradeKeyboardType)type delegate:(id<NKTradeKeyboardDelegate>)delegate{
    
    self = [super initWithFrame:CGRectMake(0, 0, NKMainWidth, 216)];
    if (self) {
        self.keyboardType = type;
        self.backgroundColor = [UIColor whiteColor];
        self.delegate = delegate;
        
        int column = type==NKTradeKeyboardTypePrice?4:5;
        
        CGFloat buttonHeight = 216.0/4.0;
        CGFloat buttonWidth = NKMainWidth/column;
        
        CGFloat oneXMargin = 0;
        CGFloat y = 0;
        
        NSArray *array;
        
        if (type==NKTradeKeyboardTypePrice) {
            array = [NSArray arrayWithObjects:
                     [NKKeyboardObject objectWithTitle:@"1" tag:101],
                     [NKKeyboardObject objectWithTitle:@"2" tag:102],
                     [NKKeyboardObject objectWithTitle:@"3" tag:103],
                     [NKKeyboardObject objectWithTitle:[UIImage imageNamed:@"icon_keyboard_hide"] tag:201],
                     
                     [NKKeyboardObject objectWithTitle:@"4" tag:104],
                     [NKKeyboardObject objectWithTitle:@"5" tag:105],
                     [NKKeyboardObject objectWithTitle:@"6" tag:106],
                     [NKKeyboardObject objectWithTitle:@"清空" tag:202 notBold:YES],
                     
                     [NKKeyboardObject objectWithTitle:@"7" tag:107],
                     [NKKeyboardObject objectWithTitle:@"8" tag:108],
                     [NKKeyboardObject objectWithTitle:@"9" tag:109],
                     [NKKeyboardObject objectWithTitle:[UIImage imageNamed:@"icon_keyboard_delete"] tag:203],
                     
                     [NKKeyboardObject objectWithTitle:@"." tag:110],
                     [NKKeyboardObject objectWithTitle:@"0" tag:100],
                     [NKKeyboardObject objectWithTitle:nil tag:999],
                     [NKKeyboardObject objectWithTitle:@"下一项" tag:204  notBold:YES],
                     
                     nil];
        }
        else{
            array = [NSArray arrayWithObjects:
                     [NKKeyboardObject objectWithTitle:@"全仓" tag:301 notBold:YES],
                     [NKKeyboardObject objectWithTitle:@"1" tag:101],
                     [NKKeyboardObject objectWithTitle:@"2" tag:102],
                     [NKKeyboardObject objectWithTitle:@"3" tag:103],
                     [NKKeyboardObject objectWithTitle:[UIImage imageNamed:@"icon_keyboard_hide"] tag:201],
                     
                     [NKKeyboardObject objectWithTitle:@"半仓" tag:302 notBold:YES],
                     [NKKeyboardObject objectWithTitle:@"4" tag:104],
                     [NKKeyboardObject objectWithTitle:@"5" tag:105],
                     [NKKeyboardObject objectWithTitle:@"6" tag:106],
                     [NKKeyboardObject objectWithTitle:@"清空" tag:202 notBold:YES],
                     
                     [NKKeyboardObject objectWithTitle:@"1/3仓" tag:303 notBold:YES],
                     [NKKeyboardObject objectWithTitle:@"7" tag:107],
                     [NKKeyboardObject objectWithTitle:@"8" tag:108],
                     [NKKeyboardObject objectWithTitle:@"9" tag:109],
                     [NKKeyboardObject objectWithTitle:[UIImage imageNamed:@"icon_keyboard_delete"] tag:203],
                     
                     [NKKeyboardObject objectWithTitle:@"1/4仓" tag:304 notBold:YES],
                     [NKKeyboardObject objectWithTitle:nil tag:999],
                     [NKKeyboardObject objectWithTitle:@"0" tag:100],
                     [NKKeyboardObject objectWithTitle:nil tag:999],
                     [NKKeyboardObject objectWithTitle:@"确定" tag:204  notBold:YES],
                     
                     nil];

        }
        
        UIButton *button;
        NKKeyboardObject *object = nil;
        for (int i=0; i<[array count]; i++) {
            if (i%column==0 && i!=0) {
                y+=buttonHeight;
            }
            button = [[UIButton alloc] initWithFrame:CGRectMake(oneXMargin+buttonWidth*(i%column), y, buttonWidth, buttonHeight)];
            [self addSubview:button];
            object = [array objectAtIndex:i];
            
            if (object.notBold) {
                button.titleLabel.font = [UIFont systemFontOfSize:16];
            }
            else {
                button.titleLabel.font = [UIFont boldSystemFontOfSize:19];
            }

            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            id title = [object title];
            
            if ([title isKindOfClass:[NSString class]]) {
                [button setTitle:title forState:UIControlStateNormal];
            }
            else if ([title isKindOfClass:[UIImage class]]){
                [button setImage:title forState:UIControlStateNormal];
            }
            [button setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#dbdfe6"]] forState:UIControlStateHighlighted];
            button.tag = [object tag];
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        UIView *line = nil;
        UIView *hLine = nil;
        
        for (int i=0; i<4; i++) {
            line = [[UIView alloc] initWithFrame:CGRectMake(0, buttonHeight*i, NKMainWidth, 0.5)];
            hLine = [[UIView alloc] initWithFrame:CGRectMake(buttonWidth*(i+1), 0, 0.5, 216)];
            [self addSubview:line];
            [self addSubview:hLine];
            line.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
            hLine.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        }
        
        UIButton *returnButton = [self viewWithTag:204];
        returnButton.backgroundColor = [UIColor colorWithHexString:@"#3b7eee"];
        [returnButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [returnButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#2f66be"]] forState:UIControlStateHighlighted];
        
    }
    return self;
}

-(void)buttonClicked:(UIButton*)button{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(keyboard:didClickedButton:)]) {
        [self.delegate keyboard:self didClickedButton:button];
    }
    
    
}

@end
