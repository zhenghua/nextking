//
//  NKTradeKeyboard.h
//  NEXTKING
//
//  Created by King on 16/7/15.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    NKTradeKeyboardTypePrice,
    NKTradeKeyboardTypeAmount
}NKTradeKeyboardType;

@protocol NKTradeKeyboardDelegate;

@interface NKTradeKeyboard : UIView

@property (nonatomic) NKTradeKeyboardType keyboardType;
@property (nonatomic, weak) id<NKTradeKeyboardDelegate> delegate;

-(instancetype)initWithType:(NKTradeKeyboardType)type delegate:(id<NKTradeKeyboardDelegate>)delegate;

@end


@protocol NKTradeKeyboardDelegate <NSObject>

@optional
-(void)keyboard:(NKTradeKeyboard*)keyboard didClickedButton:(UIButton*)button;


@end