//
//  NKDiscoverSharesViewController.m
//  NEXTKING
//
//  Created by King on 16/7/9.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKDiscoverSharesViewController.h"
#import "NKOrderService.h"
#import "NKSharesCell.h"
#import "NKSharesDetailViewController.h"

NSString *const NKCachePathDiscoverShares = @"NKCachePathDiscoverShares.NK";

@implementation NKDiscoverSharesViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self addBackButton];
    
    self.titleLabel.text = @"时空";
    
    [self addRefreshHeader];
    [self addGetMoreFooter];
    
    self.dataSource = [[NKDataStore sharedDataStore] cachedArrayOf:[self cachePathKey] andClass:[NKMShares class]];
    
    [self.showTableView.header beginRefreshing];
    
}

-(NSString*)cachePathKey{
    
    return NKCachePathDiscoverShares;
}


-(void)refreshData{
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] discoverWithOffset:0 size:20 andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
}

-(void)getMoreData{
    
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] discoverWithOffset:self.dataSource.count size:20 andRequestDelegate:rd];
    
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellIdentifier = @"NKSharesCellIdentifier";
    
    NKSharesCell *cell = (NKSharesCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NKSharesCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NKMShares *record = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:record withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKSharesCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKSharesDetailViewController *viewController = [NKSharesDetailViewController new];
    viewController.shares = self.dataSource[indexPath.row];
    [NKNC pushViewController:viewController animated:YES];
    
}





@end
