//
//  NKTradeView.h
//  NEXTKING
//
//  Created by King on 16/7/12.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NKMUser.h"

@protocol NKTradeViewDelegate;

@interface NKTradeView : UIView

@property (nonatomic, strong) NKMShares *shares;
@property (nonatomic) BOOL isBuy;
@property (nonatomic, strong) UITextField *price;

@property (nonatomic, weak) id<NKTradeViewDelegate> delegate;

-(instancetype)initWithShares:(NKMShares*)shares isBuy:(BOOL)isBuy;

-(void)showInView:(UIView*)view;

@end

@protocol NKTradeViewDelegate <NSObject>
@optional
-(void)tradeView:(NKTradeView*)tradeView didAddOrderWithOrder:(NKMOrder*)order;
@end