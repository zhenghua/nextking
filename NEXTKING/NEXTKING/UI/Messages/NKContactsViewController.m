//
//  NKContactsViewController.m
//  NEXTKING
//
//  Created by King on 16/7/4.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKContactsViewController.h"

@interface NKContactsViewController ()

@end

@implementation NKContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"通讯录";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
