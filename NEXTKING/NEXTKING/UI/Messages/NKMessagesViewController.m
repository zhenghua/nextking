//
//  NKMessagesViewController.m
//  NEXTKING
//
//  Created by King on 16/7/4.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKMessagesViewController.h"
#import "NKContactsViewController.h"

@interface NKMessagesViewController ()

@end

@implementation NKMessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.titleLabel.text = @"消息";
    
    [self addRightButtonWithTitle:@[[UIImage imageNamed:@"btn_nav_contacts"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)rightButtonClick:(id)sender{
    
    NKContactsViewController *viewController = [[NKContactsViewController alloc] init];
    [NKNC pushViewController:viewController animated:YES];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
