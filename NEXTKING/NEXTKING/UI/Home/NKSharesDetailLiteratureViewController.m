//
//  NKSharesDetailLiteratureViewController.m
//  NEXTKING
//
//  Created by King on 16/9/8.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSharesDetailLiteratureViewController.h"
#import "NKOrderService.h"
#import "NKRecordService.h"
#import "NKRecordCell.h"
#import "NKWebViewController.h"

@interface NKSharesDetailLiteratureViewController ()

@property (nonatomic, strong) NKKVOLabel  *rangeLabel;
@property (nonatomic, strong) UIButton    *followButton;

@property (nonatomic, strong) NKMShareholder *shareholder;
@property (nonatomic, strong) NSNumber   *allowSellAmount;

@property (nonatomic, strong) UIView     *stockHead;
@property (nonatomic, strong) UIView     *sellView;
@property (nonatomic, strong) UIView     *buyView;
@property (nonatomic, strong) UIView     *aboutView;

@end

@implementation NKSharesDetailLiteratureViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self addBackButton];
    
    self.titleLabel.text = self.shares.name;
    
    [self addRefreshHeader];
    [self addRightButtonWithTitle:[UIImage imageNamed:@"button_more"]];
    
    [self addTableViewHeader];
    
    [self refreshData];
    [self fetchAllowSellAmount];
    
}

-(NSString*)recordType{
    return NKRecordTypeSharesComment;
}

-(NSString*)parentID{
    return self.shares.modelID;
}

-(void)refreshData{
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithType:[self recordType] offset:0 size:20 parentID:[self parentID] andRequestDelegate:rd];
    [self.showTableView.footer resetNoMoreData];
}

-(void)getMoreData{
    NKRequestDelegate *rd = [NKRequestDelegate getMoreRequestDelegateWithTarget:self];
    [[NKRecordService sharedNKRecordService] listRecordWithType:[self recordType] offset:self.dataSource.count size:20 parentID:[self parentID] andRequestDelegate:rd];
}

-(UIView*)stockHead{
    
    if (!_stockHead) {
        _stockHead = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 192)];
        UIView *stockHead = _stockHead;
        
        
        NKKVOLabel *priceLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(10, 32, NKMainWidth-20, 48)];
        [stockHead addSubview:priceLabel];
        priceLabel.font = [UIFont systemFontOfSize:32];
        priceLabel.target = self;
        priceLabel.renderMethod = @selector(priceWithValue:label:);
        priceLabel.textAlignment = NSTextAlignmentCenter;
        
        self.rangeLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(10, 80, NKMainWidth-20, 24)];
        [stockHead addSubview:_rangeLabel];
        _rangeLabel.font = [UIFont systemFontOfSize:14];
        _rangeLabel.textAlignment = NSTextAlignmentCenter;
        _rangeLabel.adjustsFontSizeToFitWidth = YES;
        
        [priceLabel bindValueOfModel:self.shares forKeyPath:@"price"];
        
        self.followButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 112, 121, 48)];
        _followButton.centerX = NKMainWidth/2.0;
        [stockHead addSubview:_followButton];
        [_followButton addTarget:self action:@selector(followButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_followButton setBackgroundColor:[UIColor whiteColor]];
        [_followButton setTitle:@"已关注" forState:UIControlStateNormal];
        [_followButton setTitleColor:[UIColor alphaGrayColor] forState:UIControlStateNormal];
        _followButton.titleLabel.font = [UIFont systemFontOfSize:16];
        _followButton.layer.borderColor = [UIColor alphaGrayColor].CGColor;
        _followButton.layer.borderWidth = 0.5;
        _followButton.layer.cornerRadius = 4;
        _followButton.clipsToBounds = YES;
        
        [self checkButton];
        
        [stockHead addBottomLine];
    }
    
    return _stockHead;
}

-(void)addTableViewHeader{
    
    self.showTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 304+192-([self.allowSellAmount floatValue]>0?0:80))];
    [self.showTableView.tableHeaderView addSubview:self.stockHead];
    
    CGFloat y = self.stockHead.height;
    
    if (self.allowSellAmount.floatValue>0) {
        self.sellView.top = y;
        [self.showTableView.tableHeaderView addSubview:self.sellView];
        y+=self.sellView.height;
    }
    
    self.buyView.top = y;
    [self.showTableView.tableHeaderView addSubview:self.buyView];
    y+=self.buyView.height;
    
    
    self.aboutView.top = y;
    [self.showTableView.tableHeaderView addSubview:self.aboutView];
}

-(UIView*)sellView{
    
    if (!_sellView) {
        _sellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 80)];
        
        UIButton *button = [self literatureButton];
        [_sellView addSubview:button];
        button.center = CGPointMake(NKMainWidth-16-60, 40);
        [button setTitle:@"出售" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(sellButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [button setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"5DECAE"]] forState:UIControlStateNormal];

        
        NKKVOLabel *lable = [[NKKVOLabel alloc] initWithFrame:CGRectMake(24, 16, 200, 24)];
        [_sellView addSubview:lable];
        lable.font = [UIFont systemFontOfSize:16];
        lable.text = [NSString stringWithFormat:@"我已持有 %@ 股", self.allowSellAmount];
        
        lable = [[NKKVOLabel alloc] initWithFrame:CGRectMake(24, lable.top+25, 200, 24)];
        [_sellView addSubview:lable];
        lable.font = [UIFont systemFontOfSize:14];
        lable.textColor = [UIColor alphaGrayColor];
        lable.text = [NSString stringWithFormat:@"成本价 ￥%.2f", self.shareholder.price.floatValue];

        [_sellView addBottomLine];
    }
    return _sellView;
}

-(void)buyButtonClicked:(id)sender{
    
}

-(void)sellButtonClicked:(id)sender{
    
}

-(UIButton*)literatureButton{
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 121, 48)];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 0.5;
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    return button;
}

-(UIView*)buyView{
    
    if (!_buyView) {
        
        _buyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 80)];
        
        
        UIButton *button = [self literatureButton];
        [_buyView addSubview:button];
        button.center = CGPointMake(NKMainWidth-16-60, 40);
        [button setTitle:@"购买" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buyButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [button setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"F5A78C"]] forState:UIControlStateNormal];
        
        NKKVOLabel *lable = [[NKKVOLabel alloc] initWithFrame:CGRectMake(24, 16, 200, 24)];
        [_buyView addSubview:lable];
        lable.font = [UIFont systemFontOfSize:16];
        NKMWallet *wallet = [[NKMUser me] wallet];
        float money = [wallet.money floatValue]-[wallet.orderMoney floatValue];
        float price = MAX(self.shares.price.floatValue, 0.01);
        NSNumber *allowAmount =[NSNumber numberWithInt:money/price];
        lable.text = [NSString stringWithFormat:@"可购买 %@ 股", allowAmount];
        
        lable = [[NKKVOLabel alloc] initWithFrame:CGRectMake(24, lable.top+25, 200, 24)];
        [_buyView addSubview:lable];
        lable.font = [UIFont systemFontOfSize:14];
        lable.textColor = [UIColor alphaGrayColor];
        lable.text = [NSString stringWithFormat:@"买入价 ￥%@", [self.shares priceText]];
        
        
        [_buyView addBottomLine];
        
    }

    return _buyView;
}

-(UIView*)aboutView{
    
    if (!_aboutView) {
        
        _aboutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 144)];
        _aboutView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(24, 16, 100, 24)];
        [_aboutView addSubview:label];
        label.font = [UIFont systemFontOfSize:16];
        label.text = @"关于";
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(24, label.top+32, NKMainWidth-48, 48)];
        [_aboutView addSubview:label];
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor alphaGrayColor];
        label.numberOfLines = 2;
        label.text = self.shares.title;
        
        NKKVOLabel *more = [[NKKVOLabel alloc] initWithFrame:CGRectMake(24, 104, 100, 24)];
        [_aboutView addSubview:more];
        more.font = [UIFont systemFontOfSize:14];
        more.textColor = [UIColor colorWithHexString:@"438AD7"];
        more.text = @"更多";
        more.target = self;
        more.singleTapped = @selector(linkButtonClicked:);
        
        [_aboutView addBottomLine];
    }
    return _aboutView;
}

-(void)linkButtonClicked:(id)sender{
    
    NKWebViewController *viewController = [NKWebViewController new];
    viewController.url = self.shares.link;
    [NKNC pushViewController:viewController animated:YES];
}


-(void)checkButton{
    
    if ([self.shares.following boolValue]) {
        [_followButton setTitle:@"已关注" forState:UIControlStateNormal];
        [_followButton setTitleColor:[UIColor alphaGrayColor] forState:UIControlStateNormal];
        _followButton.layer.borderWidth = 0.5;
        [_followButton setBackgroundImage:nil forState:UIControlStateNormal];
    }
    else{
        [_followButton setTitle:@"关注" forState:UIControlStateNormal];
        [_followButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _followButton.layer.borderWidth = 0.0;
        [_followButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"5DECAE"]] forState:UIControlStateNormal];
    }
    
}

-(NSString*)priceWithValue:(NSNumber*)price label:(NKKVOLabel*)label{
    
    _rangeLabel.textColor = [self.shares priceColor];
    
    NSString *detailString = [NSString stringWithFormat:@"%@ (%@) 今天", [self.shares rangeText], [self.shares percentText]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:detailString];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor alphaGrayColor] range:[detailString rangeOfString:@"今天"]];
    _rangeLabel.attributedText = attributedString;
    
    return [NSString stringWithFormat:@"￥%@", [self.shares priceText]];
}


-(void)followButtonClicked:(id)sender{
    
    if ([self.shares.following boolValue]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消关注" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self unfollow];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        
        [NKProgressHUD showTextHudInView:self.view text:@"正在关注"];
        NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(followOK:)];
        [[NKOrderService sharedNKOrderService] followSharesWithID:self.shares.modelID ifUnfollow:NO requestDelegate:rd];
    }
    
}

-(void)unfollow{
    [NKProgressHUD showTextHudInView:self.view text:@"正在取消关注"];
    NKRequestDelegate *rd = [self requestDelegateWithSuccess:@selector(unfollowOK:)];
    [[NKOrderService sharedNKOrderService] followSharesWithID:self.shares.modelID ifUnfollow:YES requestDelegate:rd];
}

-(void)unfollowOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    self.shares.following = [NSNumber numberWithBool:NO];
    [self checkButton];
    
}

-(void)followOK:(NKRequest*)request{
    
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    self.shares.following = [NSNumber numberWithBool:YES];
    [self checkButton];
}

-(void)fetchAllowSellAmount{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(fetchOK:) andFailedSelector:@selector(fetchFailed:)];
    [[NKOrderService sharedNKOrderService] getShareholderWithSharesID:self.shares.modelID andRequestDelegate:rd];
    
}
-(void)fetchOK:(NKRequest*)request{
    
    NKMShareholder *holder = [request.results lastObject];
    self.shareholder = holder;
    self.allowSellAmount = [NSNumber numberWithFloat:holder.amount.floatValue-holder.sellingAmount.floatValue];
    [self addTableViewHeader];
}

-(void)fetchFailed:(NKRequest*)request{
    
}


#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKRecordCell *cell = (NKRecordCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMRecord *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NKRecordCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
