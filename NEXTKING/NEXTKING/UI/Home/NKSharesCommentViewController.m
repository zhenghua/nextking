//
//  NKSharesCommentViewController.m
//  NEXTKING
//
//  Created by King on 16/8/1.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKSharesCommentViewController.h"
#import "NKRecordService.h"

@implementation NKSharesCommentViewController


-(void)setupHeader{
    self.titleLabel.text = self.shares.name;
}

-(NSString*)recordType{
    return NKRecordTypeSharesComment;
}

-(NSString*)parentID{
    return self.shares.modelID;
}

@end
