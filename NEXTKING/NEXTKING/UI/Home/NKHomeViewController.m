//
//  NKHomeViewController.m
//  NEXTKING
//
//  Created by jinzhenghua on 25/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKHomeViewController.h"
#import "NKOrderService.h"
#import "NKSharesCell.h"
#import "NKPostViewController.h"
#import "NKSharesDetailViewController.h"
#import "NKUserService.h"
#import "NKBadgeView.h"

#import "NKDiscoverSharesViewController.h"

#import "NKSearchViewController.h"

@interface NKHomeViewController ()

@end

@implementation NKHomeViewController

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NKLoginFinishNotificationKey object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NKFollowSharesOKNotificationKey object:nil];
    
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginOK:) name:NKLoginFinishNotificationKey object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(followOK:) name:NKFollowSharesOKNotificationKey object:nil];
    }
    return self;
}

-(void)followOK:(NSNotification*)notification{
    
    [self refreshData];
}

-(void)loginOK:(NSNotification*)notification{
    
    [self refreshData];
    
    [[NKUserService sharedNKUserService] fetchNotification];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addRefreshHeader];
    self.titleLabel.text = @"时空";
    
    NSMutableArray *cachedArray = [[NKDataStore sharedDataStore] cachedArrayOf:NKCachePathFollowingShares andClass:[NKMShares class]];
    if ([cachedArray count]) {
        self.dataSource = cachedArray;
    }
   
}

-(void)rightButtonClick:(id)sender{
    
    NKSearchViewController *viewController = [NKSearchViewController new];
    [NKNC pushViewController:viewController animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshData{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshData) object:nil];
    
    NKRequestDelegate *rd = [NKRequestDelegate refreshRequestDelegateWithTarget:self];
    [[NKOrderService sharedNKOrderService] listMyFollowsWithRequestDelegate:rd];
    
    
    
}

-(NSString*)cachePathKey{
    return NKCachePathFollowingShares;
}

-(void)refreshDataOK:(NKRequest *)request{
    [super refreshDataOK:request];
    
    if ([self.dataSource count]<=0) {
        
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, self.showTableView.height)];
        self.showTableView.tableFooterView = footer;
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        [footer addSubview:button];
        [button addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.borderColor = [UIColor lightGrayColor].CGColor;
        button.layer.borderWidth = 0.5;
        button.center = CGPointMake(NKMainWidth/2, footer.height/2-50);
        [button setTitle:@"+" forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:50];
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage yy_imageWithColor:[self normalBackgroundColor]] forState:UIControlStateHighlighted];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 20)];
        [footer addSubview:label];
        label.centerY = button.bottom+15;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor lightGrayColor];
        label.font = [UIFont systemFontOfSize:14];
        label.text = @"点击探索时空";
        
        
    }
    else{
        self.showTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 1)];
    }
}

-(void)addButtonClicked:(id)sender{
    NKDiscoverSharesViewController *viewController = [NKDiscoverSharesViewController new];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)refreshDataFailed:(NKRequest *)request{
    [self.showTableView.header endRefreshing];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * CellIdentifier = @"WMFeedCellIdentifier";
    
    NKSharesCell *cell = (NKSharesCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NKSharesCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NKMShares *object = [self.dataSource objectAtIndex:indexPath.row];
    [cell showForObject:object withIndex:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [NKSharesCell cellHeightForObject:self.dataSource[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NKSharesDetailViewController *viewController = [NKSharesDetailViewController new];
    viewController.shares = self.dataSource[indexPath.row];
    [NKNC pushViewController:viewController animated:YES];
}

@end
