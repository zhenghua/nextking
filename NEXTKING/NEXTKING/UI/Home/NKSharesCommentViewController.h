//
//  NKSharesCommentViewController.h
//  NEXTKING
//
//  Created by King on 16/8/1.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKFeedDetailViewController.h"

@interface NKSharesCommentViewController : NKFeedDetailViewController

@property (nonatomic, strong) NKMShares *shares;

@end
