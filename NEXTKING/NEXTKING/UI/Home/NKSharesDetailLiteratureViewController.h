//
//  NKSharesDetailLiteratureViewController.h
//  NEXTKING
//
//  Created by King on 16/9/8.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKTableViewController.h"

@interface NKSharesDetailLiteratureViewController : NKTableViewController
@property (nonatomic, strong) NKMShares *shares;

@end
