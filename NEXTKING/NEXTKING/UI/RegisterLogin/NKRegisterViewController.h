//
//  NKRegisterViewController.h
//  NEXTKING
//
//  Created by King on 6/15/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKViewController.h"

@interface NKRegisterViewController : NKViewController

@property (nonatomic, strong) NKMAccount  *account;
@property (nonatomic, strong) UIButton    *registerButton;

@property (nonatomic, strong) UIButton    *licenceButton;

@end
