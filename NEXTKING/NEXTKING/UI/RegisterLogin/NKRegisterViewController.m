//
//  NKRegisterViewController.m
//  NEXTKING
//
//  Created by King on 6/15/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKRegisterViewController.h"
#import "NKAccountService.h"
#import "NKAccountManager.h"
#import "NKVerifyCodeViewController.h"
#import "NKWebViewController.h"

@interface NKRegisterViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *mobileField;
@property (nonatomic, strong) UITextField *passwordField;
@property (nonatomic, strong) UILabel     *countryLabel;

@property (nonatomic, strong) UIView      *inviteCodeInputView;
@property (nonatomic, strong) UITextField *inviteCodeField;



@end

@implementation NKRegisterViewController

-(void)normalizeField:(UITextField*)textField{

    textField.font = [UIFont systemFontOfSize:18];
    textField.textAlignment = NSTextAlignmentCenter;
    textField.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _mobileField) {
        [_passwordField becomeFirstResponder];
    }
    else if (textField == _passwordField) {
        if (_registerButton.enabled) {
            [self registerButtonClicked:_registerButton];
        }
    }
    return YES;
}

-(void)textFieldDidChange:(NSNotification*)notification{
    
    if (notification.object == _inviteCodeField) {
        if ([_inviteCodeField.text isEqualToString:@"161817"]) {
            [_mobileField becomeFirstResponder];
            [UIView animateWithDuration:0.3 animations:^{
                _inviteCodeInputView.alpha = 0.0;
            } completion:^(BOOL finished) {
                [_inviteCodeInputView removeFromSuperview];
                _inviteCodeInputView = nil;
                _inviteCodeField = nil;
            }];
        }
        else if (_inviteCodeField.text.length>=6){
            
        }
        
    }
    
    if (notification.object != _mobileField && notification.object != _passwordField) {
        return;
    }
    
    if (notification.object == _mobileField && [_mobileField.text length] == 11) {
        [_passwordField becomeFirstResponder];
    }
    
    if (_mobileField.text.length>0 && _passwordField.text.length>0) {
        _registerButton.enabled = YES;
    }
    else{
        _registerButton.enabled = NO;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"注册";
    
    self.countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 64, 88, 52)];
    [self.contentView addSubview:_countryLabel];
    _countryLabel.text = @"+86";
    _countryLabel.textAlignment = NSTextAlignmentCenter;
    _countryLabel.textColor = [UIColor colorWithHexString:@"#1eb4a0"];
    _countryLabel.font = [UIFont systemFontOfSize:15];
    
    
    self.mobileField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_countryLabel.frame), 64, NKMainWidth-60-CGRectGetWidth(_countryLabel.frame), 52)];
    [self.contentView addSubview:_mobileField];
    [self normalizeField:_mobileField];
    _mobileField.placeholder = @"请输入手机号码";
    _mobileField.keyboardType = UIKeyboardTypePhonePad;
    
    [_mobileField becomeFirstResponder];
    
    self.passwordField = [[UITextField alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(_mobileField.frame), NKMainWidth-60, 52.5)];
    [self.contentView addSubview:_passwordField];
    [self normalizeField:_passwordField];
    _passwordField.placeholder = @"设置密码";
    _passwordField.secureTextEntry = YES;
    
    self.registerButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_passwordField.frame), CGRectGetMaxY(_passwordField.frame)+20, CGRectGetWidth(_passwordField.frame), 46)];
    [self.contentView addSubview:_registerButton];
    [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [_registerButton addTarget:self action:@selector(registerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [_registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#a0a0a0"]] forState:UIControlStateDisabled];
    [_registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#596063"]] forState:UIControlStateHighlighted];
    
    _registerButton.enabled = NO;
    _registerButton.layer.cornerRadius = 2.0;
    _registerButton.clipsToBounds = YES;
    
    UIView *lineTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_passwordField.frame), 0.5)];
    [_passwordField addSubview:lineTop];
    lineTop.backgroundColor = [UIColor lightGrayColor];
    lineTop.alpha = 0.7;
    
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_passwordField.frame)-0.5, CGRectGetWidth(_passwordField.frame), 0.5)];
    [_passwordField addSubview:lineBottom];
    lineBottom.backgroundColor = [UIColor lightGrayColor];
    lineBottom.alpha = 0.7;
    
    UIView  *line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(_countryLabel.frame)-0.5, 6, 0.5, CGRectGetHeight(_countryLabel.frame)-12)];
    [_countryLabel addSubview:line];
    line.backgroundColor = [UIColor lightGrayColor];
    line.alpha = 0.5;
    
    
    self.licenceButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_registerButton.frame), NKMainWidth, 40)];
    [self.contentView addSubview:_licenceButton];
    [_licenceButton addTarget:self action:@selector(licenceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_licenceButton setTitle:@"使用协议" forState:UIControlStateNormal];
    [_licenceButton setTitleColor:[UIColor colorWithHexString:@"#1eb4a0"] forState:UIControlStateNormal];
    _licenceButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    
    [_mobileField resignFirstResponder];
    self.inviteCodeInputView.backgroundColor = [UIColor whiteColor];

}

-(void)licenceButtonClicked:(UIButton *)licenceButton{
    
    NKWebViewController *viewController = [[NKWebViewController alloc] init];
    viewController.url = @"http://shares.rocks/terms.html";
    [NKNC pushViewController:viewController animated:YES];
}

-(void)registerButtonClicked:(id)sender{
    
    [_mobileField resignFirstResponder];
    [_passwordField resignFirstResponder];
    
    [NKProgressHUD showTextHudInView:self.view text:nil];
    
    NSString *mobile = self.mobileField.text;
    NSString *password = [YTKNetworkPrivate md5StringFromString:self.passwordField.text];
    
    self.account = [NKMAccount accountWithAccount:mobile password:password andShouldAutoLogin:[NSNumber numberWithBool:YES]];
    self.account.codeSendDate = [NSDate date];
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(requestFinished:) andFailedSelector:@selector(requestFailed:)];
    
    [[NKAccountService sharedNKAccountService] checkMobileWithMobile:mobile password:password andRequestDelegate:rd];
    
}

-(void)requestFinished:(NKRequest *)request{
    
    NSNumber *state = [request.responseJSONObject objectOrNilForKey:@"data"];
    NSInteger result = [state integerValue];
    switch (result) {
        case 100:{
            [self login:nil];
        }
        break;
        case 101:
        case 102:{
            [NKProgressHUD hideHUDForView:self.view animated:YES];
            self.account.registerState = state;
            NKVerifyCodeViewController *viewController = [NKVerifyCodeViewController viewControllerWithAccount:self.account];
            [NKNC pushViewController:viewController animated:YES];
        }
        break;
        default:
        break;
    }
    
    
}

-(void)login:(id)sender{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(loginOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKAccountManager sharedAccountManager] loginWithAccount:self.account andRequestDelegate:rd];
}

-(void)loginOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    [[NKUI sharedNKUI] showHome];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView*)inviteCodeInputView{
    
    if (!_inviteCodeInputView) {
        _inviteCodeInputView = [[UIView alloc] initWithFrame:self.view.bounds];
        [self.view addSubview:_inviteCodeInputView];
        _inviteCodeInputView.backgroundColor = [UIColor whiteColor];
        
        self.inviteCodeField = [[UITextField alloc] initWithFrame:CGRectMake(40, 80, NKMainWidth-80, 80)];
        [_inviteCodeInputView addSubview:_inviteCodeField];
        _inviteCodeField.placeholder = @"时空邀请码";
        _inviteCodeField.keyboardType = UIKeyboardTypeNumberPad;
        _inviteCodeField.font = [UIFont boldSystemFontOfSize:40];
        [_inviteCodeField becomeFirstResponder];
        _inviteCodeField.textAlignment = NSTextAlignmentCenter;
        _inviteCodeField.layer.borderWidth = 0.5;
        _inviteCodeField.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _inviteCodeField.layer.cornerRadius = 10;
        
    }
    return _inviteCodeInputView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
