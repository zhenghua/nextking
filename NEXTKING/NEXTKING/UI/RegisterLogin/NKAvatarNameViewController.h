//
//  NKAvatarNameViewController.h
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKViewController.h"

@interface NKAvatarNameViewController : NKViewController

@property (nonatomic, strong) NKMAccount *account;

@end
