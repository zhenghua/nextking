//
//  NKWelcomeViewController.m
//  NEXTKING
//
//  Created by jinzhenghua on 25/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "NKWelcomeViewController.h"
#import "NKCellDataObject.h"

#import "NKRegisterViewController.h"
#import "NKLoginViewController.h"

@interface NKWelcomeViewController ()

@end

@implementation NKWelcomeViewController

-(void)modifyHeaderColor{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UILabel *slogan = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, NKMainWidth, 150)];
    slogan.center = CGPointMake(NKMainWidth/2, NKMainHeight/3);
    [self.contentView addSubview:slogan];
    slogan.numberOfLines = 0;
    slogan.font = [UIFont boldSystemFontOfSize:26];
    slogan.textAlignment = NSTextAlignmentCenter;
    slogan.textColor = [UIColor colorWithHexString:@"#989899"];
    
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:@"时空\n\n人的价值共享平台"];
    [attribute addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 2)];
    [attribute addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:35] range:NSMakeRange(0, 2)];
    slogan.attributedText = attribute;
    
    // Do any additional setup after loading the view.
    UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake(0, NKMainHeight-160, NKMainWidth, 80)];
    [loginButton addTarget:self action:@selector(loginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#e82856"]] forState:UIControlStateNormal];
    [loginButton setTitle:@"登录" forState:UIControlStateNormal];
    loginButton.titleLabel.font = [UIFont boldSystemFontOfSize:25];
    [self.contentView addSubview:loginButton];
    
    
    UIButton *registerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, NKMainHeight-80, NKMainWidth, 80)];
    [registerButton addTarget:self action:@selector(registerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#3bb2e1"]] forState:UIControlStateNormal];
    [registerButton setTitle:@"注册" forState:UIControlStateNormal];
    registerButton.titleLabel.font = [UIFont boldSystemFontOfSize:25];
    [self.contentView addSubview:registerButton];
}

-(void)registerButtonClicked:(id)sender{
    
    NKRegisterViewController *viewController = [[NKRegisterViewController alloc] init];
    [NKNC pushViewController:viewController animated:YES];
}

-(void)loginButtonClicked:(id)sender{
    
    NKLoginViewController *viewController = [[NKLoginViewController alloc] init];
    [NKNC pushViewController:viewController animated:YES];
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
