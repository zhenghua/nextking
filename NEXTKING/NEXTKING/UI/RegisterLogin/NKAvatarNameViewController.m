//
//  NKAvatarNameViewController.m
//  NEXTKING
//
//  Created by King on 16/7/3.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKAvatarNameViewController.h"
#import "NKImageView.h"
#import "NKKVOLabel.h"
#import "NKAccountService.h"
#import "NKAccountManager.h"

@interface NKAvatarNameViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NKImageView *avatar;
@property (nonatomic, strong) UITextField *nameFiled;

@property (nonatomic, strong) NKKVOLabel  *genderLabel;

@property (nonatomic, strong) UIButton    *registerButton;


@end

@implementation NKAvatarNameViewController

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _nameFiled) {
        [_nameFiled resignFirstResponder];
        [self genderTapped:nil];
    }
    
    return YES;
}


-(void)textFieldDidChange:(NSNotification*)notification{
    
    if (notification.object != _nameFiled) {
        return;
    }
    self.account.name = _nameFiled.text;
    [self checkButtonStatus];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"完善信息";
    
    CGFloat avatarWidth = 85.0;
    
    self.avatar = [[NKImageView alloc] initWithFrame:CGRectMake(30, 64+30, avatarWidth, avatarWidth)];
    [self.contentView addSubview:_avatar];
    //_avatar.backgroundColor = [UIColor colorWithHexString:@"#c7c7cd"];
    _avatar.image = [UIImage imageNamed:@"avatarPlaceholder"];
    _avatar.target = self;
    _avatar.singleTapped = @selector(avatarTapped:);
    
    self.nameFiled = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_avatar.frame)+30, CGRectGetMinY(_avatar.frame), NKMainWidth-CGRectGetMaxX(_avatar.frame)-60, CGRectGetHeight(_avatar.frame)/2)];
    [self.contentView addSubview:_nameFiled];
    _nameFiled.placeholder = @"设置昵称";
    _nameFiled.font = [UIFont systemFontOfSize:18];
    _nameFiled.delegate = self;
    
    self.genderLabel = [[NKKVOLabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_nameFiled.frame), CGRectGetMaxY(_nameFiled.frame), CGRectGetWidth(_nameFiled.frame), CGRectGetHeight(_nameFiled.frame))];
    [self.contentView addSubview:_genderLabel];
    _genderLabel.font = [UIFont systemFontOfSize:18];
    _genderLabel.target = self;
    _genderLabel.renderMethod = @selector(genderRender:);
    _genderLabel.singleTapped = @selector(genderTapped:);
    
    [_genderLabel bindValueOfModel:self.account forKeyPath:@"gender"];
    
    self.registerButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_avatar.frame), CGRectGetMaxY(_avatar.frame)+20, NKMainWidth-60, 46)];
    [self.contentView addSubview:_registerButton];
    [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [_registerButton addTarget:self action:@selector(registerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [_registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#a0a0a0"]] forState:UIControlStateDisabled];
    [_registerButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#596063"]] forState:UIControlStateHighlighted];
    
    _registerButton.enabled = NO;
    _registerButton.layer.cornerRadius = 2.0;
    _registerButton.clipsToBounds = YES;
    
    UIView *lineTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_genderLabel.frame), 0.5)];
    [_genderLabel addSubview:lineTop];
    lineTop.backgroundColor = [UIColor lightGrayColor];
    lineTop.alpha = 0.7;
    
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_genderLabel.frame)-0.5, CGRectGetWidth(_genderLabel.frame), 0.5)];
    [_genderLabel addSubview:lineBottom];
    lineBottom.backgroundColor = [UIColor lightGrayColor];
    lineBottom.alpha = 0.7;

    
}



-(void)registerButtonClicked:(id)sender{
    
    [_nameFiled resignFirstResponder];
    
    [NKProgressHUD showTextHudInView:self.view text:nil];
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(registerOK:) andFailedSelector:@selector(requestFailed:)];
    
    [[NKAccountService sharedNKAccountService] registerWithUsername:self.account.account password:self.account.password name:self.account.name avatar:self.account.avatarPath code:self.account.verifyCode gender:self.account.gender andRequestDelegate:rd];
    
}

-(void)registerOK:(NKRequest*)request{

    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(loginOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKAccountManager sharedAccountManager] loginWithAccount:self.account andRequestDelegate:rd];
}

-(void)loginOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    [[NKUI sharedNKUI] showHome];
}


-(NSString*)genderRender:(NSString*)value{
    
    [self checkButtonStatus];
    if ([value isEqualToString:NKGenderKeyMale]) {
        _genderLabel.textColor = [UIColor blackColor];
        return @"男";
    }
    else if ([value isEqualToString:NKGenderKeyFemale]){
        _genderLabel.textColor = [UIColor blackColor];
        return @"女";
    }
    else{
        _genderLabel.textColor = [UIColor colorWithHexString:@"#c7c7cd"];
        return @"性别";
    }
}

-(void)checkButtonStatus{
    if (self.account.avatarPath && self.account.name && self.account.name.length>0 && self.account.gender) {
        _registerButton.enabled = YES;
    }
    else{
        _registerButton.enabled = NO;
    }
}

-(void)avatarTapped:(UITapGestureRecognizer*)gesture{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing = YES;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

-(void)genderTapped:(UITapGestureRecognizer*)gesture{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.account.gender = NKGenderKeyMale;
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.account.gender = NKGenderKeyFemale;
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    if ([info objectForKey:UIImagePickerControllerEditedImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        self.avatar.image = image;

        self.account.avatarPath = [[NKFileManager fileManager] uploadImage:image block:^id(OSSTask *task) {
            if (!task.error) {
            }
            else{
                NKRequest *request = [[NKRequest alloc] init];
                request.errorDescription = task.error.localizedDescription;
                [self performSelectorOnMainThread:@selector(requestFailed:) withObject:request waitUntilDone:NO];
            }
            return nil;
        }];
        
        [self checkButtonStatus];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

@end
