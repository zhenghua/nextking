//
//  NKVerifyCodeViewController.h
//  NEXTKING
//
//  Created by King on 16/7/2.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKViewController.h"

@interface NKVerifyCodeViewController : NKViewController

@property (nonatomic, strong) NKMAccount *account;

+(instancetype)viewControllerWithAccount:(NKMAccount*)account;

@end
