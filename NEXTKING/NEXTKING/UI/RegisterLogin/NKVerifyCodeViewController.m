//
//  NKVerifyCodeViewController.m
//  NEXTKING
//
//  Created by King on 16/7/2.
//  Copyright © 2016年 jinzhenghua. All rights reserved.
//

#import "NKVerifyCodeViewController.h"
#import "NKAccountService.h"
#import "NKAccountManager.h"
#import "NKAvatarNameViewController.h"

@interface NKVerifyCodeViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *codeField;

@property (nonatomic, strong) UIButton    *submitButton;
@property (nonatomic, strong) UIButton    *resendButton;

@end

@implementation NKVerifyCodeViewController


+(instancetype)viewControllerWithAccount:(NKMAccount*)account{
    
    NKVerifyCodeViewController *viewController = [[NKVerifyCodeViewController alloc] init];
    viewController.account = account;
    return viewController;
}

-(void)normalizeField:(UITextField*)textField{
    
    textField.font = [UIFont systemFontOfSize:18];
    textField.textAlignment = NSTextAlignmentCenter;
    textField.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _codeField) {
        if (_submitButton.enabled) {
            [self submitButtonClicked:_submitButton];
        }
    }
    return YES;
}

-(void)textFieldDidChange:(NSNotification*)notification{
    
    if (notification.object != _codeField) {
        return;
    }
    if (_codeField.text.length>0) {
        _submitButton.enabled = YES;
    }
    else{
        _submitButton.enabled = NO;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addBackButton];
    self.titleLabel.text = @"输入验证码";

    UILabel *tips = [[UILabel alloc] initWithFrame:CGRectMake(10, 64, NKMainWidth-20, NKMainHeight<=480?30:42)];
    tips.text = @"本次注册需要短信验证，已发送验证码至";
    tips.font = [UIFont systemFontOfSize:15];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.textColor = [UIColor colorWithHexString:@"#a3a3a3"];
    [self.contentView addSubview:tips];
    
    UILabel *countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(tips.frame), 40, 52)];
    [self.contentView addSubview:countryLabel];
    countryLabel.textColor = [UIColor colorWithHexString:@"#9d9d9d"];
    countryLabel.textAlignment = NSTextAlignmentCenter;
    countryLabel.text = @"+86";
    countryLabel.font = [UIFont systemFontOfSize:14];
    
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(countryLabel.frame), CGRectGetMinY(countryLabel.frame), NKMainWidth-60-100-CGRectGetWidth(countryLabel.frame), CGRectGetHeight(countryLabel.frame))];
    [self.contentView addSubview:mobileLabel];
    mobileLabel.text = self.account.account;
    mobileLabel.font = [UIFont systemFontOfSize:14];
    mobileLabel.textAlignment = NSTextAlignmentCenter;
    mobileLabel.textColor = [UIColor colorWithHexString:@"#cbcbcc"];
    
    self.resendButton = [[UIButton alloc] initWithFrame:CGRectMake(NKMainWidth-30-100, 0, 100, 35)];
    [self.contentView addSubview:_resendButton];
    _resendButton.center = CGPointMake(_resendButton.center.x, mobileLabel.center.y);
    _resendButton.enabled = NO;
    [_resendButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    _resendButton.titleLabel.font = [UIFont systemFontOfSize:14];
    _resendButton.clipsToBounds = YES;
    _resendButton.layer.cornerRadius = 2.0;
    
    [_resendButton addTarget:self action:@selector(resendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    [_resendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_resendButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#1abd9b"]] forState:UIControlStateNormal];
    [_resendButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#8dddcd"]] forState:UIControlStateDisabled];
    [_resendButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#18937a"]] forState:UIControlStateHighlighted];
    
    [self countDown];
    
    
    self.codeField = [[UITextField alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(countryLabel.frame), NKMainWidth-60, 52.5)];
    [self.contentView addSubview:_codeField];
    [self normalizeField:_codeField];
    _codeField.placeholder = @"输入收到的验证码";
    _codeField.keyboardType = UIKeyboardTypeNumberPad;
    
    [_codeField becomeFirstResponder];
    
    UIView *lineTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_codeField.frame), 0.5)];
    [_codeField addSubview:lineTop];
    lineTop.backgroundColor = [UIColor lightGrayColor];
    lineTop.alpha = 0.7;
    
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_codeField.frame)-0.5, CGRectGetWidth(_codeField.frame), 0.5)];
    [_codeField addSubview:lineBottom];
    lineBottom.backgroundColor = [UIColor lightGrayColor];
    lineBottom.alpha = 0.7;
    
    self.submitButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_codeField.frame), CGRectGetMaxY(_codeField.frame)+20, CGRectGetWidth(_codeField.frame), 46)];
    [self.contentView addSubview:_submitButton];
    [_submitButton setTitle:@"提交" forState:UIControlStateNormal];
    [_submitButton addTarget:self action:@selector(submitButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_submitButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [_submitButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#a0a0a0"]] forState:UIControlStateDisabled];
    [_submitButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#596063"]] forState:UIControlStateHighlighted];
    
    _submitButton.enabled = NO;
    _submitButton.layer.cornerRadius = 2.0;
    _submitButton.clipsToBounds = YES;
    
    

}

-(void)countDown{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(countDown) object:nil];
    
    NSTimeInterval interval = 60 - [[NSDate date] timeIntervalSinceDate:self.account.codeSendDate];
    
    if (interval<=1) {
        self.resendButton.enabled = YES;
        [_resendButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    }
    else{
        
        self.resendButton.enabled = NO;
        [_resendButton setTitle:[NSString stringWithFormat:@"%ds 重新获取",(int)interval] forState:UIControlStateNormal];
         [self performSelector:@selector(countDown) withObject:nil afterDelay:1.0];
    }
    
   
}

-(void)resendButtonClicked:(id)sender{
    
    self.account.codeSendDate = [NSDate date];
    [self countDown];
    
    [NKProgressHUD showTextHudInView:self.view text:nil];
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(resendOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKAccountService sharedNKAccountService] sendSMSWithMobile:self.account.account andRequestDelegate:rd];
    
}

-(void)resendOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)submitButtonClicked:(id)sender{
    
    [NKProgressHUD showTextHudInView:self.view text:@"正在注册"];
    
    
    switch ([self.account.registerState integerValue]) {
        case 101:{
            NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(resetPasswordOK:) andFailedSelector:@selector(requestFailed:)];
            [[NKAccountService sharedNKAccountService] resetPasswordWithMobile:self.account.account password:self.account.password code:_codeField.text andRequestDelegate:rd];
        }
            break;
        case 102:{
            
            self.account.verifyCode = _codeField.text;
            
            NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(verifyOK:) andFailedSelector:@selector(requestFailed:)];
            [[NKAccountService sharedNKAccountService] verifyWithMobile:self.account.account code:self.account.verifyCode andRequestDelegate:rd];
        }
            break;
            
        default:
            break;
    }
    
    
}

-(void)resetPasswordOK:(NKRequest*)request{
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(loginOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKAccountManager sharedAccountManager] loginWithAccount:self.account andRequestDelegate:rd];
    
}

-(void)loginOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    [[NKUI sharedNKUI] showHome];
}

-(void)verifyOK:(NKRequest*)request{
    
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    
    NKAvatarNameViewController *viewController = [[NKAvatarNameViewController alloc] init];
    viewController.account = self.account;
    [NKNC pushViewController:viewController animated:YES];

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
