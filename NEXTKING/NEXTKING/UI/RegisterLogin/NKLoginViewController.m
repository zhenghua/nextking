//
//  NKLoginViewController.m
//  NEXTKING
//
//  Created by King on 6/15/16.
//  Copyright © 2016 jinzhenghua. All rights reserved.
//

#import "NKLoginViewController.h"
#import "NKAccountManager.h"
#import "NKAccountService.h"
#import "NKForgetPasswordViewController.h"

@interface NKLoginViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *mobileField;
@property (nonatomic, strong) UITextField *passwordField;
@property (nonatomic, strong) UILabel     *countryLabel;

@property (nonatomic, strong) UIButton    *loginButton;

@end


@implementation NKLoginViewController

-(void)normalizeField:(UITextField*)textField{
    
    textField.font = [UIFont systemFontOfSize:18];
    textField.textAlignment = NSTextAlignmentCenter;
    textField.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _mobileField) {
        [_passwordField becomeFirstResponder];
    }
    else if (textField == _passwordField) {
        if (_loginButton.enabled) {
            [self loginButtonClicked:_loginButton];
        }
    }
    return YES;
}

-(void)textFieldDidChange:(NSNotification*)notification{
    
    if (notification.object != _mobileField && notification.object != _passwordField) {
        return;
    }
    if (notification.object == _mobileField && [_mobileField.text length] == 11) {
        [_passwordField becomeFirstResponder];
    }
    if (_mobileField.text.length>0 && _passwordField.text.length>0) {
        _loginButton.enabled = YES;
    }
    else{
        _loginButton.enabled = NO;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButton];
    
    self.titleLabel.text = @"登录时空";
    
    self.countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 64, 88, 52)];
    [self.contentView addSubview:_countryLabel];
    _countryLabel.text = @"+86";
    _countryLabel.textAlignment = NSTextAlignmentCenter;
    _countryLabel.textColor = [UIColor colorWithHexString:@"#4cab93"];
    _countryLabel.font = [UIFont systemFontOfSize:15];
    
    
    self.mobileField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_countryLabel.frame), 64, NKMainWidth-60-CGRectGetWidth(_countryLabel.frame), 52)];
    [self.contentView addSubview:_mobileField];
    [self normalizeField:_mobileField];
    _mobileField.placeholder = @"请输入手机号码";
    _mobileField.keyboardType = UIKeyboardTypePhonePad;
    
    [_mobileField becomeFirstResponder];
    
    self.passwordField = [[UITextField alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(_mobileField.frame), NKMainWidth-60, 52.5)];
    [self.contentView addSubview:_passwordField];
    [self normalizeField:_passwordField];
    _passwordField.placeholder = @"登录密码";
    _passwordField.secureTextEntry = YES;
    
    self.loginButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_passwordField.frame), CGRectGetMaxY(_passwordField.frame)+20, CGRectGetWidth(_passwordField.frame), 46)];
    [self.contentView addSubview:_loginButton];
    [_loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [_loginButton addTarget:self action:@selector(loginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_loginButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [_loginButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#a0a0a0"]] forState:UIControlStateDisabled];
    [_loginButton setBackgroundImage:[UIImage yy_imageWithColor:[UIColor colorWithHexString:@"#596063"]] forState:UIControlStateHighlighted];
    
    _loginButton.enabled = NO;
    _loginButton.layer.cornerRadius = 2.0;
    _loginButton.clipsToBounds = YES;
    
    UIView *lineTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_passwordField.frame), 0.5)];
    [_passwordField addSubview:lineTop];
    lineTop.backgroundColor = [UIColor lightGrayColor];
    lineTop.alpha = 0.7;
    
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_passwordField.frame)-0.5, CGRectGetWidth(_passwordField.frame), 0.5)];
    [_passwordField addSubview:lineBottom];
    lineBottom.backgroundColor = [UIColor lightGrayColor];
    lineBottom.alpha = 0.7;
    
    UIView  *line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(_countryLabel.frame)-0.5, 6, 0.5, CGRectGetHeight(_countryLabel.frame)-12)];
    [_countryLabel addSubview:line];
    line.backgroundColor = [UIColor lightGrayColor];
    line.alpha = 0.5;
    
    
    UIButton *forgetPassword = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_loginButton.frame), NKMainWidth, 40)];
    [self.contentView addSubview:forgetPassword];
    [forgetPassword addTarget:self action:@selector(forgetPasswordClicked:) forControlEvents:UIControlEventTouchUpInside];
    [forgetPassword setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetPassword setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    forgetPassword.titleLabel.font = [UIFont systemFontOfSize:14];
    
    
    
    
}

-(void)forgetPasswordClicked:(id)sender{
    
    NKForgetPasswordViewController *viewController = [[NKForgetPasswordViewController alloc] init];
    [NKNC pushViewController:viewController animated:YES];
    
}

-(void)loginButtonClicked:(id)sender{
    
    [_mobileField resignFirstResponder];
    [_passwordField resignFirstResponder];
    
    [NKProgressHUD showTextHudInView:self.view text:@"正在登录"];
    NSString *mobile = self.mobileField.text;
    NSString *password = [YTKNetworkPrivate md5StringFromString:self.passwordField.text];
    
    NKRequestDelegate *rd = [NKRequestDelegate requestDelegateWithTarget:self finishSelector:@selector(loginOK:) andFailedSelector:@selector(requestFailed:)];
    [[NKAccountManager sharedAccountManager] loginWithAccount:[NKMAccount accountWithAccount:mobile password:password andShouldAutoLogin:[NSNumber numberWithBool:YES]] andRequestDelegate:rd];
    
}

-(void)loginOK:(NKRequest*)request{
    [NKProgressHUD hideHUDForView:self.view animated:YES];
    [[NKUI sharedNKUI] showHome];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
