//
//  main.m
//  NEXTKING
//
//  Created by jinzhenghua on 12/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
