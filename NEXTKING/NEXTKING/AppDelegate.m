//
//  AppDelegate.m
//  NEXTKING
//
//  Created by jinzhenghua on 12/12/2015.
//  Copyright © 2015 jinzhenghua. All rights reserved.
//

#import "AppDelegate.h"
#import "NKUI.h"
#import "YTKNetworkConfig.h"
#import "NKHomeViewController.h"
#import "NKWelcomeViewController.h"
#import "NKSettingViewController.h"
#import "NKAccountManager.h"
#import "NKDiscoverViewController.h"
#import "NKMessagesViewController.h"
#import <Bugtags/Bugtags.h>
#import "NKUserService.h"
#import "NKActivityViewController.h"

#define wxAppSecret 84a5c77ac24b6b2a67f21b8af1e3645f

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)setupRequestFilters {
    
    [[NKConfig sharedConfig] setDomainURL:@"http://zhenghua.applinzi.com/index.php"];
}

-(NKSegment*)segmentWithTitle:(NSString*)title{
    
    NKSegment *segment = [[NKSegment alloc] init];
    segment.title = title;
    segment.normalTextColor = [UIColor lightGrayColor];
    segment.highlightTextColor = [[NKConfig sharedConfig] themeColorText];
    return segment;
    
}

-(NKSegment*)segmentWithImage:(UIImage*)image{
    
    NKSegment *segment = [[NKSegment alloc] init];
    segment.title = image;
    segment.highlightSegmentColor = [UIColor colorWithHexString:@"EEF0F2"];
    return segment;
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    BugtagsOptions *options = [[BugtagsOptions alloc] init];
    options.trackingUserSteps = YES; // 具体可设置的属性请查看 Bugtags.h
    [Bugtags startWithAppKey:@"09baf6107bd4225a2631941247b385e5" invocationEvent:BTGInvocationEventNone options:options];
    
    [self setupRequestFilters];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [NKAccountManager sharedAccountManager];

    [[NKConfig sharedConfig] setStatusBarStyle:UIStatusBarStyleDefault];
    [[NKConfig sharedConfig] setThemeColorText:[UIColor blackColor]];
    //[[NKConfig sharedConfig] setThemeColorBackground:[UIColor colorWithHexString:@"#2e6cb0"]];
    [[NKConfig sharedConfig] setNavigatorBackgroundColor:[UIColor whiteColor]];
    [[NKConfig sharedConfig] setThemeColorBackground:[UIColor whiteColor]];
    
    NKUI *ui = [NKUI sharedNKUI];
    ui.needLogin = YES;
    ui.needStoreViewControllers = YES;
    ui.homeClass = [NKHomeViewController class];
    
    ui.welcomeClass = [NKWelcomeViewController class];
    
    [ui addTabs:[NSArray arrayWithObjects:[NSArray arrayWithObjects:
                                           [self segmentWithImage:[UIImage imageNamed:@"tab_home"]],
                                           //[self segmentWithImage:[UIImage imageNamed:@"tab_activity"]],
                                           [self segmentWithImage:[UIImage imageNamed:@"tab_discover"]],
                                           [self segmentWithImage:[UIImage imageNamed:@"tab_me"]],
                                         
                                           nil],
                 [NSArray arrayWithObjects:
                  ui.homeClass,
                  //[NKActivityViewController class],
                  [NKDiscoverViewController class],
                  [NKSettingViewController class],
                  nil],
                 nil]];

    
    NKNavigationController *navi = [[NKNavigationController alloc] initWithRootViewController:ui];
    self.window.rootViewController = navi;
    navi.navigationBarHidden = YES;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings NS_AVAILABLE_IOS(8_0) __TVOS_PROHIBITED{
    
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken NS_AVAILABLE_IOS(3_0){
    
    [[NKUserService sharedNKUserService] postDeviceToken:deviceToken];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error NS_AVAILABLE_IOS(3_0){
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo NS_AVAILABLE_IOS(3_0){
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    
//    if ([url.host isEqualToString:@"safepay"]) {
//        //跳转支付宝钱包进行支付，处理支付结果
//        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            NSLog(@"result = %@",resultDic);
//        }];
//    }
//    return YES;
//}
//
//// NOTE: 9.0以后使用新API接口
//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
//{
//    if ([url.host isEqualToString:@"safepay"]) {
//        //跳转支付宝钱包进行支付，处理支付结果
//        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            NSLog(@"result = %@",resultDic);
//        }];
//    }
//    return YES;
//}


@end
